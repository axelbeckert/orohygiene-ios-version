//
//  SavedMerkliste.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "SavedMerkliste.h"
#import "MerklistenProdukt.h"
#import "MerklistenReiniger.h"


@implementation SavedMerkliste

@dynamic bezeichnung;
@dynamic merklistenProdukt;
@dynamic merklistenReiniger;

@end
