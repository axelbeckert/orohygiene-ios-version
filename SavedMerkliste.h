//
//  SavedMerkliste.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MerklistenProdukt, MerklistenReiniger;

@interface SavedMerkliste : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSSet *merklistenProdukt;
@property (nonatomic, retain) NSSet *merklistenReiniger;
@end

@interface SavedMerkliste (CoreDataGeneratedAccessors)

- (void)addMerklistenProduktObject:(MerklistenProdukt *)value;
- (void)removeMerklistenProduktObject:(MerklistenProdukt *)value;
- (void)addMerklistenProdukt:(NSSet *)values;
- (void)removeMerklistenProdukt:(NSSet *)values;

- (void)addMerklistenReinigerObject:(MerklistenReiniger *)value;
- (void)removeMerklistenReinigerObject:(MerklistenReiniger *)value;
- (void)addMerklistenReiniger:(NSSet *)values;
- (void)removeMerklistenReiniger:(NSSet *)values;

@end
