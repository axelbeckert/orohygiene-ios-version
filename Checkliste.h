//
//  Checkliste.h
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Checklistenkategorie, Krankheitserreger;

NS_ASSUME_NONNULL_BEGIN

@interface Checkliste : NSManagedObject

+(Checkliste*) insertChecklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteCheckliste;

@end

NS_ASSUME_NONNULL_END

#import "Checkliste+CoreDataProperties.h"

