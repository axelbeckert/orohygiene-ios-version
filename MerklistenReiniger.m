//
//  MerklistenReiniger.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenReiniger.h"
#import "SavedMerkliste.h"


@implementation MerklistenReiniger

@dynamic uid;
@dynamic savedMerkliste;

@end
