//
//  BereichsauswahlViewController.m
//  orochemie 
//
//  Created by Axel Beckert on 16.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BereichsauswahlViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TestBereichsauswahlViewController : XCTestCase
@property (nonatomic,strong) BereichsauswahlViewController *bereichsauswahlViewController;
@end

@implementation TestBereichsauswahlViewController

- (void)setUp {
    [super setUp];
    self.bereichsauswahlViewController = [[BereichsauswahlViewController alloc]init];
    [self.bereichsauswahlViewController viewDidLoad];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDataModel {
    XCTAssert(self.bereichsauswahlViewController.navigationDataModel.count==3,@"Das NavigationDataModel solle einen Count von 3 haben!");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
