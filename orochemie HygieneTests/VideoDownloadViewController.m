//
//  orochemie_HygieneTests.m
//  orochemie HygieneTests
//
//  Created by Axel Beckert on 15.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "VideoDownloadVieWController.h"
#import "VideoHelper.h"

@interface TestVideoDownloadController : XCTestCase


@end

@implementation TestVideoDownloadController

- (void)setUp {
    [super setUp];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testVideoDownload {
    
    VideoDownloadViewController *downloadController = [[VideoDownloadViewController alloc]init];
    [downloadController viewDidLoad];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"asynchronous request"];
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        [expectation fulfill];
    }];
    
    downloadController.blockOperation = operation;
    
    [downloadController checkVideos];
    
    [self waitForExpectationsWithTimeout:400.0 handler:^(NSError * _Nullable error) {
         XCTAssertTrue([VideoHelper checkVideoFiles], @"Videos sollten komplett runtergeladen sein.");
    }];

}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
