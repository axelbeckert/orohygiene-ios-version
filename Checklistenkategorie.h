//
//  Checklistenkategorie.h
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Checkliste;

NS_ASSUME_NONNULL_BEGIN

@interface Checklistenkategorie : NSManagedObject

+(Checklistenkategorie*) insertChecklistenkategorieInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteChecklistenkategorie;

@end

NS_ASSUME_NONNULL_END

#import "Checklistenkategorie+CoreDataProperties.h"
