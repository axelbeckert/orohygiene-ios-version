//
//  Checklistenkategorie+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 30.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Checklistenkategorie.h"

NS_ASSUME_NONNULL_BEGIN

@interface Checklistenkategorie (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *bezeichnung;
@property (nullable, nonatomic, retain) NSString *index;
@property (nullable, nonatomic, retain) NSNumber *musterplan;
@property (nullable, nonatomic, retain) NSNumber *sortierung;
@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) NSNumber *branchmodel;
@property (nullable, nonatomic, retain) NSNumber *typemodel;
@property (nullable, nonatomic, retain) NSSet<Checkliste *> *checkliste;

@end

@interface Checklistenkategorie (CoreDataGeneratedAccessors)

- (void)addChecklisteObject:(Checkliste *)value;
- (void)removeChecklisteObject:(Checkliste *)value;
- (void)addCheckliste:(NSSet<Checkliste *> *)values;
- (void)removeCheckliste:(NSSet<Checkliste *> *)values;

@end

NS_ASSUME_NONNULL_END
