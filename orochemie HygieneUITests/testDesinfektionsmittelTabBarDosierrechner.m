//
//  testDesinfektionsmittelDosierrechner.m
//  orochemie 
//
//  Created by Axel Beckert on 15.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface testDesinfektionsmittelTabBarDosierrechner : XCTestCase
@property (nonatomic,strong)  XCUIApplication *app;
@property (nonatomic,strong)  XCUIElementQuery* cells;
@property (nonatomic,strong)  XCUIElementQuery* table;
@end

@implementation testDesinfektionsmittelTabBarDosierrechner

- (void)setUp {
    [super setUp];
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
    self.table = self.app.tables;
    self.cells = self.app.tables.cells;
    
    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    [self.app.collectionViews.cells.staticTexts[@"Produkte"] tap];
    [self.cells.staticTexts[@"nach Bezeichnung"] tap];
    [self.cells.staticTexts[@"A 20"] tap];
    [self.app.tabBars.buttons[@"Dosierrechner"] tap];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDesinfektionsmittelUndReinigungsmittelAufBasismerklisteSpeichern {
    
    XCUIElement *auswahlUebernehmenButton = self.app.buttons[@"Auswahl übernehmen"];
    
    [self.table.staticTexts[@"A20 Instrumentendesinfektion"] tap];
    [auswahlUebernehmenButton tap];
    XCTAssertEqual([self.cells elementBoundByIndex:0].staticTexts[@"A20 Instrumentendesinfektion"].exists, YES);
    
    [self.table.staticTexts[@"Einwirkzeit"] tap];
    [self.app.pickerWheels[@"1 von 2"] tap];
    [auswahlUebernehmenButton tap];
    XCTAssertEqual([self.cells elementBoundByIndex:1].staticTexts[@"5 min / 2,0%"].exists, YES);

    [self.table.staticTexts[@"Menge gebrauchsfertiger Lösung"] tap];
    [self.app.pickerWheels[@"1 von 10"] tap];
    [auswahlUebernehmenButton tap];
    XCTAssertEqual([self.cells elementBoundByIndex:2].staticTexts[@"1 Liter"].exists, YES);
    
    XCTAssertEqual([self.cells elementBoundByIndex:3].staticTexts[@"20"].exists, YES);
    XCTAssertEqual([self.cells elementBoundByIndex:4].staticTexts[@"1"].exists, YES);
    XCTAssertEqual([self.cells elementBoundByIndex:5].staticTexts[@"980"].exists, YES);

    
    
}


@end
