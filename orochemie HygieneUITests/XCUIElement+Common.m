//
//  XCUIElement+Common.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "XCUIElement+Common.h"

@implementation XCUIElement (Common)

-(void)scrollToElement:(XCUIElement*) element {

    while (![self visisble]) {
        [element swipeUp];
    }
}

-(BOOL) visisble {
    if(self.exists && !CGRectIsEmpty(self.frame))return NO;
    
    else {
        UIWindow* window = [UIApplication sharedApplication].windows[0];
        return CGRectContainsRect(window.frame, self.frame);
    }
    
}

//func scrollToElement(element: XCUIElement) {
//    while !element.visible() {
//        swipeUp()
//    }
//}

//func visible() -> Bool {
//    guard self.exists && !CGRectIsEmpty(self.frame) else { return false }
//    return CGRectContainsRect(XCUIApplication().windows.elementBoundByIndex(0).frame, self.frame)
//}
@end
