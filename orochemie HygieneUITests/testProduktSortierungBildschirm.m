//
//  testProduktSortierungBildschirm.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface testProduktSortierungBildschirm : XCTestCase
@property (nonatomic,strong) XCUIApplication *app;
@end

@implementation testProduktSortierungBildschirm

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
    
    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    [self.app.collectionViews.cells.staticTexts[@"Produkte"] tap];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testProduktsortierungsAuswahlSchirmObAlleNavigationspunkteVorhandenSind {
    
    XCUIElementQuery *cellQuery = [[XCUIApplication alloc] init].tables.cells;
    XCTAssertEqual([[cellQuery elementBoundByIndex:0].staticTexts[@"nach Bezeichnung"] exists], YES);
    XCTAssertEqual([[cellQuery elementBoundByIndex:1].staticTexts[@"nach Anwendung"] exists], YES);
    XCTAssertEqual([[cellQuery elementBoundByIndex:2].staticTexts[@"nach Branche"] exists], YES);
    XCTAssertEqual([[cellQuery elementBoundByIndex:3].staticTexts[@"nach Krankheitserreger"] exists], YES);
    XCTAssertEqual([[cellQuery elementBoundByIndex:4].staticTexts[@"Barcode-Reader"] exists], YES);
    XCTAssertEqual([[cellQuery elementBoundByIndex:5].staticTexts[@"Merklisten"] exists], YES);

}

-(void)testHomeButton {
    
    [self.app tabHomeButton];
    XCTAssertEqual([self.app assertMainNavbarTitle], YES);
}

-(void) testBackButton {
    [self.app tabOnBackButton];
    XCTAssertEqual([self.app assertDesinfektionsmittelNavBarTitle], YES);
}

@end
