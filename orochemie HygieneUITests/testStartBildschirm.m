//
//  orochemie_HygieneUITests.m
//  orochemie HygieneUITests
//
//  Created by Axel Beckert on 13.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface orochemie_HygieneUITests : XCTestCase
@property (nonatomic,strong) XCUIApplication *app;
@end

@implementation orochemie_HygieneUITests

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testStartbildschirmNavigationsElemente {
    
    self.app = [[XCUIApplication alloc] init];
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCTAssertEqual(collectionViewsQuery.cells.count, 3);
    XCUIElementQuery* cells =collectionViewsQuery.cells;
    
    XCTAssertEqual([cells elementBoundByIndex:0].staticTexts[@"Desinfektionsmittel"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:1].staticTexts[@"Reinigungsmittel"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:2].staticTexts[@"Produkt-Barcode scannen"].exists, true);
    
}

-(void) testEinstellbildschirmVideoDownloadSwitch {

    [self tabOnEinstellungsViewButton];
    [self checkSwitchWithIdentifier:@"Videos downloaden"];
    [self tabOnBackButton];
    
}

-(void) testEinstellbildschirmDatenSendenSwitch {
    
    [self tabOnEinstellungsViewButton];
    [self checkSwitchWithIdentifier:@"Daten senden"];
    [self tabOnBackButton];
    
}

-(void) checkSwitchWithIdentifier: (NSString*) identifier {
    
    XCUIElement *switchToTest = [self getTablesQuery].switches[identifier];
    
    [self checkElementAndSwipe:identifier];

    BOOL status =[(NSString*)switchToTest.value boolValue];
    [switchToTest tap];
    
    [self closeAndReopenEinstellungsView];
    [self checkElementAndSwipe:identifier];
    
    if(status){
        XCTAssertEqual([(NSString*)switchToTest.value boolValue], NO);
    } else {
        XCTAssertEqual([(NSString*)switchToTest.value boolValue], YES);
    }
    
    [switchToTest tap];
}

-(void)checkElementAndSwipe:(NSString*)element{
    if(![self getTablesQuery].switches[element].exists){
        XCUIElement* table = [self getTablesQuery].element;
        [table swipeUp];
    }
}


-(XCUIElementQuery*) getTablesQuery {
    return self.app.tables;
}

-(void) tabOnEinstellungsViewButton {
    [self.app.navigationBars[@"orochemie Hygiene"].buttons[@"20 gear 2"] tap];
}

-(void) tabOnBackButton {
    [[[[self.app.navigationBars[@"Einstellungen"] childrenMatchingType:XCUIElementTypeButton] matchingIdentifier:@"Zurück"] elementBoundByIndex:0] tap];
}

-(void) closeAndReopenEinstellungsView {
    [self tabOnBackButton];
    [self tabOnEinstellungsViewButton];
}


@end
