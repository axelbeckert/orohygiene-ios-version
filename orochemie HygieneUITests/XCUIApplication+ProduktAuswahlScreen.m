//
//  XCUIApplication+ProduktAuswahlScreen.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "XCUIApplication+ProduktAuswahlScreen.h"

@implementation XCUIApplication (ProduktAuswahlScreen)
-(BOOL)assertProduktSortierungNavBarTitle {
    return [self.navigationBars elementBoundByIndex:0].staticTexts[@"Produkte"].exists;
}
@end
