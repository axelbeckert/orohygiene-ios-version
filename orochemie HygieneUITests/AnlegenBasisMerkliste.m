//
//  AnlegenBasisMerkliste.m
//  orochemie 
//
//  Created by Axel Beckert on 05.07.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface AnlegenBasisMerkliste : XCTestCase
@property (nonatomic,strong)  XCUIApplication *app;
@property (nonatomic,strong)  XCUIElementQuery* cells;
@property (nonatomic,strong)  XCUIElementQuery* table;
@property (nonatomic,strong)  XCUIElementQuery* collectionViewsQuery;
@property (nonatomic,strong)  XCUIElementQuery* sheets;
@property (nonatomic,strong)  XCUIElement* merklisteButton;
@end

@implementation AnlegenBasisMerkliste

- (void)setUp {
    [super setUp];
    self.continueAfterFailure = YES;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];

    self.collectionViewsQuery = self.app.collectionViews;
    self.table = self.app.tables;
    self.cells = self.app.tables.cells;
    self.sheets = self.app.sheets[@"aktuelle Merkliste"].collectionViews;
    self.merklisteButton = self.app.navigationBars[@"Information"].buttons[@"179 notepad"];

    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)test01DesinfektionsmittelAufBasisMerklisteSpeichern {
    
    
    
    [self.collectionViewsQuery.staticTexts[@"Desinfektionsmittel"] tap];
    [self.collectionViewsQuery.staticTexts[@"Produkte"] tap];
    
    [self.table.staticTexts[@"nach Bezeichnung"] tap];
    [self.table.staticTexts[@"Instrumentendesinfektion"] tap];
    
    NSLog(@"StaticText:%@",[self.table.staticTexts debugDescription]);
    
    XCTAssertEqual([self.table.staticTexts[@"A 20 Instrumentendesinfektion - Vielseitig & schnell wirksam "] exists], YES);
    
    [self.merklisteButton tap];
    
    [self.sheets.buttons[@"aktuelle Merkliste"] tap];
    sleep(10);
    [self.merklisteButton tap];
    [self.sheets.buttons[@"Merklisten anzeigen"] tap];
    
    XCTAssertEqual([self.table.staticTexts[@"Desinfektionsmittel"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"A 20"] exists], YES);
    
    [self.app.navigationBars[@"aktuelle Merkliste"].buttons[@"Information"] tap];
    
    [self.app tabHomeButton];
    
    
}

-(void) test02ReinigungsmittelAufBasisMerklisteSpeichern {
    
    [self.collectionViewsQuery.staticTexts[@"Reinigungsmittel"] tap];
    [self.collectionViewsQuery.staticTexts[@"Produkte"] tap];
    [self.table.staticTexts[@"Nach Bezeichung"] tap];
    [self.table.staticTexts[@"Bodenbeschichtung universal"] tap];
    
    XCTAssertEqual([self.table.staticTexts[@"orochemie® Bodenbeschichtung universal - Beständig, rutschhemmend, seidenmatt"] exists], YES);
    
    [self.merklisteButton tap];
    self.sheets = self.app.sheets[@"Merklisten"].collectionViews;
    [self.sheets.buttons[@"aktuelle Merkliste"] tap];
    sleep(10);
    [self.merklisteButton tap];
    [self.sheets.buttons[@"Merklisten anzeigen"] tap];
    
    XCTAssertEqual([self.table.staticTexts[@"Desinfektionsmittel"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"A 20"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"Reinigungsmittel"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"orochemie® "] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"Bodenbeschichtung universal"] exists], YES);
    
}


-(void) test03InhaltBasisMerklisteInEigeneMerklisteSpeichern{
    [self.collectionViewsQuery.staticTexts[@"Desinfektionsmittel"] tap];
    [self.collectionViewsQuery.staticTexts[@"Produkte"] tap];
    

    [self.table.staticTexts[@"Merklisten"] tap];
    [self.app.navigationBars[@"aktuelle Merkliste"].buttons[@"Senden"] tap];
    [self.app.sheets[@"Merkliste bearbeiten:"].collectionViews.buttons[@"Merkliste speichern"] tap];
    
    XCUIElementQuery *alert = self.app.alerts[@"Eingabe erforderlich:"].collectionViews;
    [alert.textFields[@"Bezeichnung der Merkliste"] typeText:@"test"];

    [alert.buttons[@"OK"] tap];
    sleep(10);
    [self.table.staticTexts[@"test"] tap];
    
    XCTAssertEqual([self.table.staticTexts[@"Desinfektionsmittel"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"A 20"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"Reinigungsmittel"] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"orochemie® "] exists], YES);
    XCTAssertEqual([self.table.staticTexts[@"Bodenbeschichtung universal"] exists], YES);
    
    [self.app.navigationBars[@"test"].buttons[@"aktuelle Merkliste"] tap];
    [self.app.tabBars.buttons[@"aktuelle Merkliste"] tap];
    
    XCTAssertEqual([self.table.staticTexts[@"Desinfektionsmittel"] exists], NO);
    XCTAssertEqual([self.table.staticTexts[@"A 20"] exists], NO);
    XCTAssertEqual([self.table.staticTexts[@"Reinigungsmittel"] exists], NO);
    XCTAssertEqual([self.table.staticTexts[@"orochemie® "] exists], NO);
    XCTAssertEqual([self.table.staticTexts[@"Bodenbeschichtung universal"] exists], NO);
    
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
