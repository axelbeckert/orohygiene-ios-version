//
//  testProdukteNachBezeichnungBildschirm.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface testProdukteNachBezeichnungBildschirm : XCTestCase
@property (nonatomic,strong)  XCUIApplication *app;
@property (nonatomic,strong)  XCUIElementQuery* cells;
@property (nonatomic,strong)  XCUIElementQuery* table;
@property (nonatomic,strong)  XCUIElement* searchField;
@end

@implementation testProdukteNachBezeichnungBildschirm

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
    self.table = self.app.tables;
    self.cells = self.app.tables.cells;
    self.searchField = self.table.searchFields[@"Suche"];
    
    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    [self.app.collectionViews.cells.staticTexts[@"Produkte"] tap];
    [self.cells.staticTexts[@"nach Bezeichnung"] tap];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testHeadline {
    
    XCTAssertEqual([self.app.navigationBars elementBoundByIndex:0].staticTexts[@"Produkte"].exists, YES);
}

- (void)testWieVieleProdukteSindAufgelistet {
    
    XCTAssertEqual(self.cells.count, 19);
}

-(void)testWieVieleInstrumentenProdukteSindVorhanden {
    
    [self checkProduktCountWithTypeLetter:@"A " andCount:1];
}

-(void)testWieVieleFlaechenProdukteSindVorhanden {

    [self checkProduktCountWithTypeLetter:@"B " andCount:9];

}

-(void)testWieVieleCHaendeProdukteSindVorhanden {

    [self checkProduktCountWithTypeLetter:@"C " andCount:6];
    
}

-(void)testWieVieleHDHaendeProdukteSindVorhanden {
    
    [self checkProduktCountWithTypeLetter:@"HD " andCount:1];
    
}

-(void)testWieVieleSpezialProdukteSindVorhanden {
    
    [self checkProduktCountWithTypeLetter:@"D 1" andCount:1];
    
}

-(void) checkProduktCountWithTypeLetter: (NSString*)typeLetter andCount: (int)count {
    [self.searchField tap];
    [self.searchField typeText:typeLetter];
    XCTAssertEqual(self.cells.count,count);
}

-(void)testHomeButton {
    
    [self.app tabHomeButton];
    XCTAssertEqual([self.app assertMainNavbarTitle], YES);
}

-(void) testBackButton {
    [self.app tabOnBackButton];
    XCTAssertEqual([self.app assertProduktSortierungNavBarTitle], YES);
}

@end
