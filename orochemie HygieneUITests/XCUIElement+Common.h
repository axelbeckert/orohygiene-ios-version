//
//  XCUIElement+Common.h
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XCUIElement (Common)
-(void)scrollToElement:(XCUIElement*) element;
@end
