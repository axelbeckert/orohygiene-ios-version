//
//  testDesinfektionsmittelschirm.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface testDesinfektionsmittelBildschirm : XCTestCase
@property (nonatomic,strong) XCUIApplication *app;
@end

@implementation testDesinfektionsmittelBildschirm

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];

    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDesinfektionsmittelSchirmObAlleNavigationspunkteVorhandenSind {
    
    XCUIElementQuery* cells =self.app.collectionViews.cells;
    
    XCTAssertEqual([cells elementBoundByIndex:0].staticTexts[@"Produkte"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:1].staticTexts[@"Krankheitserreger von A-Z"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:2].staticTexts[@"Anwendungsvideos"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:3].staticTexts[@"Hilfsmittel"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:4].staticTexts[@"Aktuelles"].exists, true);
    XCTAssertEqual([cells elementBoundByIndex:5].staticTexts[@"Unternehmen"].exists, true);
    
}


-(void) testHomeButton {
    
    [self.app tabHomeButton];
     XCTAssertEqual([self.app assertMainNavbarTitle], true);
}


-(void) testBackButton {
    
    [self.app tabOnBackButton];
    XCTAssertEqual([self.app assertMainNavbarTitle], true);
}





@end
