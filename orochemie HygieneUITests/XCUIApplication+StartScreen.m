//
//  XCUIApplication+StartScreen.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "XCUIApplication+StartScreen.h"

@implementation XCUIApplication (StartScreen)
-(BOOL)assertMainNavbarTitle {
    return self.navigationBars[@"orochemie Hygiene"].staticTexts[@"orochemie Hygiene"].exists;
}

-(void) tabOnEinstellungsViewButton {
    [self.navigationBars[@"orochemie Hygiene"].buttons[@"20 gear 2"] tap];
}
@end
