//
//  XCUIApplication+ProdukteNAchBezeichnung.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "XCUIApplication+ProdukteNachBezeichnung.h"

@implementation XCUIApplication (ProdukteNachBezeichnung)
-(BOOL)assertProdukteNachBezeichnungNavBarTitle {
    return [self.navigationBars elementBoundByIndex:0].staticTexts[@"Produkte"].exists;
}
@end
