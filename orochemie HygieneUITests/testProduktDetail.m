//
//  testProduktDetail.m
//  orochemie 
//
//  Created by Axel Beckert on 14.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"

@interface testProduktDetail : XCTestCase
@property (nonatomic,strong)  XCUIApplication *app;
@property (nonatomic,strong)  XCUIElementQuery* cells;
@property (nonatomic,strong)  XCUIElementQuery* table;
@property (nonatomic,strong)  XCUIElement* searchField;
@end

@implementation testProduktDetail

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
    self.table = self.app.tables;
    self.cells = self.app.tables.cells;
    self.searchField = self.table.searchFields[@"Suche"];
    
    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    [self.app.collectionViews.cells.staticTexts[@"Produkte"] tap];
    [self.cells.staticTexts[@"nach Bezeichnung"] tap];
    [self.cells.staticTexts[@"A 20"] tap];
    
}

- (void)tearDown {
    [super tearDown];
}

-(void)testObAlleÜberschriftenInnerhalbDerProduktInformationVorhandenSind {

    [self checkStaticTextAndAssert:@"Produktbezeichnung"];
    [self checkStaticTextAndAssert:@"Einsatzbereich"];
    [self checkStaticTextAndAssert:@"Listung / Prüfung"];
    [self checkStaticTextAndAssert:@"Wirksamkeit"];
    [self checkStaticTextAndAssert:@"Dosierung / Einwirkzeit"];
    [self checkStaticTextAndAssert:@"Hinweise"];
    [self checkStaticTextAndAssert:@"Gefahrstoffsymbole"];
    [self checkStaticTextAndAssert:@"Checklisten"];
    
}

-(void) checkStaticTextAndAssert: (NSString*)staticText {
    
    if(!self.table.staticTexts[staticText].exists){
        [self.table.staticTexts[staticText] swipeUp];
        sleep(1);
        XCTAssertEqual(self.table.staticTexts[staticText].exists, YES);
    } else {
        XCTAssertEqual(self.table.staticTexts[staticText].exists, YES);
    }
}

- (void)testSicherheitsdatenblattWebViewButton {
    
    NSString* title =@"Sicherheitsdatenblatt";
    [self checkZusatzInformationWebviewWithButtonTitle:title andHeadline:title];
}

- (void)testBetriebsanweisungWebViewButton {
    
    NSString* title =@"Betriebsanweisung";
    [self checkZusatzInformationWebviewWithButtonTitle:title andHeadline:title];
}

- (void)testProduktinformationWebViewButton {
    
    NSString* title =@"Produktinformation";
    [self checkZusatzInformationWebviewWithButtonTitle:title andHeadline:title];
}

-(void) checkZusatzInformationWebviewWithButtonTitle: (NSString*) buttonTitle andHeadline:(NSString*)headline {
    
    [self.table.buttons[buttonTitle] tap];
    sleep(5);
    XCTAssertEqual(self.app.staticTexts[headline].exists, YES);
    XCTAssertEqual(self.app.navigationBars[@"orochemie Website"].buttons[@"Senden"].exists, YES);
    XCTAssertEqual(self.app.navigationBars[@"orochemie Website"].buttons[@"home"].exists, YES);
    XCTAssertEqual(self.app.navigationBars[@"orochemie Website"].buttons[@"Zurück"].exists, YES);
}

-(void)testHygieneChecklistenButton {
    [self.table.buttons[@"Hygienechecklisten"] tap];
    sleep(5);
    XCTAssertEqual(self.app.staticTexts[@"Checklisten für Hygieneaufgaben"].exists, YES);
}

-(void)testHomeButton {
    
    [self.app tabHomeButton];
    XCTAssertEqual([self.app assertMainNavbarTitle], YES);
}

-(void) testBackButton {
    [self.app tabOnBackButton];
    XCTAssertEqual([self.app assertProdukteNachBezeichnungNavBarTitle], YES);
}

@end
