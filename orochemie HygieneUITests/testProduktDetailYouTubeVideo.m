//
//  testProduktDetailVideo.m
//  orochemie 
//
//  Created by Axel Beckert on 15.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCUIApplicationCategorieHeaders.h"


@interface testProduktDetailYouTubeVideo : XCTestCase
@property (nonatomic,strong)  XCUIApplication *app;
@property (nonatomic,strong)  XCUIElementQuery* cells;
@property (nonatomic,strong)  XCUIElementQuery* table;
@end

@implementation testProduktDetailYouTubeVideo

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationLandscapeRight;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationFaceUp;
    
    self.app = [[XCUIApplication alloc] init];
    self.table = self.app.tables;
    self.cells = self.app.tables.cells;
    
    [self prepareAppForYouTubeVideo];
    [self.app.collectionViews.cells.staticTexts[@"Desinfektionsmittel"] tap];
    [self.app.collectionViews.cells.staticTexts[@"Produkte"] tap];
    [self.cells.staticTexts[@"nach Bezeichnung"] tap];
    [self.cells.staticTexts[@"A 20"] tap];
    [self.app.tabBars.buttons[@"Video"] tap];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testYouTubeVideo {
    
    XCUIElement* firstCell =[self.cells elementBoundByIndex:0];
    [firstCell tap];
    sleep(5);
    XCTAssertEqual(self.app.staticTexts[@"YouTube-Video"].exists, YES);
    XCTAssertEqual(self.app.staticTexts[@"YouTube"].exists, YES);
    [self switchAppBackToVideoDownload];
    
    

}

-(void) prepareAppForYouTubeVideo {
    [self.app tabOnEinstellungsViewButton];
    XCUIElement *videosDownloadenSwitch = self.table.switches[@"Videos downloaden"];
    
    if([(NSString*)videosDownloadenSwitch.value boolValue]){
        [videosDownloadenSwitch tap];
    }
    [self.app tabOnBackButton];
}

-(void) switchAppBackToVideoDownload {
    [self.app tabHomeButton];
    [self.app tabOnEinstellungsViewButton];
    XCUIElement *videosDownloadenSwitch = self.table.switches[@"Videos downloaden"];
    
    if(![(NSString*)videosDownloadenSwitch.value boolValue]){
        [videosDownloadenSwitch tap];
    }
    [self.app tabOnBackButton];
}


@end
