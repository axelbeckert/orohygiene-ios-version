//
//  Checklistenkategorie.m
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Checklistenkategorie.h"
#import "Checkliste.h"

@implementation Checklistenkategorie

+(Checklistenkategorie*) insertChecklistenkategorieInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Checklistenkategorie" inManagedObjectContext:managedObjectContext];
}
-(void)deleteChecklistenkategorie
{
    [self.managedObjectContext deleteObject:self];
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ - Sortierung:%@ - Index:%@",self.bezeichnung, self.sortierung, self.index];
}

@end
