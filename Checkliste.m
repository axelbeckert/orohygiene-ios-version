//
//  Checkliste.m
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Checkliste.h"
#import "Checklistenkategorie.h"
#import "Krankheitserreger.h"

@implementation Checkliste

+(Checkliste*) insertChecklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Checkliste" inManagedObjectContext:managedObjectContext];
}
-(void)deleteCheckliste
{
    [self.managedObjectContext deleteObject:self];
}

@end
