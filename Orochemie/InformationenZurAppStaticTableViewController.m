//
//  InformationenZurAppStaticTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 20.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import "InformationenZurAppStaticTableViewController.h"

@interface InformationenZurAppStaticTableViewController ()
@property (strong, nonatomic) IBOutlet UITextView *versionLabelOutlet;

@end

@implementation InformationenZurAppStaticTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"oro. Hygiene App";
    
    if(self.showInformationenZurAppFrom == DesinfektionChannel){
        //NSLog(@"Informationen zur.. vom Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];
        
        
    } else if(self.showInformationenZurAppFrom == ReinigerChannel){
        
        //NSLog(@"Informationen zur.. vom von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    self.versionLabelOutlet.text = [NSString stringWithFormat:@"Version %@ - Build %@", [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Informationen-zur-App-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"INFORMATIONEN ZUR APP";
    } else if (section == 1)
    {
        label.text=@"ENTWICKLERINFORMATION";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}





@end
