//
//  AddWirksamkeitenToDatabase.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "AddWirksamkeitenToDatabase.h"
#import "Logger.h"

@interface AddWirksamkeitenToDatabase()
@property(nonatomic,strong)AppDelegate* appDelegate;
@property (nonatomic,strong) NSMutableArray *mutableWirksamkeitsArray;
@property (nonatomic,strong) NSMutableArray *productArray;
@property (nonatomic,strong) NSDictionary *rawWirksamkeitsDictionary;
@property (nonatomic,strong) Wirksamkeit *wirksamkeit;
@property int wirksamkeitsId;
@end

@implementation AddWirksamkeitenToDatabase

- (NSMutableArray *) productArray
{
    if (!_productArray) {
        _productArray = [NSMutableArray new];
    }
    return _productArray;
}


-(BOOL) addWirksamkeiten:(NSArray*)wirksamkeiten{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.mutableWirksamkeitsArray = [NSMutableArray new];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"%lu Wirksamkeiten wurden vom Server geladen", (unsigned long)wirksamkeiten.count];
    
        [self.appDelegate.logger logMessage:message];
    }
    
    for (NSDictionary* dictionary in wirksamkeiten) {
        self.rawWirksamkeitsDictionary = dictionary;
        self.wirksamkeit = nil;
        [self addWirksamkeit];
        self.rawWirksamkeitsDictionary=nil;
    }
    
//    [self compareDownloadedWirksamkeitenWithStoredWirksamkeiten];
//    if([self.hygienechecklistenTableViewController respondsToSelector:@selector(performTableViewRefresh)]){
//        [self.hygienechecklistenTableViewController performTableViewRefresh];
//    }
    

    
    [self.appDelegate saveContext];
    
    return YES;
}


-(void) addWirksamkeit{
    
    NSString* rawWirksamkeitUId = self.rawWirksamkeitsDictionary[@"id"];
    self.wirksamkeitsId = [rawWirksamkeitUId intValue];
    
    [self checkIfWirksamkeitExists];
    
    if(self.wirksamkeit != nil) {
        Wirksamkeit* newWirksakeit = self.wirksamkeit;
        [self.mutableWirksamkeitsArray addObject:newWirksakeit];
        
    }
    
}


-(void)checkIfWirksamkeitExists{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Wirksamkeit"];
    NSPredicate *predicate = NULL;

    predicate = [NSPredicate predicateWithFormat:@"uid ==%i",self.wirksamkeitsId];
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    
    if(results.count){
        self.wirksamkeit =[results lastObject];
        [self checkWirksamkeitsValuesIfTheyChangedSinceLastDownload];

    } else {
        
        [self getNewWirksamkeit];
        
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Neue Wirksamkeit %@ wurde angelegt", self.wirksamkeit.bezeichnung];
            
            [self.appDelegate.logger logMessage:message];
        }
    }
    
}

-(void) getNewWirksamkeit{
    
    self.wirksamkeit = [Wirksamkeit insertWirksamkeitInManagedObjectContext:self.appDelegate.managedObjectContext];
    self.wirksamkeit.uid = [NSNumber numberWithInt:self.wirksamkeitsId];
    
    [self addValuesToWirksamkeitObject];
}

-(void) addValuesToWirksamkeitObject{
    
    self.wirksamkeit.bezeichnung = self.rawWirksamkeitsDictionary[@"bezeichnung"];
    self.wirksamkeit.wirksam = self.rawWirksamkeitsDictionary[@"wirksamkeit"];
    self.wirksamkeit.tstamp = self.rawWirksamkeitsDictionary[@"tstamp"];
        
    NSString* sorting = self.rawWirksamkeitsDictionary[@"sorting"];
    int sortingInt = [sorting intValue];
    self.wirksamkeit.sorting = [NSNumber numberWithInt:sortingInt];
    
    [self addProductsToWirksamkeit];
    
}


-(void) checkWirksamkeitsValuesIfTheyChangedSinceLastDownload{
    
    if(![self.wirksamkeit.tstamp isEqualToString:self.rawWirksamkeitsDictionary[@"tstamp"]]){
        [self removeAllProductsFromWirksamkeit];
        [self addValuesToWirksamkeitObject];
    
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Wirksamkeit %@ wurde verändert", self.wirksamkeit.bezeichnung];
            
            [self.appDelegate.logger logMessage:message];
        }
    }

}



-(void) removeAllProductsFromWirksamkeit{
    
    NSSet *produkteSet = self.wirksamkeit.produkte;
    [self.wirksamkeit removeProdukte:produkteSet];
    
}


-(void) addProductsToWirksamkeit{
    
    
    
    for (NSDictionary* product in self.rawWirksamkeitsDictionary[@"produkte"]) {
        Produkte *produkt = nil;
        produkt = [self getProductFromDatabaseWithAppUid:product];
    
        
        if(produkt!=nil){
            [self.wirksamkeit addProdukteObject:produkt];
        }
        
    }
    
//    NSLog(@"---------------------------------------------");
//    NSLog(@"Wiksamkeit: %@ ", self.wirksamkeit.bezeichnung);
//    for (Produkte* produkt in self.wirksamkeit.produkte){
//        NSLog(@"Produkt: %@ ",produkt.name);
//    }
//    NSLog(@"---------------------------------------------");
    
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Wirksamkeit %@ wurden %lu Produkte zugeordnet", self.wirksamkeit.bezeichnung, (unsigned long) self.wirksamkeit.produkte.count];
        
        [self.appDelegate.logger logMessage:message];
    }
}

-(Produkte*) getProductFromDatabaseWithAppUid:(NSDictionary*)data{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Produkte"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%@",data[@"appId"]];
    
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Produkte* product = NULL;
    
    if(results.count){
        product =[results lastObject];
        return product;
    }
    
    return nil;
    
}

-(void)compareDownloadedWirksamkeitenWithStoredWirksamkeiten{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Wirksamkeit"];
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Wirksamkeiten vergleich. %lu sind gespeichert", (unsigned long) results.count];
//        NSLog(@"Gespeicherte Wirksamkeiten: %@", results);
        [self.appDelegate.logger logMessage:message];
    }
    
    if(results.count){
        NSMutableArray *finalResults = [NSMutableArray arrayWithArray:results];
        [finalResults removeObjectsInArray:self.mutableWirksamkeitsArray];
        
        for(Wirksamkeit* wirksamkeit in finalResults){
            if(self.appDelegate.logger.isLoggingActive==1){
                NSString* message = [NSString stringWithFormat:@"%@ wurde gelöscht", wirksamkeit.bezeichnung];
                
                [self.appDelegate.logger logMessage:message];
            }
            [wirksamkeit deleteWirksamkeit];
        }
        
    }
    
}

@end
