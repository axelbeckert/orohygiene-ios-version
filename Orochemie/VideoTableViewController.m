//
//  VideoTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "VideoTableViewController.h"
#import "Video.h"
#import "VideoPlayerViewController.h"
#import "VideoTableViewCell.h"
#import "WebviewViewController.h"
#import "VideoDownloadViewController.h"
#import "VideoHelper.h"

@interface VideoTableViewController ()
@property (nonatomic,strong) NSArray *videoArray;
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;


@property BOOL noVideosToPlay;
@end

@implementation VideoTableViewController
#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Video class])];
    
    NSSortDescriptor *sortKategorie= [[NSSortDescriptor alloc] initWithKey:@"kategorie" ascending:YES];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierung" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= [NSArray arrayWithObjects:sortKategorie,sortReihenfolge, nil];
    fetch.sortDescriptors = sortArray;
    
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"kategorie"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

-(BOOL)shouldAutorotate
{
    return YES;
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations  
#else  
- (UIInterfaceOrientationMask)supportedInterfaceOrientations  
#endif
{
    
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //CoreData initialisieren
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = delegate.managedObjectContext;
    
    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.contentInset = inset;

    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    if (self.showVideoTableViewFrom == ReinigerChannel){
        //NSLog(@"Zeige Videos vom Reiniger Kanal neu");
        [oroThemeManager  customizeReinigerTableView:self.tableView];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reiniger.bezeichnung CONTAINS[c] %@",self.reiniger.bezeichnung];
        
        self.fetchedResultsController.fetchRequest.predicate = predicate;
        
        
        
    } else {
        //NSLog(@"Zeige Videos vom Desinfektions Kanal");
        [oroThemeManager  customizeTableView:self.tableView];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produkte.name CONTAINS[c] %@",self.produkt.name];
        
        self.fetchedResultsController.fetchRequest.predicate = predicate;
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    self.noVideosToPlay = ![self checkIfVideosAreThere];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarViewController setVideoPlayerIsFullscreen:NO];
    
    if(self.branchenName)
    {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.branchenName;
        self.tabBarController.navigationItem.titleView=tlabel;
        
    } else if (self.anwendungName) {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.anwendungName;
        self.tabBarController.navigationItem.titleView=tlabel;
        
    } else {
        
        self.tabBarController.title=@"Videoauswahl";
        
    }
  }



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    NSString *additionalSectionTitle =@"";
    NSLog(@"Section: %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section]);
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Desinfektion allgemein";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"F"])
    {
        if(self.showVideoTableViewFrom==DesinfektionChannel){
            additionalSectionTitle = @"Flächendesinfektion";
        } else {
            additionalSectionTitle = @"Feinsteinzeug";
        }
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"I"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"S"])
    {
        if(self.showVideoTableViewFrom==DesinfektionChannel){
            additionalSectionTitle = @"Spezialanwendungen";
        } else {
            additionalSectionTitle = @"Sanitär";
        }
    }  else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"R"])
    {
        additionalSectionTitle = @"Reinigungsmittel allgemein";
        
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"T"])
    { // Da nur ein Buchstabe geprüft werden kann steht die Prüfung auf T da das vor U kommt
        additionalSectionTitle = @"Unterhaltsreinigung";
        
    }else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"U"])
    {
        additionalSectionTitle = @"Unternehmen";
        
        
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"G"])
    {
        additionalSectionTitle = @"Garnpad";
    }
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    //Da ein T für den Index von Unterhaltsreinigung verwendet werden musste, muss hier auf T geprüft werden damit ein U angezeigt wird.
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"T"]){
        
        label.text = [NSString stringWithFormat:@"U - %@", additionalSectionTitle];
        
    } else {
        
        label.text = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    }
    return container;

    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Video *video = [self.fetchedResultsController objectAtIndexPath:indexPath];

    //[oroThemeManager customizeAccessoryView:cell];
    
    
    
    [oroThemeManager customizeTableViewCellBackground:cell];
    //UIView *backgroundView = [[UIView alloc]init];
   // backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
    
    //cell.backgroundView=backgroundView;

    cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0];
    cell.textLabel.numberOfLines=0;
    cell.textLabel.text = video.bezeichnung;
    return cell;
}



#pragma mark - Table view delegate

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
     self.noVideosToPlay = ![self checkIfVideosAreThere];

    NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
    NSString* youTubeVideo = [usingOroHygieneDefaults valueForKey:@"YouTubeVideos"];
    
    if(!self.noVideosToPlay && [youTubeVideo isEqualToString:@"YES"]){

        WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        
        Video *video=[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.urlString = video.youtube;
        controller.pdfFileName = @"video";
        controller.webViewTitle = @"YouTube-Video";
        
        [self.navigationController pushViewController:controller animated:YES];
        
        return NO;
    } else if (self.noVideosToPlay && [youTubeVideo isEqualToString:@"NO"]){
        return NO;
    }
    
    
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"VideoPlayer"])
    {
        VideoPlayerViewController *controller = segue.destinationViewController;
        //NSLog(@"selected Video: %@",[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]]);
        controller.video = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.tabBarViewController = self.tabBarViewController;
    }
}

#pragma mark- Check if Videos are there

-(BOOL)checkIfVideosAreThere{
    
    if(![VideoHelper checkVideoFiles]){
        VideoHelper *vh = [VideoHelper new];
        return [vh checkIfVideosAreThereWithStoryboard:self.storyboard andNavigationController:self.navigationController];
    }
    
    return YES;
}
@end
