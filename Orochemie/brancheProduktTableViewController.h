//
//  brancheProduktTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSMTableViewController.h"

@class Branche;

@interface brancheProduktTableViewController : JSMTableViewController
@property (nonatomic,strong) Branche *brancheProdukteTableView;
@end
