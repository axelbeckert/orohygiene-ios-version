//
//  UIImage+base64.h
//  orochemie
//
//  Created by Axel Beckert on 13.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (base64)
- (NSString *)base64String;
@end
