//
//  TechnischeAnfrageTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 13.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "TechnischeAnfrageTableViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+base64.h"
#import "TechnischeAnfrage+ccm.h"
#import "BEMCheckBox.h"
#import "DatenschutzTableViewController.h"

enum FormFieldFailure {
    
    ANSPRECHPARTNER_LEER,
    TELEFON_LEER,
    EMAIL_LEER,
    EMAIL_VALIDATION_FALSE,
    PROBLEMBESCHREIBUNG_LEER,
    PRIVACY_POLICY_CHECKBOX,
};

typedef enum FormFieldFailure formFieldFailure;

@interface TechnischeAnfrageTableViewController()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *fotoButtonOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (strong, nonatomic) IBOutlet UIButton *entfernenButtonOutlet;
- (IBAction)entfernenButtonTouchUpInside:(id)sender;
- (IBAction)fotoButtonTouchupInside:(id)sender;
- (IBAction)sendenButtonTouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sendenButtonOutlet;
@property (strong, nonatomic) IBOutlet UITextField *emailTextFieldOutlet;

@property (strong, nonatomic) IBOutlet UITextView *problembeschreibungTextViewOutlet;
@property (strong, nonatomic) IBOutlet UITextView *objektTextViewOutlet;
@property (strong, nonatomic) IBOutlet UITextView *materialtextViewOutlet;
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UITextField *ansprechpartnerTexfieldOutlet;
@property (strong, nonatomic) IBOutlet UITextField *telefonTextfieldOutlet;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticInformationZurTechnischenAnfrageCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticSendenButtonCell;
@property (strong, nonatomic) IBOutlet BEMCheckBox *privacyPolicyCheckbox;
@property (strong, nonatomic) IBOutlet UIButton *readPrivayPolicyButton;
@end

@implementation TechnischeAnfrageTableViewController
int fotoSectionHeight = 44;
int zuatzHoeheTechnischeAnfrage = 30;

#pragma mark - View Lifecycle


-(void)viewDidLoad{

    //Optik
    
    if(self.showTechnischeAnfrageFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
    } else if(self.showTechnischeAnfrageFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
        [oroThemeManager customizeButtonImage:self.fotoButtonOutlet];
    [oroThemeManager customizeButtonImage:self.entfernenButtonOutlet];
    [oroThemeManager customizeButtonImage:self.sendenButtonOutlet];
    
    self.staticInformationZurTechnischenAnfrageCell.backgroundColor=[UIColor clearColor];
    self.staticSendenButtonCell.backgroundColor = [UIColor clearColor];
    
    [self prepareRightBarButtonItems];
    
    //TextView Delegate
    self.objektTextViewOutlet.delegate=self;
    self.materialtextViewOutlet.delegate=self;
    self.problembeschreibungTextViewOutlet.delegate=self;
    
    //Foto Sektion vorbereiten
    self.imageViewOutlet.hidden = YES;
    self.entfernenButtonOutlet.enabled = NO;
    
    //AppDelegate für CoreData aktivieren
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    //Prüfen ob eine gespeicherte Anfrage vorhanden ist. Wenn ja, anzeiegen
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([TechnischeAnfrage class])];
    
    NSArray *alteAnfragenArray = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if(alteAnfragenArray.count>0){
        
        TechnischeAnfrage *alteAnfrage = [alteAnfragenArray lastObject];
        //NSLog(@"alte Anfrage vorhanden:%@",alteAnfrage);
        
        self.ansprechpartnerTexfieldOutlet.text = alteAnfrage.ansprechpartner;
        self.telefonTextfieldOutlet.text = alteAnfrage.telefon;
        self.emailTextFieldOutlet.text = alteAnfrage.email;
        self.objektTextViewOutlet.text = alteAnfrage.objekt;
        self.materialtextViewOutlet.text = alteAnfrage.material;
        self.problembeschreibungTextViewOutlet.text = alteAnfrage.problembeschreibung;
        if(alteAnfrage.image!=nil){
            self.imageViewOutlet.image = [UIImage imageWithData:alteAnfrage.image];
            
            
            if ([UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPad) {
                
                fotoSectionHeight=500;
                
            } else {
                
                fotoSectionHeight=364;
                
            }
            self.imageViewOutlet.hidden = NO;
        }
        
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"voherige Anfrage:" andMessage:@"Eine alte Anfrage wurde noch nicht erfolgreich gesendet, soll sie nun gesendet oder gelöscht werden?"];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Senden" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self anfrageSenden];
        }]];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Löschen" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self alteGespeicherteAnfrageLoeschen];
            [self formularLeeren];
        }]];
        
        [self presentViewController:alertView animated:YES completion:nil];

    }
    
    alteAnfragenArray = nil;
    fetchRequest = nil;
    
}

-(void)prepareRightBarButtonItems{
    
    NSMutableArray *buttonArray = [NSMutableArray new];
    
    //BarButton
    [buttonArray addObject:[self clearFormButton:self]];
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.imageViewOutlet.image = nil;
    self.imageViewOutlet.hidden=YES;
    self.entfernenButtonOutlet.enabled=NO;
    fotoSectionHeight=44;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    if(self.showTechnischeAnfrageFrom==ReinigerChannel){
        [tracker set:kGAIScreenName value:@"Reiniger-Technische-Anfrage"];
    } else {
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Technische-Anfrage"];
    }
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}


    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - ScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{

     [self.view endEditing:YES];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];


}
#pragma mark - TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if (indexPath.section == 3 && indexPath.row == 0) { // Verwendetes Reinigungsmittel TextView
        
        CGRect bounds = self.objektTextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.objektTextViewOutlet.font;

        CGRect sizeOfText = [self.objektTextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zuatzHoeheTechnischeAnfrage);
    }
    
    if (indexPath.section == 4 && indexPath.row == 0) { // Oberfläche / Belag TextView
        
        CGRect bounds = self.materialtextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.materialtextViewOutlet.font;
        
        CGRect sizeOfText = [self.materialtextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zuatzHoeheTechnischeAnfrage);
    }
    
    if (indexPath.section == 5 && indexPath.row == 0) { // Problembeschreibung TextView
        
        CGRect bounds = self.problembeschreibungTextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.problembeschreibungTextViewOutlet.font;
        
        CGRect sizeOfText = [self.problembeschreibungTextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zuatzHoeheTechnischeAnfrage);
    }
    
    if(indexPath.section==6 && indexPath.row ==0){
        return fotoSectionHeight;
    }
    
    if(indexPath.section==7 && indexPath.row ==0){
        return 88;
    }
    
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if(section==0){
        label.text = @"Ansprechpartner für Rückmeldung*";
    } else if(section==1){
        label.text = @"Telefon*";
    } else if (section == 2)
    {
        label.text = @"E-Mail*";
    }  else if (section == 3)
    {
        if(self.showTechnischeAnfrageFrom == ReinigerChannel){
        label.text=@"Verwendetes Reinigungsmittel";
        } else {
            label.text=@"Verwendetes Desinfektionsmittel";

        }
    } else if (section == 4)
    {

        if(self.showTechnischeAnfrageFrom == ReinigerChannel){
            label.text=@"Oberfläche / Belag";
        } else {
            label.text=@"Desinfektionsaufgabe";
            
        }
    } else if (section == 5)
    {
        label.text=@"Fragestellung*";

    } else if (section == 6)
    {
        if(self.showTechnischeAnfrageFrom == ReinigerChannel){
            label.text=@"Foto der Reinigungssituation";
        } else {
            label.text=@"Foto der Desinfektionssituation";
            
        }

    } else if (section == 7){
        return nil;
    } else if (section == 8){
        return nil;
    }

    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section==7 || section==8){
        return 0;
    }
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

#pragma mark - UITextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView
{

    UITableViewCell *cell = (UITableViewCell*) [[textView superview] superview];
    [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)textViewDidChange:(UITextView *)textView
{

    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage* image = info[UIImagePickerControllerEditedImage];
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
    }
    
      //NSLog(@"ImageSize: %f - %f", image.size.width,image.size.height);
    
    if(image){
        NSData *imgData3 = UIImageJPEGRepresentation(image, 0.4f);
        UIImage *image2 = [UIImage imageWithData:imgData3];
        image = image2;
    self.imageViewOutlet.hidden=NO;
    self.imageViewOutlet.image = image;
        //NSLog(@"Devide: %@",[UIDevice currentDevice].model);
        if ([UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPad) {
            
            fotoSectionHeight=500;
        
        } else {

        fotoSectionHeight=364;
            
            //NSLog(@"ImageViewSize: %f - %f", self.imageViewOutlet.frame.size.width,self.imageViewOutlet.frame.size.height);
            //NSLog(@"ImageSize: %f - %f", image.size.width,image.size.height);
            
        }
        
    self.entfernenButtonOutlet.enabled=YES;
    [self.tableView reloadData];
        
        //NSLog(@"ImageSize: %f - %f", self.imageViewOutlet.image.size.width,self.imageViewOutlet.image.size.height);
         //NSLog(@"ImageScale: %f ", image.scale);
        //NSLog(@"ImageViewSize: %f - %f", self.imageViewOutlet.frame.size.width,self.imageViewOutlet.frame.size.height);

    }
        
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Email Validation

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(BOOL)checkFormFields{
    
    if(self.ansprechpartnerTexfieldOutlet.text.length==0){
        [self notifyUserAboutFailure:ANSPRECHPARTNER_LEER];
        return NO;
    }
    else if(self.telefonTextfieldOutlet.text.length==0) {
        [self notifyUserAboutFailure:TELEFON_LEER];
        return NO;
    }
    else if (self.emailTextFieldOutlet.text.length==0){
        [self notifyUserAboutFailure:EMAIL_LEER];
        return NO;
    } else if (![self validateEmail:self.emailTextFieldOutlet.text]) {
        [self notifyUserAboutFailure:EMAIL_VALIDATION_FALSE];
        return NO;
    } else if (self.problembeschreibungTextViewOutlet.text.length==0){
        [self notifyUserAboutFailure:PROBLEMBESCHREIBUNG_LEER];
        return NO;
    } else if(!self.privacyPolicyCheckbox.on){
        [self notifyUserAboutFailure:PRIVACY_POLICY_CHECKBOX];
        return NO;
    }
    
    return YES;
}

-(void)notifyUserAboutFailure:(formFieldFailure)failureCode{
    NSString *message;
    
    switch (failureCode) {
            
        case ANSPRECHPARTNER_LEER: // Fehler weil Ansprechpartner leer
            message =@"Bitte tragen Sie einen Ansprechpartner ein.";
            break;
            
        case TELEFON_LEER: //Fehler weil Telefon leer
            message =@"Bitte tragen Sie eine Telefonnummer ein.";
            break;
            
        case EMAIL_LEER: //Fehler weil E-Mail leer
            message =@"Bitte tragen Sie eine gültige E-Mail Adresse ein.";
            break;
            
        case EMAIL_VALIDATION_FALSE:
            message =@"Bitte tragen Sie eine gültige E-Mail Adresse ein.";
            break;
            
        case PROBLEMBESCHREIBUNG_LEER:
            message =@"Bitte tragen Sie eine Problembeschreibung ein.";
            break;
        case PRIVACY_POLICY_CHECKBOX:
            message =@"Bitte bestätigen Sie, dass Sie die Datenschutzerklärung akzeptieren.";
            break;
        default:
            break;
    }
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Fehler" andMessage:message andOKButtonWithCompletionHandler:nil];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - Button Definitions
- (UIBarButtonItem *)clearFormButton:(id)target {

    UIBarButtonItem *clearFormButton = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                             target:self
                             action:@selector(clearForm)];
    
    return clearFormButton;
}

#pragma mark - Button Actions

-(void)clearForm{
   
    UIAlertController *alertView  = [AlertHelper getUIAlertControllerWithTitle:@"Anfrage zurücksetzen" andMessage:@"Möchten Sie wirklich die Daten im Formular löschen?" OKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [self formularLeeren];
        [self alteGespeicherteAnfrageLoeschen];
    } andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        NSLog(@"cancel");
    }];
    
    [self presentViewController:alertView animated:YES completion:nil];

}

- (IBAction)entfernenButtonTouchUpInside:(id)sender {
    
    self.imageViewOutlet.image = nil;
    self.imageViewOutlet.hidden=YES;
    self.entfernenButtonOutlet.enabled=NO;
    fotoSectionHeight=44;
    
    [self.tableView reloadData];

}



- (IBAction)fotoButtonTouchupInside:(id)sender {

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *sheet = [AlertHelper getUIActionSheetWithTitle:@"Bild von Kamera oder aus Photoalbum?" andMessage:nil andCancelButtonWithCompletionHandler:nil];
//        sheet.tag = 111;  // Kamera oder Album
        
        [sheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Kamera" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self kameraAction];
        }]];
        
        [sheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Album" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self albumAction];
        }]];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            CGRect sourceRect = CGRectZero;
            sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
            sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
            
            sheet.popoverPresentationController.sourceView = self.view;
            sheet.popoverPresentationController.sourceRect = sourceRect;
            [sheet.popoverPresentationController setPermittedArrowDirections:0];
        }
        
        
        [self presentViewController:sheet animated:YES completion:nil];

//        [sheet showInView:[UIApplication sharedApplication].keyWindow.rootViewController.view];
        
    } else {
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        if ([mediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.editing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:picker animated:YES completion:nil];
        }
    }
}



-(void)kameraAction {
    
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    
    if ([mediaTypes containsObject:(NSString *)kUTTypeImage]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        picker.allowsEditing = YES;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:picker animated:YES completion:nil];
    }
}


-(void) albumAction {
    
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    if ([mediaTypes containsObject:(NSString *)kUTTypeImage]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        picker.allowsEditing = YES;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:picker animated:YES completion:nil];
    }
}

- (IBAction)sendenButtonTouchUpInside:(id)sender {
    
    [self anfrageSenden];
}

- (IBAction)readPrivacyPoilicyButtonTouchUpInside:(id)sender {
    
    DatenschutzTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DatenschutzTableViewController"];
    
    if(self.showTechnischeAnfrageFrom == DesinfektionChannel){
        controller.showDatenschutzFrom = DesinfektionChannel;
    } else {
        controller.showDatenschutzFrom = ReinigerChannel;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - Anfrage Senden

-(void)anfrageSenden{

    [self alteGespeicherteAnfrageLoeschen];
    
    if([self checkFormFields]) {
        
        if([ConnectionCheck hasConnectivity]) {
            
            [SVProgressHUD showWithStatus:@"Sende die Anfrage" maskType:SVProgressHUDMaskTypeBlack];
            
            NSMutableDictionary *technischeAnfrageDictionary = [[NSMutableDictionary alloc]init];
            
            [technischeAnfrageDictionary setObject:  self.ansprechpartnerTexfieldOutlet.text forKey:@"ansprechpartner"];
            [technischeAnfrageDictionary setObject:  self.telefonTextfieldOutlet.text forKey:@"telefon"];
            [technischeAnfrageDictionary setObject:  self.emailTextFieldOutlet.text forKey:@"email"];
            [technischeAnfrageDictionary setObject:  self.objektTextViewOutlet.text forKey:@"reinigungsmittel"];
            [technischeAnfrageDictionary setObject:  self.materialtextViewOutlet.text  forKey:@"oberflaeche"];
            [technischeAnfrageDictionary setObject:  @"Datenschutzerklärung akzeptiert"  forKey:@"datenschutz"];
            if(self.showTechnischeAnfrageFrom==ReinigerChannel){
                [technischeAnfrageDictionary setObject: @"ReinigerChannel" forKey:@"sender"];

            } else {
                [technischeAnfrageDictionary setObject: @"DesinfektionsChannel" forKey:@"sender"];
            }
            [technischeAnfrageDictionary setObject:  self.problembeschreibungTextViewOutlet.text  forKey:@"problembeschreibung"];
            if(self.imageViewOutlet.image!=nil){
                [technischeAnfrageDictionary setObject:  self.imageViewOutlet.image.base64String  forKey:@"image"];
            }
            
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:technischeAnfrageDictionary options:0 error:nil];
            NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            NSString *post = [NSString stringWithFormat:@"%@", jsonString];
            NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
            
            [request setURL:[NSURL URLWithString:@"http://www.orohygienesystem.de/anfragen/json_technische_anfrage.php"]];
            [request setHTTPMethod:@"POST"];
            
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(((NSHTTPURLResponse *)response).statusCode==200){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [SVProgressHUD showSuccessWithStatus:@"Anfrage erfolgreich gesendet"];
                        [self formularLeeren];
                    });

                }
            }];
            
            [postDataTask resume];
            
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung"];
            
            
            //Aktuelle Anfrage speichern damit sie nicht verloren geht
            
            TechnischeAnfrage *aktuelleAnfrage = [TechnischeAnfrage insertTechnischeAnfrageInManagedObjectContext:self.appDelegate.managedObjectContext];
            aktuelleAnfrage.ansprechpartner = self.ansprechpartnerTexfieldOutlet.text;
            aktuelleAnfrage.telefon = self.telefonTextfieldOutlet.text;
            aktuelleAnfrage.email = self.emailTextFieldOutlet.text;
            aktuelleAnfrage.objekt = self.objektTextViewOutlet.text;
            aktuelleAnfrage.material = self.materialtextViewOutlet.text;
            aktuelleAnfrage.problembeschreibung = self.problembeschreibungTextViewOutlet.text;
            if(self.imageViewOutlet.image != nil){
                aktuelleAnfrage.image = UIImagePNGRepresentation(self.imageViewOutlet.image);
            }
            //NSLog(@"neue Anfrage:%@",aktuelleAnfrage);
            [self.appDelegate saveContext];
            
            //NSLog(@"neue Anfrage wurde gespeichert");
            
            
        }
        
    }
    
    
    

}

-(void)alteGespeicherteAnfrageLoeschen{
    //Prüfen ob bereits eine alte Anfrage gespeichert wurde und eventuell löschen
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([TechnischeAnfrage class])];
    
    NSArray *anfragenArray = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if(anfragenArray.count>0){
        
        TechnischeAnfrage *alteAnfrage = [anfragenArray lastObject];
        //NSLog(@"alte Anfrage:%@",alteAnfrage);
        [alteAnfrage deleteTechnischeAnfrage];
        [self.appDelegate saveContext];
        
        //NSLog(@"alte Anfrage wurde gelöscht");
    }
    
    anfragenArray = nil;
    fetchRequest = nil;

}

-(void)formularLeeren{

    self.ansprechpartnerTexfieldOutlet.text=@"";
    self.telefonTextfieldOutlet.text=@"";
    self.emailTextFieldOutlet.text=@"";
    self.objektTextViewOutlet.text=@"";
    self.materialtextViewOutlet.text=@"";
    self.problembeschreibungTextViewOutlet.text=@"";
    self.imageViewOutlet.image=nil;
    self.imageViewOutlet.hidden=YES;
    self.entfernenButtonOutlet.enabled=NO;
    fotoSectionHeight=44;
    self.privacyPolicyCheckbox.on = false;
    
    [self.tableView reloadData];
}
@end
