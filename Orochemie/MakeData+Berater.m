//
//  MakeData+Berater.m
//  orochemie
//
//  Created by Axel Beckert on 16.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MakeData+Berater.h"
#import "Berater+ccm.h"
#import "PLZ+ccm.h"

@implementation MakeData (Berater)
-(void)makeBeraterData{
   
    [self makeBerater];
    [self readCSVFileForGettingPLZForBerater];
    
}

-(void)makeBerater {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    Berater *berater1 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater1.name = @"Frau Sabine Krückeberg";
    berater1.image =@"berater_kruekeberg.png";
    berater1.telefon= @"0151-42618443";

    
    Berater *berater2 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater2.name = @"Herr Detlef Brätter";
    berater2.image =@"berater_braetter.png";
    berater2.telefon= @"0151-12281095";

    
    Berater *berater3 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater3.name = @"Herr René Walden";
    berater3.image =@"berater_walden.jpg";
    berater3.telefon= @"0160 8863352";


    Berater *berater4 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater4.name = @"Herr Marcus Koppel";
    berater4.image =@"berater_koppel.png";
    berater4.telefon= @"0160-7169632";


    Berater *berater5 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater5.name = @"Herr Markus Lengger";
    berater5.image =@"orochemie_bildblock_011_500.jpg";
    berater5.telefon= @"0160-8863352";

    
    Berater *berater6 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater6.name = @"Herr Stefan Bergauer";
    berater6.image =@"berater_bergauer.png";
    berater6.telefon= @"0152-28800850";

    
    Berater *berater7 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater7.name = @"Herr Reinhard Reiners";
    berater7.image =@"berater_reiners.png";
    berater7.telefon= @"0163-3130803";

    Berater *berater8 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater8.name = @"orochemie Sales-Team";
    berater8.image =@"orochemie_bildblock_011_500.jpg";
    berater8.telefon= @"0163-3130803";
    
    Berater *berater9 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater9.name = @"Frau Cecelia Niederland";
    berater9.image =@"berater_niederland.png";
    berater9.telefon= @"0172-1793752";
    
    Berater *berater10 = [Berater insertBeraterInManagedObjectContext:appDelegate.managedObjectContext];
    berater10.name = @"Frau Anita Wünsch";
    berater10.image =@"berater_wuensch.jpg";
    berater10.telefon= @"0151 42618443";
    
    [appDelegate saveContext];
    
    
}

-(void)readCSVFileForGettingPLZForBerater{
    
    //Fehlervariable
    NSError *errorReading;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Berater class])];
    
    NSArray* beraterArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&errorReading];
    
    //if(errorReading) NSLog(@"Error beim einlesen der Reiniger: %@", errorReading.localizedDescription);
    
    
    //Zugang zur Datei
    NSString *path=[[NSBundle mainBundle] pathForResource:@"berater_plz_2018_06_29" ofType:@"csv"]; //alt: berater_plz_2017_07_17
    NSURL *csvUrl = [NSURL fileURLWithPath:path];
	
    //Lesen der Datei
    NSString *contents = [NSString stringWithContentsOfURL:csvUrl encoding:NSUTF8StringEncoding error:&errorReading];
    
    if(errorReading) NSLog(@"error: %@", errorReading.localizedDescription);
    
    //Array mit den einzelnen Zeilen
    NSArray* lines = [contents componentsSeparatedByString:@"\n"];

    
    //Array mit den einzelnen Werten innerhalb der Zeilen
    for (NSString* line in lines)
    {
        NSArray* fields = [line componentsSeparatedByString:@","];
        
        
        //Zuweisung der Felder zum Barcode Objekt
        PLZ *plz = [PLZ insertPLZInManagedObjectContext:appDelegate.managedObjectContext];
        plz.plz = fields[0];
        plz.ort = fields[1];
        plz.land = fields[4];
        
        
        
        
        
        //Suchen des passenden Produkt-Objekt
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"name LIKE[c] %@", fields[2]];
        NSArray *filteredArr = [beraterArray filteredArrayUsingPredicate:pred];
        
        Berater *b = [filteredArr lastObject];
        
        
        
        if(b!=nil){
            
            //Holen des Berater Objektes aus dem Ursprünglichen Berater-Array
            int index = (int)[beraterArray indexOfObject:b];
            b = [beraterArray objectAtIndex:index];
            
            //hinzufügen des Barcodes zum Produkt
            [b addPlzObject:plz];
            
            ////NSLog(@"Berater: %@ mit PLZ: %@ und Ort: %@ und Land: %@", b.name, plz.plz, plz.ort, plz.land);
            
            
        } else {
            
            
            [plz deletePLZ];
            
        }
        
        
    }
    
    [appDelegate saveContext];
    
    
}
@end
