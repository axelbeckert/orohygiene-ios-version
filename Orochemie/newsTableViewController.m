//
//  newsTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 27.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "newsTableViewController.h"
#import "NewsModel.h"
#import "News.h"
#import "AppDelegate.h"
//#import "newsDetailViewController.h"
#import "WebviewViewController.h"
#import "NewsTableCell.h"



@interface newsTableViewController () 
@property (nonatomic, strong) NewsModel* nModel;
@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic,strong) AppDelegate *appDelegate;
@end

@implementation newsTableViewController 

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(NewsModel *)nModel
{
    if(!_nModel) _nModel = [[NewsModel alloc] init];
    return _nModel;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nModel = nil;
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //Timer einschalten
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
    
        
    if(self.showNewsFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
        
    } else if(self.showNewsFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];

    
  if([ConnectionCheck hasConnectivity]) {
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
  
    NSString* urlString = @"http://www.orohygienesystem.de/json.php";
    NSURL* url = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:10.0];
    [request setCachePolicy:NSURLCacheStorageAllowed];
    [request setValue:@"text/json" forHTTPHeaderField:@"Content-type"];
    
      NSURLSession *session = [NSURLSession sharedSession];
      
      NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
          if (error) {
              
              //NSLog(@"Fehler: %@", error);
              
          };
          
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
          
          if (httpResponse.statusCode != 200) {
              return;
          }

          error = nil;
          NSArray* jsonContainer = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
          if (error) {
              
              //NSLog(@"Fehler: %@", error);
              
          };
          
          for (NSDictionary* dictionary in jsonContainer) {
              News* news = [[News alloc] init];
              news.newsId = dictionary[@"id"];
              news.text1 = dictionary[@"text1"];
              news.text2 = dictionary[@"text2"];
              news.imageUrl = dictionary[@"image"];
              
              [self.nModel addNews:news];
          }
          
          
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.tableView reloadData];
              [self.timer invalidate]; //Timer ausschalten
          });
          
      }];
    
      [dataTask resume];
      
      
  } else {
      
      [self.timer invalidate]; //Timer ausschalten

      [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
      
  }

    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"News-Uebersicht-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    [self.timer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.nModel.numberOfNews;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NewsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    

    
    News* record = [self.nModel newsAtIndexPath:indexPath];
    

    cell.titleOutlet.text = record.text1;
    cell.subTitleOutlet.text=record.text2;
    cell.imageViewOutlet.image = [UIImage imageNamed:@"new.png"];
    
    if (!record.image) {
        NSBlockOperation* loadOperation = [NSBlockOperation blockOperationWithBlock:^{
           


                NSURL* url = [NSURL URLWithString:record.imageUrl];
                NSData* imagaData = [NSData dataWithContentsOfURL:url];
                record.image = [UIImage imageWithData:imagaData];

        }];

        NSBlockOperation* transferToCellOperation = [NSBlockOperation blockOperationWithBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            });
        }];
        [transferToCellOperation addDependency:loadOperation];

        NSOperationQueue* operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue addOperation:loadOperation];
        [operationQueue addOperation:transferToCellOperation];

    } else {

        cell.imageViewOutlet.image = record.image;
     
    }

    

//    
//    if (!record.image) {
//        NSBlockOperation* loadOperation = [NSBlockOperation blockOperationWithBlock:^{
//            UIImage* nimageFromCache = [delegate.cache objectForKey:record.imageUrl];
//
//            
//            if (!nimageFromCache) {
//                //NSLog(@"kein Bild im cache");
//                NSURL* url = [NSURL URLWithString:record.imageUrl];
//                NSData* imagaData = [NSData dataWithContentsOfURL:url];
//                record.image = [UIImage imageWithData:imagaData];
//                
//               
//
//               //record.image = [self scaleImage:record.image toSize:itemSize];
//                
//
//                [delegate.cache setObject:record.image forKey:record.imageUrl];
//                
//                
//            } else {
//                ////NSLog(@"--> Dieses Bild kommt vom Cache");
//                record.image = nimageFromCache;
//                
//            }
//        }];
//    
//        NSBlockOperation* transferToCellOperation = [NSBlockOperation blockOperationWithBlock:^{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            });
//        }];
//        [transferToCellOperation addDependency:loadOperation];
//        
//        NSOperationQueue* operationQueue = [[NSOperationQueue alloc] init];
//        [operationQueue addOperation:loadOperation];
//        [operationQueue addOperation:transferToCellOperation];
//        
//    } else {
//
//        //CGSize itemSize = CGSizeMake(50, 50);
//        //cell.imageView.image = [self scaleImage:record.image toSize:itemSize];
//        cell.imageViewOutlet.image = record.image;
//     
//    }

    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
        [SVProgressHUD dismiss];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"NewsDetail"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle = @"News";
        News *news =[self.nModel newsAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.pdfFileName = [NSString stringWithFormat:@"news_%@",news.newsId];
        
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/,%@.php",news.newsId];        
    }
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - AlertView Delegate & Timer Selector

- (void)cancelWeb
{
   [self.timer invalidate]; //Timer ausschalten
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [AlertHelper showAlert:alert];
    [SVProgressHUD dismiss];
}




@end
