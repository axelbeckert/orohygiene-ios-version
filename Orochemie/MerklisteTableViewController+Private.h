//
//  MerklisteTableViewController+Private.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//
#import "MerklisteTableViewController.h"
#import "Merkliste+ccm.h"
#import "SavedMerklistenTableViewController.h"
#import "ProduktDetailTabbarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "ProduktStaticTabBarViewController.h"
#import "MerklistenTabbarController.h"
#import "Produkte.h"
#import "Produkte+ccm.h"
#import "MerklistenTableViewHelper.h"

typedef void(^UITableViewControllerBlock)(UITableViewController*);

@interface MerklisteTableViewController () <SavedMerklistenTableViewControllerDelegate,MerklistenTableViewHelperDelegate>
@property(nonatomic, strong)NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSMutableDictionary *sections;
@property (nonatomic,strong) NSMutableArray *sectionTitles;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,strong) NSMutableArray *reinigerArray;
@property (nonatomic,strong) NSMutableArray *desinfektionsArray;
@property (nonatomic,strong) MerklistenTableViewHelper *merklistenTableViewHelper;

-(void)setRightBarButtonItemToNil;
@end
