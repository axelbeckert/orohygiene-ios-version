//
//  OberflaechenCollector.h
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JsonCollector.h"
@class OberflaechenUndBelaegeTableViewController;

@interface OberflaechenCollector : JsonCollector
@property (nonatomic, strong) OberflaechenUndBelaegeTableViewController *oberflaechenUndBelaegeTableViewController;
-(void) startCollectingOberflaechen;
@end
