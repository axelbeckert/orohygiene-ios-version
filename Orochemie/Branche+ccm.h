//
//  Branche+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Branche.h"

@interface Branche (ccm)
+(Branche*) insertBrancheInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteBranche;
@end
