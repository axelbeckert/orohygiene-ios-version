//
//  produktStaticTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
@class JSMLabel;
@class Produkte;

@interface ProduktStaticTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableViewCell *staticTableViewImageAndButtonCell;

@property (nonatomic, strong) Produkte *produkt;
@property(nonatomic,strong) NSString *branchenName;
@property(nonatomic,strong) NSString *anwendungName;

@property (strong, nonatomic) IBOutlet UIImageView *produktImageViewOutlet;

@property (strong, nonatomic) IBOutlet JSMLabel *produktNameLabelOutlet;


@property (strong, nonatomic) IBOutlet JSMLabel *produktZusatzLabelOutlet;

@property (strong, nonatomic) IBOutlet JSMLabel *produktEinsatzbereichLabelOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *produktListungLabelOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *produktWirksamkeitLabelOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *produktDosierungLabelOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *produktPflichttextOutlet;
@property (strong, nonatomic) IBOutlet UIButton *sicherheitsdatenblattButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *betriebsanweisungButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *hinweisButtonOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffImageEinsOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffImageZweiOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffImageDreiOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffImageVierOutlet;


@property (strong, nonatomic) IBOutlet UIButton *hygieneChecklistenButtonOutlet;
- (IBAction)sicherheitsDatenblattTouchUpInside:(id)sender;
- (IBAction)betriebsanweisungToucUpInside:(id)sender;
- (IBAction)hinweisTouchUpInside:(id)sender;
- (IBAction)hygieneChecklistenButtonAction:(id)sender;

@end
