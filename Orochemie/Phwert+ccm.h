//
//  Phwert+ccm.h
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Phwert.h"

@interface Phwert (ccm)
+(Phwert*) insertPhwertInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deletePhwert;
@end
