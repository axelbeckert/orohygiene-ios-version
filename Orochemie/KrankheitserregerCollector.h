//
//  KrankheitserregerCollector.h
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JsonCollector.h"
@class KrankheitserregerTableViewController;

@interface KrankheitserregerCollector : JsonCollector
@property (nonatomic, strong) KrankheitserregerTableViewController *krankheitserregerTableViewController;
-(void) startCollectingKrankheitserreger;
@end
