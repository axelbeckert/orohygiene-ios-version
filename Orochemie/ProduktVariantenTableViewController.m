//
//  ProduktVariantenTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 06.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktVariantenTableViewController.h"
#import "ProduktAuflistungTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "ProduktStaticTableViewController.h"
#import "ProduktStaticTabBarViewController.h"
#import "Produktvarianten.h"
#import "ProduktVariantenStaticTableViewController.h"
#import "VariantenTableViewCell.h"
#import "BestellPosition.h"
#import "BestellProdukteTableViewController.h"


@interface ProduktVariantenTableViewController ()
@property (nonatomic, strong) NSFetchedResultsController *produkteFetchedResultsController;


@end

@implementation ProduktVariantenTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


-(void) viewDidLoad
{
    [oroThemeManager  customizeTableView:self.tableView];
    
    //UIEdgeInsets inset = UIEdgeInsetsMake(45, 0, 0, 0);
    //self.tableView.contentInset = inset;
    
    
    
    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
    //NSLog(@"Produkt: %@",self.produkt.name);
    self.tableView.allowsMultipleSelection=YES;

//    UIEdgeInsets inset = UIEdgeInsetsMake(44, 0, 0, 0);
//    self.tableView.contentInset = inset;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bestellmengeGeandert:) name:@"BESTELLMENGEGEAENDERT" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.tabBarController)
    {
        self.tabBarController.title=@"Varianten";
    }
    
    self.title=@"Produktübersicht";
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"BestellmengenArray: %@",self.bestellMengenArray);
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"BestellmengenArray: %@",self.bestellMengenArray);
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.bestellProdukteTableViewController.bestellMengenArray = self.bestellMengenArray;
   
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"BestellmengenArray: %@",self.bestellMengenArray);
    
//    NSMutableArray *alleArtikel = [NSMutableArray new];
//    
//    for(BestellPosition *position in self.bestellMengenArray)
//    {
//        NSMutableArray *artikel = [NSMutableArray new];
//        NSString *bestellMenge = position.bestellMenge;
//        NSString *bezeichnung = position.produktVariante.bezeichnung1;
//        NSString *ean = position.produktVariante.barcode;
//        [artikel addObject:bestellMenge];
//        [artikel addObject:bezeichnung];
//        [artikel addObject:ean];
//        
//        [alleArtikel addObject:artikel];
//    }
//    
//
//    NSMutableDictionary *bestellungDict = [[NSMutableDictionary alloc]init];
//    
//    
//    [bestellungDict setObject:  @"Beckert" forKey:@"kundenname"];
//    [bestellungDict setObject:  @"oroBest" forKey:@"bestellung"];
//    [bestellungDict setObject:  @"D123F" forKey:@"kundennummer"];
//    
//    [bestellungDict setObject:alleArtikel forKey:@"artikel"];
//    
//    
//    
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:bestellungDict options:0 error:nil];
//    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//    
//    //NSLog(@"Dict:%@", jsonString);
//    
//    //NSString *jsonString = [loginDict JSONRepresentation];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    NSString *post = [NSString stringWithFormat:@"%@", jsonString];
//    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
//    
//    [request setURL:[NSURL URLWithString:@"https://www.tpwebservice.com/json/json.php"]];
//    [request setHTTPMethod:@"POST"];
//    
//    [request setHTTPBody:postData];
//    
//    BOOL success = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    
//    if(success)
//    {
//        //NSLog(@"Die Bestellung wurde erfolgreich gesendet");
//    }
    
    
    
}


-(void)bestellmengeGeandert:(NSNotification*)notification
{
    VariantenTableViewCell *cell = notification.object;
    BestellPosition *bestellPosition =  [BestellPosition new];
    
    bestellPosition.produktVariante = cell.produktVariante;
    bestellPosition.bestellMenge = cell.bestellMengenAnzeigeOutlet.text;
    
    //NSLog(@"Bestellm.: %@",cell.bestellMengenAnzeigeOutlet.text);

    BOOL found = NO;
    int idx;
    for (BestellPosition *position in self.bestellMengenArray) {

        if ([position.produktVariante isEqual:bestellPosition.produktVariante]) {
            
            idx =(int)[self.bestellMengenArray indexOfObject:position];
            found = YES;
        }
    }
    
    if(found) [self.bestellMengenArray removeObjectAtIndex:idx];

    if(![bestellPosition.bestellMenge isEqualToString:@"0"])
    {
    [self.bestellMengenArray addObject:bestellPosition];
    }
    
    cell = nil;
    bestellPosition=nil;
    
    //NSLog(@"Inhalt BestellmengenArray: %@",self.bestellMengenArray);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"variantenCell";
    
    VariantenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView = backgroundView;
    
    Produktvarianten *produkte =[self.produkteFetchedResultsController objectAtIndexPath:indexPath];
    //UIImage *image = [UIImage imageNamed:produkte.bild];
    cell.produktVariante = produkte;
    //[oroThemeManager customizeAccessoryView:cell];
    // Configure the cell...
    cell.titelLabelOutlet.text = produkte.bezeichnung1;
    cell.subTitleLabelOutlet.text = [NSString stringWithFormat:@"Barcode: %@", produkte.barcode ];
    //cell.imageView.image = image;
    

    return cell;
}




#pragma mark - must be overloaded methods
-(NSFetchedResultsController *)fetchedResultsController
{
    if(self.produkteFetchedResultsController !=nil){
        return self.produkteFetchedResultsController;
    }
    
    JSMCoreDataHelper *helper = [JSMCoreDataHelper new];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Produktvarianten class]) inManagedObjectContext:helper.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produkte.name CONTAINS[c] %@",self.produkt.name];
    
    
    fetchRequest.entity=entityDescription;
    fetchRequest.fetchBatchSize=64;
    fetchRequest.predicate=predicate;
    
    
    NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:@"bezeichnung1" ascending:YES selector:@selector(localizedStandardCompare:)];
    
    
    NSArray *sortArray= [NSArray arrayWithObject:sortName];
    
    fetchRequest.sortDescriptors = sortArray;
    
    self.produkteFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:helper.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    self.produkteFetchedResultsController.delegate=self;
    
    ////NSLog(@"Menge gefunden: %@", self.produkteFetchedResultsController  s)
    return self.produkteFetchedResultsController;
}



//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    
//    if([segue.identifier isEqualToString:@"produktVariantenStaticSegue"]){
//        
//        ProduktVariantenStaticTableViewController *controller = segue.destinationViewController;
//        controller.produktvariante =[self.produkteFetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
//
//        }
//}

@end
