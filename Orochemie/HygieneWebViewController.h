//
//  HygieneWebViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Hygienetipp;

@interface HygieneWebViewController : UIViewController 
@property (nonatomic,strong) Hygienetipp* hygienetipp;
- (IBAction)refreshAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)forwardAction:(id)sender;
- (IBAction)stopLoadingAction:(id)sender;




@end
