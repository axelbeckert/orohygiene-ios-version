//
//  BestelluebersichtTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "BestelluebersichtTableViewController.h"
#import "BestellungTableViewController.h"
#import "ProduktAuflistungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "BestelluebersichtTableViewCell.h"
#import "BarcodeReader.h"
#import "KrankheitserregerTableViewController.h"
#import "BestellProdukteTableViewController.h"
#import "BestellPosition.h"
#import "Produktvarianten.h"
#import <QuartzCore/QuartzCore.h>



@interface BestelluebersichtTableViewController ()

@end

@implementation BestelluebersichtTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];
    
    
    self.title=@"Produkt-Bestellung-Übersicht";
    
    
    UIBarButtonItem* bestellActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(bestellAction:)];
    
    NSArray *buttonArray = @[bestellActionButton];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBestellPosition:) name:@"DELETEBESTELLPOSITION" object:nil];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBestellPosition:) name:@"BESTELLMENGENAENDERUNG" object:nil];
    
//    self.navigationPoints = @[@"Desinfektionsmittel + Hygienepräperate",@"Zubehör Desinfektionsmitel",@"Reinigungsmittel",@"Zubehör Reinigungsmittel",@"Gesamtbestellung prüfen und Adresse eingeben"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"BestellmengenArray: %@",self.bestellMengenArray);
    if([self.bestellMengenArray count]!=0)
    {
        self.bestellungTableViewController.bestellMengenArray = self.bestellMengenArray;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)deleteBestellPosition:(NSNotification*)notification

{
    BestelluebersichtTableViewCell * cell = notification.object;
  

    [self.bestellMengenArray removeObjectAtIndex:cell.indexPathForCell.row];
    [self.tableView reloadData];


    //[self.tableView reloadData];
}


-(void)changeBestellPosition:(NSNotification*)notification

{
    //BestelluebersichtTableViewCell * cell = notification.object;
    //NSLog(@"Änderung der BestellMenge: %@", cell.mengeTextLabelOutlet.text);
}


-(void)bestellAction:(id)sender
{
    NSMutableArray *alleArtikel = [NSMutableArray new];
    
    for(BestellPosition *position in self.bestellMengenArray)
    {
        NSMutableArray *artikel = [NSMutableArray new];
        NSString *bestellMenge = position.bestellMenge;
        NSString *bezeichnung = position.produktVariante.bezeichnung1;
        NSString *ean = position.produktVariante.barcode;
        [artikel addObject:bestellMenge];
        [artikel addObject:bezeichnung];
        [artikel addObject:ean];
        
        [alleArtikel addObject:artikel];
    }
    
    
    NSMutableDictionary *bestellungDict = [[NSMutableDictionary alloc]init];
    
    
    [bestellungDict setObject:  @"Beckert" forKey:@"kundenname"];
    [bestellungDict setObject:  @"oroBest" forKey:@"bestellung"];
    [bestellungDict setObject:  @"D123F" forKey:@"kundennummer"];
    
    [bestellungDict setObject:alleArtikel forKey:@"artikel"];
    
    
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:bestellungDict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"Dict:%@", jsonString);
    
    //NSString *jsonString = [loginDict JSONRepresentation];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *post = [NSString stringWithFormat:@"%@", jsonString];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setURL:[NSURL URLWithString:@"https://www.tpwebservice.com/json/json.php"]];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:postData];

    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(((NSHTTPURLResponse *)response).statusCode==200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:@"Bestellung erfolgreich gesendet"];
                [self.bestellMengenArray removeAllObjects];
                [self.tableView reloadData];
            });

        }
    }];
    
    [postDataTask resume];
    
}


#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bestellMengenArray.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BestelluebersichtTableViewCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    BestelluebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    BestellPosition *bestellPosition = [self.bestellMengenArray objectAtIndex:indexPath.row];
    Produktvarianten *produktVariante = bestellPosition.produktVariante;
    
    NSScanner *scanner = [NSScanner scannerWithString:bestellPosition.bestellMenge];
    double d;
    
    cell.stepperValue = [scanner scanDouble:&d];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    cell.indexPathForCell = indexPath;
    cell.bestellMengenStepperOutlet.value = d;
    

    [oroThemeManager customizeAccessoryView:cell];
    //[oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    // Configure the cell...
    cell.mengeTextLabelOutlet.text = [NSString stringWithFormat:@"%@", bestellPosition.bestellMenge];
    cell.textLabelOutlet.text = [NSString stringWithFormat:@"%@", produktVariante.bezeichnung1 ];
    
    
    
    
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(indexPath.row == 0)
//    {
//        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
//        controller.bestellMengenArray = self.bestellMengenArray;
//        controller.bestellungTableViewController = self;
//        [self.navigationController pushViewController:controller animated:YES];
//        
//        
//    } else if (indexPath.row == 1)
//    {
//        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    } else if (indexPath.row == 2)
//    {
//        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
//        [self.navigationController pushViewController:controller animated:YES];
//    }  else if (indexPath.row == 3)
//    {
//        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    }  else if (indexPath.row == 4)
//    {
//        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    }
//    
    
}


@end
