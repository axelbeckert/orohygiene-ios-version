//
//  Produkte+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Produkte+CoreDataProperties.h"

@implementation Produkte (CoreDataProperties)

@dynamic bild;
@dynamic dosierung;
@dynamic einsatzbereich;
@dynamic index;
@dynamic information;
@dynamic informationPdf;
@dynamic listung;
@dynamic name;
@dynamic nameHauptbezeichnung;
@dynamic nameZusatz;
@dynamic pflichttext;
@dynamic sortierreihenfolge;
@dynamic uid;
@dynamic wirksamkeit;
@dynamic zusatz;
@dynamic anwendung;
@dynamic branche;
@dynamic gefahrstoffsymbole;
@dynamic handlungsanweisung;
@dynamic krankheitserreger;
@dynamic merkliste;
@dynamic pictogramm;
@dynamic produktdosierung;
@dynamic produktvarianten;
@dynamic sicherheit;
@dynamic video;
@dynamic wirksamkeiten;

@end
