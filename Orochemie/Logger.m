//
//  Logger.m
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Logger.h"

@interface Logger()

@end

@implementation Logger

-(void)logMessage:(NSString*)message{
    
    if(self.isLoggingActive == 1){
        [self logOnScreen:message];
    }
}

-(void) logOnScreen:(NSString*)message{
    NSLog(@"[SCREENLOGGER] %@",message);
}
@end
