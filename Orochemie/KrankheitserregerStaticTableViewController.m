//
//  VirusStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "KrankheitserregerStaticTableViewController.h"
#import "JSMToolBox.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "ProduktStaticTabBarViewController.h"
#import "ProduktempfehlungTableViewControllerDatasourceandDelegate.h"
#import "ChecklistenDataSourceandDelegate.h"
#import "WebviewViewController.h"
#import "Checkliste.h"
#import "JSMLabel.h"

#import <QuartzCore/QuartzCore.h>

@interface KrankheitserregerStaticTableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *produktempfehlungTableViewOutlet;
@property (nonatomic, strong) ProduktempfehlungTableViewControllerDatasourceandDelegate *produktEmpfehlungTableViewDelegate;
@property(nonatomic,strong) ChecklistenDataSourceandDelegate *checklistenTableViewDelegate;

@property (strong, nonatomic) IBOutlet UITableView *checklistenTableViewOtlet;
@property (strong, nonatomic) IBOutlet JSMLabel *risikogruppenLabelOutlet;

@end

@implementation KrankheitserregerStaticTableViewController
NSInteger zusatz_Hoehe = 30;


#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Details";
    [oroThemeManager customizeTableView:self.tableView];
   
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    //Aufbau Produktempfehlungstabelle
    self.produktEmpfehlungTableViewDelegate = [[ProduktempfehlungTableViewControllerDatasourceandDelegate alloc] init];
    self.produktempfehlungTableViewOutlet.delegate = self.produktEmpfehlungTableViewDelegate;
    self.produktempfehlungTableViewOutlet.dataSource = self.produktEmpfehlungTableViewDelegate;
    

    [self.produktempfehlungTableViewOutlet setSeparatorInset:UIEdgeInsetsZero];


    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
        
    self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray = [[self.krankheitserreger.produkte allObjects] sortedArrayUsingDescriptors:sortArray];
  
    //Aufbau Checklistentabelle
    self.checklistenTableViewDelegate = [[ChecklistenDataSourceandDelegate alloc] init];
    self.checklistenTableViewOtlet.delegate = self.checklistenTableViewDelegate;
    self.checklistenTableViewOtlet.dataSource = self.checklistenTableViewDelegate;
    
    [self.checklistenTableViewOtlet setSeparatorInset:UIEdgeInsetsZero];

    
    self.checklistenTableViewDelegate.checklistenArray = [self.krankheitserreger.checkliste allObjects];
    

    self.NameTextLabelOutlet.text = [NSString stringWithFormat:@"%@%@",self.krankheitserreger.name,@"\n"];
    self.NameTextLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
    
    self.ErregerTextLabelOutlet.text = [NSString stringWithFormat:@"%@%@",self.krankheitserreger.art,@"\n"];
    self.ErregerTextLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
    
    self.UebertragungswegTextLabelOutlet.text = [NSString stringWithFormat:@"%@%@",self.krankheitserreger.uerbertragungswege,@"\n"];
    self.UebertragungswegTextLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
    
    self.risikogruppenLabelOutlet.text =[NSString stringWithFormat:@"%@%@",self.krankheitserreger.risikogruppe,@"\n"];
    self.risikogruppenLabelOutlet.verticalAlignment= JSMLabelAlignmentTop;
    
    [self addTableHeader];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    
    [tracker set:kGAIScreenName value:@"Krankheitserreger-Detail-Information"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Krankheitserreger"
                                                          action:@"Show"
                                                           label:self.krankheitserreger.name                                                           value:nil] build]];

}

-(void)addTableHeader{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(hinweis:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:cHinweisWissenGewissenButton forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
    
    button.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.75];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, button.frame.size.height)];
    
    [headerView addSubview:button];
    
    self.tableView.tableHeaderView = headerView;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Krankheitserreger-Detail-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Delegate

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if(section==3){
        
        return [NSString stringWithFormat: @"Produktempfehlungen (%lu)",(unsigned long)[self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] ];
        
    } else if (section==0){
        
        return @"Name";
        
    }  else if (section==1){
        
        return @"Erregerart";
        
    }  else if (section==2){
        
        return @"Übertragungswege";
        
    } else if (section==4){
        
        return [NSString stringWithFormat: @"Checklisten (%lu)",(unsigned long)[self.checklistenTableViewDelegate.checklistenArray count] ];
    }
    
    return @"";

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Name";
    } else if (section == 1)
    {
        label.text=@"Erregerart";
    } else if (section == 2)
    {
        label.text=@"Risikogruppe gem. BioStoffV";
    }else if (section == 3)
    {
        label.text=@"Übertragungswege";
    } else if (section == 4)
    {
        label.text=[NSString stringWithFormat: @"Produktempfehlungen (%lu)",(unsigned long)[self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] ];
        
    } else if (section == 5)
    {
        label.text=[NSString stringWithFormat: @"Checklisten (%lu)",(unsigned long)[self.checklistenTableViewDelegate.checklistenArray count]];
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (indexPath.section == 0 && indexPath.row == 0) {    // Name Label
       
        
        CGRect bounds = self.NameTextLabelOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont* font = self.NameTextLabelOutlet.font;
        
        CGRect sizeOfText = [self.NameTextLabelOutlet.text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        
        
        return MAX(44, sizeOfText.size.height+zusatz_Hoehe);

    }
    
    if (indexPath.section == 1 && indexPath.row == 0) {    // Erregerart
        
        CGRect bounds = self.ErregerTextLabelOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont* font = self.ErregerTextLabelOutlet.font;
        
        CGRect sizeOfText = [self.ErregerTextLabelOutlet.text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        
        
        return MAX(44, sizeOfText.size.height+zusatz_Hoehe);

    }

    if (indexPath.section == 2 && indexPath.row == 0) {    // Risikogruppe
        
        CGRect bounds = self.risikogruppenLabelOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont* font = self.risikogruppenLabelOutlet.font;
        
        CGRect sizeOfText = [self.risikogruppenLabelOutlet.text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        
        
        return MAX(44, sizeOfText.size.height+zusatz_Hoehe);
        
    }
    
    if (indexPath.section == 3 && indexPath.row == 0) {    // Übertragungsweg
      
        CGRect bounds = self.UebertragungswegTextLabelOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont* font = self.UebertragungswegTextLabelOutlet.font;
        
        CGRect sizeOfText = [self.UebertragungswegTextLabelOutlet.text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        
        
        return MAX(44, sizeOfText.size.height+zusatz_Hoehe);
        
    }
    
    if(indexPath.section ==4 && indexPath.row == 0) { //Produktempfehlungstabelle
        
        if([self.krankheitserreger.produkte count]==0)
        {
            self.produktempfehlungTableViewOutlet.hidden = YES;
            return 50;
        }
        int hoehe = (int)[self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] * 44;

//        if(hoehe==0) [self.produktempfehlungTableViewOutlet setHidden:YES];
        
        return hoehe;
    }
    
    if(indexPath.section ==5 && indexPath.row == 0) { //Checklistentabelle
        
        int hoehe = (int)[self.checklistenTableViewDelegate.checklistenArray count] * 44;
//        NSLog(@"Anzahl Checklisten: %i", [self.checklistenTableViewDelegate.checklistenArray count]);

        self.checkListenTableViewCellOutlet.backgroundColor=[UIColor clearColor];
        self.checkListenTableViewCellOutlet.contentView.backgroundColor=[UIColor clearColor];
        self.checkListenTableViewCellOutlet.layer.borderColor= [UIColor clearColor].CGColor;
        
        if(hoehe==0)
        {
          [self.checklistenTableViewOtlet setHidden:YES];

          self.checkListenTableViewCellOutlet.hidden=YES;
           
        }

        
        return hoehe;
    }
    
    return 44;//[super tableView:tableView heightForRowAtIndexPath:indexPath];
}


#pragma mark - Segue Methoden


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"produktEmpfehlungSegue"]){
        
        produktStaticTabBarViewController *controller = segue.destinationViewController;
        NSIndexPath *indexPath = [self.produktempfehlungTableViewOutlet indexPathForSelectedRow];
        Produkte *produkt= [self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray objectAtIndex:indexPath.row];
        controller.produkt = produkt;

    }else  if([segue.identifier isEqualToString:@"checklistenSegue"]){
    
        WebviewViewController *controller = segue.destinationViewController;
        NSIndexPath *indexPath =[self.checklistenTableViewOtlet indexPathForSelectedRow];
        Checkliste *checkliste = [self.checklistenTableViewDelegate.checklistenArray objectAtIndex:indexPath.row];
        
        controller.urlString= checkliste.url;
        controller.webViewTitle=@"Checkliste";
        controller.pdfFileName=@"Checkliste";
        
        
    }

}

#pragma mark - Button Action Methoden

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Hinweis zur Auflistung

-(void)hinweis:(id)sender
{
    
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:cHinweisWissenGewissenTitle andMessage:cHinweisWissenGewissen andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
    }];
    
    [AlertHelper showAlert:alert];
    
}

@end
