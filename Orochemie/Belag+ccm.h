//
//  Belag+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Belag.h"

@interface Belag (ccm)
+(Belag*) insertBelagInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteBelag;
@end
