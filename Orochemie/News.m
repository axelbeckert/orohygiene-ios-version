//
//  News.m
//  ReadWriteFormats
//
//  Created by Axel Beckert on 27.02.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import "News.h"

@implementation News

-(NSDictionary *)asDictionary
{
    return @{@"newsId" : self.newsId,  @"text1" : self.text1, @"text2" : self.text2, @"imageUrl" : self.imageUrl, @"image" : self.image};
}
@end
