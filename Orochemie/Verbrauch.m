//
//  Verbrauch.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Verbrauch.h"
#import "Reiniger.h"
#import "Reinigungsart.h"


@implementation Verbrauch

@dynamic verbrauchsmenge;
@dynamic sortierung;
@dynamic reiniger;
@dynamic reinigungsart;

@end
