//
//  ProduktDetailStaticTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProduktDetailStaticTableViewController.h"
#import "Merkliste+ccm.h"
#import "ProductToSavedMerklisteTableViewController.h"
#import "ProductToSavedMerklisteTableViewController.h"
#import "JSMLabel.h"
#import "WebviewViewController.h"
#import "Gefahrstoffsymbole.h"
#import "Merkliste+ccm.h"
#import "MerklisteTableViewController.h"
#import "MerklistenTabbarController.h"
#import "Pictogramm.h"
#import "MerklistenTableViewHelper.h"


@interface ProduktDetailStaticTableViewController () <ProductToSavedMerklisteTableViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableViewCell *staticTableViewImageAndButtonCell;
@property(strong, nonatomic) IBOutlet JSMLabel *produktBezeichnungLabelOutlet;
@property(strong, nonatomic) IBOutlet UIImageView *produktImageOutlet;
@property(strong, nonatomic) IBOutlet JSMLabel *produktEinsatzbereichOutlet;
@property(strong, nonatomic) IBOutlet JSMLabel *produktEigenschaftenOutlet;
@property(strong, nonatomic) IBOutlet JSMLabel *produktDosierungOutlet;
@property(strong, nonatomic) IBOutlet JSMLabel *produktPhWertOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *anwendbarOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *nachhaltigkeitOutlet;
@property (strong, nonatomic) IBOutlet JSMLabel *hinweiseOutlet;
@property(nonatomic, strong) AppDelegate *delegate;
@property (strong, nonatomic) IBOutlet UIButton *sicherheitsDatenblattButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *betriebsanweisungButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *produktInformationButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *ecoLabelButton;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffsymbolEinsOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffsymbolZweiOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffsymbolDreiOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *gefahrstoffImageVierOutlet;
@property (nonatomic,strong)NSString *merklistenInfoScreen;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewThree;


@end

@implementation ProduktDetailStaticTableViewController

NSInteger zusatzHoeheReiniger = 30;

#pragma mark View Lifecycle
- (void)viewDidLoad {
  [super viewDidLoad];

    [oroThemeManager customizeReinigerTableView:self.tableView];
    [oroThemeManager customizeButtonImage:self.sicherheitsDatenblattButtonOutlet];
    [oroThemeManager customizeButtonImage:self.betriebsanweisungButtonOutlet];
    [oroThemeManager customizeButtonImage:self.produktInformationButtonOutlet];
    [oroThemeManager customizeButtonImage:self.ecoLabelButton];
    self.staticTableViewImageAndButtonCell.backgroundColor = [UIColor clearColor];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.contentInset = inset;
    
  self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    NSArray *barButtonArray = [[NSArray alloc]initWithObjects:[oroThemeManager homeBarButton:self],[oroThemeManager addToMerklisteBarButton:self], nil];
    self.tabBarController.navigationItem.rightBarButtonItems = barButtonArray;
    

  if (self.reiniger) {
      
      NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortierung"
                                                                     ascending:YES];
      
      NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
      NSArray *gefahrstoffsymbole =[[NSArray arrayWithArray:[self.reiniger.gefahrstoffsymbole allObjects]] sortedArrayUsingDescriptors:sortDescriptors];

      if([gefahrstoffsymbole count]==4)
      {
          Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
          self.gefahrstoffsymbolEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
          
          Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
          self.gefahrstoffsymbolZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
          
          Gefahrstoffsymbole *symbol3 = [gefahrstoffsymbole objectAtIndex:2];
          self.gefahrstoffsymbolDreiOutlet.image = [UIImage imageNamed:symbol3.bild];
          
          Gefahrstoffsymbole *symbol4 = [gefahrstoffsymbole objectAtIndex:3];
          self.gefahrstoffImageVierOutlet.image = [UIImage imageNamed:symbol4.bild];
          
          
      } else if([gefahrstoffsymbole count]==3)
      {
          Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
          self.gefahrstoffsymbolEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
          
          Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
          self.gefahrstoffsymbolZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
          
          Gefahrstoffsymbole *symbol3 = [gefahrstoffsymbole objectAtIndex:2];
          self.gefahrstoffsymbolDreiOutlet.image = [UIImage imageNamed:symbol3.bild];
          
          
      }else if([gefahrstoffsymbole count]==2)
      {
          Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
          self.gefahrstoffsymbolEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
          
          Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
          self.gefahrstoffsymbolZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
          
          
      } else if ([gefahrstoffsymbole count]==1)
      {
          Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
          self.gefahrstoffsymbolEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
      }
      
      //Anzeige der Pictogramme
      
      NSArray *pictrogramme = [self.reiniger.pictogramm allObjects];
      
      if([pictrogramme count]==1){
          Pictogramm *pg = [pictrogramme objectAtIndex:0];
          self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
      
      } else if ([pictrogramme count]==2){
          Pictogramm *pg = [pictrogramme objectAtIndex:0];
          self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
          
          Pictogramm *pg2 = [pictrogramme objectAtIndex:1];
          self.pictogrammImageViewTwo.image = [UIImage imageNamed:pg2.bild];
      
      } else if([pictrogramme count]==3){
          Pictogramm *pg = [pictrogramme objectAtIndex:0];
          self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
          
          Pictogramm *pg2 = [pictrogramme objectAtIndex:1];
          self.pictogrammImageViewTwo.image = [UIImage imageNamed:pg2.bild];
          
          Pictogramm *pg3 = [pictrogramme objectAtIndex:2];
          self.pictogrammImageViewThree.image = [UIImage imageNamed:pg3.bild];
      }
        self.produktBezeichnungLabelOutlet.text =
            [NSString stringWithFormat:@"%@ - %@", self.reiniger.bezeichnung,
                                       self.reiniger.zusatz];
        self.produktBezeichnungLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.produktImageOutlet.image = [UIImage imageNamed:self.reiniger.image];
        self.produktEinsatzbereichOutlet.text = self.reiniger.einsatzbereich;
        self.produktEinsatzbereichOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.produktEigenschaftenOutlet.text = self.reiniger.eigenschaften;
        self.produktEigenschaftenOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.produktDosierungOutlet.text = self.reiniger.dosierung;
        self.produktDosierungOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.produktPhWertOutlet.text = self.reiniger.phwert;
        self.produktPhWertOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.anwendbarOutlet.text = self.reiniger.anwendbar;
        self.anwendbarOutlet.verticalAlignment = JSMLabelAlignmentTop;
        self.nachhaltigkeitOutlet.text=self.reiniger.nachhaltigkeit;
        self.nachhaltigkeitOutlet.verticalAlignment=JSMLabelAlignmentTop;
        self.hinweiseOutlet.text = self.reiniger.hinweise;
        self.hinweiseOutlet.verticalAlignment=JSMLabelAlignmentTop;

     
      id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
       

      [tracker set:kGAIScreenName value:@"Reiniger-Produkt-Detail-Information"];
      [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Reiniger"
                                                            action:@"Show"
                                                             label:self.reiniger.bezeichnung
                                                             value:nil] build]];

      //Prüfen ob der InfoScreen angezeigt werden soll
      NSUserDefaults *merklistenInfoScreenDefaults = [NSUserDefaults standardUserDefaults];
      
      // holen der Info
      self.merklistenInfoScreen = [merklistenInfoScreenDefaults valueForKey:@"merklistenInfoScreen"];

  }
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.tabBarController.title =@"Information";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Produkt-Detail-Information"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    if(![self.merklistenInfoScreen isEqualToString:@"NO"]){
        [self performSegueWithIdentifier:@"infoScreenSegue" sender:self];
        
        //Setzen der Anzeige des InfoScreen auf NO
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:@"NO" forKey:@"merklistenInfoScreen"];
        [prefs synchronize];
        
        self.merklistenInfoScreen=@"NO";
        
    }
    
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - TableView Definitons

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == 0 &&
      indexPath.row == 0) { // Produktbezeichnung Label

    CGRect bounds = self.produktBezeichnungLabelOutlet.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    bounds.size = constraintSize;
    UIFont *font = self.produktBezeichnungLabelOutlet.font;

    CGRect sizeOfText = [self.produktBezeichnungLabelOutlet.text
        boundingRectWithSize:constraintSize
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:@{
                               NSFontAttributeName : font
                             }
                     context:nil];

    return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
  }

  if (indexPath.section == 1 && indexPath.row == 0) { // Bild

      if ([UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPad) {
          if ([self.reiniger.ecoLabel isEqualToNumber:@(YES)]){
              
              return 190;
          }
      return 144;//144
    
      } else {
          

        if ([self.reiniger.ecoLabel isEqualToNumber:@(NO)]){
            self.ecoLabelButton.hidden=YES;
                       return 145;
        }
        return 191;
    }
  }

  if (indexPath.section == 2 && indexPath.row == 0) { // Einsatzbereich TextView
    CGRect bounds = self.produktEinsatzbereichOutlet.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    UIFont *font = self.produktEinsatzbereichOutlet.font;

    CGRect sizeOfText = [self.produktEinsatzbereichOutlet.text
        boundingRectWithSize:constraintSize
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:@{
                               NSFontAttributeName : font
                             }
                     context:nil];

    return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
  }
  
  if (indexPath.section == 2 && indexPath.row == 1) {    // Einsatzbereich Pictogramme
        
        return 50;
  }
    
  if (indexPath.section == 3 && indexPath.row == 0) { // Anwendbar TextView
        CGRect bounds = self.anwendbarOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        UIFont *font = self.anwendbarOutlet.font;
        CGRect sizeOfText = [self.anwendbarOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
    }
    
  if (indexPath.section == 4 && indexPath.row == 0) { // Eigenschaften TextView
    CGRect bounds = self.produktEigenschaftenOutlet.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    UIFont *font = self.produktEigenschaftenOutlet.font;
    CGRect sizeOfText = [self.produktEigenschaftenOutlet.text
        boundingRectWithSize:constraintSize
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:@{
                               NSFontAttributeName : font
                             }
                     context:nil];
    //NSLog(@"Höhe Eigenschaften: %f", sizeOfText.size.height + zusatzHoeheReiniger);
    return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
  }
    


  if (indexPath.section == 5 && indexPath.row == 0) { // Dosierung TextView
    CGRect bounds = self.produktDosierungOutlet.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    UIFont *font = self.produktDosierungOutlet.font;
    CGRect sizeOfText = [self.produktDosierungOutlet.text
        boundingRectWithSize:constraintSize
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:@{
                               NSFontAttributeName : font
                             }
                     context:nil];

    return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
  }

  if (indexPath.section == 6 && indexPath.row == 0) { // pH-Wert TextView
    CGRect bounds = self.produktPhWertOutlet.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    UIFont *font = self.produktPhWertOutlet.font;
    CGRect sizeOfText = [self.produktPhWertOutlet.text
        boundingRectWithSize:constraintSize
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:@{
                               NSFontAttributeName : font
                             }
                     context:nil];

    return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
  }
    


    if (indexPath.section == 7 && indexPath.row == 0) { // Nachhaltigkeit TextView
        CGRect bounds = self.nachhaltigkeitOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        UIFont *font = self.nachhaltigkeitOutlet.font;
        CGRect sizeOfText = [self.nachhaltigkeitOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
    }
    
    if (indexPath.section == 8 && indexPath.row == 0) { // Hinweise TextView
        CGRect bounds = self.hinweiseOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        UIFont *font = self.hinweiseOutlet.font;
        CGRect sizeOfText = [self.hinweiseOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zusatzHoeheReiniger);
    }

  if (indexPath.section == 9 && indexPath.row == 0) { // Gefahrstoffsymbole

    return 80;
  }


  return 23;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(section == 1)
    {
        return nil;
    }
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Produktbezeichnung";
    }  else if (section == 2)
    {
        label.text=@"Einsatzbereich";
    } else if (section == 3)
    {
        label.text=@"Anwendung";
    }else if (section == 4)
    {
        label.text=@"Eigenschaften";
    } else if (section == 5)
    {
        if([self.reiniger.konzentrat  isEqual: @(YES)]){
        label.text=@"Konzentrat: empfohlene Dosierung";
        } else {
            label.text=@"Gebrauchsfertig";

        }
    } else if (section == 6)
    {
        label.text=@"pH-Wert";
    }  else if (section == 7)
    {
        label.text=@"Nachhaltigkeit";
    } else if (section == 8)
    {
        label.text=@"Hinweise";
    } else if (section == 9)
    {
        label.text=@"Gefahrstoffsymbole";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 0;
    }
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if([identifier isEqualToString:@"infoScreenSegue"] && [self.merklistenInfoScreen isEqualToString:@"NO"]){
        return NO;
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
   
    if([segue.identifier isEqualToString:@"infoScreenSegue"]){
        self.navigationController.modalPresentationStyle=UIModalPresentationCurrentContext;
    }
    
    
    if([segue.identifier isEqualToString:@"sicherheitsdatenblattSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Sicherheitsdatenblatt";
        controller.pdfFileName=[[NSString alloc]initWithFormat:@"datenblatt_%@.pdf",self.reiniger.informationPDF];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/datenblatt_%@.pdf",self.reiniger.informationPDF];
        controller.webViewToolbar=HideToolBar;
        //NSLog(@"datenbaltt: %@", self.reiniger.informationPDF);
        
        
        
    } else if ([segue.identifier isEqualToString:@"betriebsanweisungSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Betriebsanweisung";
        controller.pdfFileName=[NSString stringWithFormat:@"betriebsanweisung_%@.pdf",self.reiniger.informationPDF];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/betriebsanweisung_%@.pdf",self.reiniger.informationPDF];
        
        
         controller.webViewToolbar=HideToolBar;
        
        
    } else if ([segue.identifier isEqualToString:@"produktinformationSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Produktinformation";
        controller.pdfFileName=[[NSString alloc]initWithFormat:@"produktinformation_%@.pdf",self.reiniger.informationPDF];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/produktinformation_%@.pdf",self.reiniger.informationPDF];

        
        controller.webViewToolbar=HideToolBar;
        
        
    } else if ([segue.identifier isEqualToString:@"ecolabelSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Ecolabel Zertifikat";
        controller.pdfFileName=[[NSString alloc]initWithFormat:@"ecolabel_%@.pdf",self.reiniger.informationPDF];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/%@.pdf",self.reiniger.ecolabelPDF];
        
        
        controller.webViewToolbar=HideToolBar;
        
        
    }
}

#pragma mark - Merkliste Button
- (UIBarButtonItem *)addToMerklisteButton:(id)target {


UIBarButtonItem *addToMerklisteButton = [[UIBarButtonItem alloc]
                         initWithImage:[UIImage imageNamed:@"112-group.png"]
                        style:UIBarButtonItemStylePlain
                         target:self
                         action:@selector(addToMerkliste)];
    
return addToMerklisteButton;
}

- (void)addToMerkliste {

    UIAlertController *actionSheet = [AlertHelper getUIActionSheetWithTitle:@"Merklisten" andMessage:@"Produkt Speichern auf:" andCancelButtonWithCompletionHandler:nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        CGRect sourceRect = CGRectZero;
        sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
        sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
        
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = sourceRect;
        [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"aktuelle Merkliste" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self saveProduktToAktuelleMerkliste];
    }]];
    
    
    if([MerklistenTableViewHelper testDatabaseWithDelegate:self.delegate]){
        NSLog(@"Es sind Merklisten vorhanden");
    } else {
        NSLog(@"Es sind keine Merklisten vorhanden");
    }
    
    //Prüfen ob bereits mehr als eine Merkliste vorhanden ist (Basismerkliste ist immer vorhanden)
    //Fetchen der Merklisten
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];    
    NSArray *merklistenArray = [self.delegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    //Wenn mehr als eine Mekrliste vorhanden ist dann bitte den Button dem ActionSheet hinzufügen
    if(merklistenArray.count > 1){
        [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"bereits vorhandene Merkliste" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self saveProduktToSavedMerkliste];
        }]];
    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Merklisten anzeigen" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self showMerklisten];
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
  
}

-(void) showMerklisten {
    
    MerklistenTabbarController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
    controller.showMerklistenTabBarControllerFrom= ReinigerChannel;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)saveProduktToSavedMerkliste {
    
    ProductToSavedMerklisteTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductToSavedMerklisteTableViewController"];
    controller.reiniger = self.reiniger;
    controller.delegate = self;
    controller.showProductToSavedMerklisteFrom = ReinigerChannel;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)saveProduktToAktuelleMerkliste {
    
    [self.delegate.merkliste addReinigerObject:self.reiniger];
    [self.delegate saveContext];
    [self showInfoAlertWithMessage:@"Produkt wurde der Merkliste hinzugefügt"];
    [self.delegate saveMerklistenDataToMerklistenDB];
    
}

-(void) showInfoAlertWithMessage: (NSString*)message {
    
    UIAlertController *alert = [AlertHelper showInfoAlertWithMessage:message];
    [self presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(dismissSaveIsDoneInfoView:) withObject:alert afterDelay:4];
}

-(void)dismissSaveIsDoneInfoView:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ProductSavedToMerkliste Delegate Methoden
-(void) informUserAboutProductSavedToMerkliste:(Merkliste*)merkliste{
    
    [self showInfoAlertWithMessage:[NSString stringWithFormat:@"Das Produkt wurde zur Merkliste %@ hinzugefügt", merkliste.bezeichnung]];

    [self.delegate saveMerklistenDataToMerklistenDB];

}

-(void)logButtonPress:(UIButton *)button{
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    
//    [tracker set:kGAIScreenName value:@"ProduktDetailStaticTableViewController"];
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
//                                                          action:@"touch"
//                                                           label:[button.titleLabel text]
//                                                           value:nil] build]];
//    [tracker set:kGAIScreenName value:nil];
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
