//
//  produktStaticTabBarViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Produkte;



@interface produktStaticTabBarViewController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic, strong) Produkte *produkt;
@property(nonatomic, assign) id<UITabBarControllerDelegate> delegate;
@property (nonatomic,assign) BOOL videoPlayerIsFullscreen;
@property(nonatomic,strong) NSString *branchenName;
@property(nonatomic,strong) NSString *anwendungName;

@end
