//
//  DosierungTableViewProductInfoDatasourceAndDelegate.h
//  Orochemie
//
//  Created by Axel Beckert on 07.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DosierungTableViewProductInfoDatasourceAndDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray *produktDosierungArray;
@end
