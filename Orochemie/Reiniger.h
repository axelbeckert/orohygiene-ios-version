//
//  Reiniger.h
//  orochemie
//
//  Created by Axel Beckert on 01.03.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Anwendung, Belag, Gefahrstoffsymbole, Merkliste, Oberflaeche, Phwert, Produktvarianten, Reinigungsverfahren, Verbrauch, Video;

@interface Reiniger : NSManagedObject

@property (nonatomic, retain) NSString * anwendbar;
@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSString * dosierung;
@property (nonatomic, retain) NSNumber * ecoLabel;
@property (nonatomic, retain) NSString * ecolabelPDF;
@property (nonatomic, retain) NSString * eigenschaften;
@property (nonatomic, retain) NSString * einsatzbereich;
@property (nonatomic, retain) NSString * hinweise;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * informationPDF;
@property (nonatomic, retain) NSNumber * konzentrat;
@property (nonatomic, retain) NSString * mindestdosierung;
@property (nonatomic, retain) NSString * nachhaltigkeit;
@property (nonatomic, retain) NSString * phwert;
@property (nonatomic, retain) NSNumber * sortierreihenfolge;
@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * zusatz;
@property (nonatomic, retain) NSSet *anwendung;
@property (nonatomic, retain) NSSet *belag;
@property (nonatomic, retain) NSSet *gefahrstoffsymbole;
@property (nonatomic, retain) NSSet *merkliste;
@property (nonatomic, retain) NSSet *oberflaeche;
@property (nonatomic, retain) NSSet *phwert_relation;
@property (nonatomic, retain) NSSet *produktvarianten;
@property (nonatomic, retain) NSSet *reinigungsmethode;
@property (nonatomic, retain) NSSet *verbrauch;
@property (nonatomic, retain) NSSet *video;
@property (nonatomic, retain) NSSet *pictogramm;
@end

@interface Reiniger (CoreDataGeneratedAccessors)

- (void)addAnwendungObject:(Anwendung *)value;
- (void)removeAnwendungObject:(Anwendung *)value;
- (void)addAnwendung:(NSSet *)values;
- (void)removeAnwendung:(NSSet *)values;

- (void)addBelagObject:(Belag *)value;
- (void)removeBelagObject:(Belag *)value;
- (void)addBelag:(NSSet *)values;
- (void)removeBelag:(NSSet *)values;

- (void)addGefahrstoffsymboleObject:(Gefahrstoffsymbole *)value;
- (void)removeGefahrstoffsymboleObject:(Gefahrstoffsymbole *)value;
- (void)addGefahrstoffsymbole:(NSSet *)values;
- (void)removeGefahrstoffsymbole:(NSSet *)values;

- (void)addMerklisteObject:(Merkliste *)value;
- (void)removeMerklisteObject:(Merkliste *)value;
- (void)addMerkliste:(NSSet *)values;
- (void)removeMerkliste:(NSSet *)values;

- (void)addOberflaecheObject:(Oberflaeche *)value;
- (void)removeOberflaecheObject:(Oberflaeche *)value;
- (void)addOberflaeche:(NSSet *)values;
- (void)removeOberflaeche:(NSSet *)values;

- (void)addPhwert_relationObject:(Phwert *)value;
- (void)removePhwert_relationObject:(Phwert *)value;
- (void)addPhwert_relation:(NSSet *)values;
- (void)removePhwert_relation:(NSSet *)values;

- (void)addProduktvariantenObject:(Produktvarianten *)value;
- (void)removeProduktvariantenObject:(Produktvarianten *)value;
- (void)addProduktvarianten:(NSSet *)values;
- (void)removeProduktvarianten:(NSSet *)values;

- (void)addReinigungsmethodeObject:(Reinigungsverfahren *)value;
- (void)removeReinigungsmethodeObject:(Reinigungsverfahren *)value;
- (void)addReinigungsmethode:(NSSet *)values;
- (void)removeReinigungsmethode:(NSSet *)values;

- (void)addVerbrauchObject:(Verbrauch *)value;
- (void)removeVerbrauchObject:(Verbrauch *)value;
- (void)addVerbrauch:(NSSet *)values;
- (void)removeVerbrauch:(NSSet *)values;

- (void)addVideoObject:(Video *)value;
- (void)removeVideoObject:(Video *)value;
- (void)addVideo:(NSSet *)values;
- (void)removeVideo:(NSSet *)values;

- (void)addPictogrammObject:(NSManagedObject *)value;
- (void)removePictogrammObject:(NSManagedObject *)value;
- (void)addPictogramm:(NSSet *)values;
- (void)removePictogramm:(NSSet *)values;

@end
