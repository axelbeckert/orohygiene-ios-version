//
//  KontaktTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "KontaktTableViewController.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>




@interface KontaktTableViewController () 

@end

@implementation KontaktTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.showKontaktFrom == DesinfektionChannel){
        //NSLog(@"Kontakt vom Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];
        
        
    } else if(self.showKontaktFrom == ReinigerChannel){
        
        //NSLog(@"Kontakt vom von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
        
    self.title=@"orochemie";
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 48.8512;
    coordinate.longitude = 9.17287;
    
    MKUserLocation *userLocation = [[MKUserLocation alloc]init];
    userLocation.coordinate = coordinate;
    
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (
                                        coordinate, 20000, 20000);
    
    
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:@"Max Plank Straße 27, 70806 Kornwestheim" completionHandler:^(NSArray *placemarks, NSError *error) {
        MKPlacemark* placemark = [placemarks lastObject];
        [self.mapViewOutlet setCenterCoordinate:placemark.location.coordinate];
        
        
        MyAnnotaion* meineStecknadel = [[MyAnnotaion alloc] init];
        meineStecknadel.title = @"orochemie !";
        
        meineStecknadel.subtitle =@"Max Plank Str. 27, 70806 Kornwestheim";
        
        
        meineStecknadel.coordinate = placemark.location.coordinate;
        
        self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];

        
        //[self.mapViewOutlet addAnnotation:meineStecknadel];
        //[self.mapViewOutlet selectAnnotation:meineStecknadel animated:YES];
        
        [self.mapViewOutlet showAnnotations:@[meineStecknadel] animated:NO];
        [self.mapViewOutlet selectAnnotation:meineStecknadel animated:YES];
        
        [self.mapViewOutlet setRegion:region animated:YES];
        
    }];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Kontakt-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)moveCameraToCoordinate: (CLLocationCoordinate2D)coordinate
{
    // 1
    MKMapCamera *camera =
    [MKMapCamera cameraLookingAtCenterCoordinate:coordinate
                               fromEyeCoordinate:coordinate
                                     eyeAltitude:1000.0];
    // 2
    camera.pitch = 55.0f;
    // 3
    [UIView animateWithDuration:1.0
                     animations:^{
                         // 4
                         self.mapViewOutlet.camera = camera;
                     }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"orochemie Adresse & Anfahrt";
    }
    
    if(section==1)
    {
        label.text = @"orochemie Map";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

@end
