//
//  Verbrauch+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Verbrauch.h"

@interface Verbrauch (ccm)
+(Verbrauch*) insertVerbrauchInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteVerbrauch;
@end
