//
//  ImpressumTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 04.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ImpressumTableViewController.h"

@interface ImpressumTableViewController ()

@end

@implementation ImpressumTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.showImpressumFrom == DesinfektionChannel){
        //NSLog(@"Impressum vom Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];
        
        
    } else if(self.showImpressumFrom == ReinigerChannel){
        
        //NSLog(@"Impressum vom von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    self.tableView.estimatedRowHeight = 80.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self setTextForImpressumTextViews];
    [self setLinkTextAppereanceOnTextViews];
    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
      self.title=@"orochemie";


}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Impressum-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setTextForImpressumTextViews{
    
    [self.impressum setText:impressum];
    [self.inhalteUnsrerWebsite setText:inhalteUnsrerWebsite];
    [self.linksZuExternenWebsites setText:linksZuExternenWebsites];
    [self.urheberrecht setText:urheberRecht];
    [self.spamNachrichten setText:spamNachrichten];
    
}

-(void)setLinkTextAppereanceOnTextViews{
    
    [self.impressum setTintColor:[UIColor blueColor]];
    [self.inhalteUnsrerWebsite setTintColor:[UIColor blueColor]];
    [self.linksZuExternenWebsites setTintColor:[UIColor blueColor]];
    [self.urheberrecht setTintColor:[UIColor blueColor]];
    [self.spamNachrichten setTintColor:[UIColor blueColor]];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    
    if (section == 0)
    {
        label.text = @"Impressum";
    }
    
    if (section == 1)
    {
        label.text = @"1. Inhalte unserer Webseites";

    }
    
    if (section == 2)
    {
        label.text = @"2. Links zu externen Websites";
    }
    
    if (section == 3)
    {
        label.text = @"3. Urheberrecht";
    }
    
    if (section == 4)
    {
        label.text = @"4. Spam-Nachrichten";
    }
    
    return container;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

@end
