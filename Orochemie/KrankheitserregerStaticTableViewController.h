//
//  VirusStaticTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JSMLabel;

@interface KrankheitserregerStaticTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet JSMLabel *NameTextLabelOutlet;

@property (strong, nonatomic) IBOutlet JSMLabel *ErregerTextLabelOutlet;


@property (strong, nonatomic) IBOutlet JSMLabel *UebertragungswegTextLabelOutlet;

@property (strong, nonatomic) IBOutlet JSMLabel *ProduktEmpfehlungTextLabelOutlet;


@property (nonatomic, strong) Krankheitserreger *krankheitserreger;


@property (strong, nonatomic) IBOutlet UITableViewCell *checkListenTableViewCellOutlet;



@end
