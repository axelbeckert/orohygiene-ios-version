//
//  Constants.h
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

enum Show{
    ReinigerChannel,
    DesinfektionChannel
    
};
typedef enum Show show;

extern NSString *const cTextInputPath; //Pfad und Dateiname für den Textinput
extern NSString *const cEntityProdukte; //String für Produkte
extern NSString *const cEntityProdukteName; //String für Produktname
extern NSString *const cEntityBranche; //String für Produkt_Branche
extern NSString *const cEntityBrancheName; //String für Produkt_Branche_Name
extern NSString *const cEntityAnwendung; //String für Produkt_Anwenung
extern NSString *const cEntityAnwendungName; //String für Produkt_Anwendung_Name
extern NSString *const cHandlungsSenderContoller; //String für senderController von HandlungsanweisungStaticTableViewContoller
extern NSString *const cProduktSenderContoller; //String für senderController von ProduktStaticTableViewContoller
extern NSString *const cSicherheitSenderContoller; //String für senderController von ProduktStaticTableViewContoller
extern NSString *const cA20Instrumentendesinfektion;
extern NSString *const cA20InstrumentendesinfektionUntertitel;
extern NSString *const cB5WischdesinfektionVAH;
extern NSString *const cB5WischdesinfektionVAHUntertitel;
extern NSString *const cB5WischdesinfektionRKI;
extern NSString *const cB5WischdesinfektionRKIUntertitel;
extern NSString *const cB10Wischdesinfektion;
extern NSString *const cB10WischdesinfektionUntertitel;
extern NSString *const cB15Wischdesinfektion;
extern NSString *const cB15WischdesinfektionUntertitel;
extern NSString *const cB20Wischdesinfektion;
extern NSString *const cB20WischdesinfektionUntertitel;
extern NSString *const cB25Wischdesinfektion;
extern NSString *const cB25WischdesinfektionUntertitel;
extern NSString *const cD10Absauggeraetedesinfektion;
extern NSString *const cD10AbsauggeraetedesinfektionUntertitel;
extern NSString *const cWebsiteFuerBebilderteAnwendungshinweise;
extern NSString *const cWebsiteTitleFuerBebilderteAnwendungshinweise;
extern NSString *const cWebsitePDFTitleFuerBebilderteAnwendungshinweise;
extern NSString *const cWebsiteFuerHygieneChecklisten;
extern NSString *const cWebsiteTitleFuerHygieneChecklisten;
extern NSString *const cWebsitePDFTitleFuerHygieneChecklisten;
extern NSString *const cWebsiteFuerHygieneTage;
extern NSString *const cWebsiteTitleFuerHygieneTage;
extern NSString *const cWebsitePDFTitleFuerHygieneTage;
extern NSString *const cWebsiteFuerBestellung;
extern NSString *const cWebsiteTitleFuerBestellung;
extern NSString *const cWebsitePDFTitleFuerBestellung;
extern NSString *const cWebsiteFuerHygieneWissen;
extern NSString *const cWebsiteTitleFuerHygieneWissen;
extern NSString *const cWebsitePDFTitleFuerHygieneWissen;
extern NSString *const cWebsiteFuerHygieneSystem;
extern NSString *const cWebsiteTitleFuerHygieneSystem;
extern NSString *const cWebsitePDFTitleFuerHygieneSystem;
extern NSString *const urlStringWebsiteFlyer;
extern NSString *const urlStringFlyerPlan;
extern NSString *const urlStringModelPlan;
extern NSString *const urlModelPlan;
extern NSString *const urlFlyerPlan;
extern NSString *const urlKrankheitserreger;
extern NSString *const urlKrankheitserregerTstamp;
extern NSString *const urlOberflaechen;
extern NSString *const urlOberflaechenTstamp;
extern NSTimeInterval *const cDisplayTimeInfoAlerts;
extern NSString *const urlWirksamkeiten;
extern NSString *const cHinweisWissenGewissen;
extern NSString *const cHinweisWissenGewissenButton;
extern NSString *const cHinweisWissenGewissenTitle;

//Datenschutz
extern NSString *const allgemeierHinweisZumDatenschutz;
extern NSString *const datenschutzBeauftragter;
extern NSString *const voraussetzungDatenverarbeitung;
extern NSString *const datenschutzUndWebsitesDritter;
extern NSString *const datensicherheit;
extern NSString *const detailsZurDatenverarbeitung;
extern NSString* const detailsZurDatenverarbeitung2;
extern NSString* const betroffenenerechte;
extern NSString* const widerspruchsrecht;
extern NSString* const aktualisierungDatenschutz;

//Impressum
extern NSString* const impressum;
extern NSString* const inhalteUnsrerWebsite;
extern NSString* const linksZuExternenWebsites;
extern NSString* const urheberRecht;
extern NSString* const spamNachrichten;

@interface Constants : NSObject

@end
