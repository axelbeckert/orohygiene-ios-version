//
//  ProdukteViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 12.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProdukteViewController.h"

@interface ProdukteViewController ()

@end

@implementation ProdukteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
