//
//  ProduktempfehlungTableViewControllerDatasourceandDelegate.m
//  Orochemie
//
//  Created by Axel Beckert on 22.04.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktempfehlungTableViewControllerDatasourceandDelegate.h"
#import "Produkte.h"


@interface ProduktempfehlungTableViewControllerDatasourceandDelegate()<UIGestureRecognizerDelegate>

@end


@implementation ProduktempfehlungTableViewControllerDatasourceandDelegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.produktEmpfehlungArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Produkte *produkt = [self.produktEmpfehlungArray objectAtIndex:indexPath.row];
    cell.textLabel.text = produkt.name;
    cell.detailTextLabel.text = produkt.zusatz;
    cell.imageView.image= [UIImage imageNamed:produkt.bild];
    return cell;
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"scrollViewWillBeginDragging");
}


@end
