//
//  Gefahrstoffsymbole.m
//  orochemie
//
//  Created by Axel Beckert on 19.05.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Gefahrstoffsymbole.h"
#import "Produkte.h"
#import "Reiniger.h"


@implementation Gefahrstoffsymbole

@dynamic bild;
@dynamic name;
@dynamic sortierung;
@dynamic produkte;
@dynamic reiniger;

@end
