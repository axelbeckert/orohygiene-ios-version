//
//  oroTheme2.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "oroTheme2.h"

@implementation oroTheme2

-(UIImage*)backgroundImage
{
    
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }

//Returns the following figures:
//     0 for standard resolution iPhone/iPod touch,
//     1 for retina iPhone,
//     2 for standard resolution iPad,
//     3 for retina iPad
   

    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        //NSLog(@"Iphone 5 Auflösung");
        return [UIImage imageNamed:@"background_v2_1136.png"]; //Iphone 5 background_oro_theme2_1136.png

    }
    if (d==2)
    
    {
        //NSLog(@"Ipad Auflösung");
        return [UIImage imageNamed:@"background_oro_ipad_theme2_768_v2.png"]; //Ipad
    } else if (d==3)
        
    {
        //NSLog(@"Ipad Retina Auflösung");
        return [UIImage imageNamed:@"background_oro_ipad_theme2_768_v2.png"];//IPad Retina

    }

    //NSLog(@"Iphone 4 & IPhone 4 Retina Auflösung");
    return  [UIImage imageNamed:@"background_v2.png"]; //background_oro_theme2.png

}


-(UIImage*)backgroundImageForCleanerChanel
{
    
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        //NSLog(@"Iphone 5 Auflösung");
        return [UIImage imageNamed:@"background_cleaners_1136.png"]; //Iphone 5 background_oro_theme2_1136.png
        
    }
    if (d==2)
        
    {
        //NSLog(@"Ipad Auflösung");
        return [UIImage imageNamed:@"background_cleaners_ipad.png"]; //Ipad
    } else if (d==3)
        
    {
        //NSLog(@"Ipad Retina Auflösung");
        return [UIImage imageNamed:@"background_cleaners_ipad.png"];//IPad Retina
        
    }
    
    //NSLog(@"Iphone 4 & IPhone 4 Retina Auflösung");
    return  [UIImage imageNamed:@"background_cleaners.png"]; //background_oro_theme2.png
    
}

-(UIImage*)backgroundImageForSelectChanel
{
    
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        //NSLog(@"Iphone 5 Auflösung");
        return [UIImage imageNamed:@"background_select_1136.png"]; //Iphone 5 background_oro_theme2_1136.png
        
    }
    if (d==2)
        
    {
        //NSLog(@"Ipad Auflösung");
        return [UIImage imageNamed:@"background_select_ipad.png"]; //Ipad
    } else if (d==3)
        
    {
        //NSLog(@"Ipad Retina Auflösung");
        return [UIImage imageNamed:@"background_select_ipad.png"];//IPad Retina
        
    }
    
    //NSLog(@"Iphone 4 & IPhone 4 Retina Auflösung");
    return  [UIImage imageNamed:@"background_select.png"]; //background_oro_theme2.png
    
}

-(UIColor*)accessoryColor
{
    return [UIColor whiteColor];
}

-(UIColor*)tableCellTextColor
{
    return [UIColor whiteColor];
}

-(UIImage*)tableViewBackgroundPictureForHeaderView
{
    return [UIImage imageNamed:@"headline_3d.png"];
}

-(UIColor*)tableViewTextColorForHeaderView
{
    return [UIColor whiteColor];
}

-(UIFont*)tableViewFontForHeaderView
{
    return [UIFont boldSystemFontOfSize:14];
}

-(UIFont*)tableViewFontForDatenschutzHeaderView
{
    return [UIFont boldSystemFontOfSize:13];
}

-(CGFloat)tableViewHeightForHeaderView
{
    return 33;
}


-(UIImage*)imageforNavigationBar
{
    return [UIImage imageNamed:@"navbar.png"];
}


-(UIImage*)buttonBackgroundImage;
{
    return [UIImage imageNamed:@"button_3d.png"];
}

-(UIImage*)imageForButtonNormal
{
    return [UIImage imageNamed:@"button_3d.png"];
    
}

-(UIImage*)imageForButtonHighlited
{
    return [UIImage imageNamed:@"button_3d.png"];
}


-(UIColor*)labelTextColor
{
    return [UIColor whiteColor];
}

-(CGFloat)staticTableViewHeightForHeaderView
{
    return 40;
}

-(UIColor*)colorForLabelInTitleView
{
    return [UIColor whiteColor];
}

-(UIFont*)fontForLabelInTitleView
{
    return [UIFont boldSystemFontOfSize:17];
}
@end
