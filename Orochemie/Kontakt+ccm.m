//
//  Kontakt+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Kontakt+ccm.h"

@implementation Kontakt (ccm)
+(Kontakt*) insertKontaktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Kontakt" inManagedObjectContext:managedObjectContext];
}
-(void)deleteKontakt
{
    [self.managedObjectContext deleteObject:self];
}
@end
