//
//  MakeData.h
//  Orochemie
//
//  Created by Axel Beckert on 13.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MakeData : NSObject
-(void)makeData;
-(void)readCSVFileForGettingBarcodes;
- (NSString *)createUUID;
@end
