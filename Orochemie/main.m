 //
//  main.m
//  Orochemie
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    
    @autoreleasepool {
        @try {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }
        @catch (NSException *exception) {
//            NSLog(@"Uncaught exception: %@", exception.name);
//            NSLog(@"reason: %@", exception.reason);
//            NSLog(@"userInfo: %@", exception.userInfo);
//            NSLog(@"Stack trace: %@", [exception callStackSymbols]);
            @throw exception; //forward exception
        }
    }
}
