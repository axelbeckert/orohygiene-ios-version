//
//  KrankheitserregerCollector.m
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "KrankheitserregerCollector.h"
#import "AppDelegate.h"
#import "KrankheitserregerTableViewController.h"
#import "AddKrankheitserregerToDataBase.h"






@interface KrankheitserregerCollector () < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >
@property(nonatomic,strong) AppDelegate *appDelegate;
@property int collectKrankheitserregerIsActive;
@property int collectTstamp;
@end

@implementation KrankheitserregerCollector

NSString *const collectKrankheitserreger=@"collectKrankheitserreger";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.entityName = @"Krankheitserreger";
        self.collectorType = ONLY_TSTAMP;
    }
    return self;
}

-(void) startCollectingKrankheitserreger{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self collectKrankheisterreger];
    
}


-(void) collectKrankheisterreger{
    if (self.collectorType==ONLY_TSTAMP){
        
        [self collectJsonWithUrl:urlKrankheitserregerTstamp andTaskDescription:collectKrankheitserreger];
        
    } else if(self.collectorType==COMPLETE_OBJECTS){
        
        [self collectJsonWithUrl:urlKrankheitserreger andTaskDescription:collectKrankheitserreger];
        
    } else if (self.collectorType==FROM_FILE){
       
        NSDictionary* jsonContents = [self retrieveDataFromJsonFile:self.entityName];

        self.collectorType = COMPLETE_OBJECTS;
        [self checkDownloadTaskDescription:collectKrankheitserreger andBuildChecklistsWithData:(NSArray*)jsonContents];
        
    }
    
}


-(void)checkIfDownloadProcessIsActive{
    int processActive = 0;
    if(self.collectKrankheitserregerIsActive==1) processActive = 1;
    
    if(processActive==0 && self.showSVProgressHUD==1)[SVProgressHUD dismiss];
}

-(void) checkDownloadTaskDescription:(NSString*)downloadTaskDescription andBuildChecklistsWithData:(NSArray*)data{

    if([downloadTaskDescription isEqualToString:collectKrankheitserreger] && self.collectorType==COMPLETE_OBJECTS){
       
        AddKrankheitserregerToDataBase *addKrankheitserregerToDatabase = [AddKrankheitserregerToDataBase new];
        addKrankheitserregerToDatabase.krankheitserregerTableViewController = self.krankheitserregerTableViewController;
        [addKrankheitserregerToDatabase addKrankheitserreger:data];
        self.collectKrankheitserregerIsActive = 0;
        self.collectorType = ONLY_TSTAMP;
    } else if (self.collectorType==ONLY_TSTAMP){
        
        [self compareKrankheitserregerTimestampsWithData:data];
        
    }
    
    
    [self checkIfDownloadProcessIsActive];
    
    NSLog(@"Download Finished: %@", downloadTaskDescription);
    
}



-(void) compareKrankheitserregerTimestampsWithData:(NSArray*)data{
    NSDictionary*dict = (NSDictionary*)data;
    bool result;
    
    result = [self compareLatestTimestampFromDatabaseWithJsonTimestamp:dict[@"tstamp"] andEntitiy:@"Krankheitserreger"];
  
    if(!result){
        self.collectorType = COMPLETE_OBJECTS;
        [self collectKrankheisterreger];
    }
}


@end
