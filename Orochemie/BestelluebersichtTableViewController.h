//
//  BestelluebersichtTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BestellungTableViewController;

@interface BestelluebersichtTableViewController : UITableViewController
@property (nonatomic,strong) NSMutableArray *bestellMengenArray;
@property(nonatomic,strong) BestellungTableViewController *bestellungTableViewController;
@end
