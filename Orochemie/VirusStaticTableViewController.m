//
//  VirusStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "VirusStaticTableViewController.h"
#import "JSMToolBox.h"
#import "brancheProduktTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "produktStaticTabBarViewController.h"
#import "ProduktempfehlungTableViewControllerDatasourceandDelegate.h"

#import <QuartzCore/QuartzCore.h>

@interface VirusStaticTableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *produktempfehlungTableViewOutlet;
@property (nonatomic, strong) ProduktempfehlungTableViewControllerDatasourceandDelegate *produktEmpfehlungTableViewDelegate;

@end

@implementation VirusStaticTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Details";
    [oroThemeManager customizeTableView:self.tableView];
   
     
    //Aufbau Produktempfehlungstabelle
    self.produktEmpfehlungTableViewDelegate = [[ProduktempfehlungTableViewControllerDatasourceandDelegate alloc] init];
    self.produktempfehlungTableViewOutlet.delegate = self.produktEmpfehlungTableViewDelegate;
    self.produktempfehlungTableViewOutlet.dataSource = self.produktEmpfehlungTableViewDelegate;
    self.produktempfehlungTableViewOutlet.layer.cornerRadius = 8.0;
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
        
    self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray = [[self.krankheitserreger.produkte allObjects] sortedArrayUsingDescriptors:sortArray];
  

    
    
    
    self.NameTextLabelOutlet.text = self.krankheitserreger.name;
    self.ErregerTextLabelOutlet.text = self.krankheitserreger.art;
    self.UebertragungswegTextLabelOutlet.text = self.krankheitserreger.uerbertragungswege;
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if(section==3){
        
        return [NSString stringWithFormat: @"Produktempfehlungen (%i)",[self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] ];
        
    } else if (section==0){
        
        return @"Name";
        
    }  else if (section==1){
        
        return @"Erregerart";
        
    }  else if (section==2){
        
        return @"Übertragungswege";
    }
    
    return @"";

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Name";
    } else if (section == 1)
    {
        label.text=@"Erregerart";
    } else if (section == 2)
    {
        label.text=@"Übertragungswege";
    } else if (section == 3)
    {
        label.text=[NSString stringWithFormat: @"Produktempfehlungen (%i)",[self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] ];
    } 
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {    // commentTextView
        CGRect bounds = self.ErregerTextLabelOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, 250);
        UIFont* font = self.ErregerTextLabelOutlet.font;
        CGSize sizeOfText = [self.ErregerTextLabelOutlet.text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
        NSLog(@"Texthöhe %f",sizeOfText.height);
        return MAX(44, sizeOfText.height + 50);
    }
    
    if(indexPath.section ==3 && indexPath.row == 0) { //Produktempfehlungstabelle
        
        int hoehe = [self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray count] * 44;
        
        return hoehe;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"produktEmpfehlungSegue"]){
        
        produktStaticTabBarViewController *controller = segue.destinationViewController;
        NSIndexPath *indexPath = [self.produktempfehlungTableViewOutlet indexPathForSelectedRow];
        Produkte *produkt= [self.produktEmpfehlungTableViewDelegate.produktEmpfehlungArray objectAtIndex:indexPath.row];
        controller.produkt = produkt;

    }
}

@end
