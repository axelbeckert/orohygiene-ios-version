//
//  ProduktSortierungAuswahlTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 19.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProduktSortierungAuswahlTableViewController.h"
#import "ProdukteNachBezeichnungTableViewController.h"
#import "PHWertTableViewController.h"
#import "OberflaechenUndBelaegeTableViewController.h"
#import "InformationenTableViewCell.h"
#import "ReinigerAnwendungTableViewController.h"
#import "BarcodeReader.h"



@interface ProduktSortierungAuswahlTableViewController ()
@property(nonatomic,strong) NSArray *sortierungsAuswahlArray;
@end

@implementation ProduktSortierungAuswahlTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Produkt Sortierung nach";
    self.sortierungsAuswahlArray = [NSArray new];
    self.sortierungsAuswahlArray = @[
                             @"Nach Bezeichung",
                             @"Nach Anwendung",
                             @"Nach pH-Wert",
                             @"Nach Oberflächen und Belägen",
                             @"Barcode-Reader",
                             @"Merklisten"
                             ];
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Produkt-Sortierung-Auswahl"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return self.sortierungsAuswahlArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
    
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIFont *font =cell.TitleLabelOutlet.font;
    
    cell.TitleLabelOutlet.text = [self.sortierungsAuswahlArray objectAtIndex:indexPath.row];
    [cell.TitleLabelOutlet setFont:[UIFont fontWithName:font.fontName size:19.0]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Nach Bezeichung"])
    {
        ProdukteNachBezeichnungTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProdukteNachBezeichnungTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Nach Anwendung"])
    {
        ReinigerAnwendungTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReinigerAnwendungTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Nach pH-Wert"])
    {
        PHWertTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PHWertTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Nach Oberflächen und Belägen"])
    {
        OberflaechenUndBelaegeTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OberflaechenUndBelaegeTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Barcode-Reader"])
    {
        BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
        controller.showBarcodeReaderFrom=ReinigerChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if([[self.sortierungsAuswahlArray objectAtIndex:indexPath.row]isEqualToString:@"Merklisten"])
    {
        UITabBarController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    
    
}

#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
