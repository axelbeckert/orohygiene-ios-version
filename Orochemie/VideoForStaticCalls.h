//
//  VideoForStaticCalls.h
//  orochemie
//
//  Created by Axel Beckert on 18.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Video.h"

@interface VideoForStaticCalls : Video

@end
