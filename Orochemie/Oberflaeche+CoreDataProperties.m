//
//  Oberflaeche+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Oberflaeche+CoreDataProperties.h"

@implementation Oberflaeche (CoreDataProperties)

+ (NSFetchRequest<Oberflaeche *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Oberflaeche"];
}

@dynamic bezeichnung;
@dynamic index;
@dynamic kategorie;
@dynamic uid;
@dynamic tstamp;
@dynamic reiniger;

@end
