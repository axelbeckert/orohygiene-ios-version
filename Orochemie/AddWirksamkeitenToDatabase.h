//
//  AddWirksamkeitenToDatabase.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddWirksamkeitenToDatabase : NSObject
-(BOOL) addWirksamkeiten:(NSArray*)wirksamkeiten;
@end
