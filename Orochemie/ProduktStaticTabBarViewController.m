//
//  produktStaticTabBarViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktStaticTabBarViewController.h"
#import "Produkte.h"
#import "Anwendung.h"
#import "Branche.h"
#import "JSMToolBox.h"
#import "ProduktStaticTableViewController.h"
#import "ProduktVariantenTableViewController.h"
#import "DosierungStaticTableViewController.h"
#import "Produktdosierung.h"
#import "NavigationCollectionViewController.h"
#import "VideoPlayerViewController.h"
#import "VideoTableViewController.h"
#import "KrankheitserregerTableViewController.h"
#import "WebviewViewController.h"




@interface produktStaticTabBarViewController () <UINavigationControllerDelegate, UITabBarDelegate,UITableViewDelegate,DosierungStaticTableViewControllerDelegate>
@property (nonatomic,strong) NSArray *buttonArrayOne;
@property (nonatomic,strong) NSArray *buttonArrayTwo;
@end

@implementation produktStaticTabBarViewController
@synthesize produkt=_produkt;
@synthesize delegate;



- (void)home:(id)sender {
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];

    
}

-(void)closeView:(id)sender
{
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}




//-(void)receiveVideoPlayerNotification:(NSNotification*)notification
//{
//    
////   if([notification.name isEqualToString:MPMoviePlayerDidEnterFullscreenNotification])
////   {
////    self.videoPlayerIsFullscreen = YES;
////       //NSLog(@"TABBAR videoPlayerIsFullscreen: %d",self.videoPlayerIsFullscreen);
////
////   } else {
////
////        self.videoPlayerIsFullscreen = NO;
////       [self supportedInterfaceOrientations];
////           //NSLog(@"TABBAR videoPlayerIsFullscreen: %d",self.videoPlayerIsFullscreen);
////   }
//}





- (void)viewDidLoad
{


    

    
    self.tabBarController.moreNavigationController.toolbarHidden=YES;
    self.moreNavigationController.delegate=self;
    [self.tabBarController.tabBar setTintColor:[UIColor lightGrayColor]];
    

     
    self.parentViewController.automaticallyAdjustsScrollViewInsets=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;

    
    UINavigationController *moreController = self.moreNavigationController;

    
    if ([moreController.topViewController.view isKindOfClass:[UITableView class]])
    {
        
        UITableView *view = (UITableView *)moreController.topViewController.view;
        
       
        [oroThemeManager customizeTableView:view];
        
        view.delegate=self;
        


        
    }
    
    
    // TabControllers Data
    
        ProduktStaticTableViewController *controller = [[self viewControllers] objectAtIndex:0];
        controller.produkt = self.produkt;
        controller.branchenName=self.branchenName;
        controller.anwendungName=self.anwendungName;
    
  
        VideoTableViewController* controller2 = [[self viewControllers] objectAtIndex:1];
        controller2.produkt = self.produkt;
        controller2.branchenName=self.branchenName;
        controller2.anwendungName=self.anwendungName;
        controller2.tabBarViewController = self;
        controller2.showVideoTableViewFrom = DesinfektionChannel;
    
    
        DosierungStaticTableViewController *controller3 = [[self viewControllers]objectAtIndex:2];
        controller3 = [[self viewControllers] objectAtIndex:2];
        controller3.uebergebenesProdukt = self.produkt;
        controller3.tabBarViewController =@YES;
        controller3.branchenName=self.branchenName;
        controller3.delegate = self;
    
 
        WebviewViewController* controller4 = [[self viewControllers]objectAtIndex:3];
        controller4.urlString=cWebsiteFuerBestellung;
        controller4.pdfFileName=cWebsitePDFTitleFuerBestellung;
        controller4.webViewTitle=cWebsiteTitleFuerBestellung;

    
//        ProduktVariantenTableViewController *controller4 = [[self viewControllers] objectAtIndex:5];
//        controller4.produkt = self.produkt;
 
//    KrankheitserregerTableViewController *controller5= [[self viewControllers] objectAtIndex:4];
//    controller5.produkt=self.produkt;
    
    self.moreNavigationController.delegate=self;
    
    self.buttonArrayOne = [[NSArray alloc]init];
    self.buttonArrayOne = @[[oroThemeManager homeBarButton:self],[oroThemeManager addToMerklisteBarButton:controller]];
    
    self.buttonArrayTwo = [[NSArray alloc]init];
    self.buttonArrayTwo = @[[oroThemeManager printActionButton:controller3],[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = self.buttonArrayOne;

}


#pragma mark - TabBar Delegate

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{

    if(tabBar.selectedItem.title==nil)
    {
        [self.navigationController.navigationBar setHidden:YES];
        [self.moreNavigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
        self.customizableViewControllers=nil;


        
    } else {
     [self.navigationController.navigationBar setHidden:NO];
    }
}



- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *) viewController animated:(BOOL)animated
{
    
    //NSLog(@"NameView:%@",viewController.title);
    if([viewController.title isEqualToString:@"Mehr"])
    {
    UIBarButtonItem* closeView = [[UIBarButtonItem alloc]initWithTitle:@"zurück" style:UIBarButtonItemStylePlain target:self action:@selector(closeView:)];
    

    viewController.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    viewController.navigationItem.leftBarButtonItems=@[closeView];
    }
    
}


#pragma mark - TableView Delegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundView=backgroundView;
    [oroThemeManager customizeAccessoryView:cell];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0)
    {
        KrankheitserregerTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VirusTableViewController"];
        controller.produkt = self.produkt;
        
        [self.moreNavigationController pushViewController:controller animated:YES];
        
    } else if(indexPath.section == 0 && indexPath.row == 1)
    {
        ProduktVariantenTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktVariantenTableViewController"];
        controller.produkt = self.produkt;
        
        [self.moreNavigationController pushViewController:controller animated:YES];

    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showProduktDetail"]){
        ProduktStaticTableViewController *controller = segue.destinationViewController;
        controller.produkt = self.produkt;
      
    }
}


    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
   

    if(self.videoPlayerIsFullscreen==YES)
   {
       
    return UIInterfaceOrientationMaskAllButUpsideDown;

   }

    return UIInterfaceOrientationMaskPortrait;


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.customizableViewControllers=nil;


}



-(BOOL)shouldAutorotate{
    return YES;
}

-(void)dealloc
{

    
}

#pragma mark - DosierungStaticTabBarDelegate Methoden

-(void) dosierrechnerIsStarted
{
    self.navigationItem.rightBarButtonItems = self.buttonArrayTwo;

}

-(void) dosierrechnerIsStopped
{
    self.navigationItem.rightBarButtonItems = self.buttonArrayOne;
}

@end
