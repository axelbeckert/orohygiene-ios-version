//
//  VideoPlayerViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 16.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import "ProduktStaticTabBarViewController.h"
#import "AnwendungsVideosTableViewController.h"

@interface VideoPlayerViewController : UIViewController
@property (nonatomic,strong) Video *video;
@property (nonatomic,strong) produktStaticTabBarViewController *tabBarViewController;
@property (nonatomic, strong) AnwendungsVideosTableViewController *anwendungsVideosTableViewController;

@property int singleVideoFlag;

- (IBAction)closeButton:(id)sender;
@end
