//
//  Video+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 30.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "Video+ccm.h"

@implementation Video (ccm)
+(Video*) insertVideoInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Video class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteVideo{
    [self.managedObjectContext deleteObject:self];
}



-(void)setBezeichnung:(NSString*)bezeichnung
{
    [self willChangeValueForKey:@"bezeichnung"];
    [self setPrimitiveValue:bezeichnung forKey:@"bezeichnung"];
    self.index = [[bezeichnung substringToIndex:1] uppercaseString];
    [self didChangeValueForKey:@"bezeichnung"];
}
@end
