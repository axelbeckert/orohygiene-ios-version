//
//  Reinigungsverfahren.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Reiniger;

@interface Reinigungsverfahren : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSSet *reiniger;
@end

@interface Reinigungsverfahren (CoreDataGeneratedAccessors)

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

@end
