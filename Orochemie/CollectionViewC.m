//
//  CollectionViewC.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "CollectionViewC.h"
#import "CollectionViewCell.h"
#import "ProduktbezeichnungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "CollectionReusableView.h"
#import "informationenTableViewController.h"
#import "ProduktTableViewController.h"
#import "HilfsmittelTableViewController.h"
#import "UnternehmenTableViewController.h"
#import "ccmViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface CollectionViewC ()

@end

@implementation CollectionViewC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager cutomizeView:self.view];
    
    self.title=@"orochemie";
    
    pictureDataModel = @[@"Informationen",@"Produkte",@"Hilfsmittel",@"Unternehmen",@"Barcode Reader"];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return pictureDataModel.count;
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView* reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];
    

    NSLog(@"reusableView vor dem Bild");
    //[reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_haendedesinfektion.jpg"]];
    
    NSLog(@"reusableView nach dem Bild");
    
    
    return reusableView;
}
                                            
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
    
   [oroThemeManager customizeButtonBackgroundImage:cell.collectionViewImage2];

    cell.collectionViewImage2.layer.borderColor = [UIColor whiteColor].CGColor;
    
    CGFloat borderWidth = 1.0f;
    
    cell.collectionViewImage2.layer.borderWidth = borderWidth;
    
    cell.collectionViewBezeichnungLabelOutlet.text = [pictureDataModel objectAtIndex:indexPath.row];
    
    
    
    
//    if([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"a20.jpg"])
//    {
//        cell.collectionViewBezeichnungLabelOutelet.text = @"nach Produkten";
//    } else if ([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"flaechendesinfektion.jpg"])
//    {
//        cell.collectionViewBezeichnungLabelOutelet.text = @"nach Anwendung";
//    } else
//    {
//        cell.collectionViewBezeichnungLabelOutelet.text = @"nach Branchen";
//    }
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"Informationen"])
    {
        informationenTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"informationenTableViewController"];
             
        [self.navigationController pushViewController:controller animated:YES];

    } else if ([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"Produkte"])
    {
        ProduktTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"Hilfsmittel"])
    {
        HilfsmittelTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HilfsmittelTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"Unternehmen"])
    {
        UnternehmenTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"UnternehmenTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[pictureDataModel objectAtIndex:indexPath.item]isEqualToString:@"Barcode Reader"])
    {
        ccmViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}


@end
