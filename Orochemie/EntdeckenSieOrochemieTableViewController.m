//
//  EntdeckenSieOrochemieTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "EntdeckenSieOrochemieTableViewController.h"


@interface EntdeckenSieOrochemieTableViewController ()
@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;
@property  BOOL firstStart;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

@end

@implementation EntdeckenSieOrochemieTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if(self.showEntdeckenSieOrochemieFrom == DesinfektionChannel){
        //NSLog(@"Entdecken Sie .. von Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];

        
    } else if(self.showEntdeckenSieOrochemieFrom == ReinigerChannel){
        
        //NSLog(@"Entdecken Sie .. von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];

    }

    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    
    //self.tableView.separatorColor = [UIColor clearColor];
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.title=@"orochemie";
    // Set up the image we want to scroll & zoom and add it to the scroll view
    self.pageImages = [NSArray arrayWithObjects:
                       [UIImage imageNamed:@"orochemie_bildblock_001_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_002_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_003_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_004_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_005_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_006_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_010_500.jpg"],
                       [UIImage imageNamed:@"orochemie_bildblock_011_500.jpg"],
                       nil];
    self.firstStart = YES;
    
    NSInteger pageCount = self.pageImages.count;
    
    // Set up the page control
    self.pageControl.currentPage = 2;

    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
       


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * 1;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    self.scrollView = nil;
    self.pageControl = nil;
    self.pageImages = nil;
    self.pageViews = nil;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Entdecken-Sie-Orochemie-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages we want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    

    // Load an individual page, first seeing if we've already loaded it
   
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        frame = CGRectInset(frame, 10.0f, 0.0f);
        
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = frame;
        newPageView.layer.borderColor=[[UIColor whiteColor]CGColor];
        newPageView.layer.borderWidth=1.5;
        
        [self.scrollView addSubview:newPageView];
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages which are now on screen
    [self loadVisiblePages];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate Methoden


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.font = [UIFont fontWithName:label.font.fontName size:13.0];
    
    if(section == 0)
    {
        label.text = @"Endecken Sie orochemie";
    }
    
    
    if (section == 1)
    {
        label.text = @"Das Unternehmen";
    }
    
    if (section == 2)
    {
        label.text = @"orochemie bietet zertifizierte Qualität";
    }
    
    if (section == 3)
    {
        label.text = @"Verantwortung für Mensch und Umwelt";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

@end