//
//  VerbrauchsrechnerTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 04.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VerbrauchsrechnerTableViewController.h"

@interface VerbrauchsrechnerTableViewController ()
@property (strong, nonatomic) IBOutlet UIView *tastaturContentViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *anzeigeContentViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *flaecheBackgroundOutlet;
@property (strong, nonatomic) IBOutlet UIView *verbrauchBackgroundOutlet;
@property (strong, nonatomic) IBOutlet UIView *produktInformationContentBackgroundOutlet;
@property (strong, nonatomic) IBOutlet UILabel *qmEingabeLabelOutlet;
@property(nonatomic,strong) NSString *stringCollection;
@property (strong, nonatomic) IBOutlet UILabel *verbrauchLabelOutlet;
- (IBAction)deleteButtonAction:(id)sender;



@end

@implementation VerbrauchsrechnerTableViewController

#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeReinigerTableView:self.tableView];
    UIColor *color = [UIColor colorWithWhite:0.0 alpha:0.3];
    UIColor *color2 = [UIColor colorWithWhite:0.0 alpha:0.5];

    self.tableView.separatorColor = color;
    self.tastaturContentViewOutlet.backgroundColor = color;
    self.anzeigeContentViewOutlet.backgroundColor = color;
    self.flaecheBackgroundOutlet.backgroundColor = color2;
    self.verbrauchBackgroundOutlet.backgroundColor = color;
    self.produktInformationContentBackgroundOutlet.backgroundColor = color;
    
    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.contentInset = inset;
    
    self.stringCollection = [[NSString alloc]init];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if(self.tabBarController){
        self.tabBarController.title = @"Verbrauchsrechner";
    }
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Reiniger-Verbrauchsrechner"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Button Methoden

- (IBAction)digitButtonTouchUpInside:(UIButton *)sender
{
#warning: unterschied zwischen Anzeige in der GUI und Rechung innerhalb der Funtkion: Sprich zwei Variablen mit den Werten- eine zum Anzeigen, eine zum Rechnen

    NSString* digit = sender.titleLabel.text;
    
    self.stringCollection = [self.stringCollection stringByAppendingString:digit];
    NSLog(@"stringCollection: %@", self.stringCollection);
    
    self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text stringByAppendingString:digit];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    numberFormatter.locale = [NSLocale currentLocale];// this ensures the right separator behavior
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.usesGroupingSeparator = YES;
    [numberFormatter setAllowsFloats:TRUE];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

    NSLog(@"%@",self.qmEingabeLabelOutlet.text);
    
    NSNumber *digitAsNumber;
    NSRange range = [self.qmEingabeLabelOutlet.text rangeOfString:@"."];
    if (range.location == NSNotFound) {
      digitAsNumber = [numberFormatter numberFromString:self.qmEingabeLabelOutlet.text];
 
    } else {
        NSString *strippedString = [self.qmEingabeLabelOutlet.text stringByReplacingOccurrencesOfString:@"." withString:@""];
        digitAsNumber = [numberFormatter numberFromString:strippedString];
    }
    
    if([digit isEqualToString:@"0"]){
            NSRange kommaRange = [self.qmEingabeLabelOutlet.text rangeOfString:@","];
        if (kommaRange.location != NSNotFound){
            NSLog(@"Komma und Null vorhanden");
        }
    }

    NSLog(@"return from Formatter: %@", [self formatNumber:self.qmEingabeLabelOutlet.text andDigit:digit]);

   NSLog(@"%@",[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:digitAsNumber]]);
    NSString *test =[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:digitAsNumber]];
    NSLog(@"Test: %@",test);
    self.qmEingabeLabelOutlet.text = [self formatNumber:self.qmEingabeLabelOutlet.text andDigit:digit];
}

- (IBAction)decimalPointButtonTouchUpInside:(UIButton *)sender
{
    NSRange range = [self.qmEingabeLabelOutlet.text rangeOfString:@","];
    if (range.location == NSNotFound) {
        self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text stringByAppendingString:@","];
    }
    NSRange range2 = [self.stringCollection rangeOfString:@","];
    if (range2.location == NSNotFound) {
        self.stringCollection = [self.stringCollection stringByAppendingString:@","];
    }
}

- (IBAction)deleteButtonAction:(id)sender {
    
   self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text substringToIndex:self.qmEingabeLabelOutlet.text.length-(self.qmEingabeLabelOutlet.text.length>0)];
    
    self.stringCollection = [self.stringCollection substringToIndex:self.stringCollection.length-(self.stringCollection.length>0)];
    
}


-(NSString*)formatNumber:(NSString*)numberString andDigit: (NSString*)digit
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    numberFormatter.locale = [NSLocale currentLocale];// this ensures the right separator behavior
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.usesGroupingSeparator = YES;
    [numberFormatter setAllowsFloats:TRUE];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMinimumFractionDigits:0];
    
    NSNumber *digitAsNumber;
    NSRange range = [numberString rangeOfString:@"."];
    if (range.location == NSNotFound) {
        digitAsNumber = [numberFormatter numberFromString:numberString];
        
    } else {
        NSString *strippedString = [numberString stringByReplacingOccurrencesOfString:@"." withString:@""];
        digitAsNumber = [numberFormatter numberFromString:strippedString];
    }
    
    if([digit isEqualToString:@"0"]){
        NSRange kommaRange = [self.qmEingabeLabelOutlet.text rangeOfString:@","];
        if (kommaRange.location != NSNotFound){
            NSLog(@"Komma und Null vorhanden");
        }
    }
    
    
    NSLog(@"formatNumber: %@",[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:digitAsNumber]]);

    
    return [numberFormatter stringFromNumber:digitAsNumber];
}
@end
