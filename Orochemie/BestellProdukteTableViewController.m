//
//  BestellProdukteTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "BestellProdukteTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "ProduktStaticTableViewController.h"
#import "ProduktStaticTabBarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "SectionHeaderTableViewCell.h"
#import "ProduktVariantenTableViewController.h"
#import "BestellungTableViewController.h"

@interface BestellProdukteTableViewController ()
@property (nonatomic, strong) NSFetchedResultsController *produkteFetchedResultsController;
@end

@implementation BestellProdukteTableViewController

@synthesize produkteFetchedResultsController = _produkteFetchedResultsController;

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) viewDidLoad
{
    

    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    [oroThemeManager  customizeTableView:self.tableView];
    
    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.title=@"Desinfektionsmittel + Hygienepräperate";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

-(void)viewDidAppear:(BOOL)animated
{

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.bestellungTableViewController.bestellMengenArray = self.bestellMengenArray;

}

- (BOOL)shouldAutorotate {
    
    return NO;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    }
    
    NSString *sectionTitle = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return sectionTitle;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"produktbezeichnungCell";
    static NSString *CellIdentifier = @"ProduktuebersichtTableViewCell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    ProduktuebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Produkte *produkte =[self.produkteFetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Configure the cell...
    //    cell.textLabel.text = produkte.name;
    //    cell.detailTextLabel.text =  produkte.untertitel;
    //    cell.imageView.image = image;
    
    
    [oroThemeManager customizeTableCellText:cell];
    [oroThemeManager customizeAccessoryView:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.titleLabelOutlet.text = [produkte.name substringToIndex:7];
        
    } else {
        cell.titleLabelOutlet.text = [produkte.name substringToIndex:4];
    }
    
    if([produkte.name isEqualToString:@"B 5 Wischdesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:4];
    } else if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:7];
    } else {
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:5];
    }
    
    //HD 410 Händedesinfektion
    
    cell.subtitleLabelOutlet.text = produkte.zusatz;
    cell.imageViewOutlet.image = [UIImage imageNamed:produkte.bild];
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
#pragma mark - must be overloaded methods
-(NSFetchedResultsController *)fetchedResultsController
{
    if(self.produkteFetchedResultsController !=nil){
        return self.produkteFetchedResultsController;
    }
    
    JSMCoreDataHelper *helper = [JSMCoreDataHelper new];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:cEntityProdukte inManagedObjectContext:helper.managedObjectContext];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"anwendung.name CONTAINS[c] %@",@"fläche"];
    
    
    fetchRequest.entity=entityDescription;
    fetchRequest.fetchBatchSize=64;
    //fetchRequest.predicate=predicate;
    
    //
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
    fetchRequest.sortDescriptors = sortArray;
    
    self.produkteFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:helper.managedObjectContext sectionNameKeyPath:@"index" cacheName:nil];
    
    self.produkteFetchedResultsController.delegate=self;
    
    return self.produkteFetchedResultsController;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"O"])
    {
        additionalSectionTitle = @"Vliestücher";
    }
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"BestellProdukteSegue"]){
        ProduktVariantenTableViewController *controller = segue.destinationViewController;
        controller.produkt = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.bestellMengenArray = self.bestellMengenArray;
        //NSLog(@"BestellProdukteSegue Bestellung: %@", controller.bestellMengenArray);
        controller.bestellProdukteTableViewController = self;
    }

}

@end
