//
//  Reiniger+ccm.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Reiniger+ccm.h"

@implementation Reiniger (ccm)
+(Reiniger*) insertReinigerInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Reiniger class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteReiniger
{
    [self.managedObjectContext deleteObject:self];
}

-(void)setBezeichnung:(NSString*)bezeichnung
{
    
    [self willChangeValueForKey:@"bezeichnung"];
    [self setPrimitiveValue:bezeichnung forKey:@"bezeichnung"];
    self.index = [[bezeichnung substringWithRange:NSMakeRange(5,1)] uppercaseString];
   
    [self didChangeValueForKey:@"bezeichnung"];
}
@end
