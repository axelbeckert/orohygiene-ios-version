//
//  Produkte+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkte.h"

@interface Produkte (ccm)
+(Produkte*) insertProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteProdukt;
@end
