//
//  MerklistenProdukt.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SavedMerkliste;

@interface MerklistenProdukt : NSManagedObject

@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) SavedMerkliste *savedMerkliste;

@end
