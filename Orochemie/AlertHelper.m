//
//  AlertHelper.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "AlertHelper.h"
#import "UIAlertController+displayAlert.h"
#import "AlertHelperObject.h"

@interface AlertHelper()

@end


@implementation AlertHelper

+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block {
    
    UIAlertController *basicAlert = [UIAlertController alertControllerWithTitle:title
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Abbruch", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:block];
    
    [basicAlert addAction:cancelAction];
    
    return basicAlert;
}

+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message andOKButtonWithCompletionHandler:(AlertActionHelperBlock)block {
    
    UIAlertController *basicAlert = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleCancel
                                   handler:block];
    
    [basicAlert addAction:okAction];
    
    return basicAlert;
}

+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message OKButtonWithCompletionHandler:(AlertActionHelperBlock)okBlock andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)cancelBlock  {
    
    UIAlertController *basicAlert = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:okBlock];
    
    [basicAlert addAction:okAction];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Abbruch", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:cancelBlock];
    
    [basicAlert addAction:cancelAction];
    
    return basicAlert;
}

+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message {
    
    UIAlertController *basicAlert = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    return basicAlert;
}

+(void)addTextFieldToUIAlertController:(UIAlertController *) alertController withPlaceHolderText: (NSString*)placeHolderText{
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = placeHolderText;
     }];
}

+(void)addTextFieldToUIAlertController:(UIAlertController *) alertController withPlaceHolderText: (NSString*)placeHolderText andKeyboardType: (UIKeyboardType*)keyboardType{
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = placeHolderText;
         textField.keyboardType = keyboardType;
         
     }];
    
}

+(UIAlertAction *) getCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block{
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Abbruch", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:block];
    
    return cancelAction;
}

+(UIAlertController *) getUIActionSheetWithTitle: (NSString *) title andMessage: (NSString*) message andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block {
    
    UIAlertController *basicAlert = [UIAlertController alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [basicAlert addAction:[self getCancelButtonWithCompletionHandler:block]];
    
    return basicAlert;
}




+(UIAlertAction*) addUIAlertActionToAlertWithTitle: (NSString*)title andCompletionHandler: (AlertActionHelperBlock)block {
    
    UIAlertAction *alertAction = [UIAlertAction
                                  actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                  handler:block];
    
    return alertAction;
    
    
}



+(UIAlertController*)showInfoAlertWithMessage: (NSString *)infoMessage{
    
    UIAlertController *infoAlert = [UIAlertController
                                             alertControllerWithTitle:@"Info"
                                             message:infoMessage
                                             preferredStyle:UIAlertControllerStyleAlert];
    return infoAlert;
}

+(void)showAlert: (UIAlertController*)alert {
    [alert show];
}

+(void)showErrorAlert: (NSError*)error {
    
    [AlertHelper showAlert:[AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:error.localizedDescription andOKButtonWithCompletionHandler:nil]];
}


+(void) showInfoAlertAndCloseItAfterTwoSecondsWithMessage: (NSString *)infoMessage andAction:(dispatch_block_t) action{
    
    UIAlertController *infoAlert = [UIAlertController
                                             alertControllerWithTitle:@"Info"
                                             message:infoMessage
                                             preferredStyle:UIAlertControllerStyleAlert];

    [AlertHelper showAlert:infoAlert];
    
    
    AlertHelperObject* helperObject = [AlertHelperObject new];
    helperObject.alertController = infoAlert;
    helperObject.block = action;
    
    [self performSelector:@selector(dismissInfoAlert:)
               withObject:helperObject
               afterDelay:5];
    
}

+ (void)dismissInfoAlert:(AlertHelperObject *)helperObject {
    UIAlertController *alertView = helperObject.alertController;
    [alertView dismissViewControllerAnimated:false completion:nil];
    helperObject.block();
}
@end
