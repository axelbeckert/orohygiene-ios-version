//
//  EinwirkzeitDosierungViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DosierungStaticTableViewController;
@class Produktdosierung;

@interface EinwirkzeitDosierungViewController : UIViewController

<UIPickerViewDataSource, UIPickerViewDelegate>{
    __weak IBOutlet UIPickerView *EinwirkzeitPickerViewOutlet;
    
    NSArray *arrayPickerEinwirkzeitDosierungPicker;
    
}

@property (nonatomic,weak) DosierungStaticTableViewController *dosierungStaticTableViewController;
@property (nonatomic, strong) NSString *ausgewaehltesProdukt;
@property (nonatomic, strong) NSString *ausgewaehltesProduktUntertitel;
@property (strong, nonatomic) IBOutlet UIToolbar *einwirkzeitToolbar;
@property (strong, nonatomic) IBOutlet UIView *topToolBarOutlet;
@property (nonatomic,strong) Produktdosierung *produktdosierung;

@end
