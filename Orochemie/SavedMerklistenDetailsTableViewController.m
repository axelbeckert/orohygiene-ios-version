//
//  SavedMerklistenDetailsTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "SavedMerklistenDetailsTableViewController.h"
#import "ProduktDetailTabbarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "Produkte+ccm.h"
#import "ProduktStaticTabBarViewController.h"
#import "SavedMerklistenDetailsTableViewController+Private.h"


@implementation SavedMerklistenDetailsTableViewController




#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Optik
    if(self.showSavedMerklistenDetailsFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
    } else if(self.showSavedMerklistenDetailsFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
    }
    
    //Titel setzen
    self.title = self.merkliste.bezeichnung;

    //Produkt-Array initalisieren
    
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
    
    self.reinigerArray = [NSMutableArray arrayWithArray:[[self.merkliste.reiniger allObjects ] sortedArrayUsingDescriptors:sortArray]];
    self.desinfektionsArray =[NSMutableArray arrayWithArray:[[self.merkliste.produkte allObjects]sortedArrayUsingDescriptors:sortArray]];
    
    self.merklistenTableViewHelper = [[MerklistenTableViewHelper alloc]init];
    self.merklistenTableViewHelper.delegate = self;
                              
    //Sections aufbauen
    self.sections = [[NSMutableDictionary alloc]init];
    [self.sections setObject:self.desinfektionsArray forKey:@"0"];
    [self.sections setObject:self.reinigerArray forKey:@"1"];
    
    //SectionTitle aufbauen
    self.sectionTitles = [[NSMutableArray alloc]initWithObjects:@"Desinfektionsmittel", @"Reinigungsmittel",nil];
    
    //AppDelegate initialisieren
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [self prepareRightBarButtonItems];

    
    //Insets anlegen
    if(self.tabBarController){
        
        UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 49, 0);
        self.tableView.contentInset = inset;
    }
    
    
}

-(void)prepareRightBarButtonItems{
    
    NSMutableArray *buttonArray = [NSMutableArray new];
    
    if((self.reinigerArray.count>0 || self.desinfektionsArray.count>0)){
        //Bar Button Item
        UIBarButtonItem *sendButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendButtonAction)];
        [buttonArray addObject:sendButton];
    }
    
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Details zu gespeicherten Merklisten anzeigen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [(NSDictionary*)[self.sections objectForKey:@(section).stringValue] count];

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProduktuebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProduktuebersichtTableViewCell" forIndexPath:indexPath];
    

    [oroThemeManager customizeTableCellText:cell];
    if(indexPath.section==0){
        Produkte *produkt = [self.desinfektionsArray objectAtIndex:indexPath.row];
        
        cell.titleLabelOutlet.text = produkt.nameHauptbezeichnung;
        cell.descriptionLabelOutlet.text = produkt.nameZusatz;
        cell.subtitleLabelOutlet.text = produkt.zusatz;
        cell.imageViewOutlet.image = [UIImage imageNamed:produkt.bild];
    
    
    } else if(indexPath.section==1) {
        Reiniger *reiniger = [self.reinigerArray objectAtIndex:indexPath.row];
        
        cell.titleLabelOutlet.text = [reiniger.bezeichnung substringToIndex:11];
        cell.descriptionLabelOutlet.text = [reiniger.bezeichnung substringFromIndex:11];
        cell.subtitleLabelOutlet.text = reiniger.zusatz;
        cell.imageViewOutlet.image = [UIImage
                                      imageNamed:[NSString stringWithFormat:@"%@.jpg", reiniger.image]];
    
    }
    
    return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    if(indexPath.section==0){
        Produkte *produkt = [self.desinfektionsArray objectAtIndex:indexPath.row];
        
        [self.merkliste removeProdukteObject:produkt];
        [self.desinfektionsArray removeObjectAtIndex:indexPath.row];

            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        
        
    }
    
    else if(indexPath.section==1){
        Reiniger *reiniger = [self.reinigerArray objectAtIndex:indexPath.row];
        
        [self.merkliste removeReinigerObject:reiniger];
        [self.reinigerArray removeObjectAtIndex:indexPath.row];
        
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        

        
    }
    
    [self.appDelegate saveContext];
    [self.appDelegate saveMerklistenDataToMerklistenDB];
     
    [self prepareRightBarButtonItems];
    }
    
     [tableView reloadData];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;

    label.text = [self.sectionTitles objectAtIndex:section];
    
    return container;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   
    if ([(NSDictionary*)[self.sections objectForKey:@(section).stringValue] count]>0){
        return [oroThemeManager customizeTableViewHeigtForHeaderView];
    } else {
        return 0;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0){
        produktStaticTabBarViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
        controller.produkt = [self.desinfektionsArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        [self.navigationController pushViewController:controller animated:YES];
    }  else   if(indexPath.section==1){
        ProduktDetailTabbarViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktDetailTabbarViewController"];
        controller.reiniger = [self.reinigerArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)presentAlertController: (UIAlertController*)alert animated:(BOOL)animated completion:(UITableViewControllerBlock)block {
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@"delegate was called with: present ViewController");
}
@end
