//
//  PictureContainerView.h
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureContainerView : UIView
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@end
