//
//  HygienetippModel.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Hygienetipp;

@interface HygienetippModel : NSObject
-(void) addHygieneTipp: (Hygienetipp*) hygienetipp;

-(NSInteger) numberOfHygieneTipps;

-(Hygienetipp*) hygieneTippAtIndexPath: (NSIndexPath*) indexPath;
@end
