//
//  EinwirkzeitDosierungViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "EinwirkzeitDosierungViewController.h"
#import "DosierungStaticTableViewController.h"
#import "Produktdosierung.h"
#import "Produkteinwirkzeit.h"

@interface EinwirkzeitDosierungViewController ()
@property(nonatomic,strong) NSArray *einwirkzeitArray;
@property (nonatomic,strong) Produkteinwirkzeit *produkteinwirkzeit;
@end

@implementation EinwirkzeitDosierungViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.produktdosierung!=nil){
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortierung"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
  
               self.einwirkzeitArray = [[NSArray arrayWithArray:[self.produktdosierung.produkteinwirkzeit allObjects]] sortedArrayUsingDescriptors:sortDescriptors];

    }
    

    //self.einwirkzeitToolbar.barStyle = UIBarStyleBlackOpaque;
    self.topToolBarOutlet.layer.borderWidth = 0.5;
    self.topToolBarOutlet.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.4].CGColor;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return [self.einwirkzeitArray count];
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    Produkteinwirkzeit *produkteinwirkzeit =[ self.einwirkzeitArray objectAtIndex:row];
    
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UILabel *label = [[UILabel alloc]init];
        label.frame = self.view.bounds;
        

        label.text = produkteinwirkzeit.einwirkzeit;
        label.font=[UIFont boldSystemFontOfSize:25.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines=0;
        
        UIView *myView = [[UIView alloc]init];
        myView.frame = self.view.bounds;
        [myView addSubview:label];
        
        return myView;
        
    } else {
        
        
        UILabel *label = [[UILabel alloc]init]; //initWithFrame:CGRectMake(0, 0, 350, 40)];
        label.frame = self.view.bounds;

        label.text = produkteinwirkzeit.einwirkzeit;
        label.font=[UIFont boldSystemFontOfSize:11.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines=0;
        
        UIView *myView = [[UIView alloc]init]; //initWithFrame:CGRectMake(0, 0, 350, 40)];
        myView.frame = self.view.bounds;
        [myView addSubview:label];
        
        
        return myView;
        
    }
    

}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.produkteinwirkzeit = [self.einwirkzeitArray objectAtIndex:row];
}


-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return 90;
    }
    
    return 44;
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!self.produkteinwirkzeit){
        self.dosierungStaticTableViewController.produkteinwirkzeit = [self.einwirkzeitArray objectAtIndex:0];
        self.dosierungStaticTableViewController.einwirkzeitZustazinformationTextOutlet.text = [self.dosierungStaticTableViewController.produkteinwirkzeit.einwirkzeit stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        [self.dosierungStaticTableViewController rechne2];
        
        
        
    } else {
        self.dosierungStaticTableViewController.produkteinwirkzeit = self.produkteinwirkzeit;
        self.dosierungStaticTableViewController.einwirkzeitZustazinformationTextOutlet.text = [self.dosierungStaticTableViewController.produkteinwirkzeit.einwirkzeit stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        [self.dosierungStaticTableViewController rechne2];
        
    }
    
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif  
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)auswahlUebernehmenButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
