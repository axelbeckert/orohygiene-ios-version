//
//  WirksamkeitenTableViewController.m
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "WirksamkeitenTableViewController.h"
#import "ProduktAuflistungTableViewController.h"

@interface WirksamkeitenTableViewController ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) AppDelegate *delegate;
@end

@implementation WirksamkeitenTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Wirksamkeit class])];
    NSSortDescriptor *sortName = nil;
    NSString *sectionKeyPath = [[NSString alloc]init];

    sortName= [[NSSortDescriptor alloc] initWithKey:@"sorting" ascending:YES];
    sectionKeyPath = @"bezeichnung";
    
    NSArray *sortArray= [NSArray arrayWithObject:sortName];
    fetch.sortDescriptors = sortArray;
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:sectionKeyPath
                                 cacheName:nil];

    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - ScreenRotation

-(BOOL)shouldAutorotate
{
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [oroThemeManager  customizeTableView:self.tableView];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    //Core Data initalisieren
    self.delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = self.delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    
    [self addViewTitle];
    [self addTableHeader];

}
    
    


-(void) addViewTitle{
    
    
    self.title = @"Wirksamkeiten";
    
    if ( [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] ) {
        self.title = @"Desinfektionsmittelwirksamkeiten";
    }
    
}

-(void)addTableHeader{

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(hinweis:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:cHinweisWissenGewissenButton forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
    
    button.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.75];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, button.frame.size.height)];
    
    [headerView addSubview:button];
    
    self.tableView.tableHeaderView = headerView;

}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Wirksamkeit *wirksamkeit =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    cell.textLabel.text= wirksamkeit.bezeichnung;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"= wirksam gegen %@",wirksamkeit.wirksam];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 64;
    
}





#pragma mark - Segue Methoden

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showProdukteForWirksamkeit"]){
        
        ProduktAuflistungTableViewController *controller = segue.destinationViewController;
        controller.wirksamkeitForProduktResult =[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        NSLog(@"Wirksamkeit ausgewählt: %@", controller.wirksamkeitForProduktResult);
        
    }
    
}

#pragma mark - Hinweis zur Auflistung

-(void)hinweis:(id)sender
{

    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:cHinweisWissenGewissenTitle andMessage:cHinweisWissenGewissen andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
    }];
    
    [AlertHelper showAlert:alert];
    
    
}

@end
