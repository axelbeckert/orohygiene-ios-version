//
//  Produktdosierung+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktdosierung+ccm.h"

@implementation Produktdosierung (ccm)
+(Produktdosierung*) insertProduktdosierungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Produktdosierung" inManagedObjectContext:managedObjectContext];
}
-(void)deleteProduktdosierung
{
    [self.managedObjectContext deleteObject:self];
}
@end
