//
//  oroTheme.h
//  Orochemie
//
//  Created by Axel Beckert on 27.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#define IS_PHONEPOD5() ([UIScreen mainScreen].bounds.size.height == 568.0f && [UIScreen mainScreen].scale == 2.f && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

@interface oroTheme : NSObject
+(void)customizeTableView:(UITableView*)theTableView;
+(void)customizeCollectionView:(UICollectionView*)theCollectionView;
+(void)cutomizeView: (UIView*)theView;
+(void)customizeButtonBackgroundImage:(UIImageView*)theImageView;
+(void)customizeAccessoryView:(UITableViewCell*)theTableViewCell;
+(UIImage*) customizeImageforNavigationBar;
@end
