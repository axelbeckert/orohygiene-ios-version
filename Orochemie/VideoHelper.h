//
//  VideoHelper.h
//  orochemie
//
//  Created by Axel Beckert on 28.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoHelper : NSObject
-(BOOL)checkIfVideosAreThereWithStoryboard:(UIStoryboard*)storyboard andNavigationController:(UINavigationController*)navigationController;
+(BOOL)checkVideoFiles;
-(void)downloadVideos;
@end
