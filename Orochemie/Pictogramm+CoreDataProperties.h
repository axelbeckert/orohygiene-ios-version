//
//  Pictogramm+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pictogramm.h"

NS_ASSUME_NONNULL_BEGIN

@interface Pictogramm (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *bild;
@property (nullable, nonatomic, retain) NSSet<Produkte *> *produkt;
@property (nullable, nonatomic, retain) NSSet<Reiniger *> *reiniger;

@end

@interface Pictogramm (CoreDataGeneratedAccessors)

- (void)addProduktObject:(Produkte *)value;
- (void)removeProduktObject:(Produkte *)value;
- (void)addProdukt:(NSSet<Produkte *> *)values;
- (void)removeProdukt:(NSSet<Produkte *> *)values;

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet<Reiniger *> *)values;
- (void)removeReiniger:(NSSet<Reiniger *> *)values;

@end

NS_ASSUME_NONNULL_END
