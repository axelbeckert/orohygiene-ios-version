//
//  CollectionViewC.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationCollectionViewController : UIViewController <UICollectionViewDataSource>
{
  NSArray* navigationDataModel;  
}

@property (strong, nonatomic) IBOutlet UICollectionView *navigationCollectionView;

@end
