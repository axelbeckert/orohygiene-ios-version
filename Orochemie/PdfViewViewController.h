//
//  PdfViewViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 13.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Produkte;

enum PDFWebviewShow{
    PDFWebviewShowHygieneCheckListen,
    PDFWebviewShowSicherheitsDatenblatt,
    PDFWebviewShowBetriebsAnweisung,
    PDFWebviewShowProduktInformation,
    PDFWebviewShowHygieneWissen
};
typedef enum PDFWebviewShow pdfWebViewShow;

@interface PdfViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *pdfWebViewOutlet;
@property(nonatomic,strong) Produkte* produkt;
@property(nonatomic,strong) NSString *senderController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *actionButtonOutlet;
@property (nonatomic,assign) pdfWebViewShow pdfWebViewShow;
- (IBAction)shareButtonAction:(id)sender;




@end
