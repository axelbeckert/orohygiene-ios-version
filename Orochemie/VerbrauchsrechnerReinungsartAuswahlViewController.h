//
//  VerbrauchsrechnerReinungsartAuswahlViewController.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerbrauchsRechnerStaticTableViewController.h"

@interface VerbrauchsrechnerReinungsartAuswahlViewController : UIViewController 
<UIPickerViewDataSource, UIPickerViewDelegate>{
    __weak IBOutlet UIPickerView *reinigungsartAuswahlPickerViewOutlet;
}

@property(nonatomic,strong) Reiniger *reiniger;
@property (strong, nonatomic) IBOutlet UIView *topToolBarOutlet;
@property(nonatomic,strong) VerbrauchsRechnerStaticTableViewController *verbrauchsRechnerStaticTableViewController;
@property(nonatomic,strong) NSArray *verbrauchsArray;
@property(nonatomic,strong) NSArray *reinigungsartenArray;
@end
