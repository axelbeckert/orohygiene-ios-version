//
//  ProduktVariantenStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 06.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktVariantenStaticTableViewController.h"
#import "Produktvarianten.h"

@interface ProduktVariantenStaticTableViewController ()

@end

@implementation ProduktVariantenStaticTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];
    //NSLog(@"Variante: %@", self.produktvariante.bezeichnung1);
    
    self.artikelnummerTextOutlet.text = self.produktvariante.artikelnummer;
    self.matchcodeTextOutelet.text = self.produktvariante.matchcode;
    self.bezeichnung1TextOutlet.text = self.produktvariante.bezeichnung1;
    self.bezeichnung2TextOutlet.text = self.produktvariante.bezeichnung2;
    self.barcodeTextOutlet.text= self.produktvariante.barcode;
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.tabBarController)
    {
        self.tabBarController.title=@"Varianten-Detail";
    } else {
        self.title=@"Varianten-Detail";
    }
        self.title=@"Varianten-Detail";    
}



    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    
    
    return UIInterfaceOrientationMaskPortrait;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
