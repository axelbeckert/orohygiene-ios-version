//
//  Pictogramm+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 01.03.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Pictogramm+ccm.h"

@implementation Pictogramm (ccm)
+(Pictogramm*) insertPictogrammInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Pictogramm" inManagedObjectContext:managedObjectContext];

}
-(void)deletePictogramm{
    
    [self.managedObjectContext deleteObject:self];

}
@end
