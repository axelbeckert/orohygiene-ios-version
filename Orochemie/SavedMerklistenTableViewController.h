//
//  SavedMerklistenTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SavedMerklistenTableViewControllerDelegate <NSObject>
@optional
-(void) showRightBarButtonMenueItems;

@end

@interface SavedMerklistenTableViewController : UITableViewController
@property (nonatomic,weak) id <SavedMerklistenTableViewControllerDelegate> delegate;
@property (nonatomic,assign) show showSavedMerklistenFrom;
@end
