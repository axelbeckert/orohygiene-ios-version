//
//  VirusStaticTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VirusStaticTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *NameTextLabelOutlet;

@property (strong, nonatomic) IBOutlet UITextView *ErregerTextLabelOutlet;


@property (strong, nonatomic) IBOutlet UILabel *UebertragungswegTextLabelOutlet;

@property (strong, nonatomic) IBOutlet UILabel *ProduktEmpfehlungTextLabelOutlet;


@property (nonatomic, strong) Krankheitserreger *krankheitserreger;




@end
