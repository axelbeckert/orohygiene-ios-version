//
//  ProduktDosierungViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DosierungStaticTableViewController;
@class Produkte;

#define PICKER_PRODUKTAUSWAHL 0

@interface ProduktDosierungViewController : UIViewController
<UIPickerViewDataSource, UIPickerViewDelegate>{
    __weak IBOutlet UIPickerView *DosierungPickerViewOutlet;
    
    
}
@property (strong, nonatomic) IBOutlet UIToolbar *produktDosierungToolbar;
@property (nonatomic,strong) Produkte *uebergebenesProdukt;
@property(nonatomic, weak) DosierungStaticTableViewController *dosierungStaticTableViewController;
@property (strong, nonatomic) IBOutlet UIView *topToolBarDosierungAuswahlViewOutlet;
@end
