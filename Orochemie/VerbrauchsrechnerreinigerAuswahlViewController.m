//
//  VerbrauchsrechnerreinigerAuswahlViewController.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VerbrauchsrechnerreinigerAuswahlViewController.h"
#import "Verbrauch+ccm.h"
#import "VerbrauchsRechnerStaticTableViewController.h"

@interface VerbrauchsrechnerreinigerAuswahlViewController ()
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,strong) Reiniger *reiniger;
@end

@implementation VerbrauchsrechnerreinigerAuswahlViewController

#pragma mark - fetched ResultsController
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY verbrauch.verbrauchsmenge > 0"];
    fetch.predicate = predicate;
                              
//    if(self.uebergebenesProdukt){
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produkte.name CONTAINS[c] %@",self.uebergebenesProdukt.name];
//        fetch.predicate=predicate;
//    }
    
    NSSortDescriptor *sortByName =
    [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];
    
    fetch.sortDescriptors = @[ sortByName ];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.managedObjectContext = delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    
    
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         [tracker set:kGAIScreenName value:@"Reiniger-VerbrauchsrechnerReinigerAuswahl-Schirm"];
         [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    });


}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    if([[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects]!=0){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        Reiniger *reiniger = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if(!self.reiniger){
           
            self.verbrauchsRechnerStaticTableViewController.reiniger = reiniger;
            self.verbrauchsRechnerStaticTableViewController.produktMainTextLabelOutlet.text = reiniger.bezeichnung;
            self.verbrauchsRechnerStaticTableViewController.produktTextLabelOutlet.text=@"Produkt";
            [self.verbrauchsRechnerStaticTableViewController checkReiniger];
   
        } else {
            self.verbrauchsRechnerStaticTableViewController.reiniger = self.reiniger;
            self.verbrauchsRechnerStaticTableViewController.produktMainTextLabelOutlet.text = self.reiniger.bezeichnung;
            self.verbrauchsRechnerStaticTableViewController.produktTextLabelOutlet.text=@"Produkt";
            [self.verbrauchsRechnerStaticTableViewController checkReiniger];
            [self.verbrauchsRechnerStaticTableViewController rechneVerbrauch];
        }
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return [[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    Reiniger *reiniger =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        
        
        
        label.text = reiniger.bezeichnung;
        label.font=[UIFont boldSystemFontOfSize:30.0];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        
        [myView addSubview:label];
        
        return myView;
        
        
    } else {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        
        
        
        label.text = reiniger.bezeichnung;
        label.font=[UIFont boldSystemFontOfSize:15.0];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        
        [myView addSubview:label];
        
        return myView;
        
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return 100;
    }
    
    return 44;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    self.reiniger = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
}

- (IBAction)auswahlUebernehmenButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
