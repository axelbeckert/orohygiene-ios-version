//
//  Branche+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Branche+ccm.h"

@implementation Branche (ccm)
+(Branche*) insertBrancheInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Branche" inManagedObjectContext:managedObjectContext];
}
-(void)deleteBranche
{
    [self.managedObjectContext deleteObject:self];
}
@end
