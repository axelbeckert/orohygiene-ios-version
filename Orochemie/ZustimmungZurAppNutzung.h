//
//  ZustimmungZurAppNutzung.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZustimmungZurAppNutzung : NSObject
+(void)einholungZustimmung;
@end
