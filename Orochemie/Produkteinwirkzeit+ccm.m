//
//  Produkteinwirkzeit+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkteinwirkzeit+ccm.h"

@implementation Produkteinwirkzeit (ccm)
+(Produkteinwirkzeit*) insertProdukteinwirkzeitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Produkteinwirkzeit" inManagedObjectContext:managedObjectContext];
}
-(void)deleteProdukteinwirkzeit
{
    [self.managedObjectContext deleteObject:self];
}
@end
