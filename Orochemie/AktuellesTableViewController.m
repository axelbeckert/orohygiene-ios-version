//
//  informationenTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 27.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "AktuellesTableViewController.h"
#import "newsTableViewController.h"
#import "HygieneTippsTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomColoredAccessory.h"
#import "NewsletterTableViewController.h"
#import "InformationenTableViewCell.h"
#import "WebviewViewController.h"

@interface AktuellesTableViewController ()
@property (nonatomic,strong) NSArray *navigationPoints;
@end

@implementation AktuellesTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSArray*) navigationPoints
{
    if (!_navigationPoints) _navigationPoints =[[NSArray alloc]init];
    return _navigationPoints;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    if(self.showAktuellesFrom == DesinfektionChannel){
        //NSLog(@"Unternehmen von Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];
        
        
        
        
        
    } else if(self.showAktuellesFrom == ReinigerChannel){
        
        //NSLog(@"Unternehmen von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }


    
    self.navigationPoints = @[@"News",@"Hygienetipps",@"Newsletter",@"Hygienetage"];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Aktuelles-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.navigationPoints.count;

}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    cell.TitleLabelOutlet.text = [self.navigationPoints objectAtIndex:indexPath.row];
    

        return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        newsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"newsTableViewController"];
        if(self.showAktuellesFrom == ReinigerChannel){
        controller.showNewsFrom = ReinigerChannel;
        } else {
            controller.showNewsFrom = DesinfektionChannel;
        }
        [self.navigationController pushViewController:controller animated:YES];
    
    } else if (indexPath.row == 1)
    {
        HygieneTippsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HygieneTippsTableViewController"];
        if(self.showAktuellesFrom == ReinigerChannel){
            controller.showHygieneTippsFrom = ReinigerChannel;
        } else {
            controller.showHygieneTippsFrom = DesinfektionChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
    
    } else if (indexPath.row == 2)
    {
        NewsletterTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsletterTableViewController"];
        if(self.showAktuellesFrom == ReinigerChannel){
            controller.showNewsletterFrom = ReinigerChannel;
        } else {
            controller.showNewsletterFrom = DesinfektionChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 3)
    {
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerHygieneTage;
        controller.webViewTitle=cWebsiteTitleFuerHygieneTage;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneTage;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
