//
//  ReadJsonFile.h
//  orochemie 
//
//  Created by Axel Beckert on 27.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReadJsonFile : NSObject
- (NSDictionary *)JSONFromFile:(NSString*)file;
@end
