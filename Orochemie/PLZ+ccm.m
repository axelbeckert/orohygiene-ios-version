//
//  PLZ+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "PLZ+ccm.h"

@implementation PLZ (ccm)
+(PLZ*)insertPLZInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"PLZ" inManagedObjectContext:managedObjectContext];
}
-(void)deletePLZ
{
    [self.managedObjectContext deleteObject:self];
}
@end
