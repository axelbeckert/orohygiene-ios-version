//
//  Produktvarianten.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte;

@interface Produktvarianten : NSManagedObject

@property (nonatomic, retain) NSString * artikelnummer;
@property (nonatomic, retain) NSString * barcode;
@property (nonatomic, retain) NSString * bezeichnung1;
@property (nonatomic, retain) NSString * bezeichnung2;
@property (nonatomic, retain) NSString * matchcode;
@property (nonatomic, retain) Produkte *produkte;
@property (nonatomic, retain) NSManagedObject *reiniger;

@end
