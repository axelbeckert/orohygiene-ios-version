//
//  Phwert+ccm.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Phwert+ccm.h"

@implementation Phwert (ccm)
+(Phwert*) insertPhwertInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Phwert" inManagedObjectContext:managedObjectContext];
}
-(void)deletePhwert
{
    [self.managedObjectContext deleteObject:self];
}

-(void)setBezeichnung:(NSString*)bezeichnung
{
    [self willChangeValueForKey:@"bezeichnung"];
    [self setPrimitiveValue:bezeichnung forKey:@"bezeichnung"];
    self.index = [[bezeichnung substringToIndex:1] uppercaseString];
    [self didChangeValueForKey:@"bezeichnung"];
}
@end
