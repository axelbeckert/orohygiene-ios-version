//
//  UIAlertController+displayAlert.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (displayAlert)
- (void)show;
- (void)show:(BOOL)animated;
@end


@interface UIAlertController (Private)

@property (nonatomic, strong) UIWindow *alertWindow;

@end