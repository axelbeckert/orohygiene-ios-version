//
//  Produktdosierung.m
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktdosierung.h"
#import "Produkte.h"
#import "Produkteinwirkzeit.h"


@implementation Produktdosierung

@dynamic hauptbezeichnung;
@dynamic sortierung;
@dynamic zusatzbezeichnung;
@dynamic produkte;
@dynamic produkteinwirkzeit;

@end
