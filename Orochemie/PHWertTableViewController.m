//
//  PHWertTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "PHWertTableViewController.h"
#import "ProdukteNachBezeichnungTableViewController.h"

@interface PHWertTableViewController ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@end

@implementation PHWertTableViewController

#pragma mark - NSFechtedResultsController

-(NSFetchedResultsController *) fetchedResultsController{
    if (_fetchedResultsController) return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Phwert class])];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:NO];
    
    fetch.sortDescriptors = @[sortByName];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetch
                                                                   managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"index"
                                                                              cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if(error){
        //NSLog(@"Fehler: %@",error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Nach pH-Wert";
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Nach-PH-Wert-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> info = self.fetchedResultsController.sections[section];
    return [info numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Phwert *pHWert = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    cell.textLabel.text = pHWert.bezeichnung;
    
    return cell;
}

#pragma mark - tableView indexTitle
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}
#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


 #pragma mark - Segue Methoden
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 if([segue.identifier isEqualToString:@"produktNachBezeichnungSegue"]){
     ProdukteNachBezeichnungTableViewController *controller = segue.destinationViewController;
     controller.pHWert = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
 }
 
 
 }

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
