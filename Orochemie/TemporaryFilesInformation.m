//
//  TemporaryFilesInformation.m
//  orochemie
//
//  Created by Axel Beckert on 17.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import "TemporaryFilesInformation.h"

@implementation TemporaryFilesInformation


+(void)deleteContentOfLastTemporaryFolder
{
    NSError *error = nil;
    NSString *documentsDirectory = NSTemporaryDirectory();
    
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", @"DiskImage"];

    NSArray *filteredArray = [files filteredArrayUsingPredicate:predicate];


    NSArray *lastDirectory=[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,[filteredArray lastObject]] error:&error];
   

    
    if(lastDirectory.count > 0){
        if (error == nil) {
            for (NSString *path in lastDirectory) {
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",[filteredArray lastObject],path]];
                
                BOOL removeSuccess = [[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error];
                files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fullPath error:&error];
                if (!removeSuccess) {
                    NSLog(@"Fehler: %@",error.localizedDescription);
                }
                

            }
        } else {
            NSLog(@"Fehler: %@",error.localizedDescription);
        }
        
        
        
    }
    


}

+(NSString *)getLastFilePathFromLastTempFolderFile
{
    NSError *error = nil;
    NSString *documentsDirectory = NSTemporaryDirectory();
    
    //Hole die Inhalte vom TEMP Verzeichnis
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];

    //Filter bauen für die Ordner die DiskImage beinhalten
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", @"DiskImage"];
    
    //Filter anwenden auf Array files
    NSArray *filteredArray = [files filteredArrayUsingPredicate:predicate];

    //letzten Pfad zurück geben
    NSArray *lastDirectory=[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,[filteredArray lastObject]] error:&error];
    
    if(lastDirectory.count==0){
        return @"";
    }
    else {
    return [NSString stringWithFormat:@"%@%@/%@",documentsDirectory,[filteredArray lastObject], [lastDirectory lastObject] ];
    }
}

+(void)clearTempSystemFolder
{
    NSError *error = nil;
    // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSTemporaryDirectory(), NSUserDomainMask, YES);
    NSString *documentsDirectory = NSTemporaryDirectory();
    //NSString *pdfPath =[self.documentsPath stringByAppendingString:@"/pdfFiles"];
    
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    
    
    if(files.count > 0){
        if (error == nil) {
            for (NSString *path in files) {
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",documentsDirectory,path]];
                
                BOOL removeSuccess = [[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error];
                files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fullPath error:&error];
                if (!removeSuccess) {
                    NSLog(@"Fehler aufgetreten: %@", error.localizedDescription);
                }
                
            }
        } else {
            // Error
        }
        
    }
}

@end
