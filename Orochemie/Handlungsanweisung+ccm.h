//
//  Handlungsanweisung+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Handlungsanweisung.h"

@interface Handlungsanweisung (ccm)
+(Handlungsanweisung*) insertHandlungsanweisungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteHandlungsanweisung;
@end
