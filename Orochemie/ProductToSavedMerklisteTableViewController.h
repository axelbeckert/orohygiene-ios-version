//
//  ProductToSavedMerklisteTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 06.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProductToSavedMerklisteTableViewControllerDelegate <NSObject>
@optional
-(void) informUserAboutProductSavedToMerkliste:(Merkliste*)merkliste;

@end
@interface ProductToSavedMerklisteTableViewController : UITableViewController
@property (nonatomic,weak) id <ProductToSavedMerklisteTableViewControllerDelegate> delegate;
@property(nonatomic,strong) Reiniger *reiniger;
@property(nonatomic,strong) Produkte *produkt;
@property(nonatomic,assign) show showProductToSavedMerklisteFrom;

@end
