//
//  ChecklistenCollector.h
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JsonCollector.h"
@class HygieneChecklistenTableViewController;

@interface WirksamkeitenCollector : JsonCollector
@property (nonatomic, strong) HygieneChecklistenTableViewController *hygienechecklistenTableViewController;
-(void) startCollectingWirksamkeiten;

@end
