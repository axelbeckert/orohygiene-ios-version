//
//  HygieneTippsTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "HygieneTippsTableViewController.h"
#import "Hygienetipp.h"
#import "HygienetippModel.h"
#import "HygieneWebViewController.h"
#import "AppDelegate.h"
#import "NewsTableCell.h"
#import "WebviewViewController.h"




@interface HygieneTippsTableViewController ()
@property (nonatomic, strong) HygienetippModel* hModel;
@property (nonatomic,strong) AppDelegate* appDelegate;
@property (nonatomic,strong) NSTimer *timer;


@end


@implementation HygieneTippsTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(HygienetippModel *)hModel
{
    if(!_hModel) _hModel = [[HygienetippModel alloc] init];
    return _hModel;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.showHygieneTippsFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
        
    } else if(self.showHygieneTippsFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //Timer einschalten
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];

    
    //Title Label
    
    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
    tlabel.text = @"Hygiene-Tipps";
    self.navigationItem.titleView=tlabel;
    
    
    if([ConnectionCheck hasConnectivity]) {

    
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
    self.hModel = nil;
    
    NSString* urlString = @"http://www.orohygienesystem.de/json_hygieneTipp.php";
    NSURL* url = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:10.0];
    [request setCachePolicy:NSURLCacheStorageAllowed];
    [request setValue:@"text/json" forHTTPHeaderField:@"Content-type"];
    
    
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
         if (error) {
             //NSLog(@"Fehler: %@", error);
         };
         
         
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;

         if (httpResponse.statusCode != 200) {

             return;
             
         }
         
         error = nil;
         NSArray* jsonContainer = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
         if (error) {
             //NSLog(@"Fehler: %@", error);
         };

         
         for (NSDictionary* dictionary in jsonContainer) {
             Hygienetipp* hygieneTipp = [[Hygienetipp alloc] init];
             hygieneTipp.hygieneTippId = dictionary[@"id"];
             hygieneTipp.hygieneTippText1 = dictionary[@"text1"];
             hygieneTipp.hygieneTippText2 = dictionary[@"text3"];
             hygieneTipp.hygieneTippUrl = dictionary[@"text2"];
             
             
             [self.hModel addHygieneTipp:hygieneTipp];
         }
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [self.timer invalidate]; //Timer ausschalten
             [self.tableView reloadData];
             
         });
         
     }];
    
        [dataTask resume];
        
    } else {
        [self.timer invalidate]; //Timer ausschalten
      
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
        
    }

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Hygiene-Tipp-Uebersicht-Screen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.hModel.numberOfHygieneTipps;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    NewsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  


    Hygienetipp* hygieneTipp = [[Hygienetipp alloc] init];
    
    hygieneTipp = [self.hModel hygieneTippAtIndexPath:indexPath];
    

    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    cell.titleOutlet.text = hygieneTipp.hygieneTippText1;
    cell.subTitleOutlet.text = hygieneTipp.hygieneTippText2;
    
    
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
        [SVProgressHUD dismiss];
    }
}


#pragma mark - Table view delegate



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"hygieneTippWebViewSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        Hygienetipp *hygienetipp =[self.hModel hygieneTippAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.urlString = [NSString stringWithFormat:@"https://www.orochemie.de/newsletter_hygienetipp/%@",hygienetipp.hygieneTippUrl];
        controller.webViewTitle= @"Hygienetipp";
        controller.pdfFileName=[NSString stringWithFormat:@"%@", hygienetipp.hygieneTippUrl];
        
    }
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - AlertView Delegate & Timer Selector

- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [self.timer invalidate]; //Timer ausschalten
    
    UIAlertController* alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [AlertHelper showAlert:alert];
    
    [SVProgressHUD dismiss];
    
}


@end
