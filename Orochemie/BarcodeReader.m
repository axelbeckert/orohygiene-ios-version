//
//  ccmViewController.m
//  BarcodeScanner
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "BarcodeReader.h"
#import "JSMToolBox.h"
#import "Produkte.h"
#import "ProduktStaticTabBarViewController.h"
#import "Reiniger+ccm.h"
#import "ProduktDetailTabbarViewController.h"
#import "Produktvarianten.h"
#import "WebviewViewController.h"
 

@interface BarcodeReader ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,weak) Produkte *produkt;
@property (nonatomic,strong) Reiniger* reiniger;
@property(nonatomic,strong) NSString* barcode;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic,strong) AVCaptureStillImageOutput* stillImageOutput;
@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL haveImage;
@property (strong, nonatomic) IBOutlet UITextView *barcodeTextViewOutlet;


@end

@implementation BarcodeReader

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Produktvarianten class])];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"barcode CONTAINS[cd] %@",self.barcode];
    fetch.predicate=predicate;
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]initWithKey:@"barcode" ascending:YES];
    fetch.sortDescriptors =@[sort];
    
    
    
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //CoreData initalisierung
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = delegate.managedObjectContext;
    
    
    
    self.title = @"Barcode lesen";
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
  
    if(self.showBarcodeReaderFrom == DesinfektionChannel){

    [oroThemeManager cutomizeView:self.view];
          
        
    } else if(self.showBarcodeReaderFrom == ReinigerChannel){
        
    [oroThemeManager customizeCleanerView:self.view];
        
    }
    
    

    [oroThemeManager customizeButtonImage:self.scanButtonOutlet];
    
    [oroThemeManager customizeLabel:self.titleLabelOutlet];
   
    // Initially make the captureSession object nil.
    _captureSession = nil;
    
    // Set the initial value of the flag to NO.
    _isReading = NO;
    
    //Start Loading the Beep Sound
    [self loadBeepSound];
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Barcode-Reader-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)ScanButtonTouchUpInside:(id)sender {
    
    
    if (!_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
            [self.scanButtonOutlet setTitle:@"Stop" forState:UIControlStateNormal];
                    }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self performSelectorOnMainThread:@selector(stopReading:) withObject:nil waitUntilDone:NO];
        // The bar button item's title should change again.
        [self.scanButtonOutlet setTitle:@"Barcode lesen" forState:UIControlStateNormal];
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
    
////////// Kommentar entfernen wenn Barcodereader ohne Kamerafunktion getestet werden soll! Oberen teil bitte kommentieren!!
    
//    self.barcode = @"-06157719";
//    [self.fetchedResultsController.fetchRequest setFetchLimit:1];
//    [self.fetchedResultsController performFetch:nil];
//    
//    NSLog(@"fetchedResultsController: %lu", (unsigned long)[self.fetchedResultsController.fetchedObjects count]);
//    if(!self.fetchedResultsController.fetchedObjects.count == 0)
//    {
//        
//        
//        Produktvarianten *pv =[self.fetchedResultsController.fetchedObjects lastObject];
//        
//        
//        if(pv.produkte){
//            
//            
//            produktStaticTabBarViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
//            controller.produkt = (Produkte*)pv.produkte;
//            
//            //                    NSLog(@"pushViewController");
//            [self.navigationController pushViewController:controller animated:YES];
//        } else if(pv.reiniger) {
//            
//            ProduktDetailTabbarViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktDetailTabbarViewController"];
//            controller.reiniger = (Reiniger*)pv.reiniger;
//            
//            //                 NSLog(@"pushViewController");
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//        
//        
//    } else {
//        
//        if([self.barcode containsString:@"http://"]){
//            WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
//            controller.webViewTitle=@"QR-Code eingelesen";
//            controller.pdfFileName=@"qrcode.pdf";
//            controller.urlString=self.barcode;
//            
//            [self.navigationController pushViewController:controller animated:YES];
//            
//        } else {
//            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"EAN nicht gefunden" message:@"Das gewünschte Produkt konnte nicht gefunden werden." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//        }
//        
//        
//    }
//    
//    //clean up BarcodeImage and BarcodeString and BarcodeTextField
//    self.barcode = @"";
//    self.fetchedResultsController = nil;
    




}



#pragma mark - Private method implementation

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    
    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG,AVVideoCodecKey,nil];
    [_stillImageOutput setOutputSettings:outputSettings];
    
    [_captureSession addOutput:_stillImageOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("barcodeQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode128Code,AVMetadataObjectTypeInterleaved2of5Code,AVMetadataObjectTypeQRCode,AVMetadataObjectTypeCode39Code,nil]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:self.barcodeTextViewOutlet.layer.bounds];//_viewPreview
    [self.barcodeTextViewOutlet.layer addSublayer:_videoPreviewLayer];
    
    self.barcode = [NSString new];
    self.resultTextOutlet.text = @"";
    //self.resultImageOutlet.image = nil;
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}


-(void)stopReading:(id)barcode{

//    NSLog(@"StopReading Barcode:%@", barcode);
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    _stillImageOutput =nil;
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
    
    //Hide Toolbar
    self.navigationController.toolbarHidden=YES;
    
    //Rename scanButtonLabel
    [self.scanButtonOutlet setTitle:@"Barcode lesen" forState:UIControlStateNormal];
    
    if([barcode isKindOfClass:[NSString class]]){
       
       self.barcode = barcode;
        
       [self.fetchedResultsController.fetchRequest setFetchLimit:1];
       [self.fetchedResultsController performFetch:nil];
        
//        NSLog(@"fetchedResultsController");
        if(self.fetchedResultsController.fetchedObjects.count != 0)
        {
            
            
            Produktvarianten *pv =[self.fetchedResultsController.fetchedObjects lastObject];
            
            
            if(pv.produkte){
                
                
                produktStaticTabBarViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
                controller.produkt = (Produkte*)pv.produkte;
                
//                    NSLog(@"pushViewController");
                [self.navigationController pushViewController:controller animated:YES];
            } else if(pv.reiniger) {
                
                ProduktDetailTabbarViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktDetailTabbarViewController"];
                controller.reiniger = (Reiniger*)pv.reiniger;
                
//                 NSLog(@"pushViewController");
                [self.navigationController pushViewController:controller animated:YES];
            }
            
            
        } else {
            
            if([self.barcode containsString:@"http://"]){
                WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
                controller.webViewTitle=@"QR-Code eingelesen";
                controller.pdfFileName=@"qrcode.pdf";
                controller.urlString=self.barcode;
                
                [self.navigationController pushViewController:controller animated:YES];

            } else {
                UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"EAN nicht gefunden" andMessage:@"Das gewünschte Produkt konnte nicht gefunden werden." andOKButtonWithCompletionHandler:nil];
                
                [AlertHelper showAlert:alert];
            }
            
            
        }
        
        //clean up BarcodeImage and BarcodeString and BarcodeTextField
        self.barcode = @"";
        self.fetchedResultsController = nil;

    }
    
}

-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
//        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [_audioPlayer prepareToPlay];
    }
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
  
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0 && _isReading==YES) {
        // Get the metadata object.
        
        
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
                if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeEAN13Code]||[[metadataObj type] isEqualToString:AVMetadataObjectTypeEAN8Code] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode128Code] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode] || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode39Code]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            

            [self.resultTextOutlet performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            
            [self performSelectorOnMainThread:@selector(stopReading:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            
            _isReading = NO;
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
        }
    }
    
}




@end
