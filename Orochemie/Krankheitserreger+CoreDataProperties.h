//
//  Krankheitserreger+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Krankheitserreger.h"


NS_ASSUME_NONNULL_BEGIN

@interface Krankheitserreger (CoreDataProperties)

+ (NSFetchRequest<Krankheitserreger *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *art;
@property (nullable, nonatomic, copy) NSString *index;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *risikogruppe;
@property (nullable, nonatomic, copy) NSString *uerbertragungswege;
@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *tstamp;
@property (nullable, nonatomic, retain) NSSet<Checkliste *> *checkliste;
@property (nullable, nonatomic, retain) NSSet<Produkte *> *produkte;

@end

@interface Krankheitserreger (CoreDataGeneratedAccessors)

- (void)addChecklisteObject:(Checkliste *)value;
- (void)removeChecklisteObject:(Checkliste *)value;
- (void)addCheckliste:(NSSet<Checkliste *> *)values;
- (void)removeCheckliste:(NSSet<Checkliste *> *)values;

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet<Produkte *> *)values;
- (void)removeProdukte:(NSSet<Produkte *> *)values;

@end

NS_ASSUME_NONNULL_END
