//
//  Test.h
//  orochemie
//
//  Created by Axel Beckert on 01.10.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Test : NSObject
@property (nonatomic, strong) NSString* hygieneTippId;
@property (nonatomic, strong) NSString* hygieneTippText1;
@property (nonatomic, strong) NSString* hygieneTippText2;
@property (nonatomic, strong) NSString* hygieneTippUrl;
@property (nonatomic, strong) NSString* image;


-(NSDictionary*) asHygieneTippDictionary;
@end
