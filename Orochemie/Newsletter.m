//
//  Newsletter.m
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "Newsletter.h"

@implementation Newsletter
-(NSDictionary *)asNewsletterDictionary
{
    return @{@"newsletterId" : self.newsletterId,  @"newsletterText1" : self.newsletterText1, @"newsletterText2" : self.newsletterText2, @"newsletterUrl" : self.newsletterUrl};
}
@end
