//
//  Branche.m
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "Branche.h"
#import "Produkte.h"


@implementation Branche

@dynamic bild;
@dynamic index;
@dynamic name;
@dynamic produkte;

@end
