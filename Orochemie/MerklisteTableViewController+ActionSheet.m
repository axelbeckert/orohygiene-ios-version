//
//  MerklisteTableViewController+ActionSheet.m
//  orochemie 
//
//  Created by Axel Beckert on 06.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklisteTableViewController+ActionSheet.h"
#import "MerklisteTableViewController+Private.h"


typedef void (^AlertActionHelperBlock)(UIAlertAction* alertAction);

@implementation MerklisteTableViewController (ActionSheet)

- (void)merklistenActionSheet {
    
        __weak typeof(self) weakSelf = self;
        UIAlertController *actionSheet = [AlertHelper getUIActionSheetWithTitle:@"Merkliste bearbeiten:" andMessage:@"Bitte wählen Sie eine Aktion aus!" andCancelButtonWithCompletionHandler:nil];

        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            CGRect sourceRect = CGRectZero;
            sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
            sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
            
            actionSheet.popoverPresentationController.sourceView = self.view;
            actionSheet.popoverPresentationController.sourceRect = sourceRect;
            [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
        }
    
        [actionSheet addAction:[AlertHelper
                                addUIAlertActionToAlertWithTitle:@"Merkliste speichern"
                                andCompletionHandler:^(UIAlertAction *alertAction) {
                                  [weakSelf getSaveMerklisteAlertView];
                                }]];

        [actionSheet addAction:[AlertHelper
                                addUIAlertActionToAlertWithTitle:@"Produkt löschen"
                                andCompletionHandler:^(UIAlertAction *alertAction) {
                                   [weakSelf switchMerklistenTableViewToEditingMode];
                                }]];
        [actionSheet addAction:[AlertHelper
                                addUIAlertActionToAlertWithTitle:@"Merkliste leeren"
                                andCompletionHandler:^(UIAlertAction *alertAction) {
                                   [weakSelf cleanMerkliste];
                                }]];
        [actionSheet addAction:[AlertHelper
                                addUIAlertActionToAlertWithTitle:@"Merkliste versenden"
                               andCompletionHandler:^(UIAlertAction *alertAction) {
                                   weakSelf.merklistenTableViewHelper.desinfektionsArray = weakSelf.desinfektionsArray;
                                   weakSelf.merklistenTableViewHelper.reinigerArray = weakSelf.reinigerArray;
                                   weakSelf.merklistenTableViewHelper.merklistenTitel = @"aktuelle Merkliste";
                                   
                                   [weakSelf.merklistenTableViewHelper sendMerklisteByEmail];
                               }]];
        
        [self presentViewController:actionSheet animated:YES completion:nil];

}


#pragma mark - Merkliste ActionSheet SubAction Merkliste Speichern Actions

-(void) getSaveMerklisteAlertView {
    
    UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Eingabe erforderlich:" andMessage:@"Bitte geben Sie einen Namen für die Merkliste ein" andCancelButtonWithCompletionHandler:nil];
    
    [AlertHelper addTextFieldToUIAlertController:alertView
                                    withPlaceHolderText:@"Bezeichnung der Merkliste"];
    
    [self addOkButtonActionForMerklistenBezeichnungsAlert:alertView];
    
    [self presentViewController:alertView animated:YES completion:nil];
}


-(void) addOkButtonActionForMerklistenBezeichnungsAlert:(UIAlertController *) eingabeMerklistenBezeichnungAlert  {
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *merklistenBezeichnung = eingabeMerklistenBezeichnungAlert.textFields.firstObject;
                                   [self saveItemsOfBasicMerklisteToNewMerklisteWithMerklistenName:merklistenBezeichnung.text];
                                   [self.tableView reloadData];
                                   [self.merklistenTableViewHelper showInfoAlertAndCloseItAfterTwoSecondsWithMessage:@"Die Merkliste wurde gespeichert"];
                                   [self.delegate saveMerklistenDataToMerklistenDB];
                                   [self removeRightBarButtonItemFromMerklistenView];
                                   
                               }];
    
    [eingabeMerklistenBezeichnungAlert addAction:okAction];
}

#pragma mark - Merklisten ActionSheet SubAction Merkliste Speichern OKButtonActions

-(void) saveItemsOfBasicMerklisteToNewMerklisteWithMerklistenName: (NSString *) merklistenName {
    
    Merkliste *merkliste = [Merkliste insertMerklisteInManagedObjectContext: self.delegate.managedObjectContext];
    merkliste.bezeichnung = merklistenName;
    
    NSSet *reinigerSet = [NSSet setWithSet:self.delegate.merkliste.reiniger];
    
    merkliste.reiniger = reinigerSet;
    
    NSSet *desinfektionsmittelSet = [NSSet setWithSet:self.delegate.merkliste.produkte];
    merkliste.produkte = desinfektionsmittelSet;
    
    [self cleanUpBasicMerklisteWithReinigerSet:reinigerSet andWithDesinfektionsmittelSet:desinfektionsmittelSet];
    
}

-(void) cleanUpBasicMerklisteWithReinigerSet: (NSSet *)reinigerSet andWithDesinfektionsmittelSet:(NSSet *)desinfektionsmittelSet{
    
    [self.delegate.merkliste removeReiniger:reinigerSet];
    [self.delegate.merkliste removeProdukte:desinfektionsmittelSet];
    [self.delegate saveContext];
    
    [self.reinigerArray removeAllObjects];
    [self.desinfektionsArray removeAllObjects];
    
}

-(void) removeRightBarButtonItemFromMerklistenView {
    
    self.tabBarController.selectedViewController=[self.tabBarController.viewControllers objectAtIndex:1];
    [self setRightBarButtonItemToNil];
}

#pragma mark - Merkliste ActionSheet SubAction Produkt löschen

-(void) switchMerklistenTableViewToEditingMode {
    
    self.tableView.editing = YES;
    self.tabBarController.navigationItem.rightBarButtonItems = nil;
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(stopEditing)];
    self.tabBarController.navigationItem.rightBarButtonItem = item;
    
}

#pragma mark - Merkliste ActionSheet Merkliste leeren
-(void)cleanMerkliste {

    [self cleanReinigerArray];
    [self cleanDesinfektionsmittelArray];
    [self.delegate saveContext];
    [self getAllProductsFromMerkliste];
    [self getAllreinigerFromMerkliste];
    [self.tableView reloadData];
    
    self.tabBarController.navigationItem.rightBarButtonItems = nil;
    
    [self.merklistenTableViewHelper showInfoAlertAndCloseItAfterTwoSecondsWithMessage:@"Merkliste wurde geleert"];
}

-(void)cleanReinigerArray {
    
    if(self.reinigerArray.count >0){
        NSSet *reinigerSet =[NSSet setWithArray:[self.delegate.merkliste.reiniger allObjects]];
        [self.delegate.merkliste removeReiniger:reinigerSet];
        [self.reinigerArray removeAllObjects];
    }
}

-(void)cleanDesinfektionsmittelArray {
    
    if(self.desinfektionsArray.count >0){
        NSSet *desinfektionsSet =[NSSet setWithArray:[self.delegate.merkliste.produkte allObjects]];
        [self.delegate.merkliste removeProdukte:desinfektionsSet];
        [self.desinfektionsArray removeAllObjects];
    }
}

-(void) getAllProductsFromMerkliste {
    if(self.delegate.merkliste.produkte.count>0){
        [self.desinfektionsArray addObjectsFromArray:[self.delegate.merkliste.produkte allObjects]];
    }
}

-(void) getAllreinigerFromMerkliste {
    if(self.delegate.merkliste.reiniger.count>0){
        [self.reinigerArray addObjectsFromArray:[self.delegate.merkliste.reiniger allObjects]];
    }
}

@end
