//
//  UIImage+base64.m
//  orochemie
//
//  Created by Axel Beckert on 13.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "UIImage+base64.h"

@implementation UIImage (base64)
- (NSString *)base64String {
    NSData * data = [UIImagePNGRepresentation(self) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [NSString stringWithUTF8String:[data bytes]];
}
@end
