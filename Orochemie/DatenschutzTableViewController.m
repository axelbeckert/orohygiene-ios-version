//
//  DatenschutzTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 20.08.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "DatenschutzTableViewController.h"

@interface DatenschutzTableViewController ()

@end

@implementation DatenschutzTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if(self.showDatenschutzFrom == DesinfektionChannel){
        //NSLog(@"Datenschutz vom Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];
        
        
    } else if(self.showDatenschutzFrom == ReinigerChannel){
        
        //NSLog(@"atenschutz vom von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    [self setLinkTextAppereanceOnTextViews];
    [self setTextToDatenschutzTextViews];
    
    self.tableView.estimatedRowHeight = 80.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    self.title=@"orochemie !";

}

-(void)setLinkTextAppereanceOnTextViews{
    

    [self.allgemeierHinweisZumDatenschutz setTintColor:[UIColor blueColor]];
    [self.datenschutzbeauftragter setTintColor:[UIColor blueColor]];
    [self.voraussetzungDatenverarbeitung setTintColor:[UIColor blueColor]];
    [self.datenschutzUndWebsitesDritter setTintColor:[UIColor blueColor]];
    [self.datensicherheit setTintColor:[UIColor blueColor]];
    [self.detailsZurDatenverarbeitung setTintColor:[UIColor blueColor]];
    [self.detailsZurDatenverarbeitung2 setTintColor:[UIColor blueColor]];
    [self.betroffenenrechte setTintColor:[UIColor blueColor]];
    [self.widerspruchsrecht setTintColor:[UIColor blueColor]];
    [self.aktualisierungDatenschutzerklaerung setTintColor:[UIColor blueColor]];
}

-(void)setTextToDatenschutzTextViews{
    
    [self.allgemeierHinweisZumDatenschutz setText:allgemeierHinweisZumDatenschutz];
    [self.datenschutzbeauftragter setText: datenschutzBeauftragter];
    [self.voraussetzungDatenverarbeitung setText:voraussetzungDatenverarbeitung];
    [self.datenschutzUndWebsitesDritter setText:datenschutzUndWebsitesDritter];
    [self.datensicherheit setText:datensicherheit];
    [self.detailsZurDatenverarbeitung setText:detailsZurDatenverarbeitung];
    [self.detailsZurDatenverarbeitung2 setText:detailsZurDatenverarbeitung2];
    [self.betroffenenrechte setText:betroffenenerechte];
    [self.widerspruchsrecht setText:widerspruchsrecht];
    [self.aktualisierungDatenschutzerklaerung setText:aktualisierungDatenschutz];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Datenschutz-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Table view delegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForDatenschutzHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Datenschutz - orochemie GmbH + Co. KG";
    } else if (section == 1)
    {
        label.text = @"1. Allgemeines";
    } else if (section == 2)
    {
        return
        [self headerViewSectionWithAditionalHeightAndLabelText:@"2.Firma und Kontaktdaten des für die Datenverarbeitung Verantwortlichen sowie des betrieblichen Datenschutzbeauftragten"];
    } else if (section == 3)
    {
        label.text = @"3. Voraussetzungen der Datenverarbeitung";
    } else if (section == 4)
    {
        label.text = @"4. Datenschutz und Websites Dritter ";
    } else if (section == 5)
    {
        label.text = @"5. Datensicherheit";
    } else if (section == 6)
    {
        label.text =@"6. Details zur Datenverarbeitung";
 
    } else if (section == 7)
    {
        label.text = @"6. Details zur Datenverarbeitung II";
        
    } else if (section == 8)
    {
        label.text = @"7. Betroffenenrechte";
        
    } else if (section == 9) {
        
        label.text = @"8. Widerspruchsrecht";
        
    } else if (section == 10) {
        
        label.text = @"9. Aktualisierung der Datenschutzerklärung";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section ==2) return 80;
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    NSLog(@"section: %ld -- %ld", (long)[indexPath section],[indexPath row]);
//    return 200;
//}


/**headerViewSectionWithAditionalHeightAndLabelText * headerViewSectionSix
 *
 * Special header view for section headlines with long text
 *
 **/
-(UIView *)headerViewSectionWithAditionalHeightAndLabelText:(NSString*)labelText{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForDatenschutzHeaderViewHeight];
    UILabel *label= container.subviews.lastObject;
    
    label.text = labelText;
    return container;
}







@end
