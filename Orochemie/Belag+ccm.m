//
//  Belag+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Belag+ccm.h"

@implementation Belag (ccm)
+(Belag*) insertBelagInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Belag" inManagedObjectContext:managedObjectContext];
}
-(void)deleteBelag
{
    [self.managedObjectContext deleteObject:self];
}
@end
