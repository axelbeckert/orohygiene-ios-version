//
//  HygieneChecklistenTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 27.02.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HygieneChecklistenTableViewController : UITableViewController
@property (nonatomic,assign) int musterplaene;
-(void) performTableViewRefresh;
@end
