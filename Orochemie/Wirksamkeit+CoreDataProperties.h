//
//  Wirksamkeit+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Wirksamkeit.h"

NS_ASSUME_NONNULL_BEGIN

@interface Wirksamkeit (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *bezeichnung;
@property (nullable, nonatomic, retain) NSString *tstamp;
@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) NSString *wirksam;
@property (nullable, nonatomic, retain) NSNumber *sorting;
@property (nullable, nonatomic, retain) NSSet<Produkte *> *produkte;

@end

@interface Wirksamkeit (CoreDataGeneratedAccessors)

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet<Produkte *> *)values;
- (void)removeProdukte:(NSSet<Produkte *> *)values;

@end

NS_ASSUME_NONNULL_END
