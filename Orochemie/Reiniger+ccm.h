//
//  Reiniger+ccm.h
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Reiniger.h"

@interface Reiniger (ccm)
+(Reiniger*) insertReinigerInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteReiniger;
@end
