//
//  JsonCollector.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

enum JsonCollectorType {
    COMPLETE_OBJECTS = 1,
    ONLY_TSTAMP,
    FROM_FILE
};
typedef enum JsonCollectorType jsonCollectorType;

@interface JsonCollector : NSObject

@property int showSVProgressHUD;
@property (nonatomic, assign) jsonCollectorType collectorType;
@property (nonatomic, strong) NSString* entityName;

-(void) collectJsonWithUrl: (NSString*) collectionUrl andTaskDescription:(NSString*)taskDescription;
-(BOOL) compareLatestTimestampFromDatabaseWithJsonTimestamp:(NSString*)timestamp andEntitiy:(NSString*)entity;
-(BOOL) retrieveObjectsFromDatabaseForEntity;
-(NSDictionary*)retrieveDataFromJsonFile:(NSString*)file;

@end
