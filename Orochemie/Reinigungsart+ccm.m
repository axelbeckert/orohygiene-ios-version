//
//  Reinigungsart+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsart+ccm.h"

@implementation Reinigungsart (ccm)
+(Reinigungsart*) insertReinigungsartInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Reinigungsart class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteReinigungsart{
    [self.managedObjectContext deleteObject:self];
}
@end
