//
//  SectionHeaderTableViewCell.h
//  Orochemie
//
//  Created by Axel Beckert on 28.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *textLabelOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;

@end
