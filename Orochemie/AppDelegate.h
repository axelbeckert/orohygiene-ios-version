//
//  AppDelegate.h
//  Orochemie
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merkliste+ccm.h"

@class ChecklistenCollector;
@class WirksamkeitenCollector;
@class KrankheitserregerCollector;
@class OberflaechenCollector;
@class Logger;




@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSCache* cache;
@property (nonatomic,assign) NSTimeInterval timeToStopLoading;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic,strong) Merkliste *merkliste;
@property (nonatomic,strong) ChecklistenCollector *checklistenCollector;
@property (nonatomic,strong) WirksamkeitenCollector *wirksamkeitenCollector;
@property (nonatomic,strong) KrankheitserregerCollector *krankheitserregerCollector;
@property (nonatomic,strong) OberflaechenCollector *oberflaechenCollector;
@property (nonatomic,strong) Logger *logger;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)saveMerklistenDataToMerklistenDB;

@end
