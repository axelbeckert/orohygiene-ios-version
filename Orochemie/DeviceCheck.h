//
//  DeviceCheck.h
//  orochemie Hygiene
//
//  Created by Axel Beckert on 29.06.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceCheck : NSObject
- (NSString*) deviceName;
@end
