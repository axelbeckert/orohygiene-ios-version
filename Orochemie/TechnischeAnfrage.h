//
//  TechnischeAnfrage.h
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TechnischeAnfrage : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * material;
@property (nonatomic, retain) NSString * objekt;
@property (nonatomic, retain) NSString * problembeschreibung;
@property (nonatomic, retain) NSString * ansprechpartner;
@property (nonatomic, retain) NSString * telefon;

@end
