//
//  MerklistenReiniger+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenReiniger+ccm.h"

@implementation MerklistenReiniger (ccm)
+(MerklistenReiniger*)insertMerklistenProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"MerklistenReiniger" inManagedObjectContext:managedObjectContext];
}
-(void)deleteMerklistenReiniger
{
    [self.managedObjectContext deleteObject:self];
}
@end
