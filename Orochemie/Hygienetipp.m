//
//  Hygienetipp.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "Hygienetipp.h"

@implementation Hygienetipp

-(NSDictionary *)asHygieneTippDictionary
{
    return @{@"hygieneTippId" : self.hygieneTippId,  @"hygieneTippText1" : self.hygieneTippText1, @"hygieneTippText2" : self.hygieneTippText2, @"hygieneTippUrl" : self.hygieneTippUrl};
}
@end
