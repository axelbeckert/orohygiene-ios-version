//
//  HygieneWebViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "HygieneWebViewController.h"
#import "Hygienetipp.h"
#import <QuartzCore/QuartzCore.h>
#import "BNHtmlPdfKit.h"

@interface HygieneWebViewController () <UIWebViewDelegate, BNHtmlPdfKitDelegate>
@property(nonatomic,strong) AppDelegate *appDelegate;
@property(nonatomic,strong) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UIWebView *hygieneTippWebViewOutlet;
@property(nonatomic,assign) BOOL videoIsOn;
@property int loadingCounter;
@property (strong, nonatomic) BNHtmlPdfKit *pdfKit;
@property(nonatomic,strong) UIBarButtonItem* printActionButton;
@end

@implementation HygieneWebViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Title Label
    
    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
    tlabel.text = @"Hygienetipp";
    self.navigationItem.titleView=tlabel;
    
    self.hygieneTippWebViewOutlet.delegate = self;
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    

    
    
    self.printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
    
    NSArray *buttonArray = @[self.printActionButton, [oroThemeManager homeBarButton:self]];

    self.navigationItem.rightBarButtonItems = buttonArray;
 

    
    
    [self loadRequest];
    
	// Do any additional setup after loading the view.
}

-(void)loadRequest
{
    
    if([ConnectionCheck hasConnectivity])
    {
        
        
        
        NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/newsletter_hygienetipp/%@",self.hygienetipp.hygieneTippUrl];
        //NSLog(@"hygieneTippUrl: %@", urlString);
        NSURL *hygieneTippUrl = [NSURL URLWithString:urlString];
        
        [self.hygieneTippWebViewOutlet loadRequest:[NSURLRequest requestWithURL:hygieneTippUrl]];
        
        
        
   
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
    }
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    
    NSString *string = [NSString stringWithFormat:@"%@",request ];
    NSRange range = [string rangeOfString:@"youtube.com"];
    //NSLog(@"shouldStartLoadWithRequest - Range Video: %i",range.length);
    if(range.length!=0)
    {
        self.videoIsOn = YES;
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        if([self.timer isValid]){
            [self.timer invalidate];
            
            //NSLog(@"shouldStartLoadWithRequest - Timer aus");
        }
        
    }
    
    
    return YES;
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde
    
    self.loadingCounter++;
    if(!self.videoIsOn){
        if(![self.timer isValid])
        {
            //NSLog(@"webViewDidStartLoad - Timer an (%i)",self.loadingCounter);
            self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
        }
    }
    
    
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:@"Fehler beim Laden"];
        self.loadingCounter=0;
    //NSLog(@"error:%@",error);
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadingCounter--;
    //NSLog(@"loadingCounter: %i",self.loadingCounter);
    
    
    
    if(self.loadingCounter==0)
    {
        [self.timer invalidate]; //Timer ausschalten
        self.timer=nil;
        self.videoIsOn=NO;
        //NSLog(@"webViewDidFinishLoad - Timer aus");
    }
    
    
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    self.hygieneTippWebViewOutlet.hidden=YES;
    [self.timer invalidate]; //Timer ausschalten
    
    UIAlertController* alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Erneut laden" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self loadRequest];
        [self.timer invalidate];
        self.timer=nil;
        self.hygieneTippWebViewOutlet.hidden=NO;
        [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    }]];
    
    [AlertHelper showAlert:alert];
    [SVProgressHUD dismiss];
    
}

- (IBAction)refreshAction:(id)sender
{
    [self.hygieneTippWebViewOutlet reload];
}

- (IBAction)backAction:(id)sender
{
    [self.hygieneTippWebViewOutlet goBack];
}

- (IBAction)forwardAction:(id)sender
{
    [self.hygieneTippWebViewOutlet goForward];
}

- (IBAction)stopLoadingAction:(id)sender
{
    
    [self.hygieneTippWebViewOutlet stopLoading];
}


- (void)printAction:(id)sender
{
    

    self.pdfKit = [[BNHtmlPdfKit alloc]initWithPageSize:BNPageSizeA4];
    self.pdfKit.delegate = self;
    

    [self.pdfKit saveUrlAsPdf:self.hygieneTippWebViewOutlet.request.URL toFile:nil];
    
	

}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfData:(NSData *)data
{
    
    ////// - neu
    NSURL *fileUrl;
    NSString *pdfFileName = [self.hygienetipp.hygieneTippUrl stringByReplacingOccurrencesOfString:@".html" withString:@".pdf"];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:pdfFileName];
    
    NSString *htmlContent = [NSMutableString stringWithString:[self.hygieneTippWebViewOutlet stringByEvaluatingJavaScriptFromString:@"document.body.outerHTML"]];
    
    NSCachedURLResponse* response = [[NSURLCache sharedURLCache]
                                     cachedResponseForRequest:[self.hygieneTippWebViewOutlet request]];
    
    
    
    if([htmlContent isEqualToString:@""])
    {
        NSData *pdfFile = [response data];
        [pdfFile writeToFile:filePath atomically:YES];
        
        
    } else {
        
        
        NSData *pdfFile = data;
        [pdfFile writeToFile:filePath atomically:YES];
        
    }
    
    fileUrl = [NSURL fileURLWithPath:filePath];
    
    NSString *string = [NSString stringWithFormat:@"Link zur Seite: https://www.orochemie.de/newsletter_hygienetipp/%@.\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.hygienetipp.hygieneTippUrl];
    
    
    
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl,string] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                         //UIActivityTypeCopyToPasteboard,
                                         UIActivityTypePostToWeibo,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         //UIActivityTypeSaveToCameraRoll,
                                         //UIActivityTypeMail,
                                         //UIActivityTypePrint,
                                         UIActivityTypeMessage,
                                         UIActivityTypeAssignToContact,
                                         ];
    
    
    
    NSString *subject = [NSString stringWithFormat:@"oro Hygiene App: %@", pdfFileName];
	[controller setValue:subject forKey:@"subject"];
    
	controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){//NSLog(@"Löschen war nicht erfolgreich");
        };
        
	};
    
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [self.printActionButton valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
	
	[self presentViewController:controller animated:YES completion:nil];
    
 
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error;
{
    //NSLog(@"Error: %@", error.localizedDescription);
}


@end
