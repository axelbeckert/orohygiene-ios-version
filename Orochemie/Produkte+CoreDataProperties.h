//
//  Produkte+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Produkte.h"

NS_ASSUME_NONNULL_BEGIN

@interface Produkte (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *bild;
@property (nullable, nonatomic, retain) NSString *dosierung;
@property (nullable, nonatomic, retain) NSString *einsatzbereich;
@property (nullable, nonatomic, retain) NSString *index;
@property (nullable, nonatomic, retain) NSString *information;
@property (nullable, nonatomic, retain) NSString *informationPdf;
@property (nullable, nonatomic, retain) NSString *listung;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *nameHauptbezeichnung;
@property (nullable, nonatomic, retain) NSString *nameZusatz;
@property (nullable, nonatomic, retain) NSString *pflichttext;
@property (nullable, nonatomic, retain) NSNumber *sortierreihenfolge;
@property (nullable, nonatomic, retain) NSString *uid;
@property (nullable, nonatomic, retain) NSString *wirksamkeit;
@property (nullable, nonatomic, retain) NSString *zusatz;
@property (nullable, nonatomic, retain) NSSet<Anwendung *> *anwendung;
@property (nullable, nonatomic, retain) NSSet<Branche *> *branche;
@property (nullable, nonatomic, retain) NSSet<Gefahrstoffsymbole *> *gefahrstoffsymbole;
@property (nullable, nonatomic, retain) Handlungsanweisung *handlungsanweisung;
@property (nullable, nonatomic, retain) NSSet<Krankheitserreger *> *krankheitserreger;
@property (nullable, nonatomic, retain) NSSet<Merkliste *> *merkliste;
@property (nullable, nonatomic, retain) NSSet<Pictogramm *> *pictogramm;
@property (nullable, nonatomic, retain) NSSet<Produktdosierung *> *produktdosierung;
@property (nullable, nonatomic, retain) NSSet<Produktvarianten *> *produktvarianten;
@property (nullable, nonatomic, retain) Sicherheit *sicherheit;
@property (nullable, nonatomic, retain) NSSet<Video *> *video;
@property (nullable, nonatomic, retain) NSSet<Wirksamkeit *> *wirksamkeiten;

@end

@interface Produkte (CoreDataGeneratedAccessors)

- (void)addAnwendungObject:(Anwendung *)value;
- (void)removeAnwendungObject:(Anwendung *)value;
- (void)addAnwendung:(NSSet<Anwendung *> *)values;
- (void)removeAnwendung:(NSSet<Anwendung *> *)values;

- (void)addBrancheObject:(Branche *)value;
- (void)removeBrancheObject:(Branche *)value;
- (void)addBranche:(NSSet<Branche *> *)values;
- (void)removeBranche:(NSSet<Branche *> *)values;

- (void)addGefahrstoffsymboleObject:(Gefahrstoffsymbole *)value;
- (void)removeGefahrstoffsymboleObject:(Gefahrstoffsymbole *)value;
- (void)addGefahrstoffsymbole:(NSSet<Gefahrstoffsymbole *> *)values;
- (void)removeGefahrstoffsymbole:(NSSet<Gefahrstoffsymbole *> *)values;

- (void)addKrankheitserregerObject:(Krankheitserreger *)value;
- (void)removeKrankheitserregerObject:(Krankheitserreger *)value;
- (void)addKrankheitserreger:(NSSet<Krankheitserreger *> *)values;
- (void)removeKrankheitserreger:(NSSet<Krankheitserreger *> *)values;

- (void)addMerklisteObject:(Merkliste *)value;
- (void)removeMerklisteObject:(Merkliste *)value;
- (void)addMerkliste:(NSSet<Merkliste *> *)values;
- (void)removeMerkliste:(NSSet<Merkliste *> *)values;

- (void)addPictogrammObject:(Pictogramm *)value;
- (void)removePictogrammObject:(Pictogramm *)value;
- (void)addPictogramm:(NSSet<Pictogramm *> *)values;
- (void)removePictogramm:(NSSet<Pictogramm *> *)values;

- (void)addProduktdosierungObject:(Produktdosierung *)value;
- (void)removeProduktdosierungObject:(Produktdosierung *)value;
- (void)addProduktdosierung:(NSSet<Produktdosierung *> *)values;
- (void)removeProduktdosierung:(NSSet<Produktdosierung *> *)values;

- (void)addProduktvariantenObject:(Produktvarianten *)value;
- (void)removeProduktvariantenObject:(Produktvarianten *)value;
- (void)addProduktvarianten:(NSSet<Produktvarianten *> *)values;
- (void)removeProduktvarianten:(NSSet<Produktvarianten *> *)values;

- (void)addVideoObject:(Video *)value;
- (void)removeVideoObject:(Video *)value;
- (void)addVideo:(NSSet<Video *> *)values;
- (void)removeVideo:(NSSet<Video *> *)values;

- (void)addWirksamkeitenObject:(Wirksamkeit *)value;
- (void)removeWirksamkeitenObject:(Wirksamkeit *)value;
- (void)addWirksamkeiten:(NSSet<Wirksamkeit *> *)values;
- (void)removeWirksamkeiten:(NSSet<Wirksamkeit *> *)values;

@end

NS_ASSUME_NONNULL_END
