//
//  ccmViewController.m
//  BarcodeScanner
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ccmViewController.h"
#import "JSMToolBox.h"
#import "Produkte.h"
#import "produktStaticTabBarViewController.h"
 

@interface ccmViewController ()
@property (nonatomic, strong) NSFetchedResultsController *produktFetchedResultsController;

@property (nonatomic,weak) Produkte *produkt;
@property(nonatomic,weak) NSString* barcode;
@end

@implementation ccmViewController

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
     self.title = @"Barcode lesen";
    //self.view.backgroundColor = [UIColor colorWithRed:223.0/255 green:224.0/255 blue:225.0/255 alpha:1.0];
    
    [oroThemeManager cutomizeView:self.view];
    [oroThemeManager customizeButtonImage:self.scanButtonOutlet];
    
    [oroThemeManager customizeLabel:self.titleLabelOutlet];
    
    //[oroThemeManager customizeButtonBackgroundImage:self.scanButtonOutlet];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ScanButtonTouchUpInside:(id)sender {
    
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentViewController:reader animated:YES completion:nil];
    
    
    //[reader release];

}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    //Eingelsener Barcode wird dem property Barcode zugewiesen
    
    self.barcode = symbol.data;
    
    //Suche nach einem - Zeichen im Barcode
    NSCharacterSet *searchCharacters = [NSCharacterSet characterSetWithCharactersInString:@"-"];
    //Start search
    NSRange resultRange = [self.barcode rangeOfCharacterFromSet:searchCharacters];

    //Wenn - Zeichen gefunden dann hänge PZN dran
    if(resultRange.length != 0)
    {
        NSString *zusatzstring = @"PZN";
        zusatzstring = [zusatzstring stringByAppendingString:self.barcode];
        self.barcode = zusatzstring;
        
    }
    self.resultTextOutlet.text = self.barcode;
    
    NSLog(@"eingelesener Barcode %@",self.barcode);
        
    
    // EXAMPLE: do something useful with the barcode image
    self.resultImageOutlet.image =[info objectForKey: UIImagePickerControllerOriginalImage];
    

    //Suche das Produkt anhand des Barcodes
    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];

    if(!self.produktFetchedResultsController.fetchedObjects.count == 0)
    {
        self.produkt =  [self.produktFetchedResultsController.fetchedObjects lastObject];
        produktStaticTabBarViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
        controller.produkt = self.produkt;
        [self.navigationController pushViewController:controller animated:YES];
        [reader dismissViewControllerAnimated:YES completion:nil];

        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"EAN nicht gefunden" message:@"Das gewünschte Produkt konnte nicht gefunden werden. Bitte suchen Sie noch einmal nach Produktnummer oder -name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [reader dismissViewControllerAnimated:YES completion:nil];
    
    }
    
   
    symbol = nil;
    self.barcode = nil;
    reader = nil;
    self.produkt = nil;
    self.produktFetchedResultsController = nil;

    
}
                                    
-(NSFetchedResultsController *)fetchedResultsController
{
        if(self.produktFetchedResultsController !=nil){
            return self.produktFetchedResultsController;
        }
    
    NSLog(@"self.brancheProduktFetchedResultsController startet");
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:cEntityProdukte inManagedObjectContext:[JSMCoreDataHelper managedObjectContext]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produktvarianten.barcode CONTAINS[c] %@",self.barcode];
        
        
        fetchRequest.entity=entityDescription;
        fetchRequest.fetchBatchSize=64;
        fetchRequest.predicate=predicate;
        
        NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:cEntityProdukteName ascending:YES];
        
        NSArray *sortArray= [NSArray arrayWithObject:sortName];
        fetchRequest.sortDescriptors = sortArray;
        
        self.produktFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[JSMCoreDataHelper managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        

    

        return self.produktFetchedResultsController;

        
    
        
        
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
}

-(void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}
@end
