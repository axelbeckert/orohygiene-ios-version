//
//  WebsiteChecklist.m
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "WebsiteChecklist.h"
#import "HygieneChecklistenTableViewController.h"
#import "ChecklistenkategorieCheck.h"
#import "Checkliste+CoreDataProperties.h"

@interface WebsiteChecklist()
@property(nonatomic,strong)AppDelegate* appDelegate;
@property(nonatomic,strong)ChecklistenkategorieCheck *checklistenKategorieCheck;
@property BOOL isDeleted;
@property BOOL isHidden;
@end


@implementation WebsiteChecklist

-(BOOL) addWebsiteChecklistToDatabase:(NSArray*)websiteChecklist{
   
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.checklistenKategorieCheck = [ChecklistenkategorieCheck new];
    self.checklistenKategorieCheck.appDelegate = self.appDelegate;
    
    for (NSDictionary* dictionary in websiteChecklist) {
        [self checkData:dictionary];
        [self addChecklist:dictionary];
    }
    if([self.hygienechecklistenTableViewController respondsToSelector:@selector(performTableViewRefresh)]){
        [self.hygienechecklistenTableViewController performTableViewRefresh];
    }
    
    [self.appDelegate saveContext];
    
    return YES;
}


-(void) addChecklist:(NSDictionary*)checklist{
    
    Checkliste* checkliste = nil;
    NSString* rawChecklistenUId =checklist[@"uid"];
    int checklistUid = [rawChecklistenUId intValue];
    
    checkliste =[self checkChecklisteWithChecklistenID:checklistUid];
    
    if(checkliste!=nil){
        checkliste = [self checkChecklistValues:checkliste ifTheyChangedSinceLastDownload:checklist];
    }
    
    if(checkliste==nil && !self.isDeleted && !self.isHidden){
        checkliste = [self getNewChecklisteWithUid:checklistUid];
        checkliste.kurzbezeichnung = checklist[@"shortDescription"];
        checkliste.name = checklist[@"description"];
        checkliste.url= checklist[@"url"];
        checkliste.tstamp= checklist[@"tstamp"];
        
    }
    if(checkliste != nil) {
        [self.checklistenKategorieCheck addChecklistenCategorieToChecklisteWithData:checklist
     
                                                                  andCheckliste:checkliste ];
    }
}



-(Checkliste*)checkChecklisteWithChecklistenID:(int)checklistenUid{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
    NSPredicate *predicate = NULL;

    predicate = [NSPredicate predicateWithFormat:@"uid =%i AND websitemodel = 1",checklistenUid];

    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    
    Checkliste* checkliste = NULL;
    
    if(results.count){
        checkliste =[results lastObject];
        return checkliste;
    }
    
    return nil;
    
}

-(Checkliste*) getNewChecklisteWithUid:(int)checklistenUid{
    
    Checkliste *checkliste = [Checkliste insertChecklisteInManagedObjectContext:self.appDelegate.managedObjectContext];
    checkliste.uid = [NSNumber numberWithInt:checklistenUid];
    checkliste.websitemodel = @(YES);
    return checkliste;
    
}

-(Checkliste*) checkChecklistValues:(Checkliste*)checkliste ifTheyChangedSinceLastDownload:(NSDictionary*)data{

    if(self.isDeleted || self.isHidden){
        [checkliste deleteCheckliste];
        [self.appDelegate saveContext];
        return nil;
    }
    
    if(![checkliste.kurzbezeichnung isEqualToString:data[@"shortDescription"]]){
        checkliste.kurzbezeichnung = data[@"shortDescription"];
        checkliste.name = data[@"description"];
    }

    if(![checkliste.url isEqualToString:data[@"url"]]){
        checkliste.url = data[@"url"];
    }
    
    return checkliste;
}

-(void) checkData:(NSDictionary*)data
{
    NSString* rawDeletedCheck =data[@"deleted"];
    NSString* rawHiddenCheck =data[@"hidden"];
    
    self.isDeleted =[rawDeletedCheck boolValue];
    self.isHidden = [rawHiddenCheck boolValue];

}

@end
