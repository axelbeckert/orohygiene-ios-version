//
//  VideoPlayerViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 16.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Video.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>


@interface VideoPlayerViewController ()
@property (nonatomic, strong) AVPlayerViewController* moviePlayer;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property  int fullScreenEntered;
@property int movieIsStarted;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraintOutlet;

@end

@implementation VideoPlayerViewController


#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.view.backgroundColor = [UIColor blackColor];


    if (self.moviePlayer ) {
        [self.moviePlayer.view removeFromSuperview];
    }
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

        NSString *filePath = [documentsPath stringByAppendingPathComponent:self.video.datei];
        filePath = [filePath stringByAppendingString:@".mp4"];

    NSURL *videoUrl = [NSURL fileURLWithPath:filePath];
    
    
    if(![self.video.bezeichnung isEqualToString:@""])
    {
//        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoUrl];
//        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
//        self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
//        self.moviePlayer.shouldAutoplay = NO;
//        
//        
//        self.moviePlayer.view.frame = self.imageViewOutlet.frame;
//        self.moviePlayer.view.tag = 100;
        
        self.moviePlayer = [[AVPlayerViewController alloc]init];
        self.moviePlayer.player = [AVPlayer playerWithURL:videoUrl];
        
        
        [self.view addSubview:self.moviePlayer.view];
         self.moviePlayer.showsPlaybackControls = YES;

//        [self.moviePlayer.player play];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             
        
        [tracker set:kGAIScreenName value:@"Video-Player-Detail-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Video"
                                                              action:@"Show"
                                                               label:self.video.bezeichnung
                                                               value:nil] build]];
 
    } else {
        [SVProgressHUD showErrorWithStatus:@"kein Video verfügbar"];
    }
    

}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if(self.tabBarViewController!=nil)
    [self.tabBarViewController setVideoPlayerIsFullscreen:YES];


}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.imageViewOutlet updateConstraintsIfNeeded];
    self.moviePlayer.view.frame = self.imageViewOutlet.frame;
    
    if(![self.video.bezeichnung isEqualToString:@""] ){ //&& self.moviePlayer.player.status!=MPMoviePlaybackStatePlaying
    self.moviePlayer.view.frame = self.imageViewOutlet.frame;
    [self.view addSubview:self.moviePlayer.view];
    [self.moviePlayer.player play];
    }
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Video-Player-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];

}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

     [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
    self.anwendungsVideosTableViewController.watchVideoNow = 0;
    [UIViewController attemptRotationToDeviceOrientation];
    
    

}



-(void)viewDidLayoutSubviews{

    self.moviePlayer.view.frame = self.imageViewOutlet.frame;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interfacorientations

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif  
{
    
//    return self.singleVideoFlag==1 ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskPortrait;
    
    return UIInterfaceOrientationMaskAllButUpsideDown;
    
}

-(BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - Button Actions

- (IBAction)closeButton:(id)sender {
    
    [self.tabBarViewController setVideoPlayerIsFullscreen:NO];
    [self dismissViewControllerAnimated:YES completion:nil];

}


@end
