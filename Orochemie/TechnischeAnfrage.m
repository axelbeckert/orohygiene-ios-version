//
//  TechnischeAnfrage.m
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "TechnischeAnfrage.h"


@implementation TechnischeAnfrage

@dynamic email;
@dynamic image;
@dynamic material;
@dynamic objekt;
@dynamic problembeschreibung;
@dynamic ansprechpartner;
@dynamic telefon;

@end
