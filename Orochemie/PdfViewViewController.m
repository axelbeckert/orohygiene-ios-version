//
//  PdfViewViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 13.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "PdfViewViewController.h"
#import "Produkte.h"

@interface PdfViewViewController () <UIWebViewDelegate, UIAlertViewDelegate>
@property(nonatomic,strong) AppDelegate *appDelegate;
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,assign) BOOL videoIsOn;
@property int loadingCounter;
@property(nonatomic,strong) NSString *pdfFileName;
-(void)loadRequest;
@end

@implementation PdfViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pdfWebViewOutlet.delegate = self;
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
       //NSLog(@"ProduktSegue: %i", self.produktinformationSegue);

    [self loadRequest];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"PDF-View-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)loadRequest
{
    if([ConnectionCheck hasConnectivity])
    {
        if(self.pdfWebViewShow == PDFWebviewShowBetriebsAnweisung || [self.title isEqualToString:@"Betriebsanweisung"])
        {
            
            self.title =@"Betriebsanweisung";
            NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/de/download/betriebsanweisung_%@.pdf",self.produkt.informationPdf];
            
            
            self.pdfFileName = [[NSString alloc]initWithFormat:@"betriebsanweisung_%@.pdf",self.produkt.informationPdf];
            
            NSURL *betriebsanweisungUrl = [NSURL URLWithString:urlString];
            
            [self.pdfWebViewOutlet loadRequest:[NSURLRequest requestWithURL:betriebsanweisungUrl]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker set:kGAIScreenName value:@"PDF-View-Detail-Information"];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"WEB"
                                                                  action:@"Betriebsanweisung"
                                                                   label:self.produkt.informationPdf                                                                   value:nil] build]];
            
        } else if (self.pdfWebViewShow == PDFWebviewShowSicherheitsDatenblatt  || [self.title isEqualToString:@"Sicherheitsdatenblatt"])
        {
            self.title =@"Sicherheitsdatenblatt";
            NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/de/download/datenblatt_%@.pdf",self.produkt.informationPdf];
            
            self.pdfFileName = [[NSString alloc]initWithFormat:@"datenblatt_%@.pdf",self.produkt.informationPdf];
            
            NSURL *sicherheitsdatenblattUrl = [NSURL URLWithString:urlString];
            
            [self.pdfWebViewOutlet loadRequest:[NSURLRequest requestWithURL:sicherheitsdatenblattUrl]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             

            
            [tracker set:kGAIScreenName value:@"PDF-View-Detail-Information"];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"WEB"
                                                                  action:@"Sicherheitsdatenblatt"
                                                                   label:self.produkt.informationPdf                                                                   value:nil] build]];
            
        } else if (self.pdfWebViewShow==PDFWebviewShowProduktInformation   || [self.title isEqualToString:@"Produktinformation"])
        {
            self.title =@"Produktinformation";
            NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/de/download/produktinformation_%@.pdf",self.produkt.informationPdf];
            
            self.pdfFileName = [[NSString alloc]initWithFormat:@"produktinformation_%@.pdf",self.produkt.informationPdf];

            
            NSURL *produktinformationUrl = [NSURL URLWithString:urlString];
            
            [self.pdfWebViewOutlet loadRequest:[NSURLRequest requestWithURL:produktinformationUrl]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             

          
            [tracker set:kGAIScreenName value:@"PDF-View-Detail-Information"];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"WEB"
                                                                  action:@"Produktinformation"
                                                                   label:self.produkt.informationPdf                                                                   value:nil] build]];
        
        } else if (self.pdfWebViewShow == PDFWebviewShowHygieneWissen)
        {
            self.title =@"Hygienewissen";
            self.actionButtonOutlet.enabled=NO;
            NSString *urlString = [NSString stringWithFormat:@"https://www.hygienewissen.de/pages"];
            
            NSURL *hygienewissenUrl = [NSURL URLWithString:urlString];
            
            [self.pdfWebViewOutlet loadRequest:[NSURLRequest requestWithURL:hygienewissenUrl]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             
            
            
            [tracker set:kGAIScreenName value:@"PDF-View-Detail-Information"];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"WEB"
                                                                  action:@"Hygienewissen"
                                                                   label:self.produkt.informationPdf                                                                   value:nil] build]];
        
        }
        
    } else
    {
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    
    NSString *string = [NSString stringWithFormat:@"%@",request ];
    NSRange range = [string rangeOfString:@"youtube.com"];
    //NSLog(@"shouldStartLoadWithRequest - Range Video: %i",range.length);
    if(range.length!=0)
    {
        self.videoIsOn = YES;
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        if([self.timer isValid]){
            [self.timer invalidate];
            
            //NSLog(@"shouldStartLoadWithRequest - Timer aus");
        }
        
    }
    
    
    return YES;
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde
    
    self.loadingCounter++;
    if(!self.videoIsOn){
        if(![self.timer isValid])
        {
            //NSLog(@"webViewDidStartLoad - Timer an (%i)",self.loadingCounter);
            self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
        }
    }
    
    
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:@"Fehler beim Laden"];
     self.loadingCounter=0;
    //NSLog(@"error:%@",error);
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadingCounter--;
    //NSLog(@"loadingCounter: %i",self.loadingCounter);
    
    
    
    if(self.loadingCounter==0)
    {
        [self.timer invalidate]; //Timer ausschalten
        self.timer=nil;
        self.videoIsOn=NO;
        //NSLog(@"webViewDidFinishLoad - Timer aus");
    }
    
    
    [SVProgressHUD dismiss];
}

#pragma mark - AlertView Delegate & Timer Selector
- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"videoIsOn=%i",self.videoIsOn);
    if(!self.videoIsOn)
    {
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        self.pdfWebViewOutlet.hidden=YES;
        [self.timer invalidate]; //Timer ausschalten
        
        UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Erneut Laden" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self loadRequest];
            self.pdfWebViewOutlet.hidden=NO;
            [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
        }]];
        
        
        [AlertHelper showAlert:alert];
        
    }
    
}



-(void)viewDidDisappear:(BOOL)animated
{
   
    [super viewDidDisappear:animated];
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    self.pdfWebViewOutlet = nil;
    [self.timer invalidate];
    self.timer=nil;
}





- (IBAction)shareButtonAction:(id)sender {
    
       //NSString *htmlContent = [NSMutableString stringWithString:[self.pdfWebViewOutlet stringByEvaluatingJavaScriptFromString:@"document.body.outerHTML"]];
    //NSLog(@"Content: %@",htmlContent);

    
    NSData *pdfFile = [NSData dataWithContentsOfURL:self.pdfWebViewOutlet.request.URL];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* filePath = [documentsPath stringByAppendingPathComponent:self.pdfFileName];
    [pdfFile writeToFile:filePath atomically:YES];
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    
    
    
    
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                      UIActivityTypeCopyToPasteboard,
                                      UIActivityTypePostToWeibo,
                                      //UIActivityTypePostToFacebook,
                                      //UIActivityTypePostToTwitter,
                                      //UIActivityTypeSaveToCameraRoll,
                                      //UIActivityTypeMail,
                                      //UIActivityTypePrint,
                                      UIActivityTypeMessage,
                                      UIActivityTypeAssignToContact,
                                      ];
    
    NSString *subject = [NSString stringWithFormat:@"Mail von der oro Hygiene App: %@", self.pdfFileName];
	[controller setValue:subject forKey:@"subject"];
    
	controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){//NSLog(@"Löschen war nicht erfolgreich");
        };
        
        
	};
	
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [sender valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
    
	[self presentViewController:controller animated:YES completion:nil];

}
@end
