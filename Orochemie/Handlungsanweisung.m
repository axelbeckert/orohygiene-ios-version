//
//  Handlungsanweisung.m
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Handlungsanweisung.h"
#import "Produkte.h"


@implementation Handlungsanweisung

@dynamic anweisung;
@dynamic produkte;

@end
