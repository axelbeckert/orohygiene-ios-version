//
//  MerklisteTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//
#import "MerklisteTableViewController.h"
#import "MerklisteTableViewController+Private.h"



@implementation MerklisteTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];

    //Optik
    if(self.showMerklistenTableViewControllerFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
    } else if(self.showMerklistenTableViewControllerFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
    }
    
    //Titel
    self.tabBarController.title = @"aktuelle Merkliste";
    
    //AppDelegate initialiseren
    self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.merklistenTableViewHelper = [[MerklistenTableViewHelper alloc] init];
    self.merklistenTableViewHelper.delegate = self;
    
    //Right Bar button items
    [self prepareRightBarButtonItems];
    
    
    self.savedMerklistenTableViewController.delegate = self;
    
    if(self.tabBarController){
    
        UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 49, 0);
        self.tableView.contentInset = inset;
    }

    //Produkt-Array initalisieren
    self.reinigerArray = [NSMutableArray arrayWithArray:[self.delegate.merkliste.reiniger allObjects]];
    self.desinfektionsArray =[NSMutableArray arrayWithArray:[self.delegate.merkliste.produkte allObjects]];
    //Sections aufbauen
    self.sections = [[NSMutableDictionary alloc]init];
    [self.sections setObject:self.desinfektionsArray forKey:@"0"];
    [self.sections setObject:self.reinigerArray forKey:@"1"];
    
    //SectionTitle aufbauen
    self.sectionTitles = [[NSMutableArray alloc]initWithObjects:@"Desinfektionsmittel", @"Reinigungsmittel",nil];

    
}

-(void)prepareRightBarButtonItems{

     NSMutableArray *buttonArray = [NSMutableArray new];

    
    if([self.delegate.merkliste.reiniger allObjects].count > 0 || [self.delegate.merkliste.produkte allObjects].count > 0){
        [buttonArray addObject:[self callMerklistenActionSheetButton:self]];
    }
    
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.merklistenTabbarController.navigationItem.rightBarButtonItems = buttonArray;

}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Globale-Merkliste"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

   return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
    //NSLog(@"1: Number of Rows in Section: %i - %i ",section, [[self.sections objectForKey:@(section).stringValue] count]);
    return [(NSDictionary*)[self.sections objectForKey:@(section).stringValue] count];
//    if(section==0) return self.desinfektionsArray.count;
//    if (section==1) return self.reinigerArray.count;
//    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ProduktuebersichtTableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:@"ProduktuebersichtTableViewCell"
                                      forIndexPath:indexPath];


    [oroThemeManager customizeTableCellText:cell];
    if(indexPath.section==0){
        Produkte *produkt = [self.desinfektionsArray objectAtIndex:indexPath.row];
        
        cell.titleLabelOutlet.text = produkt.nameHauptbezeichnung;
        cell.descriptionLabelOutlet.text = produkt.nameZusatz;
        cell.subtitleLabelOutlet.text = produkt.zusatz;
        cell.imageViewOutlet.image = [UIImage imageNamed:produkt.bild];
        
        
    } else if(indexPath.section==1) {
        Reiniger *reiniger = [self.reinigerArray objectAtIndex:indexPath.row];
        
        cell.titleLabelOutlet.text = [reiniger.bezeichnung substringToIndex:11];
        cell.descriptionLabelOutlet.text = [reiniger.bezeichnung substringFromIndex:11];
        cell.subtitleLabelOutlet.text = reiniger.zusatz;
        cell.imageViewOutlet.image = [UIImage
                                      imageNamed:[NSString stringWithFormat:@"%@.jpg", reiniger.image]];
        
    }

  return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView
    commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
     forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if(indexPath.section==0){
            Produkte *produkt = [self.desinfektionsArray objectAtIndex:indexPath.row];
            [self.delegate.merkliste removeProdukteObject:produkt];
            [self.desinfektionsArray removeObjectAtIndex:indexPath.row];
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
            [tableView reloadData];
           
            
        }
        
        else if(indexPath.section==1){
            Reiniger *reiniger = [self.reinigerArray objectAtIndex:indexPath.row];
            
            [self.delegate.merkliste removeReinigerObject:reiniger];
            [self.reinigerArray removeObjectAtIndex:indexPath.row];
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
            [tableView reloadData];
        }
        
        [self.delegate saveContext];
        [self.delegate saveMerklistenDataToMerklistenDB];
        
        if(self.reinigerArray.count==0 && self.desinfektionsArray.count==0){
            self.tabBarController.navigationItem.rightBarButtonItems = nil;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    label.text = [self.sectionTitles objectAtIndex:section];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    
    if ([(NSDictionary *)[self.sections objectForKey:@(section).stringValue] count]>0){
        return [oroThemeManager customizeTableViewHeigtForHeaderView];
    } else {
        return 0;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
  if (indexPath.section==0){
        produktStaticTabBarViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
        controller.produkt = [self.desinfektionsArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        [self.navigationController pushViewController:controller animated:YES];
 }  else   if(indexPath.section==1){
     ProduktDetailTabbarViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktDetailTabbarViewController"];
     controller.reiniger = [self.reinigerArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
     [self.navigationController pushViewController:controller animated:YES];
 }
}

-(void)setRightBarButtonItemToNil {
    self.merklistenTabbarController.navigationItem.rightBarButtonItem=nil;
}

#pragma mark - Merkliste Buttons
- (UIBarButtonItem *)callMerklistenActionSheetButton:(id)target {
    
    UIBarButtonItem *MerklistenActionSheetButton = [[UIBarButtonItem alloc]
                                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                    target:self
                                                    action:@selector(merklistenActionSheet)];

    
    return MerklistenActionSheetButton;
}

-(void)stopEditing{

    self.tableView.editing=NO;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
    
    if(!(self.reinigerArray.count==0 && self.desinfektionsArray.count==0)){
    self.tabBarController.navigationItem.rightBarButtonItem = [self callMerklistenActionSheetButton:self];
    }
}

#pragma mark - SavedMerklistenTableViewControllerDelegate

-(void)showRightBarButtonMenueItems{


    self.tabBarController.title = @"aktuelle Merkliste";
    [self prepareRightBarButtonItems];
//    if([self.delegate.merkliste.reiniger allObjects].count > 0 || [self.delegate.merkliste.produkte allObjects].count > 0){
//        self.merklistenTabbarController.navigationItem.rightBarButtonItem = [self callMerklistenActionSheetButton:self];
//    } else {
//        self.merklistenTabbarController.navigationItem.rightBarButtonItem = nil;
//    }
}

-(void)presentAlertController: (UIAlertController*)alert animated:(BOOL)animated completion:(UITableViewControllerBlock)block {
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@"delegate was called with: present ViewController");
}

@end
