//
//  MerklistenTableViewHelper.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MerklisteTableViewController.h"

typedef void(^UITableViewControllerBlock)(UITableViewController*);
typedef void (^AlertActionHelperBlock)(UIAlertAction* alertAction);

@protocol MerklistenTableViewHelperDelegate <NSObject>
-(void)presentAlertController: (UIAlertController*)alert animated:(BOOL)animated completion:(UITableViewControllerBlock)block;
@end


@interface MerklistenTableViewHelper : NSObject

@property (nonatomic, weak) id <MerklistenTableViewHelperDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *reinigerArray;
@property (nonatomic,strong) NSMutableArray *desinfektionsArray;
@property (nonatomic,strong) NSString *merklistenTitel;

-(NSMutableArray *) addAllMerklistenProductsToDesinfektionsmittelArray;
-(NSMutableArray *) addAllMerklistenReinigerToReinigerArray;

-(void)sendMerklisteByEmail;
-(void) showInfoAlertAndCloseItAfterTwoSecondsWithMessage: (NSString *)infoMessage;

+(BOOL) testDatabaseWithDelegate: (AppDelegate*)delegate;
@end
