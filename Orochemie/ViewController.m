//
//  ViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.buttonNewsOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    [self.buttonHygienetippsOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    [self.buttonProdukteOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    [self.buttonDosierrechnerOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    [self.buttonKrankheitserregerOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    [self.buttonUnternehmenOutlet setBackgroundImage:[UIImage imageNamed:@"button.jpg"] forState:UIControlStateNormal];
    
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
