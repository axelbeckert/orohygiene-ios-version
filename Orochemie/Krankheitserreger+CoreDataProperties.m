//
//  Krankheitserreger+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Krankheitserreger+CoreDataProperties.h"

@implementation Krankheitserreger (CoreDataProperties)

+ (NSFetchRequest<Krankheitserreger *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Krankheitserreger"];
}

@dynamic art;
@dynamic index;
@dynamic name;
@dynamic risikogruppe;
@dynamic uerbertragungswege;
@dynamic uid;
@dynamic tstamp;
@dynamic checkliste;
@dynamic produkte;

@end
