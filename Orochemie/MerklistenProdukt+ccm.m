//
//  MerklistenProdukt+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenProdukt+ccm.h"

@implementation MerklistenProdukt (ccm)
+(MerklistenProdukt*)insertMerklistenProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"MerklistenProdukt" inManagedObjectContext:managedObjectContext];
}
-(void)deleteMerklistenProdukt
{
    [self.managedObjectContext deleteObject:self];
}
@end
