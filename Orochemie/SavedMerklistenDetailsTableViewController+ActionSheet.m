//
//  SavedMerklistenDetailsTableViewController+ActionSheet.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "SavedMerklistenDetailsTableViewController+ActionSheet.h"
#import "SavedMerklistenDetailsTableViewController+Private.h"

@implementation SavedMerklistenDetailsTableViewController (ActionSheet)

-(void) sendButtonAction {
    
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *actionSheet = [AlertHelper getUIActionSheetWithTitle:@"Merkliste bearbeiten:" andMessage:@"Bitte wählen Sie eine Aktion aus!" andCancelButtonWithCompletionHandler:nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        CGRect sourceRect = CGRectZero;
        sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
        sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
        
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = sourceRect;
        [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Produkt löschen"
                           andCompletionHandler:^(UIAlertAction *alertAction) {
                             [weakSelf switchMerklistenTableViewToEditingMode];
                           }]];
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Merkliste versenden"
                            andCompletionHandler:^(UIAlertAction *alertAction) {
                                 weakSelf.merklistenTableViewHelper.desinfektionsArray = weakSelf.desinfektionsArray;
                                 weakSelf.merklistenTableViewHelper.reinigerArray = weakSelf.reinigerArray;
                                 weakSelf.merklistenTableViewHelper.merklistenTitel =
                                 [NSString stringWithFormat:@"Merkliste: %@", weakSelf.merkliste.bezeichnung];
                                 [weakSelf.merklistenTableViewHelper sendMerklisteByEmail];
                             }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)switchMerklistenTableViewToEditingMode {
    self.tableView.editing = YES;
    self.navigationItem.rightBarButtonItems=nil;
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(stopMerklistenTableViewEditingMode)];
    
    self.navigationItem.rightBarButtonItem = item;
}

-(void)stopMerklistenTableViewEditingMode{
    
    self.tableView.editing=NO;
    self.navigationItem.rightBarButtonItems=nil;
    
    if(self.reinigerArray.count>0 || self.desinfektionsArray.count>0) {
        
        UIBarButtonItem *sendButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendButtonAction)];
        self.navigationItem.rightBarButtonItem = sendButton;
        
    }
}

@end
