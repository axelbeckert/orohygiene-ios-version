//
//  Sicherheit.h
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte;

@interface Sicherheit : NSManagedObject

@property (nonatomic, retain) NSString * schutzmassnahmen;
@property (nonatomic, retain) Produkte *produkte;

@end
