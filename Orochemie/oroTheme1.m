//
//  oroTestTheme1.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "oroTheme1.h"

@implementation oroTheme1


-(UIImage*)backgroundImage
{
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        return [UIImage imageNamed:@"background_oro1136.png"]; //Iphone 5
    }
    return  [UIImage imageNamed:@"background_oro.png"];
    
}


-(UIColor*)accessoryColor
{
    return [UIColor blackColor];
}

-(UIColor*)tableCellTextColor
{
    return [UIColor blackColor];
}

-(UIImage*)tableViewBackgroundPictureForHeaderView
{
    return [UIImage imageNamed:@"button.png"];
}

-(UIColor*)tableViewTextColorForHeaderView
{
    return [UIColor whiteColor];
}

-(UIFont*)tableViewFontForHeaderView
{
    return [UIFont boldSystemFontOfSize:14];
}

-(UIFont*)tableViewFontForDatenschutzHeaderView
{
    return [UIFont boldSystemFontOfSize:13];
}

-(CGFloat)tableViewHeightForHeaderView
{
    return 33;
}

-(UIImage*)imageforNavigationBar
{
    return [UIImage imageNamed:@"navbar.png"];
}


-(UIImage*)buttonBackgroundImage;
{
    return [UIImage imageNamed:@"button_3d.png"];
}

-(UIImage*)imageForButtonNormal
{
    return [UIImage imageNamed:@"button_3d.png"];

}

-(UIImage*)imageForButtonHighlited
{
    return [UIImage imageNamed:@"button_3d.png"];
}

-(UIColor*)labelTextColor
{
    return [UIColor whiteColor];
}

-(CGFloat)staticTableViewHeightForHeaderView
{
    return 40;
}

-(UIColor*)colorForLabelInTitleView
{
    return [UIColor whiteColor];
}

-(UIFont*)fontForLabelInTitleView
{
    return [UIFont boldSystemFontOfSize:17];
}
@end
