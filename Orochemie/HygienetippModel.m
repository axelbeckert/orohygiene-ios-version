//
//  HygienetippModel.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "HygienetippModel.h"
#import "Hygienetipp.h"

@interface HygienetippModel ()
@property (nonatomic, strong) NSMutableArray* hygieneTipps;
@end

@implementation HygienetippModel

-(NSMutableArray *)hygieneTipps
{
    if (!_hygieneTipps) _hygieneTipps = [[NSMutableArray alloc] init];
    return _hygieneTipps;
}

-(void) addHygieneTipp: (Hygienetipp*) hygienetipp
{
   [self.hygieneTipps addObject:hygienetipp];
}

-(NSInteger) numberOfHygieneTipps
{
    return self.hygieneTipps.count;
}

-(Hygienetipp*) hygieneTippAtIndexPath: (NSIndexPath*) indexPath
{
  return self.hygieneTipps [indexPath.row];
}


-(NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len
{
    return [self.hygieneTipps countByEnumeratingWithState:state objects:buffer count:len];
}

@end



