//
//  Video+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 30.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "Video.h"

@interface Video (ccm)
+(Video*) insertVideoInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteVideo;
@end
