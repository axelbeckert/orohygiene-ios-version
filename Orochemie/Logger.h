//
//  Logger.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logger : NSObject
@property int screenLogging;
@property int isLoggingActive;

-(void)logMessage:(NSString*)message;
@end
