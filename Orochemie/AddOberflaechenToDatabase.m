//
//  AddOberflaechenToDatabase.m
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "AddOberflaechenToDatabase.h"

#import "Oberflaeche+ccm.h"
#import "Logger.h"
#import "OberflaechenUndBelaegeTableViewController.h"
#import "AppDelegate.h"


@interface AddOberflaechenToDatabase()
@property(nonatomic,strong)AppDelegate* appDelegate;
@property (nonatomic,strong) NSMutableArray *mutableOberflaechenArray;
@property (nonatomic,strong) NSMutableArray *productArray;
@property (nonatomic,strong) NSDictionary *rawOberflaechenDictionary;
@property (nonatomic,strong) NSDictionary *rawOberflaecheDictionary;
@property (nonatomic,strong) Oberflaeche *oberflaeche;
@property int oberflacheUid;
@property (nonatomic, strong) NSString *aktuelleOberflaechenKategorie;
@end


@implementation AddOberflaechenToDatabase


- (NSMutableArray *) productArray
{
    if (!_productArray) {
        _productArray = [NSMutableArray new];
    }
    return _productArray;
}


-(BOOL) addOberflaechen:(NSArray*)oberflaechen{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.mutableOberflaechenArray = [NSMutableArray new];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"%lu Oberflächen wurden vom Server geladen", (unsigned long)oberflaechen.count];
        
        [self.appDelegate.logger logMessage:message];
    }
    
    for (NSDictionary* dictionary in oberflaechen) {
        self.aktuelleOberflaechenKategorie = dictionary[@"bezeichnung"];
        self.rawOberflaechenDictionary = dictionary[@"belaege"];
        self.oberflaeche = nil;
        [self addOberflaechen];
        self.rawOberflaechenDictionary=nil;
        self.aktuelleOberflaechenKategorie = @"";
    }
    
    [self compareDownloadedOberflaechenWithStoredOberflaechen];
    
    if([self.oberflaechenUndBelaegeTableViewController respondsToSelector:@selector(performTableViewRefresh)]){
        [self.oberflaechenUndBelaegeTableViewController performTableViewRefresh];
    }
    
    [self.appDelegate saveContext];
    
    return YES;
}


-(void) addOberflaechen{
    
    for (NSDictionary* oberflaeche in self.rawOberflaechenDictionary){
        self.rawOberflaecheDictionary = oberflaeche;
        [self addOberflaeche];
        self.rawOberflaecheDictionary = nil;
    }
    
}

-(void) addOberflaeche{
    
    NSString* rawOberflaechenUId = self.rawOberflaecheDictionary[@"uid"];
    self.oberflacheUid = [rawOberflaechenUId intValue];
    
    [self checkIfOberflaecheExists];
    
    if(self.oberflaeche != nil) {
        Oberflaeche* newOberflaeche = self.oberflaeche;
        [self.mutableOberflaechenArray addObject:newOberflaeche];
        
    }
    
}


-(void)checkIfOberflaecheExists{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Oberflaeche"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%i",self.oberflacheUid];
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    
    if(results.count){
        self.oberflaeche =[results lastObject];
        [self checkOberflaechenValuesIfTheyChangedSinceLastDownload];
        
    } else {
        
        [self retrieveNewOberflaeche];
        
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Neuer Oberflaeche %@ wurde angelegt", self.oberflaeche.bezeichnung];
            [self.appDelegate.logger logMessage:message];
        }
    }
    
}

-(void) retrieveNewOberflaeche{
    
    self.oberflaeche = [Oberflaeche insertOberflaecheInManagedObjectContext:self.appDelegate.managedObjectContext];
    self.oberflaeche.uid = [NSNumber numberWithInt:self.oberflacheUid];
    
    
    [self addValuesToOberflaechenObject];
}

-(void) addValuesToOberflaechenObject{
    
    self.oberflaeche.bezeichnung = self.rawOberflaecheDictionary[@"bezeichnung"];
    self.oberflaeche.kategorie = self.aktuelleOberflaechenKategorie;
    self.oberflaeche.tstamp = self.rawOberflaecheDictionary[@"tstamp"];
    NSString *firstLetter = [self.oberflaeche.bezeichnung substringToIndex:1];
    self.oberflaeche.index = firstLetter;
    
    [self addProductsToOberflaeche];

}


-(void) checkOberflaechenValuesIfTheyChangedSinceLastDownload{
    
    if(![self.oberflaeche.tstamp isEqualToString:self.rawOberflaecheDictionary[@"tstamp"]]){
        [self removeAllProductsFromOberflaeche];
        [self addValuesToOberflaechenObject];
        
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Oberflaeche %@ wurde verändert", self.oberflaeche.bezeichnung];
            
            [self.appDelegate.logger logMessage:message];
        }
    }
}

-(void) removeAllProductsFromOberflaeche{
    
    NSSet *produkteSet = self.oberflaeche.reiniger;
    [self.oberflaeche removeReiniger:produkteSet];
    
}


-(void) addProductsToOberflaeche{
    
    for (NSDictionary* dictReiniger in self.rawOberflaecheDictionary[@"produkte"]) {
        Reiniger *reiniger = nil;
        reiniger = [self getProductFromDatabaseWithAppUid:dictReiniger];
        
        if(reiniger!=nil){
            [self.oberflaeche addReinigerObject:reiniger];
        }
        
    }
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Oberflaeche %@ wurden %lu Produkte zugeordnet", self.oberflaeche.bezeichnung, (unsigned long) self.oberflaeche.reiniger.count];
        
        [self.appDelegate.logger logMessage:message];
    }
}


-(Reiniger*) getProductFromDatabaseWithAppUid:(NSDictionary*)data{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Reiniger"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%@",data[@"appId"]];
    
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Reiniger* reiniger = NULL;
    
    if(results.count){
        reiniger =[results lastObject];
        return reiniger;
    }
    
    return nil;
    
}

-(void)compareDownloadedOberflaechenWithStoredOberflaechen{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Oberflaeche"];
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Oberflaechen-Vergleich. %lu sind gespeichert", (unsigned long) results.count];
        [self.appDelegate.logger logMessage:message];
    }
    
    if(results.count){
        NSMutableArray *finalResults = [NSMutableArray arrayWithArray:results];
        [finalResults removeObjectsInArray:self.mutableOberflaechenArray];
        
        for(Oberflaeche* oberflaeche in finalResults){
            if(self.appDelegate.logger.isLoggingActive==1){
                NSString* message = [NSString stringWithFormat:@"%@ wurde gelöscht", oberflaeche.bezeichnung];
                
                [self.appDelegate.logger logMessage:message];
            }
            [oberflaeche deleteOberflaeche];
        }
        
    }
    
}


@end
