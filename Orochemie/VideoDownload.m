//
//  VideoDownload.m
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VideoDownload.h"
@interface VideoDownload()
@property (nonatomic,strong) NSMutableData *receivedData;

@end

@implementation VideoDownload



-(void)downloadVideos{
   
    NSLog(@"Download started");
    // Create the request.
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.tpwebservice.com/Orochemie-Haende-richtig-desinfizieren.mp4"]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    self.receivedData = [NSMutableData dataWithCapacity: 0];
    
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (!theConnection) {
        // Release the receivedData object.
        self.receivedData = nil;
        NSLog(@"Download did fail");
        // Inform the user that the connection failed.
    }



}


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"%s",__PRETTY_FUNCTION__);

        [self.receivedData appendData:data];
    NSLog(@"receivedData: %lu",(unsigned long)data.length
 );

}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{

    NSLog(@"Response length: %lld", [response expectedContentLength]);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"test.mp4"];
    [self.receivedData writeToFile:filePath atomically:YES];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error.localizedDescription);
}




@end
