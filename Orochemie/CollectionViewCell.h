//
//  CollectionViewCell.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *collectionViewImage;
@property (strong, nonatomic) IBOutlet UIImageView *collectionViewImage2;
@property (strong, nonatomic) IBOutlet UILabel *collectionViewBezeichnungLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *collectionViewProduktbezeichnungLabelOutlet;

@property (strong, nonatomic) IBOutlet UILabel *collectionViewProduktbezeichnung2Outlet;
@end
