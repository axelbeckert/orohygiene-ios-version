//
//  Sicherheit+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Sicherheit.h"

@interface Sicherheit (ccm)
+(Sicherheit*) insertSicherheitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteSicherheit;
@end
