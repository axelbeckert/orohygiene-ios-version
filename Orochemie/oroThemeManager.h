//
//  oroThemeManager.h
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProduktuebersichtTableViewCell.h"

@class AnwendungTableViewCell;
@protocol oroTheme <NSObject>

-(UIImage*)backgroundImage;
-(UIImage*)backgroundImageForCleanerChanel;
-(UIImage*)backgroundImageForSelectChanel;
-(UIColor*)accessoryColor;
-(UIImage*)imageforNavigationBar;
-(UIImage*)buttonBackgroundImage;
-(UIColor*)tableCellTextColor;
-(UIImage*)tableViewBackgroundPictureForHeaderView;
-(UIColor*)tableViewTextColorForHeaderView;
-(UIFont*)tableViewFontForHeaderView;
-(UIFont*)tableViewFontForDatenschutzHeaderView;
-(CGFloat)tableViewHeightForHeaderView;
-(UIImage*)imageForButtonNormal;
-(UIImage*)imageForButtonHighlited;
-(UIColor*)labelTextColor;
-(CGFloat)staticTableViewHeightForHeaderView;
-(UIColor*)colorForLabelInTitleView;
-(UIFont*)fontForLabelInTitleView;

@end

@interface oroThemeManager : NSObject
#pragma mark - Desinfektion
+(id<oroTheme>)sharedTheme;
+(void)setSharedTheme:(id<oroTheme>)inTheme;
+(void)applyTheme;

//TableView
+(void)customizeTableView:(UITableView*)theTableView;
+(void)customizeAccessoryView:(UITableViewCell*)theTableViewCell;
+(void)customizeTableCellText:(ProduktuebersichtTableViewCell*)tableCell;
+(void)customizeTableViewStandardTableViewCellText:(UITableViewCell*)tableCell;
+(CGFloat)customizeTableViewHeigtForHeaderView;
+(UIView*)customizeTableViewViewForHeaderView;
+(UIView*)customizeStaticTableViewViewForHeaderView;
+(CGFloat)customizeStaticTableViewHeigtForHeaderView;
+(void)customizeTableViewAnwendungTableViewCellText:(AnwendungTableViewCell *)tableCell;
+(void)customizeTableViewCellBackground:(UITableViewCell*)tableCell;
+(UIView*)customizeStaticTableViewViewForDatenschutzHeaderView;
+(UIView*)customizeStaticTableViewViewForDatenschutzHeaderViewHeight;

//View
+(void)cutomizeView: (UIView*)theView;


//NavigationBar
+(UIImage*) customizeImageforNavigationBar;

//Button
+(void) customizeButtonImage: (UIButton*)theButton;
+(void) customizeLabel:(UILabel*)theLabel;
+(void)customizeButtonBackgroundImage:(UIImageView*)theImageView;
+(UIImage*)setUiBarButtonImage;
+(UIImage*)setUiToolBarButtonImage;
+(UIBarButtonItem*) homeBarButton:(id)target;
+(UIBarButtonItem*) addToMerklisteBarButton :(id)target;
+(UIBarButtonItem*) printActionButton :(id)target;

//Label
+(UILabel*)customizeTitleLabel;


#pragma mark - Select
+(void)customizeSelectView: (UIView*)theView;

#pragma mark - Reiniger
//TableView
+(void)customizeReinigerTableView:(UITableView*)theTableView;
+(void)customizeCleanerView: (UIView*)theView;
@end
