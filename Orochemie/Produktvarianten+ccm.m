//
//  Produktvarianten+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktvarianten+ccm.h"

@implementation Produktvarianten (ccm)
+(Produktvarianten*) insertProduktvariantenInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Produktvarianten" inManagedObjectContext:managedObjectContext];
}
-(void)deleteProduktvarianten
{
    [self.managedObjectContext deleteObject:self];
}
@end
