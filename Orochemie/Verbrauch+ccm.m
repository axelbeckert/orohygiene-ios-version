//
//  Verbrauch+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Verbrauch+ccm.h"

@implementation Verbrauch (ccm)
+(Verbrauch*) insertVerbrauchInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Verbrauch class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteVerbrauch{
    [self.managedObjectContext deleteObject:self];
}
@end
