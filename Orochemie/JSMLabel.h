//
//  JSMLabel.h
//  orochemie
//
//  Created by Axel Beckert on 25.02.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    JSMLabelAlignmentTop,
    JSMLabelAligmentBottom
    
} JSMLabelAlignment;

@interface JSMLabel : UILabel
@property (nonatomic) JSMLabelAlignment verticalAlignment;
@end
