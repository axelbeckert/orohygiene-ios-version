//
//  AddKrankheitserregerToDataBase.m
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "AddKrankheitserregerToDataBase.h"
#import "Logger.h"
#import "Checklistenkategorie.h"
#import "Checkliste.h"
#import "KrankheitserregerTableViewController.h"

@interface AddKrankheitserregerToDataBase()
@property(nonatomic,strong)AppDelegate* appDelegate;
@property (nonatomic,strong) NSMutableArray *mutableKrankheitserregerArray;
@property (nonatomic,strong) NSMutableArray *productArray;
@property (nonatomic,strong) NSDictionary *rawKrankheitsreregerDictionary;
@property (nonatomic,strong) Krankheitserreger *krankheitserreger;
@property (nonatomic,strong) Checklistenkategorie *checklistenKategorie;
@property int krankheitserregerId;
@end

@implementation AddKrankheitserregerToDataBase

- (NSMutableArray *) productArray
{
    if (!_productArray) {
        _productArray = [NSMutableArray new];
    }
    return _productArray;
}


-(BOOL) addKrankheitserreger:(NSArray *)krankheitserreger{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.mutableKrankheitserregerArray = [NSMutableArray new];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"%lu Krankheisterreger wurden vom Server geladen", (unsigned long)krankheitserreger.count];
        
        [self.appDelegate.logger logMessage:message];
    }
    
    for (NSDictionary* dictionary in krankheitserreger) {
        self.rawKrankheitsreregerDictionary = dictionary;
        self.krankheitserreger = nil;
        [self addKrankheitserreger];
        self.rawKrankheitsreregerDictionary=nil;
    }
    
        [self compareDownloadedKrnakheitserregerWithStoredKrankheitserreger];
    if([self.krankheitserregerTableViewController respondsToSelector:@selector(performTableViewRefresh:)]){
        NSLog(@"PERFOMR KRANKHEITS REFRESH");
            [self.krankheitserregerTableViewController performTableViewRefresh];
        }
    
    
    
    [self.appDelegate saveContext];
    
    return YES;
}


-(void) addKrankheitserreger{
    
    NSString* rawKrankheitserregerUId = self.rawKrankheitsreregerDictionary[@"uid"];
    self.krankheitserregerId = [rawKrankheitserregerUId intValue];
    
    [self checkIfKrankheitserregerExists];
    
    if(self.krankheitserreger != nil) {
        Krankheitserreger* newKrankheisterreger = self.krankheitserreger;
        [self.mutableKrankheitserregerArray addObject:newKrankheisterreger];
        
    }
    
}


-(void)checkIfKrankheitserregerExists{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Krankheitserreger"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%i",self.krankheitserregerId];
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    
    if(results.count){
        self.krankheitserreger =[results lastObject];
        [self checkKrankheitserregerValuesIfTheyChangedSinceLastDownload];
        
    } else {
        
        [self getNewKrankheitserreger];
        
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Neuer Krankheitserreger %@ wurde angelegt", self.krankheitserreger.name];
            
            [self.appDelegate.logger logMessage:message];
        }
    }
    
}

-(void) getNewKrankheitserreger{
    
    self.krankheitserreger = [Krankheitserreger insertKrankheitserregerInManagedObjectContext:self.appDelegate.managedObjectContext];
    self.krankheitserreger.uid = [NSNumber numberWithInt:self.krankheitserregerId];
    
    [self addValuesToKrankheitserregerObject];
}

-(void) addValuesToKrankheitserregerObject{
    
    self.krankheitserreger.name = self.rawKrankheitsreregerDictionary[@"bezeichnung"];
    self.krankheitserreger.art = self.rawKrankheitsreregerDictionary[@"erregerart"];
    self.krankheitserreger.tstamp = self.rawKrankheitsreregerDictionary[@"tstamp"];
    self.krankheitserreger.uerbertragungswege = self.rawKrankheitsreregerDictionary[@"uebertragungswege"];
    self.krankheitserreger.risikogruppe = self.rawKrankheitsreregerDictionary[@"risikogruppe"];
    
    NSString *firstLetter = [self.krankheitserreger.name substringToIndex:1];
    self.krankheitserreger.index = firstLetter;
    
    [self addProductsToKrankheitserreger];
    [self retrieveChecklisten];
    
}


-(void) checkKrankheitserregerValuesIfTheyChangedSinceLastDownload{
    
    if(![self.krankheitserreger.tstamp isEqualToString:self.rawKrankheitsreregerDictionary[@"tstamp"]]){
        [self removeAllProductsFromKrankheitserreger];
        [self removeAllChecklistenFromKrankheitserreger];
        [self addValuesToKrankheitserregerObject];
        
        if(self.appDelegate.logger.isLoggingActive==1){
            NSString* message = [NSString stringWithFormat:@"Krankheitserreger %@ wurde verändert", self.krankheitserreger.name];
            
            [self.appDelegate.logger logMessage:message];
        }
    }

    [self retrieveChecklisten];
    
}



-(void) removeAllProductsFromKrankheitserreger{
    
    NSSet *produkteSet = self.krankheitserreger.produkte;
    [self.krankheitserreger removeProdukte:produkteSet];
    
}


-(void) addProductsToKrankheitserreger{
    

    for (NSDictionary* dictProduct in self.rawKrankheitsreregerDictionary[@"produktArray"]) {
        Produkte *produkt = nil;
        produkt = [self getProductFromDatabaseWithAppUid:dictProduct];
        
        
        if(produkt!=nil){
            [self.krankheitserreger addProdukteObject:produkt];
        }
        
    }
    
//    NSLog(@"---------------------------------------------");
//    NSLog(@"Krankheitserreger--: %@ ", self.krankheitserreger.name);
//    for (Produkte* produkt in self.krankheitserreger.produkte){
//        NSLog(@"Produkt: %@ ",produkt.name);
//    }
//    NSLog(@"---------------------------------------------");
    
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Krankheitserreger %@ wurden %lu Produkte zugeordnet", self.krankheitserreger.name, (unsigned long) self.krankheitserreger.produkte.count];
        
        [self.appDelegate.logger logMessage:message];
    }
}


-(Produkte*) getProductFromDatabaseWithAppUid:(NSDictionary*)data{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Produkte"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%@",data[@"appuid"]];
    
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Produkte* product = NULL;
    
    if(results.count){
        product =[results lastObject];
        return product;
    }
    
    return nil;
    
}

-(void)compareDownloadedKrnakheitserregerWithStoredKrankheitserreger{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Krankheitserreger"];
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(self.appDelegate.logger.isLoggingActive==1){
        NSString* message = [NSString stringWithFormat:@"Krankheisterreger-Vergleich. %lu sind gespeichert", (unsigned long) results.count];
//        NSLog(@"Gespeicherte Krankheitserreger: %@", results);
        [self.appDelegate.logger logMessage:message];
    }
    
    if(results.count){
        NSMutableArray *finalResults = [NSMutableArray arrayWithArray:results];
        [finalResults removeObjectsInArray:self.mutableKrankheitserregerArray];
        
        for(Krankheitserreger* krankheitserreger in finalResults){
            if(self.appDelegate.logger.isLoggingActive==1){
                NSString* message = [NSString stringWithFormat:@"%@ wurde gelöscht", krankheitserreger.name];
                
                [self.appDelegate.logger logMessage:message];
            }
            [krankheitserreger deleteKrankheitserreger];
        }
        
    }
    
}

/*!
 @brief retrieveChecklisten: Retrieves the checklisten from database if they are there. Otherwise it creates new checklists.
 
 @discussion To use it, simply call @c[self retrieveChecklisten];
 */
-(void) retrieveChecklisten {
    
    NSDictionary* checklistenDictionary = self.rawKrankheitsreregerDictionary[@"checklistenArray"];
    
    for (NSDictionary* checklisteDict in checklistenDictionary ){
        Checkliste *checkliste = nil;
        checkliste = [self retrieveChecklisteFormDatabaseWith:checklisteDict];
        
        if(checkliste!=nil){
            [self.krankheitserreger addChecklisteObject:checkliste];
        }
//        else {
//           [self.krankheitserreger addChecklisteObject:[self retrieveNewChecklisteFromData:checklisteDict]];
//        }
        
    }
}


-(Checkliste*) retrieveChecklisteFormDatabaseWith:(NSDictionary*)data{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
    NSPredicate *predicate = NULL;
    
    predicate = [NSPredicate predicateWithFormat:@"uid ==%@",data[@"uid"]];
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Checkliste* checkliste = NULL;
    
    if(results.count){
        checkliste =[results lastObject];
//        NSLog(@"Checkliste gefunden:%@",checkliste.name);
//        [self compareChecklistTimestamp:checkliste withRawDataTimestamp:data];
        return checkliste;
    }
 
    return nil;
    
}

-(void)compareChecklistTimestamp:(Checkliste*) checkliste withRawDataTimestamp:(NSDictionary*)data{
    
    if (![checkliste.tstamp isEqualToString:data[@"tstamp"]]){
//        NSLog(@"Checklist timestamp is different:%@:%@ - %@",checkliste.name,checkliste.tstamp,data[@"tstamp"] );
        [self addValuesToCheckliste:checkliste fromData:data];
        
    }
}

-(void) removeAllChecklistenFromKrankheitserreger{
    
    NSSet *checklistenSet = self.krankheitserreger.checkliste;
    [self.krankheitserreger removeCheckliste:checklistenSet];
    
}

-(Checkliste*) retrieveNewChecklisteFromData:(NSDictionary*)data{
    
    [self retrieveChecklistenKategorieWithData:data];
    
    Checkliste *newCheckliste = [Checkliste insertChecklisteInManagedObjectContext:self.appDelegate.managedObjectContext];
    
    [self addValuesToCheckliste:newCheckliste fromData:data];
    
    return newCheckliste;

}

-(void) addValuesToCheckliste:(Checkliste*) checkliste fromData:(NSDictionary*)data{
    
    [self retrieveChecklistenKategorieWithData:data];
    
    checkliste.uid=data[@"uid"];
    checkliste.kurzbezeichnung=data[@"shortDescription"];
    checkliste.name=data[@"description"];
    checkliste.url=data[@"url"];
    checkliste.tstamp=data[@"tstamp"];
    checkliste.branchmodel = @(NO);
    checkliste.typemodel=@(NO);
    checkliste.websitemodel=@(NO);
    checkliste.checklistenkategorie = self.checklistenKategorie;
    
//    NSLog(@"Kategorie: %@",self.checklistenKategorie);
    
    NSString* rawSortOrder = data[@"appSorting"];
    int sortOrder = [rawSortOrder intValue];
    checkliste.sortierung = @(sortOrder);
    
//    NSLog(@"AddValuesToCheckliste: %@",checkliste);
}

-(void) retrieveChecklistenKategorieWithData:(NSDictionary*)data{
    
    [self retrieveChecklistenKategorieFromDatabaseWithData:data];
    
}

-(void) retrieveChecklistenKategorieFromDatabaseWithData:(NSDictionary*)data{
    
    NSDictionary* checklistenKategorieData = data[@"type"];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checklistenkategorie"];
    NSPredicate *predicate = NULL;
    
    NSString* propertyName = @"bezeichnung";
    NSString* propertyValue = checklistenKategorieData[@"description"];

    predicate = [NSPredicate predicateWithFormat:@"%K == %@",propertyName,propertyValue];
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(results.count){
        self.checklistenKategorie =[results lastObject];

    }

}

@end
