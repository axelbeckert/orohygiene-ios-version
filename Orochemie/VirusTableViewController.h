//
//  VirusTableViewControllerlViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMTableViewController.h"

@interface VirusTableViewController : JSMTableViewController
@end
