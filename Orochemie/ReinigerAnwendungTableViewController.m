//
//  ReinigerAnwendungTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ReinigerAnwendungTableViewController.h"
#import "Anwendung+ccm.h"
#import "ProdukteNachBezeichnungTableViewController.h"

@interface ReinigerAnwendungTableViewController () <UISearchBarDelegate>
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,strong) UISearchBar *searchBar;
@end

@implementation ReinigerAnwendungTableViewController
#pragma mark - NSFechtedResultsController

-(NSFetchedResultsController *) fetchedResultsController{
    if (_fetchedResultsController) return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Anwendung class])];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSPredicate * anwendungPredicate = [NSPredicate predicateWithFormat:@"reinigerAnwendung == YES"];
    fetch.predicate = anwendungPredicate;
    
    fetch.sortDescriptors = @[sortByName];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetch
            managedObjectContext:self.managedObjectContext
            sectionNameKeyPath:@"index"
            cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if(error){
        //NSLog(@"Fehler: %@",error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    [oroThemeManager customizeReinigerTableView:self.tableView];

    self.searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectZero];
    [self.searchBar sizeToFit];
    self.searchBar.delegate = self;
    self.searchBar.translucent = YES;
    self.searchBar.backgroundImage = [UIImage new];
    self.searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    self.searchBar.placeholder = @"Suche";
    
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:self.searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Nach Anwendung";
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Reiniger-Anwendung-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    //NSLog(@"gefetchete Objekte: %i",[_fetchedResultsController fetchedObjects].count);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [self.searchBar sizeToFit];
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif

{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> info = self.fetchedResultsController.sections[section];
    return [info numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Anwendung *anwendung = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    cell.textLabel.text = anwendung.name;
    cell.detailTextLabel.text=anwendung.zusatzbezeichnung;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark - tableView indexTitle
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}
#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

#pragma mark - SerachBar Delegate

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    

    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.name CONTAINS[c] %@", searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Segue Methoden

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"produktNachBezeichnungSegue"]){
        ProdukteNachBezeichnungTableViewController *controller = segue.destinationViewController;
        controller.anwendung = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
    
    
}

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
