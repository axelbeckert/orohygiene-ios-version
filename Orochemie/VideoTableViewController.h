//
//  VideoTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produkte.h"
#import "ProduktStaticTabBarViewController.h"
#import "JSMTableViewController.h"


@interface VideoTableViewController : JSMTableViewController
@property (nonatomic,strong) Produkte *produkt;
@property (nonatomic,strong) Reiniger *reiniger;
@property(nonatomic,strong) NSString *branchenName;
@property(nonatomic,strong) NSString *anwendungName;
@property(nonatomic,strong) produktStaticTabBarViewController *tabBarViewController;
@property(nonatomic,assign) show showVideoTableViewFrom;

@end
