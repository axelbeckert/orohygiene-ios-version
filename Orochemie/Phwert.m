//
//  Phwert.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Phwert.h"
#import "Reiniger.h"


@implementation Phwert

@dynamic bezeichnung;
@dynamic index;
@dynamic reiniger;

@end
