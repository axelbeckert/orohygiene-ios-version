//
//  AddOberflaechenToDatabase.h
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OberflaechenUndBelaegeTableViewController;

@interface AddOberflaechenToDatabase : NSObject
@property (nonatomic, strong) OberflaechenUndBelaegeTableViewController *oberflaechenUndBelaegeTableViewController;
-(BOOL) addOberflaechen:(NSArray*)oberflaechen;
@end
