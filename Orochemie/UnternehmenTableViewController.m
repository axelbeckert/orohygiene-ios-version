//
//  UnternehmenTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "UnternehmenTableViewController.h"
#import "KontaktTableViewController.h"
#import "ImpressumTableViewController.h"
#import "InformationenTableViewCell.h"
#import "DatenschutzTableViewController.h"
#import "WebviewViewController.h"
#import "VideoPlayerViewController.h"
#import "Video.h"
#import "VideoForStaticCalls.h"
#import "EntdeckenSieOrochemieTableViewController.h"
#import "InformationenZurAppStaticTableViewController.h"
#import "BeraterInMeinerNaeheTableViewController.h"
#import "VideoHelper.h"

@interface UnternehmenTableViewController ()
@property (nonatomic,strong) NSArray *unternehmenNavigationPoints;
@property (nonatomic,strong) NSArray *aktuellesNavigationPoints;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) VideoHelper *videoHelper;
@property (nonatomic,strong) NSString *youTube;

@end

@implementation UnternehmenTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(BOOL)shouldAutorotate
{
    return NO;
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //App Delegate
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = delegate.managedObjectContext;
    
    if(self.showUnternehmenFrom == DesinfektionChannel){
        //NSLog(@"Unternehmen von Desinfektion-Kanal");
        [oroThemeManager customizeTableView:self.tableView];

        

        
    
    } else if(self.showUnternehmenFrom == ReinigerChannel){
       
        //NSLog(@"Unternehmen von Reiniger-Kanal");
        [oroThemeManager customizeReinigerTableView:self.tableView];

    }
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.unternehmenNavigationPoints = @[@"Entdecken Sie orochemie",
                                         @"Unternehmensfilm",
                                         @"Hygienetage - Der Film",
                                         @"Hygienetage",
                                         @"Kontakt",
                                         @"Berater in meiner Nähe",
                                         @"Informationen zur App",
                                         @"Impressum",
                                         @"Datenschutzerklärung",
                                         ];
    

    self.videoHelper = [[VideoHelper alloc]init];
    

   





}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Unternehmen-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

        return self.unternehmenNavigationPoints.count;

}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";

        InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


    
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;

        cell.TitleLabelOutlet.text = [self.unternehmenNavigationPoints objectAtIndex:indexPath.row];


    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(indexPath.row == 0)
    {
        EntdeckenSieOrochemieTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EntdeckenSieOrochemieTableViewController"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showEntdeckenSieOrochemieFrom = DesinfektionChannel;
        } else {
            controller.showEntdeckenSieOrochemieFrom = ReinigerChannel;
        }
        [self.navigationController pushViewController:controller animated:YES];
        
    }
//    if (indexPath.row==1){
//        WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
//        controller.urlString = @"https://www.orochemie.de/de/download/orochemie_unternehmenspraesentation.pdf";
//        controller.pdfFileName = @"orochemie_unternehmenspraesentation";
//        controller.webViewTitle = @"Unternehmenspräsentation";
//        controller.loadPdfFile=1;
//
//        [self.navigationController pushViewController:controller animated:YES];
    
//    }
    if(indexPath.row == 1)
    {
        
        NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
        NSString* youTubeVideo = [usingOroHygieneDefaults valueForKey:@"YouTubeVideos"];
        self.youTube = youTubeVideo;
        
        NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
        NSPredicate *videoPredicate = [NSPredicate predicateWithFormat:@"bezeichnung == %@",@"orochemie Unternehmensfilm"];
        
        fetch.predicate = videoPredicate;
        
        NSArray *videoArray = [self.managedObjectContext executeFetchRequest:fetch error:nil];
        
        Video *video = [videoArray lastObject];

        
        if([VideoHelper checkVideoFiles] && ![self.youTube isEqualToString:@"YES"])
        {
        
            VideoPlayerViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
            controller.video=video;
            controller.singleVideoFlag = 1;
            [self.navigationController pushViewController:controller animated:YES];
            //NSLog(@"CheckVideoFiles == YES");
            
        } else if((![VideoHelper checkVideoFiles] && [self.youTube isEqualToString:@"YES"]) || ([VideoHelper checkVideoFiles] && [self.youTube isEqualToString:@"YES"])) {
        
                
                WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
                      //NSLog(@"CheckVideoFiles == NO & YOUTUBE == YES");
                controller.urlString = video.youtube;
                controller.pdfFileName = @"video";
                controller.webViewTitle = @"YouTube-Video";
                
                [self.navigationController pushViewController:controller animated:YES];
        } else {
        
            [self checkVideos];
            //NSLog(@"checkVideos");
        }
        

        
    }     if(indexPath.row == 2)
    {

        NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
        NSString* youTubeVideo = [usingOroHygieneDefaults valueForKey:@"YouTubeVideos"];
        self.youTube = youTubeVideo;
   
        
        NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
        NSPredicate *videoPredicate = [NSPredicate predicateWithFormat:@"bezeichnung == %@",@"orochemie Hygienetage"];
        
        fetch.predicate = videoPredicate;
        
        NSArray *videoArray = [self.managedObjectContext executeFetchRequest:fetch error:nil];
        
        Video *video = [videoArray lastObject];
        
        
        if([VideoHelper checkVideoFiles] && ![self.youTube isEqualToString:@"YES"])
        {
            
            VideoPlayerViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
            controller.video=video;
            controller.singleVideoFlag = 1;
            [self.navigationController pushViewController:controller animated:YES];
            //NSLog(@"CheckVideoFiles == YES");
            
        } else if((![VideoHelper checkVideoFiles] && [self.youTube isEqualToString:@"YES"]) || ([VideoHelper checkVideoFiles] && [self.youTube isEqualToString:@"YES"])) {
            
            
            WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
            //NSLog(@"CheckVideoFiles == NO & YOUTUBE == YES");
            controller.urlString = video.youtube;
            controller.pdfFileName = @"video";
            controller.webViewTitle = @"YouTube-Video";
            
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            
            [self checkVideos];
            //NSLog(@"checkVideos");
        }

        
        
    } else if (indexPath.row == 3)
    {
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerHygieneTage;
        controller.webViewTitle=cWebsiteTitleFuerHygieneTage;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneTage;
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 4)
    {
        KontaktTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"KontaktTableViewController"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showKontaktFrom = DesinfektionChannel;
        } else {
            controller.showKontaktFrom = ReinigerChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }  else if (indexPath.row == 5)
    {
        BeraterInMeinerNaeheTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BeraterInMeinerNaeheTableViewController"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showBeraterInMeinerNaeheFrom = DesinfektionChannel;
        } else {
            controller.showBeraterInMeinerNaeheFrom = ReinigerChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 7)
    {
        ImpressumTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ImpressumTableViewController"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showImpressumFrom = DesinfektionChannel;
        } else {
            controller.showImpressumFrom = ReinigerChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 8)
    {
        DatenschutzTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DatenschutzTableViewController"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showDatenschutzFrom = DesinfektionChannel;
        } else {
            controller.showDatenschutzFrom = ReinigerChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 6)
    {
        InformationenZurAppStaticTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InformationZurApp"];
        
        if(self.showUnternehmenFrom == DesinfektionChannel){
            controller.showInformationenZurAppFrom = DesinfektionChannel;
        } else {
            controller.showInformationenZurAppFrom = ReinigerChannel;
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }
}


-(BOOL)checkVideos {


    
    if(![self.videoHelper checkIfVideosAreThereWithStoryboard:self.storyboard andNavigationController:self.navigationController ]) {
        return YES;
    
    }
    else {
        return NO;
    }
    


    

}
#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
