//
//  CollectionViewTestControllerViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewTestControllerViewController : UICollectionViewController <UICollectionViewDataSource>

@end
