//
//  CustomProduktCell.h
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProduktCell : UITableViewCell
//@property (nonatomic,readonly)  UIImageView *cellImageView; war imageView
@property (nonatomic,retain)  NSString *titleLabel;
@property (nonatomic,retain)  UILabel *descriptionLabel;
@property (nonatomic,retain)  UILabel *subTitleLabel;



@end
