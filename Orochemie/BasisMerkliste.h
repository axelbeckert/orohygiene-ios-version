//
//  BasisMerkliste.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BasisMerkliste : NSManagedObject

@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * produktart;

@end
