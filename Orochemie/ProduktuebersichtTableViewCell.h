//
//  ProdukteuebersichtTableViewCell.h
//  Orochemie
//
//  Created by Axel Beckert on 07.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProduktuebersichtTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabelOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;

@end
