//
//  ReinigungNavifationViewController.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ReinigungNavigationViewController.h"
#import "CollectionReusableView.h"
#import "CollectionViewCell.h"
#import "ProduktSortierungAuswahlTableViewController.h"
#import "AnwendungsVideosTableViewController.h"
#import "ReinigerHilfsmittelTableViewController.h"
#import "UnternehmenTableViewController.h"
#import "AktuellesTableViewController.h"
#import "TechnischeAnfrageTableViewController.h"
#import "OberflaechenUndBelaegeTableViewController.h"
#import "DeviceCheck.h"

@interface ReinigungNavigationViewController ()
@property(nonatomic, strong) NSArray *navigationDataModel;
@property (nonatomic,strong)IBOutlet NSLayoutConstraint *topConstraint;
@end

@implementation ReinigungNavigationViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  [self adjustTopLayoutConstraint];
  [oroThemeManager customizeCleanerView:self.view];
  self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
  self.title = @"Reinigungsmittel";



  self.navigationDataModel = @[ @"Produkte",
                                @"Oberflächen und Beläge",
                                @"Anwendungsvideos",
                                @"Hilfsmittel",
                                @"Unternehmen",
                                @"Aktuelles",
                                ];


}

-(void)adjustTopLayoutConstraint{
    
    DeviceCheck *deviceCheck = [DeviceCheck new];
    
    if([[deviceCheck deviceName] rangeOfString:@"iPad"].location != NSNotFound){
        self.topConstraint.constant = 50;
    } else {
        if ([[deviceCheck deviceName] rangeOfString:@"Plus"].location != NSNotFound) {
            self.topConstraint.constant = 0;
        } else {
            self.topConstraint.constant = 50;
        }
        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Navigations-Schirm-Reinigerkanal"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - CollectionView Delegate

- (NSInteger)numberOfSectionsInCollectionView:
                 (UICollectionView *)collectionView {
  return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return self.navigationDataModel.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
  CollectionReusableView *reusableView = [collectionView
      dequeueReusableSupplementaryViewOfKind:kind
                         withReuseIdentifier:@"CollectionReusableView"
                                forIndexPath:indexPath];

  return reusableView;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {

  CollectionViewCell *cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell"
                                                forIndexPath:indexPath];

  [oroThemeManager customizeButtonBackgroundImage:cell.collectionViewImage2];

  cell.collectionViewImage2.layer.borderColor = [UIColor whiteColor].CGColor;

  CGFloat borderWidth = 1.0f;

  cell.collectionViewImage2.layer.borderWidth = borderWidth;

  cell.collectionViewBezeichnungLabelOutlet.text =
      [self.navigationDataModel objectAtIndex:indexPath.row];

  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  if ([[self.navigationDataModel objectAtIndex:indexPath.item]
          isEqualToString:@"Produkte"]) {
    ProduktSortierungAuswahlTableViewController *controller = [self.storyboard
        instantiateViewControllerWithIdentifier:@"ProduktSortierungAuswahlTableViewController"];

    [self.navigationController pushViewController:controller animated:YES];
  
  } else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Anwendungsvideos"]) {
      AnwendungsVideosTableViewController *controller = [self.storyboard
                                                                 instantiateViewControllerWithIdentifier:@"AnwendungsVideosTableViewController"];
      controller.showVideosFrom = ReinigerChannel;
      
      [self.navigationController pushViewController:controller animated:YES];
  
  }  else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                 isEqualToString:@"Hilfsmittel"]) {
      ReinigerHilfsmittelTableViewController *controller = [self.storyboard
                                                         instantiateViewControllerWithIdentifier:@"ReinigerHilfsmittelTableViewController"];
            
      [self.navigationController pushViewController:controller animated:YES];
  
  } else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Unternehmen"]) {
      UnternehmenTableViewController *controller = [self.storyboard
                                                            instantiateViewControllerWithIdentifier:@"UnternehmenTableViewController"];
      controller.showUnternehmenFrom = ReinigerChannel;
      
      [self.navigationController pushViewController:controller animated:YES];
  
  } else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Aktuelles"]) {
      AktuellesTableViewController *controller = [self.storyboard
                                                    instantiateViewControllerWithIdentifier:@"informationenTableViewController"];
      controller.showAktuellesFrom = ReinigerChannel;
      
      [self.navigationController pushViewController:controller animated:YES];
  
  } else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Technische Anfrage"]) {
      TechnischeAnfrageTableViewController *controller = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"TechnischeAnfrageTableViewController"];

      controller.showTechnischeAnfrageFrom = ReinigerChannel;
      [self.navigationController pushViewController:controller animated:YES];
  
  }  else   if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                 isEqualToString:@"Oberflächen und Beläge"]) {
      OberflaechenUndBelaegeTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OberflaechenUndBelaegeTableViewController"];
      
      [self.navigationController pushViewController:controller animated:YES];
  }
}





#pragma mark - UICollectionViewDelegateFlowLayout

- (CGFloat)collectionView:(UICollectionView *)collectionView
                                 layout:(UICollectionViewLayout *)
    collectionViewLayout
    minimumLineSpacingForSectionAtIndex:(NSInteger)section {
  if ([UIScreen mainScreen].currentMode.size.height == 1136) // Iphone 5
  {
    return 3; //war 12

  } else if ([UIScreen mainScreen].currentMode.size.height == 2048 ||
             [UIScreen mainScreen].currentMode.size.height == 1024 ||
             [UIScreen mainScreen].currentMode.size.height == 768) // Ipad
  {
    return 25;
  }

  return 1;
}

#pragma mark - View Lifecycle

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  if ([self.navigationController.navigationBar isHidden]) {
    [self.navigationController.navigationBar setHidden:NO];
  }
}



#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
