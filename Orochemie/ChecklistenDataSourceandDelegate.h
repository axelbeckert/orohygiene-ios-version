//
//  ChecklistenDataSourceandDelegate.h
//  orochemie
//
//  Created by Axel Beckert on 20.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChecklistenDataSourceandDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray *checklistenArray;
@end
