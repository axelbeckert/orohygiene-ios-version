//
//  Branche.h
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte;

@interface Branche : NSManagedObject

@property (nonatomic, retain) NSString * bild;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *produkte;
@end

@interface Branche (CoreDataGeneratedAccessors)

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet *)values;
- (void)removeProdukte:(NSSet *)values;

@end
