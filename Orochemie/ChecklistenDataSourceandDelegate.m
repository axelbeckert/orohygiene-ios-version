//
//  ChecklistenDataSourceandDelegate.m
//  orochemie
//
//  Created by Axel Beckert on 20.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ChecklistenDataSourceandDelegate.h"
#import "Checkliste.h"

@implementation ChecklistenDataSourceandDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.checklistenArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Checkliste *checkliste = [self.checklistenArray objectAtIndex:indexPath.row];
    cell.textLabel.text = checkliste.kurzbezeichnung;
    cell.detailTextLabel.text = checkliste.name;
    //cell.imageView.image= [UIImage imageNamed:produkt.bild];
    return cell;
    
}


@end
