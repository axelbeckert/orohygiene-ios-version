//
//  WriksamkeitenCollector.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "WirksamkeitenCollector.h"
#import "AppDelegate.h"
#import "HygieneChecklistenTableViewController.h"
#import "AddWirksamkeitenToDatabase.h"


@interface WirksamkeitenCollector () < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >
@property(nonatomic,strong) AppDelegate *appDelegate;
@property int collectWirksamkeitenIsActive;
@end

@implementation WirksamkeitenCollector
NSString *const collectWirksamkeiten=@"collectWirksamkeiten";

-(void) startCollectingWirksamkeiten{
    
    [self collectWirksamkeiten];
    
}


-(void) collectWirksamkeiten{
    [self collectJsonWithUrl:urlWirksamkeiten andTaskDescription:collectWirksamkeiten];
}


-(void)checkIfDownloadProcessIsActive{
    int processActive = 0;
    if(self.collectWirksamkeitenIsActive==1) processActive = 1;
    
    if(processActive==0 && self.showSVProgressHUD==1)[SVProgressHUD dismiss];
}

-(void) checkDownloadTaskDescription:(NSString*)downloadTaskDescription andBuildChecklistsWithData:(NSArray*)data{
    
    if([downloadTaskDescription isEqualToString:collectWirksamkeiten]){

         AddWirksamkeitenToDatabase *addWirksamkeitenToDatabase = [AddWirksamkeitenToDatabase new];
//        modelplanChecklist.hygienechecklistenTableViewController = self.hygienechecklistenTableViewController;
        [addWirksamkeitenToDatabase addWirksamkeiten:data];
        self.collectWirksamkeitenIsActive = 0;
    }
    [self checkIfDownloadProcessIsActive];
    
    NSLog(@"Download Finished: %@", downloadTaskDescription);

}

@end
