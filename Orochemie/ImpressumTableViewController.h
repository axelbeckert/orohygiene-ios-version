//
//  ImpressumTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 04.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImpressumTableViewController : UITableViewController
@property (nonatomic,assign) show showImpressumFrom;

@property (strong, nonatomic) IBOutlet UITextView *impressum;
@property (strong, nonatomic) IBOutlet UITextView *inhalteUnsrerWebsite;
@property (strong, nonatomic) IBOutlet UITextView *linksZuExternenWebsites;
@property (strong, nonatomic) IBOutlet UITextView *urheberrecht;
@property (strong, nonatomic) IBOutlet UITextView *spamNachrichten;

@end
