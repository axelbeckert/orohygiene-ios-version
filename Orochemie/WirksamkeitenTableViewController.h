//
//  WirksamkeitenTableViewController.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WirksamkeitenTableViewController : UITableViewController

@end
