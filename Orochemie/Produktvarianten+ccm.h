//
//  Produktvarianten+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktvarianten.h"

@interface Produktvarianten (ccm)
+(Produktvarianten*) insertProduktvariantenInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteProduktvarianten;
@end
