//
//  AlertHelper.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AlertActionHelperBlock)(UIAlertAction* alertAction);

@interface AlertHelper : NSObject

//AlertView
+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block;
+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message andOKButtonWithCompletionHandler:(AlertActionHelperBlock)block;
+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message OKButtonWithCompletionHandler:(AlertActionHelperBlock)okBlock andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)cancelBlock;
+(UIAlertController *) getUIAlertControllerWithTitle: (NSString *) title andMessage: (NSString*) message;

+(void)addTextFieldToUIAlertController:(UIAlertController *) alertController withPlaceHolderText: (NSString*)placeHolderText;
+(void)addTextFieldToUIAlertController:(UIAlertController *) alertController withPlaceHolderText: (NSString*)placeHolderText andKeyboardType: (UIKeyboardType*)keyboardType;

+(UIAlertAction *) getCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block;

+(UIAlertController*)showInfoAlertWithMessage: (NSString *)infoMessage;
+(void)showAlert: (UIAlertController*)alert;
+(void)showErrorAlert: (NSError*)error;

+(void) showInfoAlertAndCloseItAfterTwoSecondsWithMessage: (NSString *)infoMessage andAction:(dispatch_block_t) action;
//ActionSheet
+(UIAlertController *) getUIActionSheetWithTitle: (NSString *) title andMessage: (NSString*) message andCancelButtonWithCompletionHandler:(AlertActionHelperBlock)block;
+(UIAlertAction*) addUIAlertActionToAlertWithTitle: (NSString*)title andCompletionHandler: (AlertActionHelperBlock)block;
@end
