//
//  MakeData+Reiniger.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MakeData+Reiniger.h"
#import "Video.h"
#import "Produktvarianten.h"
#import "Anwendung+ccm.h"
#import "Gefahrstoffsymbole+ccm.h"
#import "Produktvarianten+ccm.h"
#import "Verbrauch+ccm.h"
#import "Reinigungsart+ccm.h"
#import "Pictogramm+ccm.h"


@interface MakeData()

@end

@implementation MakeData (Reiniger)


- (void)makeReinigerData {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
#pragma mark - Reiniger
    
    Reiniger *hygienereiniger = [Reiniger
                                    insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    hygienereiniger.bezeichnung = @"orochemie® Hygienereiniger";
    hygienereiniger.zusatz =
    @"Intensiv reinigend & verträglich mit desinfizierten Flächen";
    hygienereiniger.image = @"rm_hygienereiniger_gruppe.png";
    hygienereiniger.phwert = @"11-12";
    hygienereiniger.einsatzbereich =
    @"Intensiv- und Unterhaltsreiniger, z. B. für Kunststoffflächen, Feinsteinzeug-Fliesen, Stein- und Kunststoffböden. Verwendbar auf desinfizierten Oberflächen und Böden.";
    hygienereiniger.eigenschaften =
    @"Konzentrat - sparsam im Gebrauch. Keine Verminderung der Desinfektionswirkung von aufgebrachten Desinfektionsmitteln (kein Kleben, kein Verfärben, kein Seifenfehler). Extrem gute Reinigungswirkung. Hygienischer, anhaltender Duft.";
    hygienereiniger.dosierung =
    @"50 ml auf 10 Liter Wasser; bei starker Verschmutzung 100 ml auf 10 Liter Wasser.";
    hygienereiniger.anwendbar = @"Unterhaltsreinigung, Automatenreinigung, manuelle Reinigung, Flächenreinigung - im Wechsel mit Flächendesinfektion";
    hygienereiniger.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    hygienereiniger.hinweise=@"Eignet sich nicht für alkali-empfindliche Flächen (Linoleum, Kautschuk etc.).";
    hygienereiniger.informationPDF =@"hygienereiniger";
    hygienereiniger.uid=@"A9948C27-D5C1-4BC9-96E2-1E3AC6028370";
    hygienereiniger.mindestdosierung=@"50";
    hygienereiniger.index=@"H";

    
    Reiniger *schonreiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    schonreiniger.bezeichnung = @"orochemie® Schonreiniger";
    schonreiniger.zusatz=@"Kraftvoll & materialschonend";
    schonreiniger.image=@"rm_schonreiniger_gruppe.png";
    schonreiniger.einsatzbereich=@"Unterhaltsreiniger für alle abwaschbaren Materialien, Oberflächen und Bodenbeläge. Auch für alkali-empfindliche Materialien (Linoleum, Kautschuk etc.) und Feinsteinzeug-Fliesen. Verwendbar auf desinfizierten Oberflächen und Böden.";
    schonreiniger.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Sehr gute Reinigungswirkung, beste Materialverträglichkeit. Keine Verminderung der Desinfektionswirkung von aufgebrachten Desinfektionsmitteln (kein Kleben, kein Verfärben, kein Seifenfehler).";
    schonreiniger.dosierung=@"25-50 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung höher dosieren.";
    schonreiniger.phwert=@"8-9";
    schonreiniger.anwendbar=@"Unterhaltsreinigung, Automatenreinigung, manuelle Reinigung, Flächenreinigung - im Wechsel mit Flächendesinfektion";
    schonreiniger.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    schonreiniger.hinweise=@"-";
    schonreiniger.informationPDF=@"schonreiniger";
    schonreiniger.uid=@"784823C2-A984-47A2-A4D2-BB8024F94332";
    schonreiniger.mindestdosierung=@"25";
    schonreiniger.index=@"S";
    
    Reiniger *duftreinigerOrange = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    duftreinigerOrange.bezeichnung=@"orochemie® Duftreiniger orange";
    duftreinigerOrange.zusatz=@"Anhaltend frischer Orangenduft";
    duftreinigerOrange.image=@"rm_duftreiniger_gruppe.png";
    duftreinigerOrange.einsatzbereich=@"Neutraler Alkoholreiniger für die tägliche Unterhaltsreinigung von wasser- und alkoholbeständigen Materialien, Oberflächen und Bodenbelägen, z. B. versiegeltes Parkett, Laminat, Granit, Keramik- und Feinsteinzeug-Fliesen.";
    duftreinigerOrange.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Kraftvolle, materialschonende Reinigungswirkung. Trocknet streifenfrei auf, hinterlässt strahlenden Glanz. Entfernt Bleistift- und Kugelschreiber-Verunreinigungen von glatten Flächen. Mit frischem Orangenduft.";
    duftreinigerOrange.dosierung=@"25-50 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung höher dosieren.";
    duftreinigerOrange.phwert=@"6-7";
    duftreinigerOrange.anwendbar=@"Unterhaltsreinigung, Automatenreinigung, manuelle Reinigung";
    duftreinigerOrange.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    duftreinigerOrange.hinweise=@"Versiegelte Holzböden nur nebelfeucht wischen. Vorsicht bei alkoholempfindlichen Flächen, z. B. Acrylglas.";
    duftreinigerOrange.informationPDF=@"duftreiniger_orange";
    duftreinigerOrange.uid=@"5003EB32-7BC2-4268-87E4-F03C52CCC0F0";
    duftreinigerOrange.mindestdosierung=@"25";
    duftreinigerOrange.index=@"D";
    
    Reiniger *wischpflege = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    wischpflege.bezeichnung=@"orochemie® Wischpflege";
    wischpflege.zusatz=@"Reinigt & pflegt zugleich";
    wischpflege.image=@"rm_wischpflege_gruppe.png";
    wischpflege.einsatzbereich=@"Für alle wasserfesten und beschichteten Böden z. B. PVC, Linoleum, offenporige Fliesen, Klinker, versiegeltes Parkett. Auch für Sportböden geeignet.";
    wischpflege.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Kraftvolle Reinigung und wirksame Pflege in einem Arbeitsgang. Auf Seifenbasis. Selbstglänzend ohne Schichtaufbau, polierbar. Rutschhemmend gem. DIN V 18032-2.";
    wischpflege.dosierung=@"20 ml auf 10 Liter Wasser; bei der Einpflege 200 ml auf 10 Liter Wasser.";
    wischpflege.phwert=@"5-6";
    wischpflege.anwendbar=@"Unterhaltsreinigung, Automatenreinigung, manuelle Reinigung";
    wischpflege.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    wischpflege.hinweise=@"Versiegelte Holzböden nur nebelfeucht wischen.";
    wischpflege.informationPDF=@"wischpflege";
    wischpflege.uid=@"3858CC12-7365-4245-8084-36A2BAE9F6D3";
    wischpflege.mindestdosierung=@"20";
    wischpflege.index=@"W";
    
    Reiniger *feinsteinzeugReiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeugReiniger.bezeichnung=@"orochemie® Feinsteinzeugreiniger";
    feinsteinzeugReiniger.zusatz=@"Intensiv reinigend";
    feinsteinzeugReiniger.image=@"rm_feinsteinzeugreiniger_gruppe.png";
    feinsteinzeugReiniger.einsatzbereich=@"Alkalischer Grund- und Unterhaltsreiniger für Feinsteinzeug-Fliesen.";
    feinsteinzeugReiniger.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Extrem gute Reinigungswirkung. Angenehmer Duft.";
    feinsteinzeugReiniger.dosierung=@"50 ml auf 10 Liter Wasser; bei starker Verschmutzung 100 ml auf 10 Liter Wasser.";
    feinsteinzeugReiniger.phwert=@"11-12";
    feinsteinzeugReiniger.anwendbar=@"Unterhaltsreinigung, Feinsteinzeugreinigung, Automatenreinigung, manuelle Reinigung";
    feinsteinzeugReiniger.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    feinsteinzeugReiniger.hinweise=@"Eignet sich nicht für alkali-empfindliche Flächen (Linoleum, Kautschuk etc.).";
    feinsteinzeugReiniger.informationPDF=@"feinsteinzeugreiniger";
    feinsteinzeugReiniger.uid=@"FFF0B14A-F02C-47AF-9189-BA6779D0AFE2";
    feinsteinzeugReiniger.mindestdosierung=@"50";
    feinsteinzeugReiniger.index=@"F";
    
    Reiniger *reinigerTensidfrei = [Reiniger
                                       insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    reinigerTensidfrei.bezeichnung = @"orochemie® Reiniger tensidfrei";
    reinigerTensidfrei.zusatz = @"Vielseitig einsetzbar";
    reinigerTensidfrei.image = @"rm_reiniger_tensidfrei_gruppe.png";
    reinigerTensidfrei.einsatzbereich =
    @"Für die Unterhalts- und Intensivreinigung von abwaschbaren Materialien, Oberflächen und Bodenbelägen. Besonders geeignet für die Reinigung von Feinsteinzeug-Fliesen in Verbindung mit geeigneten Textilien (z.B. Mikrofasertextilien). Hervorragend für die Reinigung von Teppichbelägen und Polstermöbeln. Einsetzbar für die Wand- und Deckenreinigung.";
    reinigerTensidfrei.eigenschaften =
    @"Konzentrat - sparsam im Gebrauch. Kraftvolle, materialschonende Reinigungswirkung. Rückstandsfreie Reinigung.";
    reinigerTensidfrei.dosierung =@"10-100 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung höher dosieren.";
    reinigerTensidfrei.phwert=@"6-7";
    reinigerTensidfrei.anwendbar=@"Unterhaltsreinigung, Teppichreinigung (Garnpadverfahren, Sprühextraktion), Feinsteinzeugreinigung, Automatenreinigung, manuelle Reinigung";
    reinigerTensidfrei.nachhaltigkeit=@"Das Produkt entspricht der OECD-Richtlinie; ist somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    reinigerTensidfrei.hinweise=@"-";
    reinigerTensidfrei.informationPDF=@"reiniger_tensidfrei";
    reinigerTensidfrei.uid=@"D3E9F1D7-B226-4911-997A-508D646E3B4C";
    reinigerTensidfrei.mindestdosierung=@"10";
    reinigerTensidfrei.index=@"R";
    
    Reiniger *neutralreiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    neutralreiniger.bezeichnung=@"orochemie® Neutralreiniger";
    neutralreiniger.zusatz=@"Universell & kraftvoll";
    neutralreiniger.image=@"rm_neutralreiniger_gruppe.png";
    neutralreiniger.einsatzbereich=@"Neutraler, universell einsetzbarer, kraftvoller Reiniger. Die Anwendung erfolgt auf allen abwaschbaren Flächen.";
    neutralreiniger.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Hervorragende Reinigungswirkung. Angenehmer Duft.";
    neutralreiniger.dosierung=@"50 ml auf 10 Liter Wasser; bei starker Verschmutzung 100 ml auf 10 Liter Wasser.";
    neutralreiniger.phwert=@"7-8";
    neutralreiniger.anwendbar=@"Unterhaltsreinigung, manuelle Reinigung";
    neutralreiniger.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    neutralreiniger.hinweise=@"-";
    neutralreiniger.informationPDF=@"neutralreiniger";
    neutralreiniger.uid=@"ECCD5A08-3192-4C11-A7DE-CD942E97DA54";
    neutralreiniger.mindestdosierung=@"50";
    neutralreiniger.index=@"N";
    
    Reiniger *universalReinigerEco = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    universalReinigerEco.bezeichnung=@"orochemie® Universalreiniger eco";
    universalReinigerEco.zusatz=@"Parfüm- & farbstofffrei";
    universalReinigerEco.image=@"rm_universalreiniger_eco_gruppe.png";
    universalReinigerEco.einsatzbereich=@"Neutraler, universell einsetzbarer Reiniger für Fußböden und andere abwaschbare Flächen. Auch für den Lebensmittelbereich geeignet.";
    universalReinigerEco.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Kraftvolle Reinigungswirkung. Parfüm- und farbstofffrei.";
    universalReinigerEco.dosierung=@"50 ml auf 10 Liter Wasser; bei starker Verschmutzung 100 ml auf 10 Liter Wasser";
    universalReinigerEco.phwert=@"6-7";
    universalReinigerEco.anwendbar=@"Unterhaltsreinigung, Küchenreinigung, manuelle Reinigung";
    universalReinigerEco.nachhaltigkeit=@"EU Ecolabel: DE/020/233. Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    universalReinigerEco.hinweise=@"Im Lebensmittelbereich nach der Reinigung  Lebensmittel berührende Flächen mit Trinkwasser spülen.";
    universalReinigerEco.informationPDF=@"universalreiniger";
    universalReinigerEco.ecolabelPDF=@"urkunde_eu_umweltzeichen_oro_uniersalreiniger_eco";
    universalReinigerEco.ecoLabel = @(YES);
    universalReinigerEco.uid=@"5CC5E8A3-FA6F-457D-830B-D924420F755E";
    universalReinigerEco.mindestdosierung=@"50";
    universalReinigerEco.index=@"U";
    
    Reiniger *glasReinigerEco = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    glasReinigerEco.bezeichnung=@"orochemie® Glasreiniger eco";
    glasReinigerEco.zusatz=@"Kraftvoll & streifenfrei";
    glasReinigerEco.image=@"rm_glasreiniger_eco_gruppe.png";
    glasReinigerEco.einsatzbereich=@"Zur Reinigung von Fenstern, Autoscheiben, Spiegeln, Glastischen, etc.";
    glasReinigerEco.eigenschaften=@"Gebrauchsfertige, alkoholhaltige Lösung zum Sprühen. Sparsam im Gebrauch. Hervorragende Reinigungswirkung: Entfernt Fettschmutz, Fingerabdrücke, Nikotinreste, etc. Angenehmer Duft. Reinigt streifen- und rückstandsfrei.";
    glasReinigerEco.dosierung=@"Unverdünnt 3 x aufsprühen pro qm.";
    glasReinigerEco.phwert=@"7-8";
    glasReinigerEco.anwendbar=@"Unterhaltsreinigung, manuelle Reinigung";
    glasReinigerEco.nachhaltigkeit=@"EU Ecolabel: DE/020/232. Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    glasReinigerEco.hinweise=@"Vorsicht bei alkoholempfindlichen Flächen, z. B.  Acrylglas und lackierten Oberflächen. Bei empfindlichen Stellen auf Tuch sprühen, Fläche abwischen und sofort mit trockenem Tuch nachwischen. An schwer einsehbarer Stelle evtl. vorab Verträglichkeit prüfen.";
    glasReinigerEco.informationPDF=@"glasreiniger";
    glasReinigerEco.ecolabelPDF=@"urkunde_eu_umweltzeichen_oro_glasreiniger_eco";
    glasReinigerEco.ecoLabel=@(YES);
    glasReinigerEco.uid=@"2D25066D-5E39-41E4-8134-877E96ACA79D";
    glasReinigerEco.index=@"G";
    
//    Reiniger *kunstlederReiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
//    kunstlederReiniger.bezeichnung=@"orochemie® Kunstlederreinigung und -pflege";
//    kunstlederReiniger.zusatz=@"Materialschonend";
//    kunstlederReiniger.image=@"rm_kunstlederreiniger_gruppe.png";
//    kunstlederReiniger.einsatzbereich=@"Für die porentiefe Reinigung und Pflege von Kunstlederoberflächen.";
//    kunstlederReiniger.eigenschaften=@"Gebrauchsfertiger Reiniger. Intensive Reinigungswirkung: Entfernt Flecken aller Art, Ränder und Verfärbungen.  Sehr gute Materialverträglichkeit – geeignet für alle Kunstlederbezüge.";
//    kunstlederReiniger.dosierung=@"Unverdünnt auf Fläche sprühen.";
//    kunstlederReiniger.phwert=@"3-4";
//    kunstlederReiniger.anwendbar=@"Unterhaltsreinigung, manuelle Reinigung";
//    kunstlederReiniger.nachhaltigkeit=@"Ist ökologisch unbedenklich. Die enthaltenen Tenside entsprechen den Vorgaben zur biologischen Abbaubarkeit. Applikation aus treibgasfreiem Patentspender. Verpackung ist thermisch und stofflich verwertbar.";
//    kunstlederReiniger.hinweise=@"Mit Tuch oder Spezialschwamm verreiben. Überschüssiges Produkt mit trockenem Tuch abwischen. Problemlos einsetzbar bei Kunstleder. Bei anderen Lederoberflächen muss die Materialverträglichkeit an einer unauffälligen Stelle geprüft werden. Für Leder dürfen die Spezialschwämme nicht verwendet werden.";
//    kunstlederReiniger.informationPDF=@"kunstlederreiniger";
//    kunstlederReiniger.uid=@"EB5DF288-C14B-4BB3-9A20-5D75EAA1850F";
//    kunstlederReiniger.index=@"K";
    
    Reiniger *sanitaerReiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReiniger.bezeichnung=@"orochemie® Sanitärreiniger eco";
    sanitaerReiniger.zusatz=@"Kraftvoll & umweltverträglich";
    sanitaerReiniger.image=@"rm_sanitaerreiniger_eco_gruppe.png";
    sanitaerReiniger.einsatzbereich=@"Zur Unterhaltsreinigung von Wand- und Bodenfliesen, Waschbecken, Badewannen aus säurefestem Kunststoff und Email, Duschen, Duschabtrennungen und Toiletten.";
    sanitaerReiniger.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Kraftvolle Reinigungswirkung: Entfernt Wasserflecken, Kalkbeläge, Rückstände von Seifen. Sehr gute Materialverträglichkeit: Enthält keine Scheuerkomponenten. Verwendbar auf desinfizierten Oberflächen und Böden: kein Kleben oder Verfärben. Keine Minderung der Desinfektionswirkung von aufgebrachten Desinfektionsmitteln (kein Seifenfehler). Auf Zitronensäurebasis. Frei von Phosphorsäure und Phosphaten.";
    sanitaerReiniger.dosierung=@"50-100 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung unverdünnt auftragen.";
    sanitaerReiniger.phwert=@"1-2";
    sanitaerReiniger.anwendbar=@"Unterhaltsreinigung, Sanitärreinigung, manuelle Reinigung";
    sanitaerReiniger.nachhaltigkeit=@"EU Ecolabel: DE/020/283. Ist ökologisch unbedenklich, da alle  Tenside bei geeigneter Verdünnung im Abwasser biologisch abbaubar sind. Verpackung ist thermisch und stofflich verwertbar.";
    sanitaerReiniger.hinweise=@"Fugen vorwässern. Nach dem Reinigungsvorgang Fläche gründlich mit Leitungswasser nachspülen. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien. ";
    sanitaerReiniger.informationPDF=@"sanitaerreiniger";
    sanitaerReiniger.uid=@"81A4A0B7-405F-4B43-9B48-5DD868EDC116";
    sanitaerReiniger.mindestdosierung=@"50";
    sanitaerReiniger.ecoLabel=@(YES);
    sanitaerReiniger.ecolabelPDF=@"urkunde_eu_umweltzeichen_oro_sanitaerreiniger_eco";
    sanitaerReiniger.index=@"S";
    
//    Reiniger *sanitaerReinigerViskos = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
//    sanitaerReinigerViskos.bezeichnung=@"orochemie® Sanitärreiniger viskos";
//    sanitaerReinigerViskos.zusatz=@"Hervorragende Flächenhaftung";
//    sanitaerReinigerViskos.image=@"rm_sanitaerreiniger_viskos_gruppe.png";
//    sanitaerReinigerViskos.einsatzbereich=@"Zur Unterhaltsreinigung des kompletten Bad- und Sanitärbereich.";
//    sanitaerReinigerViskos.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Viskos: Haftet länger an senkrechten Flächen und verlängert somit die Einwirkzeit. Kraftvolle Reinigungswirkung: Entfernt Wasserflecken, Kalkbeläge, Rückstände von Seifen. Sehr gute Materialverträglichkeit: Enthalt keine Scheuerkomponenten. Auf Phosphorsäurebasis.";
//    sanitaerReinigerViskos.dosierung=@"50-100 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung unverdünnt auftragen.";
//    sanitaerReinigerViskos.phwert=@"0-1";
//    sanitaerReinigerViskos.anwendbar=@"Unterhaltsreinigung, Sanitärreinigung, manuelle Reinigung";
//    sanitaerReinigerViskos.nachhaltigkeit=@"Ist ökologisch unbedenklich, da alle  Tenside bei geeigneter Verdünnung im Abwasser biologisch abbaubar sind. Verpackung ist thermisch und stofflich verwertbar.";
//    sanitaerReinigerViskos.hinweise=@"Fugen vorwässern. Nach dem Reinigungsvorgang Fläche gründlich mit Leitungswasser nachspülen. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien. ";
//    sanitaerReinigerViskos.informationPDF=@"sanitaerreiniger_viskos";
//    sanitaerReinigerViskos.uid=@"82DDC75D-6686-4973-9607-27C3CC6DE870";
//    sanitaerReinigerViskos.mindestdosierung=@"50";
//    sanitaerReinigerViskos.index=@"S";
    
    Reiniger *sanitaerReinigerKf = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerKf.bezeichnung=@"orochemie® Sanitärreiniger kf";
    sanitaerReinigerKf.zusatz=@"Kennzeichnungsfrei & wohlduftend";
    sanitaerReinigerKf.image=@"rm_sanitaerreiniger_kf_gruppe.png";
    sanitaerReinigerKf.einsatzbereich=@"Zur Unterhaltsreinigung von Wand- und Bodenfliesen, Badewannen aus säurefestem Kunststoff und Email, Duschen, Duschabtrennungen, Waschbecken, WC etc.";
    sanitaerReinigerKf.eigenschaften=@"Wohlduftendes Konzentrat – sparsam im Gebrauch. Entfernt Wasserflecken, Kalkbeläge, Rückstände von Seifen und Urinstein. Sehr gute Materialverträglichkeit bei säurefesten Flächen. Enthält keine Scheuerkomponenten. Verwendbar auf desinfizierten Oberflächen und Böden: kein Kleben oder Verfärben. Keine Minderung der Desinfektionswirkung von aufgebrachten Desinfektionsmitteln (kein Seifenfehler).Auf Zitronensäurebasis. Frei von Phosphorsäure und Phosphaten.";
    sanitaerReinigerKf.dosierung=@"100 ml auf 10 l Wasser. Bei hartnäckigen Verschmutzungen unverdünnt verwenden.";
    sanitaerReinigerKf.phwert=@"2-3";
    sanitaerReinigerKf.anwendbar=@"Unterhaltsreinigung, Sanitärreinigung, manuelle Reinigung, Automatenreinigung";
    sanitaerReinigerKf.nachhaltigkeit=@"Ist ökologisch unbedenklich, da alle Tenside bei geeigneter Verdünnung im Abwasser biologisch abbaubar sind. Verpackung ist thermisch und stofflich verwertbar.";
    sanitaerReinigerKf.hinweise=@"Fugen vorwässern. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien.";
    sanitaerReinigerKf.informationPDF=@"sanitaerreiniger_kf";
    sanitaerReinigerKf.uid=@"7C619E7B-73BF-440F-ACF2-3C4B38B073EC";
    sanitaerReinigerKf.mindestdosierung=@"100";
    sanitaerReinigerKf.index=@"S";
    
    Reiniger *grundReinigerSauer = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    grundReinigerSauer.bezeichnung=@"orochemie® Grundreiniger sauer";
    grundReinigerSauer.zusatz=@"Extra stark";
    grundReinigerSauer.image=@"rm_grundreiniger_sauer_gruppe.png";
    grundReinigerSauer.einsatzbereich=@"Zur kraftvollen Grundreinigung von Wand- und Bodenfliesen, Waschbecken, Badewannen aus säurefestem Kunststoff und Email, Duschen, Duschabtrennungen und Toiletten; auch einsetzbar auf Feinsteinzeugfliesen nach vorangegangener Wässerung und im Schwimmbadbereich.";
    grundReinigerSauer.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Entfernt Wasserflecken, Kalkbeläge, Rückstände von Seifen und Urinstein, Rost und Kesselstein. Auf Phosphorsäurebasis. ";
    grundReinigerSauer.dosierung=@"50-100 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung unverdünnt auftragen.";
    grundReinigerSauer.phwert=@"0-1";
    grundReinigerSauer.anwendbar=@"Grundreinigung, Sanitärreinigung, Feinsteinzeugreinigung, manuelle Reinigung";
    grundReinigerSauer.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    grundReinigerSauer.hinweise=@"Fugen vorwässern. Nach dem Reinigungsvorgang Fläche gründlich mit Leitungswasser nachspülen. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien. ";
    grundReinigerSauer.informationPDF=@"grundreiniger_sauer";
    grundReinigerSauer.uid=@"9701C9FF-A0B7-470B-8F80-2111C8C2FF24";
    grundReinigerSauer.mindestdosierung=@"50";
    grundReinigerSauer.index=@"G";
    
    Reiniger *wcReiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    wcReiniger.bezeichnung=@"orochemie® WC-Reiniger";
    wcReiniger.zusatz=@"Mit Frischeduft";
    wcReiniger.image=@"rm_wc_reiniger.png";
    wcReiniger.einsatzbereich=@"Für die Grund- und Unterhaltsreinigung von Toiletten und Bidets.";
    wcReiniger.eigenschaften=@"Gebrauchsfertig -  sparsam in der Anwendung. Auf Zitronensäurebasis. Hervorragende Reinigungswirkung, sehr gute Materialverträglichkeit. Viskos: Haftet länger an senkrechten Flächen und verlängert somit die Einwirkzeit. Mit frischem Duft.";
    wcReiniger.dosierung=@"Unverdünnt anwenden.";
    wcReiniger.phwert=@"2-3";
    wcReiniger.anwendbar=@"Unterhaltsreiniung, Sanitärreinigung, manuelle Reinigung";
    wcReiniger.nachhaltigkeit=@"Ist ökologisch unbedenklich, da alle  Tenside bei geeigneter Verdünnung im Abwasser biologisch abbaubar sind. Verpackung ist thermisch und stofflich verwertbar.";
    wcReiniger.hinweise=@"Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien. ";
    wcReiniger.informationPDF=@"wc_reiniger";
    wcReiniger.uid=@"C2C088F1-A3D1-4343-A323-9DB62144F1C3";
    wcReiniger.index=@"W";
    
//    Reiniger *kalkloeser = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
//    kalkloeser.bezeichnung=@"orochemie® Kalklöser";
//    kalkloeser.zusatz=@"Umweltschonend mit Zitronensäure";
//    kalkloeser.image=@"rm_kalkloeser_kueche_gruppe.png";
//    kalkloeser.einsatzbereich=@"Intensiv- und Grundreiniger für säurefeste Fußböden, Flächen und Gegenstände im Lebensmittel-, Industrie- und Sanitärbereich.";
//    kalkloeser.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Gründliche Reinigungswirkung: Entfernt Kalk, Wasserflecken, Seifenreste. Geeignet für die Geräteentkalkung. Umweltschonend, frei von Phosphorsäure und Phosphaten.Auf Zitronensäurebasis. Farbstofffrei, keine Geruchsbelästigung - daher für den Lebensmittelbereich geeignet.";
//    kalkloeser.dosierung=@"100 ml auf 10 Liter Wasser; bei hartnäckiger Verschmutzung unverdünnt auftragen.";
//    kalkloeser.phwert=@"1-2";
//    kalkloeser.anwendbar=@"Küchenreinigung, manuelle Reinigung";
//    kalkloeser.nachhaltigkeit=@"Fugen vorwässern. Nach dem Reinigungsvorgang Fläche gründlich mit Leitungswasser nachspülen. Im Lebensmittelbereich nach der Reinigung  Lebensmittel berührende Flächen und Geräte mit Trinkwasser spülen. Hinweise des Geräteherstellers beachten. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien (Aluminium etc.). ";
//    kalkloeser.hinweise=@"Fugen vorwässern. Nach dem Reinigungsvorgang Fläche gründlich mit Leitungswasser nachspülen. Im Lebensmittelbereich nach der Reinigung  Lebensmittel berührende Flächen und Geräte mit Trinkwasser spülen. Hinweise des Geräteherstellers beachten. Nicht anwenden auf Marmor, Kalksteinflächen und anderen säureempfindlichen Materialien (Aluminium etc.). ";
//    kalkloeser.informationPDF=@"kalkloeser";
//    kalkloeser.uid=@"7FF1F71A-E6B4-4DC1-B5CF-3C2A032A88B4";
//    kalkloeser.mindestdosierung=@"100";
//    kalkloeser.index=@"K";
    
    Reiniger *fettloeser = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    fettloeser.bezeichnung=@"orochemie® Fettlöser";
    fettloeser.zusatz=@"Extra starke Fettlösekraft";
    fettloeser.image=@"rm_fettloeser_kueche_gruppe.png";
    fettloeser.einsatzbereich=@"Extra starker Reiniger zur manuellen und maschinellen Entfernung von Fettablagerungen und anderen alkalilöslichen Verunreinigungen auf Fußböden, Wandfliesen, Arbeitsplatten, etc. in Küchen, Metzgereien und anderen Lebensmittel verarbeitenden Betrieben. Auch  für die Industriereinigung geeignet.";
    fettloeser.eigenschaften=@"Konzentrat - sparsam im Gebrauch. Sehr gute Materialverträglichkeit bei alkalibeständigen Oberflächen (Edelstahl, Kunststoffe wie Polyethylen, Polypropylen etc., Keramik, Glaskeramik, glasierte Fliesen u. ä.). Farbstofffrei, keine Geruchsbelästigung - daher für den Lebensmittelbereich geeignet.";
    fettloeser.dosierung=@"100 ml auf 10 Liter Wasser; bei starker Verschmutzung bis zu 500 ml auf 10 Liter Wasser.";
    fettloeser.phwert=@"12-13";
    fettloeser.anwendbar=@"Küchenreinigung, Automatenreinigung, manuelle Reinigung";
    fettloeser.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    fettloeser.hinweise=@"Im Lebensmittelbereich nach der Reinigung  Lebensmittel berührende Flächen mit Trinkwasser spülen. Bei Anwendung auf alkaliempfindlichen Flächen Materialprüfung an unauffälliger Stelle durchführen.";
    fettloeser.informationPDF=@"fettloeser";
    fettloeser.uid=@"BCD521E3-B024-4402-8BE9-5F3B7E072537";
    fettloeser.mindestdosierung=@"100";
    fettloeser.index=@"F";
    
//    Reiniger *ofenReiniger = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
//    ofenReiniger.bezeichnung=@"orochemie® Ofen- und Grillreiniger";
//    ofenReiniger.zusatz=@"Entfernt angebrannte Rückstände";
//    ofenReiniger.image=@"rm_ofen_grillreiniger.png";
//    ofenReiniger.einsatzbereich=@"Extra starker Spezialreiniger zur Entfernung von Fettablagerungen und angebrannten  Rückständen. Geeignet für Backöfen, Konvektomaten, Dunstabzugshauben etc.";
//    ofenReiniger.eigenschaften=@"Gebrauchsfertig - einfach in der Handhabung. Sehr gute Materialverträglichkeit bei alkalibeständigen Oberflächen (Edelstahl, Kunststoff, Keramik, Glaskeramik u. ä.)";
//    ofenReiniger.dosierung=@"Unverdünnt auf Fläche sprühen.";
//    ofenReiniger.phwert=@"13-14";
//    ofenReiniger.anwendbar=@"Küchenreinigung, manuelle Reinigung";
//    ofenReiniger.nachhaltigkeit=@"Ist ökologisch unbedenklich, da alle organischen Inhaltsstoffe biologisch abbaubar sind. Verpackung ist thermisch und stofflich verwertbar.";
//    ofenReiniger.hinweise=@"Nach Reinigung gründlich mit Trinkwasser nachspülen. Nicht geeignet für alkaliempfindliche Oberflächen (z. B. Aluminium, Messing) oder lackierte Oberflächen.";
//    ofenReiniger.informationPDF=@"ofen_grillreiniger";
//    ofenReiniger.uid=@"5EBB42BE-36B9-4CD0-B768-C2ADF4481755";
//    ofenReiniger.index=@"O";
    
    Reiniger *bodenbeschichtungUniversal = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    bodenbeschichtungUniversal.bezeichnung=@"orochemie® Bodenbeschichtung universal";
    bodenbeschichtungUniversal.zusatz=@"Beständig, rutschhemmend, seidenmatt";
    bodenbeschichtungUniversal.image=@"rm_bodenbeschichtung_universal_gruppe.png";
    bodenbeschichtungUniversal.einsatzbereich=@"Universell einsetzbare, äußerst strapazierbare Bodenbeschichtung für wasserfeste und beschichtbare Fußbodenbeläge außer Holz, Kork, Laminat und PU-versiegelte Bodenbeläge. Bestens geeignet für stark frequentierte Flächen.";
    bodenbeschichtungUniversal.eigenschaften=@"Konzentrat. Auf der Basis von Acrylat-Copolymeren. Mit sehr guter Bodenhaftung und Deckkraft. Erzeugt seidenmattes Erscheinungsbild und gepflegte Optik. Rutschhemmend, trittsicher, polierbar. Angenehmer Duft.";
    bodenbeschichtungUniversal.dosierung=@"empfohlene Dosierung: Unverdünnt als Konzentrat auftragen.";
    bodenbeschichtungUniversal.phwert=@"8-9";
    bodenbeschichtungUniversal.anwendbar=@"Bodenbeschichtung";
    bodenbeschichtungUniversal.nachhaltigkeit=@"Verpackung ist thermisch und stofflich verwertbar.";
    bodenbeschichtungUniversal.hinweise=@"Auf grundgereinigtem, durchgetrocknetem Belag dünn auftragen. In der Regel – je nach Belag – zwei Aufträge erforderlich. Zwischen den Aufträgen vollständig durchtrocknen lassen.";
    bodenbeschichtungUniversal.informationPDF=@"bodenbeschichtung_universal";
    bodenbeschichtungUniversal.uid=@"BF70FF86-0579-464B-A7DD-1CBD25033D74";
    bodenbeschichtungUniversal.index=@"B";
    
    Reiniger *grundreinigerUniversal = [Reiniger insertReinigerInManagedObjectContext:appDelegate.managedObjectContext];
    grundreinigerUniversal.bezeichnung=@"orochemie® Grundreiniger universal";
    grundreinigerUniversal.zusatz=@"Entfernt Beschichtungen & starke Verschmutzungen";
    grundreinigerUniversal.image=@"rm_grundreiniger_universal_gruppe.png";
    grundreinigerUniversal.einsatzbereich=@"Universell einsetzbarer, kraftvoller Grundreiniger für wasserfeste Beläge und Flächen, z. B. PVC, Natur-/Kunststeine, Keramik, Linoleum etc. Beseitigt Polymer-/Wachsbeschichtungen, Verkrustungen und starke Verschmutzungen von Böden und Flächen.";
    grundreinigerUniversal.eigenschaften=@"Konzentrat. Sehr gutes Lösevermögen – extra stark. Schnelle Wirksamkeit – dadurch zeitsparend. Beste Materialverträglichkeit.";
    grundreinigerUniversal.dosierung=@"empfohlene Dosierung: Zur Entfernung von Beschichtungen 1 – 3 Liter auf 10 Liter Wasser.";
    grundreinigerUniversal.phwert=@"10-11";
    grundreinigerUniversal.anwendbar=@"Grundreinigung, manuelle Reinigung, Bodenbeschichtung";
    grundreinigerUniversal.nachhaltigkeit=@"Die enthaltenen Tenside entsprechen der OECD-Richtlinie; sind somit biologisch abbaubar. Verpackung ist thermisch und stofflich verwertbar.";
    grundreinigerUniversal.hinweise=@"Nicht auf alkaliempfindlichen Flächen und Metallen (Aluminium etc.) anwenden. Bei der Grundreinigung von Böden besteht Rutschgefahr.";
    grundreinigerUniversal.informationPDF=@"grundreiniger_universal";
    grundreinigerUniversal.uid=@"29B20ED9-9529-450E-9EF1-FABC82D678EA";
    grundreinigerUniversal.mindestdosierung=@"100";
    grundreinigerUniversal.index=@"G";
    
#pragma mark - Gefahrstoffsymbole für Reiniger
    
    Gefahrstoffsymbole *ghs_achtung = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:appDelegate.managedObjectContext];
    
    ghs_achtung.bild=@"ghs_ausrufezeichen.png";
    ghs_achtung.name=@"ghs achtung";
    ghs_achtung.sortierung=@(50);
    
    Gefahrstoffsymbole *ghs_entzuendlich = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:appDelegate.managedObjectContext];
    
    ghs_entzuendlich.bild=@"ghs_flamme.png";
    ghs_entzuendlich.name=@"ghs entzündlich";
    ghs_entzuendlich.sortierung=@(40);
    
    Gefahrstoffsymbole *ghs_aetzend = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:appDelegate.managedObjectContext];
    
    ghs_aetzend.bild=@"ghs_aetzend.png";
    ghs_aetzend.name=@"ghs ätzend";
    ghs_aetzend.sortierung=@(20);
    
    Gefahrstoffsymbole *ghs_gesundheitsgefahr = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:appDelegate.managedObjectContext];
    
    ghs_gesundheitsgefahr.bild=@"ghs_gesundheitsgefahr.png";
    ghs_gesundheitsgefahr.name=@"ghs gesundheitsgefahr";
    ghs_gesundheitsgefahr.sortierung=@(10);
    
    Gefahrstoffsymbole *ghs_umweltgefahr = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:appDelegate.managedObjectContext];
    
    ghs_umweltgefahr.bild=@"ghs_umweltgefahr.png";
    ghs_umweltgefahr.name=@"ghs umweltgefahr";
    ghs_umweltgefahr.sortierung=@(30);
    
    
    [hygienereiniger addGefahrstoffsymboleObject:ghs_achtung];
    
    [feinsteinzeugReiniger addGefahrstoffsymboleObject:ghs_achtung];
    
    [neutralreiniger addGefahrstoffsymboleObject:ghs_aetzend];
    
    [sanitaerReiniger addGefahrstoffsymboleObject:ghs_aetzend];
    
//    [sanitaerReinigerViskos addGefahrstoffsymboleObject:ghs_aetzend];
//    [sanitaerReinigerViskos addGefahrstoffsymboleObject:ghs_umweltgefahr];
    
    [wcReiniger addGefahrstoffsymboleObject:ghs_achtung];
    
    [grundReinigerSauer addGefahrstoffsymboleObject:ghs_aetzend];
    
//    [kalkloeser addGefahrstoffsymboleObject:ghs_achtung];
    
    [fettloeser addGefahrstoffsymboleObject:ghs_aetzend];
    
//    [ofenReiniger addGefahrstoffsymboleObject:ghs_aetzend];
    
    [feinsteinzeugReiniger addGefahrstoffsymboleObject:ghs_achtung];
    
    [grundreinigerUniversal addGefahrstoffsymboleObject:ghs_aetzend];
    [grundreinigerUniversal addGefahrstoffsymboleObject:ghs_achtung];
    
    
    
#pragma mark - Anwendungen
    
    Anwendung *unterhaltsreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    unterhaltsreinigung.name =@"Unterhaltsreinigung";
    unterhaltsreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *grundreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    grundreinigung.name=@"Grundreinigung";
    grundreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *sanitaerreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerreinigung.name = @"Sanitärreinigung";
    sanitaerreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *reinigung_kueche_lebensmittelbereich = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    reinigung_kueche_lebensmittelbereich.name=@"Reinigung Küche";
    reinigung_kueche_lebensmittelbereich.zusatzbezeichnung = @"in Küche/Lebensmittelbereich";
    reinigung_kueche_lebensmittelbereich.reinigerAnwendung=@(YES);

    Anwendung *reinigung_lebensmittelbereich = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    reinigung_lebensmittelbereich.name=@"Reinigung Lebensmittelbereich";
    reinigung_lebensmittelbereich.zusatzbezeichnung = @"in Küche/Lebensmittelbereich";
    reinigung_lebensmittelbereich.reinigerAnwendung=@(YES);
    
    Anwendung *feinsteinzeug = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeug.name = @"Feinsteinzeugreinigung";
    feinsteinzeug.reinigerAnwendung=@(YES);
    
    Anwendung *teppichreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    teppichreinigung.name=@"Teppichreinigung";
    teppichreinigung.zusatzbezeichnung = @"(Garnpadverfahren)";
    teppichreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *automatenreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    automatenreinigung.name =@"Automatenreinigung";
    automatenreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *flaechenreinigung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    flaechenreinigung.name =@"Flächenreinigung";
    flaechenreinigung.zusatzbezeichnung=@"im Wechsel mit Flächendesinfektion";
    flaechenreinigung.reinigerAnwendung=@(YES);
    
    Anwendung *ecolabelReiniger = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    ecolabelReiniger.name = @"Ecolabel-Reiniger";
    ecolabelReiniger.reinigerAnwendung=@(YES);
    
    Anwendung *bodenbeschichtung = [Anwendung insertAnwendungInManagedObjectContext:appDelegate.managedObjectContext];
    bodenbeschichtung.name = @"Bodenbeschichtung";
    bodenbeschichtung.reinigerAnwendung=@(YES);
    
    NSSet *unterhaltsreinigungSet = [NSSet setWithObjects:hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,
                                     reinigerTensidfrei,neutralreiniger,universalReinigerEco,glasReinigerEco,sanitaerReiniger,
                                     wcReiniger,sanitaerReinigerKf, nil];
    [unterhaltsreinigung addReiniger:unterhaltsreinigungSet];
    
    NSSet *grundreinigungsSet = [NSSet setWithObjects:grundReinigerSauer,grundreinigerUniversal, nil];
    [grundreinigung addReiniger:grundreinigungsSet];
    
    NSSet *sanitaerreinigungsSet = [NSSet setWithObjects:sanitaerReiniger,grundReinigerSauer,wcReiniger, sanitaerReinigerKf, nil];
    [sanitaerreinigung addReiniger:sanitaerreinigungsSet];
    
    
    NSSet *reinigungKuechenSet = [NSSet setWithObjects:universalReinigerEco,fettloeser, nil];
    
    [reinigung_kueche_lebensmittelbereich addReiniger:reinigungKuechenSet];
    
    NSSet *reinigungLebensmittelSet = [NSSet setWithObjects:universalReinigerEco,fettloeser, nil];
    
    [reinigung_lebensmittelbereich addReiniger:reinigungLebensmittelSet];
    
    NSSet *teppichReinigungsSet = [NSSet setWithObjects:reinigerTensidfrei, nil];
    [teppichreinigung addReiniger:teppichReinigungsSet];
    
    NSSet *feinsteinSet = [NSSet setWithObjects:feinsteinzeugReiniger,reinigerTensidfrei,grundReinigerSauer,hygienereiniger,schonreiniger,sanitaerReiniger, grundreinigerUniversal, nil];
    [feinsteinzeug addReiniger:feinsteinSet];
    
    NSSet *automatenSet = [NSSet setWithObjects:hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,fettloeser,sanitaerReinigerKf,grundReinigerSauer,grundreinigerUniversal,sanitaerReiniger, nil];
    [automatenreinigung addReiniger:automatenSet];
    
    NSSet *flaechenSet = [NSSet setWithObjects:hygienereiniger,schonreiniger, nil];
    [flaechenreinigung addReiniger:flaechenSet];
    
    NSSet *ecoLabelSet =[NSSet setWithObjects:universalReinigerEco,glasReinigerEco,sanitaerReiniger, nil];
    [ecolabelReiniger addReiniger:ecoLabelSet];
    
    NSSet *bodenbeschichtungSet =[NSSet setWithObjects:bodenbeschichtungUniversal,grundreinigerUniversal, nil];
    [bodenbeschichtung addReiniger:bodenbeschichtungSet];
    
#pragma mark - pH - Wert
    
    Phwert *sauer = [Phwert
                     insertPhwertInManagedObjectContext:appDelegate.managedObjectContext];
    sauer.bezeichnung = @"Sauer (pH-Wert 0-5)";
    
    Phwert *neutral = [Phwert
                      insertPhwertInManagedObjectContext:appDelegate.managedObjectContext];
    neutral.bezeichnung = @"Neutral (pH-Wert 6-8)";
    
    Phwert *alkalisch = [Phwert insertPhwertInManagedObjectContext:appDelegate.managedObjectContext];
    alkalisch.bezeichnung = @"Alkalisch (pH-Wert 9-14)";
    
    NSSet *sauerSet = [NSSet setWithObjects:sanitaerReiniger,grundReinigerSauer,wcReiniger,sanitaerReinigerKf, nil];
    
    [sauer addReiniger:sauerSet];
    
    NSSet *neutralSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco,glasReinigerEco,bodenbeschichtungUniversal, nil];
    [neutral addReiniger:neutralSet];
    
    NSSet *alkalischSet = [NSSet setWithObjects:hygienereiniger,feinsteinzeugReiniger,fettloeser,grundreinigerUniversal, nil];
    [alkalisch addReiniger:alkalischSet];
    
#pragma mark - Oberflaeche
    
//    Oberflaeche *gummi = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    gummi.bezeichnung = @"Gummi/Kautschuk";
//    gummi.kategorie=@"Elastische Beläge";
//
//    NSSet *gummiSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//    [gummi addReiniger:gummiSet];
//
//    Oberflaeche *linoleum = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    linoleum.bezeichnung = @"Linoleum";
//    linoleum.kategorie=@"Elastische Beläge";
//
//    NSSet *linoleumSet = [NSSet setWithObjects:grundreinigerUniversal,bodenbeschichtungUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco,nil];
//    [linoleum addReiniger:linoleumSet];
//
//    Oberflaeche *pvc = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    pvc.bezeichnung = @"PVC";
//    pvc.kategorie=@"Elastische Beläge";
//
//    NSSet *pvcSet = [NSSet setWithObjects:grundreinigerUniversal,bodenbeschichtungUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,sanitaerReinigerKf, nil];
//    [pvc addReiniger:pvcSet];
//
//    Oberflaeche *kork = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    kork.bezeichnung = @"Kork";
//    kork.kategorie=@"Holz/Laminat: nebelfeuchte Anwendung";
//
//    NSSet *korkSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//    [kork addReiniger:korkSet];
//
//    Oberflaeche *laminat = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    laminat.bezeichnung = @"Laminat";
//    laminat.kategorie=@"Holz/Laminat: nebelfeuchte Anwendung";
//
//    NSSet *laminatSet = [NSSet setWithObjects:hygienereiniger,schonreiniger,duftreinigerOrange,reinigerTensidfrei,neutralreiniger,universalReinigerEco,hygienereiniger,nil];
//    [laminat addReiniger:laminatSet];
//
//    Oberflaeche *parkett = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    parkett.bezeichnung = @"Parkett/Dielen";
//    parkett.kategorie=@"Holz/Laminat: nebelfeuchte Anwendung";
//
//    NSSet *parkettSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//    [parkett addReiniger:parkettSet];
//
//    Oberflaeche *betonwerkstein = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    betonwerkstein.bezeichnung = @"Betonwerkstein";
//    betonwerkstein.kategorie=@"Natur-/Kunststein";
//
//    NSSet *betonwerksteinSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//    [betonwerkstein addReiniger:betonwerksteinSet];
//
//    Oberflaeche *feinsteinzeugOberflaeche = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    feinsteinzeugOberflaeche.bezeichnung = @"Feinsteinzeug";
//    feinsteinzeugOberflaeche.kategorie=@"Natur-/Kunststein";
//
//    NSSet *feinsteinzeugOberflaecheSet = [NSSet setWithObjects:grundreinigerUniversal,feinsteinzeugReiniger,reinigerTensidfrei,grundReinigerSauer,hygienereiniger,schonreiniger,  nil];
//
//    [feinsteinzeugOberflaeche addReiniger:feinsteinzeugOberflaecheSet];
//
//    Oberflaeche *gneis = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    gneis.bezeichnung = @"Gneis";
//    gneis.kategorie=@"Natur-/Kunststein";
//
//    NSSet *gneisSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf, nil];
//
//    [gneis addReiniger:gneisSet];
//
//    Oberflaeche *granit = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    granit.bezeichnung = @"Granit";
//    granit.kategorie=@"Natur-/Kunststein";
//
//    NSSet *granitSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf, nil];
//
//    [granit addReiniger:granitSet];
//
//    Oberflaeche *jura = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    jura.bezeichnung = @"Jura";
//    jura.kategorie=@"Natur-/Kunststein";
//
//    NSSet *juraSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [jura addReiniger:juraSet];
//
//    Oberflaeche *kalkstein = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    kalkstein.bezeichnung = @"Kalkstein";
//    kalkstein.kategorie=@"Natur-/Kunststein";
//
//    NSSet *kalksteinSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [kalkstein addReiniger:kalksteinSet];
//
//    Oberflaeche *keramikfliesen = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    keramikfliesen.bezeichnung = @"Keramikfliesen";
//    keramikfliesen.kategorie=@"Natur-/Kunststein";
//
//    NSSet *keramikfliesenSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf,wcReiniger, nil];
//
//    [keramikfliesen addReiniger:keramikfliesenSet];
//
//    Oberflaeche *klinker = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    klinker.bezeichnung = @"Klinker";
//    klinker.kategorie=@"Natur-/Kunststein";
//
//    NSSet *klinkerSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf, nil];
//
//    [klinker addReiniger:klinkerSet];
//
//    Oberflaeche *marmor = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    marmor.bezeichnung = @"Marmor";
//    marmor.kategorie=@"Natur-/Kunststein";
//
//    NSSet *marmorSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [marmor addReiniger:marmorSet];
//
//    Oberflaeche *muschelkalk = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    muschelkalk.bezeichnung = @"Muschelkalk";
//    muschelkalk.kategorie=@"Natur-/Kunststein";
//
//    NSSet *muschelkalkSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [muschelkalk addReiniger:muschelkalkSet];
//
//    Oberflaeche *sandstein = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    sandstein.bezeichnung = @"Sandstein";
//    sandstein.kategorie=@"Natur-/Kunststein";
//
//    NSSet *sandsteinSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [sandstein addReiniger:sandsteinSet];
//
//    Oberflaeche *schiefer = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    schiefer.bezeichnung = @"Schiefer";
//    schiefer.kategorie=@"Natur-/Kunststein";
//
//    NSSet *schieferSet = [NSSet setWithObjects:grundreinigerUniversal,schonreiniger,wischpflege,neutralreiniger,universalReinigerEco, nil];
//
//    [schiefer addReiniger:schieferSet];
//
//    Oberflaeche *sicherheitsfliesen = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    sicherheitsfliesen.bezeichnung = @"Sicherheitsfliesen";
//    sicherheitsfliesen.kategorie=@"Natur-/Kunststein";
//
//    NSSet *sicherheitsfliesenSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf, nil];
//
//    [sicherheitsfliesen addReiniger:sicherheitsfliesenSet];
//
//    Oberflaeche *solnhofer = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    solnhofer.bezeichnung = @"Solnhofer";
//    solnhofer.kategorie=@"Natur-/Kunststein";
//
//    NSSet *solnhoferSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [solnhofer addReiniger:solnhoferSet];
//
//    Oberflaeche *steinteppich = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    steinteppich.bezeichnung = @"Steinteppich";
//    steinteppich.kategorie=@"Natur-/Kunststein";
//
//    NSSet *steinteppichSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,sanitaerReinigerKf, nil];
//
//    [steinteppich addReiniger:steinteppichSet];
//
//    Oberflaeche *terrakotta = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    terrakotta.bezeichnung = @"Terrakotta/Tonware";
//    terrakotta.kategorie=@"Natur-/Kunststein";
//
//    NSSet *terrakottaSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,wischpflege,feinsteinzeugReiniger,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [terrakotta addReiniger:terrakottaSet];
//
//    Oberflaeche *travertin = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    travertin.bezeichnung = @"Travertin";
//    travertin.kategorie=@"Natur-/Kunststein";
//
//    NSSet *travertinSet = [NSSet setWithObjects:schonreiniger,duftreinigerOrange,wischpflege,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [travertin addReiniger:travertinSet];
//
//    Oberflaeche *baumwolle = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    baumwolle.bezeichnung = @"Baumwolle";
//    baumwolle.kategorie=@"Textile Beläge: Garnpadreinigung";
//
//    NSSet *baumwolleSet = [NSSet setWithObjects:reinigerTensidfrei, nil];
//
//    [baumwolle addReiniger:baumwolleSet];
//
//    Oberflaeche *kunstfaser = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    kunstfaser.bezeichnung = @"Kunstfaser";
//    kunstfaser.kategorie=@"Textile Beläge: Garnpadreinigung";
//
//    NSSet *kunstfaserSet = [NSSet setWithObjects:reinigerTensidfrei, nil];
//
//    [kunstfaser addReiniger:kunstfaserSet];
//
//    Oberflaeche *seide = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    seide.bezeichnung = @"Seide";
//    seide.kategorie=@"Textile Beläge: Garnpadreinigung";
//
//    NSSet *seideSet = [NSSet setWithObjects:reinigerTensidfrei, nil];
//
//    [seide addReiniger:seideSet];
//
//    Oberflaeche *wolle = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    wolle.bezeichnung = @"Wolle";
//    wolle.kategorie=@"Textile Beläge: Garnpadreinigung";
//
//    NSSet *wolleSet = [NSSet setWithObjects:reinigerTensidfrei, nil];
//
//    [wolle addReiniger:wolleSet];
//
//    Oberflaeche *acrylglas = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    acrylglas.bezeichnung = @"Acrylglas (Plexiglas/PMMA)";
//    acrylglas.kategorie=@"Sonstige Materialien";
//
//    NSSet *acrylglasSet = [NSSet setWithObjects:hygienereiniger,sanitaerReiniger,schonreiniger,neutralreiniger,universalReinigerEco, nil];
//
//    [acrylglas addReiniger:acrylglasSet];
//
//
//    Oberflaeche *aluminium = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    aluminium.bezeichnung = @"Aluminium";
//    aluminium.kategorie=@"Sonstige Materialien";
//
//    NSSet *aluminiumSet = [NSSet setWithObjects:glasReinigerEco,schonreiniger,duftreinigerOrange,reinigerTensidfrei,neutralreiniger,universalReinigerEco, nil];
//
//    [aluminium addReiniger:aluminiumSet];
//
//    Oberflaeche *edelstahl = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    edelstahl.bezeichnung = @"Edelstahl";
//    edelstahl.kategorie=@"Sonstige Materialien";
//
//    NSSet *edelstahlSet = [NSSet setWithObjects:grundreinigerUniversal,hygienereiniger,schonreiniger,duftreinigerOrange,reinigerTensidfrei,neutralreiniger,universalReinigerEco,sanitaerReiniger,grundReinigerSauer,fettloeser,sanitaerReinigerKf, glasReinigerEco,wcReiniger,grundReinigerSauer,fettloeser, nil];
//
//    [edelstahl addReiniger:edelstahlSet];
//
//    Oberflaeche *glas = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    glas.bezeichnung = @"Glas";
//    glas.kategorie=@"Sonstige Materialien";
//
//    NSSet *glasSet = [NSSet setWithObjects:hygienereiniger,schonreiniger,universalReinigerEco, grundreinigerUniversal,reinigerTensidfrei,glasReinigerEco,sanitaerReiniger, nil];
//
//    [glas addReiniger:glasSet];
//
////    Oberflaeche *kunstleder = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
////    kunstleder.bezeichnung = @"Kunstleder";
////    kunstleder.kategorie=@"Sonstige Materialien";
////
////    NSSet *kunstlederSet = [NSSet setWithObjects:kunstlederReiniger, nil];
////
////    [kunstleder addReiniger:kunstlederSet];
//
//    Oberflaeche *kupfer = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    kupfer.bezeichnung = @"Kupfer";
//    kupfer.kategorie=@"Sonstige Materialien";
//
//    NSSet *kupferSet = [NSSet setWithObjects:hygienereiniger,schonreiniger,duftreinigerOrange,reinigerTensidfrei,neutralreiniger,universalReinigerEco,fettloeser, nil];
//
//    [kupfer addReiniger:kupferSet];
//
//    Oberflaeche *plexiglas = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    plexiglas.bezeichnung = @"Plexiglas (Acrylglas/PMMA)";
//    plexiglas.kategorie=@"Sonstige Materialien";
//
//    NSSet *plexiglasSet = [NSSet setWithObjects:hygienereiniger, sanitaerReiniger, schonreiniger,neutralreiniger,universalReinigerEco, nil];
//
//    [plexiglas addReiniger:plexiglasSet];
//
//    Oberflaeche *pmma = [Oberflaeche insertOberflaecheInManagedObjectContext:appDelegate.managedObjectContext];
//    pmma.bezeichnung = @"PMMA (Acrylglas/Plexiglas)";
//    pmma.kategorie=@"Sonstige Materialien";
//
//    NSSet *pmmaSet = [NSSet setWithObjects:hygienereiniger, sanitaerReiniger,schonreiniger,neutralreiniger,universalReinigerEco, nil];
//    [pmma addReiniger:pmmaSet];
    

    
#pragma mark - ReinigerVideo
    
    
    Video *spezialReiniger = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    spezialReiniger.bezeichnung =
    @"orochemie Reiniger - verträglich mit desinfizierten Flächen";
    spezialReiniger.datei=@"orochemie_hygienereiniger_schonreiniger_v3_mobil";
    spezialReiniger.kategorie=@"R";
    spezialReiniger.sortierung=@(140);
    spezialReiniger.reinigerVideo=@(1);
    spezialReiniger.filesize=@"4863958";
    spezialReiniger.youtube=@"https://www.youtube.com/watch?v=RN8uCca1e2c";
    
    [hygienereiniger addVideoObject:spezialReiniger];
    [schonreiniger addVideoObject:spezialReiniger];
    
    Video *feinsteinzeugVideo = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeugVideo.bezeichnung =
    @"Reinigung von Feinsteinzeug";
    feinsteinzeugVideo.datei=@"reiniger_feinsteinzeug_v2_270p";
    feinsteinzeugVideo.kategorie=@"F";
    feinsteinzeugVideo.sortierung=@(180);
    feinsteinzeugVideo.reinigerVideo=@(1);
    feinsteinzeugVideo.filesize=@"9129023";
    feinsteinzeugVideo.youtube=@"https://www.youtube.com/watch?v=jmT1CVC7zE4";
    
    [feinsteinzeugReiniger addVideoObject:feinsteinzeugVideo];
    [reinigerTensidfrei addVideoObject:feinsteinzeugVideo];
    [grundReinigerSauer addVideoObject:feinsteinzeugVideo];
    
    
    Video *garnpadReinigungVideo = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    garnpadReinigungVideo.bezeichnung =
    @"Die Garnpadreinigung textiler Beläge ";
    garnpadReinigungVideo.datei=@"reiniger_garnpadreinigung_v2_270p";
    garnpadReinigungVideo.kategorie=@"G";
    garnpadReinigungVideo.sortierung=@(170);
    garnpadReinigungVideo.reinigerVideo=@(1);
    garnpadReinigungVideo.filesize=@"5872445";
    garnpadReinigungVideo.youtube=@"https://www.youtube.com/watch?v=Re6SeJvboSQ";
    
    [reinigerTensidfrei addVideoObject:garnpadReinigungVideo];

    Video *reinigungsWagenVideo = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    reinigungsWagenVideo.bezeichnung =
    @"Der Reinigungswagen im Gesundheitswesen";
    reinigungsWagenVideo.datei=@"reiniger_reinigungswagen_v3_270p";
    reinigungsWagenVideo.kategorie=@"R";
    reinigungsWagenVideo.sortierung=@(160);
    reinigungsWagenVideo.reinigerVideo=@(1);
    reinigungsWagenVideo.filesize=@"13849356";
    reinigungsWagenVideo.youtube=@"https://www.youtube.com/watch?v=XFOUey5eJsI";
    
    [hygienereiniger addVideoObject:reinigungsWagenVideo];
    [schonreiniger addVideoObject:reinigungsWagenVideo];
    [wischpflege addVideoObject:reinigungsWagenVideo];
    [glasReinigerEco addVideoObject:reinigungsWagenVideo];
    [sanitaerReiniger addVideoObject:reinigungsWagenVideo];
//    [sanitaerReinigerViskos addVideoObject:reinigungsWagenVideo];
    [sanitaerReinigerKf addVideoObject:reinigungsWagenVideo];
    [wcReiniger addVideoObject:reinigungsWagenVideo];

    
    Video *sanitaerRaeumeVideo = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerRaeumeVideo.bezeichnung =
    @"Die Reinigung von Sanitärräumen";
    sanitaerRaeumeVideo.datei=@"reiniger_sanitaerraeume_v2_270p";
    sanitaerRaeumeVideo.kategorie=@"S";
    sanitaerRaeumeVideo.sortierung=@(150);
    sanitaerRaeumeVideo.reinigerVideo=@(1);
    sanitaerRaeumeVideo.filesize=@"10307488";
    sanitaerRaeumeVideo.youtube=@"https://www.youtube.com/watch?v=AxLrvGaey0Q";
    
    [duftreinigerOrange addVideoObject:sanitaerRaeumeVideo];
    [hygienereiniger addVideoObject:sanitaerRaeumeVideo];
    [sanitaerReiniger addVideoObject:sanitaerRaeumeVideo];
//    [sanitaerReinigerViskos addVideoObject:sanitaerRaeumeVideo];
    [sanitaerReinigerKf addVideoObject:sanitaerRaeumeVideo];
    [wcReiniger addVideoObject:sanitaerRaeumeVideo];
    [grundReinigerSauer addVideoObject:sanitaerRaeumeVideo];
    
    Video *unterhaltsReinigungVideo = [Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];
    unterhaltsReinigungVideo.bezeichnung =
    @"Die Unterhaltsreinigung";
    unterhaltsReinigungVideo.datei=@"reiniger_unterhaltsreinigung_v3_270p";
    // Da nur ein Buchstabe geprüft werden kann steht die Prüfung auf T da das vor U kommt (Unternehmen / Unterhaltsreinigung
    unterhaltsReinigungVideo.kategorie=@"T";
    unterhaltsReinigungVideo.sortierung=@(190);
    unterhaltsReinigungVideo.reinigerVideo=@(1);
    unterhaltsReinigungVideo.filesize=@"8741880";
    unterhaltsReinigungVideo.youtube=@"https://www.youtube.com/watch?v=YoLfKVYOiJ8";

    [wischpflege addVideoObject:unterhaltsReinigungVideo];
    [duftreinigerOrange addVideoObject:unterhaltsReinigungVideo];
    [schonreiniger addVideoObject:unterhaltsReinigungVideo];
    
    Video *orochemieImageFilm =[Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];    orochemieImageFilm.datei=@"orochemie_desinfektionsmittel_unternehmensfilm_v5_mobile";
    orochemieImageFilm.bezeichnung=@"orochemie Unternehmensfilm";
    orochemieImageFilm.kategorie=@"U";
    orochemieImageFilm.sortierung=@(200);
    orochemieImageFilm.reinigerVideo=@(1);
    orochemieImageFilm.filesize=@"10645993"; //9508900
    orochemieImageFilm.youtube=@"https://www.youtube.com/watch?v=u_yv3o1xTRQ";
    
    Video *hygienetage =[Video insertVideoInManagedObjectContext:appDelegate.managedObjectContext];    hygienetage.datei=@"orochemie_hygienetag_v2_mobile";
    hygienetage.bezeichnung=@"orochemie Hygienetage";
    hygienetage.kategorie=@"U";
    hygienetage.sortierung=@(210);
    hygienetage.reinigerVideo=@(1);
    hygienetage.filesize=@"5411819";
    hygienetage.youtube=@"https://www.youtube.com/watch?v=Et6S_iI1KV0";
    
#pragma mark - Reinigerverbrauchsrechner
    
    Reinigungsart *nebelfeucht = [Reinigungsart insertReinigungsartInManagedObjectContext:appDelegate.managedObjectContext];
    nebelfeucht.bezeichnung = @"Feuchtwischen ";
    nebelfeucht.bezeichnung2 = @"(nebelfeucht)";
    nebelfeucht.kennung = @"nebelfeucht";
    nebelfeucht.sortierung=@(10);
    nebelfeucht.wassermenge=@(5);
    
    Reinigungsart *feucht = [Reinigungsart insertReinigungsartInManagedObjectContext:appDelegate.managedObjectContext];
    feucht.bezeichnung = @"Nasswischen";
    feucht.bezeichnung2=@"(feucht, 1-stufig)";
    feucht.kennung=@"feucht";
    feucht.sortierung=@(20);
    feucht.wassermenge=@(12);

    Reinigungsart *nassfeucht = [Reinigungsart insertReinigungsartInManagedObjectContext:appDelegate.managedObjectContext];
    nassfeucht.bezeichnung=@"Nasswischen";
    nassfeucht.bezeichnung2=@"(nassfeucht, 2-stufig)";
    nassfeucht.kennung=@"nassfeucht";
    nassfeucht.sortierung=@(30);
    nassfeucht.wassermenge=@(25);
    
    
    Verbrauch *hygienereinigerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    hygienereinigerNebelfeucht.reinigungsart = nebelfeucht;
    hygienereinigerNebelfeucht.verbrauchsmenge = @(0.025);
    hygienereinigerNebelfeucht.reiniger = hygienereiniger;

    
    Verbrauch *hygienereinigerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    hygienereinigerFeucht.reinigungsart = feucht;
    hygienereinigerFeucht.verbrauchsmenge = @(0.06);
    hygienereinigerFeucht.reiniger = hygienereiniger;

 
    Verbrauch *hygienereinigerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    hygienereinigerNassfeucht.reinigungsart = nassfeucht;
    hygienereinigerNassfeucht.verbrauchsmenge = @(0.125);
    hygienereinigerNassfeucht.reiniger = hygienereiniger;

    Verbrauch *schonreinigerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    schonreinigerNebelfeucht.reinigungsart = nebelfeucht;
    schonreinigerNebelfeucht.verbrauchsmenge = @(0.0125);
    schonreinigerNebelfeucht.reiniger = schonreiniger;
    
    Verbrauch *schonreinigerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    schonreinigerFeucht.reinigungsart = feucht;
    schonreinigerFeucht.verbrauchsmenge = @(0.03);
    schonreinigerFeucht.reiniger = schonreiniger;
    
    
    Verbrauch *schonreinigerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    schonreinigerNassfeucht.reinigungsart = nassfeucht;
    schonreinigerNassfeucht.verbrauchsmenge = @(0.0625);
    schonreinigerNassfeucht.reiniger = schonreiniger;
    
    Verbrauch *duftreinigerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    duftreinigerNebelfeucht.reinigungsart = nebelfeucht;
    duftreinigerNebelfeucht.verbrauchsmenge = @(0.0125);
    duftreinigerNebelfeucht.reiniger = duftreinigerOrange;
    
    Verbrauch *duftreinigerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    duftreinigerFeucht.reinigungsart = feucht;
    duftreinigerFeucht.verbrauchsmenge = @(0.03);
    duftreinigerFeucht.reiniger = duftreinigerOrange;
    
    
    Verbrauch *duftreinigerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    duftreinigerNassfeucht.reinigungsart = nassfeucht;
    duftreinigerNassfeucht.verbrauchsmenge = @(0.0625);
    duftreinigerNassfeucht.reiniger = duftreinigerOrange;
    
    Verbrauch *wischpflegeNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    wischpflegeNebelfeucht.reinigungsart = nebelfeucht;
    wischpflegeNebelfeucht.verbrauchsmenge = @(0.01);
    wischpflegeNebelfeucht.reiniger = wischpflege;
    
    Verbrauch *wischpflegeFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    wischpflegeFeucht.reinigungsart = feucht;
    wischpflegeFeucht.verbrauchsmenge = @(0.024);
    wischpflegeFeucht.reiniger = wischpflege;
    
    
    Verbrauch *wischpflegeNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    wischpflegeNassfeucht.reinigungsart = nassfeucht;
    wischpflegeNassfeucht.verbrauchsmenge = @(0.05);
    wischpflegeNassfeucht.reiniger = wischpflege;
    
    Verbrauch *feinsteinzeugNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeugNebelfeucht.reinigungsart = nebelfeucht;
    feinsteinzeugNebelfeucht.verbrauchsmenge = @(0.025);
    feinsteinzeugNebelfeucht.reiniger = feinsteinzeugReiniger;
    
    Verbrauch *feinsteinzeugFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeugFeucht.reinigungsart = feucht;
    feinsteinzeugFeucht.verbrauchsmenge = @(0.06);
    feinsteinzeugFeucht.reiniger = feinsteinzeugReiniger;
    
    
    Verbrauch *feinsteinzeugNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    feinsteinzeugNassfeucht.reinigungsart = nassfeucht;
    feinsteinzeugNassfeucht.verbrauchsmenge = @(0.125);
    feinsteinzeugNassfeucht.reiniger = feinsteinzeugReiniger;
    
    Verbrauch *reinigerTensidfreiNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    reinigerTensidfreiNebelfeucht.reinigungsart = nebelfeucht;
    reinigerTensidfreiNebelfeucht.verbrauchsmenge = @(0.005);
    reinigerTensidfreiNebelfeucht.reiniger = reinigerTensidfrei;
    
    Verbrauch *reinigerTensidfreiFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    reinigerTensidfreiFeucht.reinigungsart = feucht;
    reinigerTensidfreiFeucht.verbrauchsmenge = @(0.012);
    reinigerTensidfreiFeucht.reiniger = reinigerTensidfrei;
    
    
    Verbrauch *reinigerTensidfreiNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    reinigerTensidfreiNassfeucht.reinigungsart = nassfeucht;
    reinigerTensidfreiNassfeucht.verbrauchsmenge = @(0.025);
    reinigerTensidfreiNassfeucht.reiniger = reinigerTensidfrei;
 
    Verbrauch *neutralreinigerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    neutralreinigerNebelfeucht.reinigungsart = nebelfeucht;
    neutralreinigerNebelfeucht.verbrauchsmenge = @(0.025);
    neutralreinigerNebelfeucht.reiniger = neutralreiniger;
    
    Verbrauch *neutralreinigerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    neutralreinigerFeucht.reinigungsart = feucht;
    neutralreinigerFeucht.verbrauchsmenge = @(0.06);
    neutralreinigerFeucht.reiniger = neutralreiniger;
    
    
    Verbrauch *neutralreinigerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    neutralreinigerNassfeucht.reinigungsart = nassfeucht;
    neutralreinigerNassfeucht.verbrauchsmenge = @(0.125);
    neutralreinigerNassfeucht.reiniger = neutralreiniger;
    
 
    
    Verbrauch *universalreinigerEcoNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    universalreinigerEcoNebelfeucht.reinigungsart = nebelfeucht;
    universalreinigerEcoNebelfeucht.verbrauchsmenge = @(0.025);
    universalreinigerEcoNebelfeucht.reiniger = universalReinigerEco;
    
    Verbrauch *universalreinigerEcoFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    universalreinigerEcoFeucht.reinigungsart = feucht;
    universalreinigerEcoFeucht.verbrauchsmenge = @(0.06);
    universalreinigerEcoFeucht.reiniger = universalReinigerEco;
    
    
    Verbrauch *universalreinigerEcoNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    universalreinigerEcoNassfeucht.reinigungsart = nassfeucht;
    universalreinigerEcoNassfeucht.verbrauchsmenge = @(0.125);
    universalreinigerEcoNassfeucht.reiniger = universalReinigerEco;
    
    Verbrauch *sanitaerReinigerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerNebelfeucht.reinigungsart = nebelfeucht;
    sanitaerReinigerNebelfeucht.verbrauchsmenge = @(0.025);
    sanitaerReinigerNebelfeucht.reiniger = sanitaerReiniger;
    
    Verbrauch *sanitaerReinigerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerFeucht.reinigungsart = feucht;
    sanitaerReinigerFeucht.verbrauchsmenge = @(0.06);
    sanitaerReinigerFeucht.reiniger = sanitaerReiniger;
    
    
    Verbrauch *sanitaerReinigerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerNassfeucht.reinigungsart = nassfeucht;
    sanitaerReinigerNassfeucht.verbrauchsmenge = @(0.125);
    sanitaerReinigerNassfeucht.reiniger = sanitaerReiniger;
    
    
//    Verbrauch *sanitaerReinigerViskosNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    sanitaerReinigerViskosNebelfeucht.reinigungsart = nebelfeucht;
//    sanitaerReinigerViskosNebelfeucht.verbrauchsmenge = @(0.025);
//    sanitaerReinigerViskosNebelfeucht.reiniger = sanitaerReinigerViskos;
    
//    Verbrauch *sanitaerReinigerViskosFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    sanitaerReinigerViskosFeucht.reinigungsart = feucht;
//    sanitaerReinigerViskosFeucht.verbrauchsmenge = @(0.06);
//    sanitaerReinigerViskosFeucht.reiniger = sanitaerReinigerViskos;
    
//    Verbrauch *sanitaerReinigerViskosNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    sanitaerReinigerViskosNassfeucht.reinigungsart = nassfeucht;
//    sanitaerReinigerViskosNassfeucht.verbrauchsmenge = @(0.125);
//    sanitaerReinigerViskosNassfeucht.reiniger = sanitaerReinigerViskos;
 
    Verbrauch *sanitaerReinigerKfNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerKfNebelfeucht.reinigungsart = nebelfeucht;
    sanitaerReinigerKfNebelfeucht.verbrauchsmenge = @(0.05);
    sanitaerReinigerKfNebelfeucht.reiniger = sanitaerReinigerKf;
    
    Verbrauch *sanitaerReinigerKfFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerKfFeucht.reinigungsart = feucht;
    sanitaerReinigerKfFeucht.verbrauchsmenge = @(0.12);
    sanitaerReinigerKfFeucht.reiniger = sanitaerReinigerKf;
    
    Verbrauch *sanitaerReinigerKfNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    sanitaerReinigerKfNassfeucht.reinigungsart = nassfeucht;
    sanitaerReinigerKfNassfeucht.verbrauchsmenge = @(0.25);
    sanitaerReinigerKfNassfeucht.reiniger = sanitaerReinigerKf;
    
    Verbrauch *grundReinigerSauerNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundReinigerSauerNebelfeucht.reinigungsart = nebelfeucht;
    grundReinigerSauerNebelfeucht.verbrauchsmenge = @(0.025);
    grundReinigerSauerNebelfeucht.reiniger = grundReinigerSauer;
    
    
    Verbrauch *grundReinigerSauerFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundReinigerSauerFeucht.reinigungsart = feucht;
    grundReinigerSauerFeucht.verbrauchsmenge = @(0.06);
    grundReinigerSauerFeucht.reiniger = grundReinigerSauer;
    
    
    Verbrauch *grundReinigerSauerNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundReinigerSauerNassfeucht.reinigungsart = nassfeucht;
    grundReinigerSauerNassfeucht.verbrauchsmenge = @(0.125);
    grundReinigerSauerNassfeucht.reiniger = grundReinigerSauer;
    
//    Verbrauch *kalkloeserNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    kalkloeserNebelfeucht.reinigungsart = nebelfeucht;
//    kalkloeserNebelfeucht.verbrauchsmenge = @(0.05);
//    kalkloeserNebelfeucht.reiniger = kalkloeser;
    
//    Verbrauch *kalkloeserFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    kalkloeserFeucht.reinigungsart = feucht;
//    kalkloeserFeucht.verbrauchsmenge = @(0.12);
//    kalkloeserFeucht.reiniger = kalkloeser;
    
    
//    Verbrauch *kalkloeserNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
//    kalkloeserNassfeucht.reinigungsart = nassfeucht;
//    kalkloeserNassfeucht.verbrauchsmenge = @(0.25);
//    kalkloeserNassfeucht.reiniger = kalkloeser;
    
    Verbrauch *fettloeserNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    fettloeserNebelfeucht.reinigungsart = nebelfeucht;
    fettloeserNebelfeucht.verbrauchsmenge = @(0.05);
    fettloeserNebelfeucht.reiniger = fettloeser;
    
    Verbrauch *fettloeserFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    fettloeserFeucht.reinigungsart = feucht;
    fettloeserFeucht.verbrauchsmenge = @(0.12);
    fettloeserFeucht.reiniger = fettloeser;
    
    
    Verbrauch *fettloeserNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    fettloeserNassfeucht.reinigungsart = nassfeucht;
    fettloeserNassfeucht.verbrauchsmenge = @(0.25);
    fettloeserNassfeucht.reiniger = fettloeser;

    
    Verbrauch *grundreinigerUniversalNebelfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundreinigerUniversalNebelfeucht.reinigungsart = nebelfeucht;
    grundreinigerUniversalNebelfeucht.verbrauchsmenge = @(0.05);
    grundreinigerUniversalNebelfeucht.reiniger = grundreinigerUniversal;
    
    Verbrauch *grundreinigerUniversalFeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundreinigerUniversalFeucht.reinigungsart = feucht;
    grundreinigerUniversalFeucht.verbrauchsmenge = @(0.12);
    grundreinigerUniversalFeucht.reiniger = grundreinigerUniversal;
    
    
    Verbrauch *grundreinigerUniversalNassfeucht = [Verbrauch insertVerbrauchInManagedObjectContext:appDelegate.managedObjectContext];
    grundreinigerUniversalNassfeucht.reinigungsart = nassfeucht;
    grundreinigerUniversalNassfeucht.verbrauchsmenge = @(0.25);
    grundreinigerUniversalNassfeucht.reiniger = grundreinigerUniversal;
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Pictogramm Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    Pictogramm *absaugungPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    absaugungPictogramm.bild = @"picto_absaugung_sw.png";
    
    Pictogramm *einscheibenmaschinePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    einscheibenmaschinePictogramm.bild = @"picto_einscheibenmaschine_sw.png";
    
    Pictogramm *flaechePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    flaechePictogramm.bild = @"picto_flaeche_sw.png";
    
    Pictogramm *fliesenPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    fliesenPictogramm.bild = @"picto_fliesen_sw.png";
    
    Pictogramm *glasPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    glasPictogramm.bild = @"picto_glas_sw.png";
    
    Pictogramm *handPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    handPictogramm.bild = @"picto_hand_sw.png";
    
    Pictogramm *instrumentePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    instrumentePictogramm.bild = @"picto_instrumente_sw.png";
    
    Pictogramm *kuechePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    kuechePictogramm.bild = @"picto_kueche_sw.png";
    
    Pictogramm *maschinenreinigerPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    maschinenreinigerPictogramm.bild = @"picto_maschinenreiniger_sw.png";
    
    Pictogramm *spenderPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    spenderPictogramm.bild = @"picto_spender_sw.png";
    
    Pictogramm *spruehflaschePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    spruehflaschePictogramm.bild = @"picto_spruehflasche_sw.png";
    
    Pictogramm *teppichPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    teppichPictogramm.bild = @"picto_teppich_sw.png";
    
    Pictogramm *waschbeckenPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    waschbeckenPictogramm.bild = @"picto_waschbecken_sw.png";
    
    Pictogramm *wcFlaschePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    wcFlaschePictogramm.bild = @"picto_wc_flasche_sw.png";
    
    Pictogramm *wcPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    wcPictogramm.bild = @"picto_wc_sw.png";
    
    Pictogramm *wischenHandPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    wischenHandPictogramm.bild = @"picto_wischen_hand_sw.png";
    
    Pictogramm *wischerPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:appDelegate.managedObjectContext];
    wischerPictogramm.bild = @"picto_wischer_sw.png";

    [bodenbeschichtungUniversal addPictogrammObject:flaechePictogramm];
    
    [duftreinigerOrange addPictogrammObject:wischenHandPictogramm];
    [duftreinigerOrange addPictogrammObject:fliesenPictogramm];
    
    [feinsteinzeugReiniger addPictogrammObject:fliesenPictogramm];
    
    [fettloeser addPictogrammObject:fliesenPictogramm];
    [fettloeser addPictogrammObject:kuechePictogramm];
    
    [glasReinigerEco addPictogrammObject:glasPictogramm];
    
    [grundReinigerSauer addPictogrammObject:wcPictogramm];
    [grundReinigerSauer addPictogrammObject:waschbeckenPictogramm];
    
    [grundreinigerUniversal addPictogrammObject:einscheibenmaschinePictogramm];
    [grundreinigerUniversal addPictogrammObject:flaechePictogramm];
    
    [hygienereiniger addPictogrammObject:wischenHandPictogramm];
    [hygienereiniger addPictogrammObject:fliesenPictogramm];
    
//    [kalkloeser addPictogrammObject:fliesenPictogramm];
//    [kalkloeser addPictogrammObject:kuechePictogramm];
    
//    [kunstlederReiniger addPictogrammObject:spruehflaschePictogramm];
    
    [neutralreiniger addPictogrammObject:wischenHandPictogramm];
    [neutralreiniger addPictogrammObject:fliesenPictogramm];
    
//    [ofenReiniger addPictogrammObject:kuechePictogramm];
    
    [reinigerTensidfrei addPictogrammObject:teppichPictogramm];
    [reinigerTensidfrei addPictogrammObject:maschinenreinigerPictogramm];
    [reinigerTensidfrei addPictogrammObject:fliesenPictogramm];

    
    [sanitaerReiniger addPictogrammObject:wcPictogramm];
    [sanitaerReiniger addPictogrammObject:waschbeckenPictogramm];
    
    [sanitaerReinigerKf addPictogrammObject:wcPictogramm];
    [sanitaerReinigerKf addPictogrammObject:waschbeckenPictogramm];
    
//    [sanitaerReinigerViskos addPictogrammObject:wcFlaschePictogramm];
//    [sanitaerReinigerViskos addPictogrammObject:waschbeckenPictogramm];
    
    [schonreiniger addPictogrammObject:wischenHandPictogramm];
    [schonreiniger addPictogrammObject:fliesenPictogramm];
    
    [universalReinigerEco addPictogrammObject:fliesenPictogramm];
    [universalReinigerEco addPictogrammObject:kuechePictogramm];
    
    [wcReiniger addPictogrammObject:wcFlaschePictogramm];
    
    [wischpflege addPictogrammObject:fliesenPictogramm];
    
    
    
    
#pragma mark - Merkliste
    
    Merkliste *merkliste = [Merkliste
                            insertMerklisteInManagedObjectContext:appDelegate.managedObjectContext];
    merkliste.bezeichnung = @"basisMerkliste";
    
    [appDelegate saveContext];
    
    [self readCSVFileForGettingBarcodesForReiniger];
}

#pragma mark - Barcodes

-(void)readCSVFileForGettingBarcodesForReiniger{
    
    //Fehlervariable
    NSError *errorReading;

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];

    NSArray* produktArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&errorReading];
    
    //if(errorReading) NSLog(@"Error beim einlesen der Reiniger: %@", errorReading.localizedDescription);

    
    //Zugang zur Datei
    NSString *path=[[NSBundle mainBundle] pathForResource:@"Reiniger_Barcodes" ofType:@"csv"];
    NSURL *csvUrl = [NSURL fileURLWithPath:path];
	
    //Lesen der Datei
    NSString *contents = [NSString stringWithContentsOfURL:csvUrl encoding:NSUTF8StringEncoding error:&errorReading];
    
    if(errorReading) NSLog(@"error: %@", errorReading.localizedDescription);
    
    //Array mit den einzelnen Zeilen
    NSArray* lines = [contents componentsSeparatedByString:@"\n"];
    
    //Array mit den einzelnen Werten innerhalb der Zeilen
    for (NSString* line in lines)
    {
        NSArray* fields = [line componentsSeparatedByString:@";"];
        
        
        //Zuweisung der Felder zum Barcode Objekt
        Produktvarianten *produtVariante = [Produktvarianten insertProduktvariantenInManagedObjectContext:appDelegate.managedObjectContext];
        produtVariante.artikelnummer = fields[0];
        produtVariante.matchcode = fields[1];
        produtVariante.bezeichnung1 = fields[2];
        produtVariante.bezeichnung2 = fields[3];
        produtVariante.barcode = fields[4];
        
        
        
        //NSString * bezeichnung_1=[produtVariante.bezeichnung1 substringWithRange:NSMakeRange(0, 8)];
        
        //Suchen des passenden Produkt-Objekt
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"uid LIKE[c] %@", produtVariante.matchcode];
        NSArray *filteredArr = [produktArray filteredArrayUsingPredicate:pred];
        
        Reiniger *r = [filteredArr lastObject];
        
        
        
        if(r!=nil){
            
            //Holen des Produkt Objektes aus dem Ursprünglichen Produkt-Array
            int index = (int)[produktArray indexOfObject:r];
            r = [produktArray objectAtIndex:index];
            
            //hinzufügen des Barcodes zum Produkt
            [r addProduktvariantenObject:produtVariante];
            

            
            
        } else {
            
            [produtVariante deleteProduktvarianten];
            
        }
        
        
    }
    
    [appDelegate saveContext];
    

}

@end
