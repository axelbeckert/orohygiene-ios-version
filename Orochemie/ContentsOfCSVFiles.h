//
//  ContentsOfCSVFiles.h
//  orochemie
//
//  Created by Axel Beckert on 07.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentsOfCSVFiles : NSObject
-(void)getContentsOfBarcodeCSVFile:(NSString*)csvFile;

@end
