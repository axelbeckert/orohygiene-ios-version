//
//  oroDefaultTheme.h
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "oroThemeManager.h"
#import "CustomColoredAccessory.h"

@interface oroDefaultTheme : NSObject<oroTheme>

@end
