//
//  informationenTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 27.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "informationenTableViewController.h"
#import "newsTableViewController.h"
#import "HygieneTippsTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomColoredAccessory.h"
#import "NewsletterTableViewController.h"
#import "InformationenTableViewCell.h"
#import "WebviewViewController.h"

@interface informationenTableViewController ()
@property (nonatomic,strong) NSArray *navigationPoints;
@end

@implementation informationenTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSArray*) navigationPoints
{
    if (!_navigationPoints) _navigationPoints =[[NSArray alloc]init];
    return _navigationPoints;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [oroThemeManager customizeTableView:self.tableView];
    
    self.navigationPoints = @[@"News",@"Hygienetipps",@"Newsletter",@"Hygienetage"];
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.navigationPoints.count;

}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    
    // Configure the cell...

    cell.TitleLabelOutlet.text = [self.navigationPoints objectAtIndex:indexPath.row];
    

        return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        newsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"newsTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    
    } else if (indexPath.row == 1)
    {
        HygieneTippsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HygieneTippsTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    
    } else if (indexPath.row == 2)
    {
        NewsletterTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsletterTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 3)
    {
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerHygieneTage;
        controller.webViewTitle=cWebsiteTitleFuerHygieneTage;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneTage;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

@end
