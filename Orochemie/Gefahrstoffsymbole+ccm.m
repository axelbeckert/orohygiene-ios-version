//
//  Gefahrstoffsymbole+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Gefahrstoffsymbole+ccm.h"

@implementation Gefahrstoffsymbole (ccm)
+(Gefahrstoffsymbole*) insertGefahrstoffsymboleInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Gefahrstoffsymbole" inManagedObjectContext:managedObjectContext];
}
-(void)deleteGefahrstoffsymbole
{
    [self.managedObjectContext deleteObject:self];
}
@end
