//
//  Pictogramm+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 01.03.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Pictogramm.h"

@interface Pictogramm (ccm)
+(Pictogramm*) insertPictogrammInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deletePictogramm;
@end
