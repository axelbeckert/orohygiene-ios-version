//
//  CollectionViewTestControllerViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "CollectionViewTestControllerViewController.h"
#import "CollectionViewCell.h"
#import "CollectionReusableView.h"

@interface CollectionViewTestControllerViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *collectionViewImageOutlet;
@property(nonatomic,strong) NSArray* pictureDataModel;

@end

@implementation CollectionViewTestControllerViewController

-(NSArray *)pictureDataModel{
    if (!_pictureDataModel) _pictureDataModel = [[NSArray alloc] init];
    return _pictureDataModel;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.pictureDataModel = [[NSArray alloc]initWithObjects:@"a20.jpg",@"a30.jpg",@"b5.jpg",@"b10.jpg",@"c20.jpg",@"d10.jpg", @"a20.jpg",@"a30.jpg",@"b5.jpg",@"b10.jpg",@"c20.jpg",@"d10.jpg",nil];
    

     self.pictureDataModel = @[
                               @{@"categoryName" : @"Instrumentendesinfektion",
                                 @"images": @[@"a20.jpg",@"a30.jpg",@"a20.jpg",@"a30.jpg"],
                                 },
                               @{@"categoryName" : @"Flächendesinfektion",
                                 @"images": @[@"b5.jpg",@"b10.jpg",@"b5.jpg",@"b10.jpg",@"b5.jpg"]
                                 },
                               @{@"categoryName" : @"Händedesinfektion",
                                 @"images" : @[@"c20.jpg",@"c20.jpg",@"c20.jpg",@"c20.jpg",@"c20.jpg"]
                                 },
                               @{@"categoryName" : @"Spezialanwendung",
                                 @"images" : @[@"d10.jpg",@"d10.jpg",@"d10.jpg",@"d10.jpg",@"d10.jpg"]
                                 }];
    
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.pictureDataModel.count;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.pictureDataModel[section][@"images"] count];
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView* reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];
    
    if([self.pictureDataModel[indexPath.section][@"categoryName"]isEqualToString:@"Instrumentendesinfektion"])
    {
        [reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_instrumentendesinfektion.jpg"]];
    
    } else if ([self.pictureDataModel[indexPath.section][@"categoryName"]isEqualToString:@"Flächendesinfektion"])
    {
        [reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_flaechendesinfektion.jpg"]];
    
    } else if ([self.pictureDataModel[indexPath.section][@"categoryName"]isEqualToString:@"Händedesinfektion"])
    {
        [reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_haendedesinfektion.jpg"]];
        
    } else
    {
        [reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_spezialdesinfektion.jpg"]];
    }

    
    
    
    reusableView.resuableViewHeaderLabelOutlet.text =self.pictureDataModel[indexPath.section][@"categoryName"];

    
    
    return reusableView;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
    
    [cell.collectionViewImage setImage:[UIImage imageNamed:self.pictureDataModel[indexPath.section][@"images"][indexPath.row]]];
    
    
    
    if([self.pictureDataModel[indexPath.section][@"images"][indexPath.row]isEqualToString:@"a20.jpg"])
    {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Instrumentendesinfektion";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"";
    
    } else if ([self.pictureDataModel[indexPath.section][@"images"][indexPath.row]isEqualToString:@"a30.jpg"])
    {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Desinfektion f. rot. Instrumente";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"";
    
    } else if ([self.pictureDataModel[indexPath.section][@"images"][indexPath.row]isEqualToString:@"b5.jpg"])
    {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Wischdesinfektion";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"Für den Seuchenfall & bei Clostridium dificile";
    
    } else if ([self.pictureDataModel[indexPath.section][@"images"][indexPath.row]isEqualToString:@"b10.jpg"])
    
    {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Wischdesinfektion";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"Schaumarm";
    
    } else if ([self.pictureDataModel[indexPath.section][@"images"][indexPath.row]isEqualToString:@"c20.jpg"])
        
    {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Hände + Hautdesinfektion";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"Parfüm + farbstofffrei";
        
    } else     {
        cell.collectionViewProduktbezeichnungLabelOutlet.text = @"Absauggerätedesinfektion";
        cell.collectionViewProduktbezeichnung2Outlet.text = @"";
    }
    
        
    return cell;
    
    
}


@end
