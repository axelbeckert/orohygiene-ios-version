//
//  NewsModel.h
//  ReadWriteFormats
//
//  Created by Axel Beckert on 27.02.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import <Foundation/Foundation.h>
@class News;

@interface NewsModel : NSObject
-(void) addNews: (News*) news;

-(NSInteger) numberOfNews;

-(News*) newsAtIndexPath: (NSIndexPath*) indexPath;
@end
