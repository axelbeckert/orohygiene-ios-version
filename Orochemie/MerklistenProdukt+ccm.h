//
//  MerklistenProdukt+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenProdukt.h"

@interface MerklistenProdukt (ccm)
+(MerklistenProdukt*)insertMerklistenProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteMerklistenProdukt;
@end
