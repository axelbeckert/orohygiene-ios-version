//
//  brancheTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "brancheTableViewController.h"
#import "JSMTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "ProduktAuflistungTableViewController.h"
#import "InformationenTableViewCell.h"


@interface brancheTableViewController()<UISearchBarDelegate>
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@end

@implementation brancheTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Branche class])];
    
    
    NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:cEntityBrancheName ascending:YES];
    
    NSArray *sortArray= [NSArray arrayWithObject:sortName];
    fetch.sortDescriptors = sortArray;
    
    
    fetch.sortDescriptors = @[ sortName ];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"index"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}


-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark - View Lifecycle
-(void) viewDidLoad

{
    [oroThemeManager  customizeTableView:self.tableView];
    
    //CoreData initalisieren
    AppDelegate* delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    

    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    //SerachBar
    UISearchBar *searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
       [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Desinfektion-Branche-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

#pragma mark - TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    return [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
 
    
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Branche *branche =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
  
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    [oroThemeManager customizeAccessoryView:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    // Configure the cell...
    cell.TitleLabelOutlet.text = branche.name;
    cell.imageOutlet.image = [UIImage imageNamed:branche.bild];
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{


    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];

    return container;
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showProdukteForBranche"]){
        
        ProduktAuflistungTableViewController *controller = segue.destinationViewController;
        controller.brancheForProduktResult = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        
    }
}

#pragma mark - SearchBarDelegate Methoden

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    

    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.name CONTAINS[c] %@", searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

@end
