//
//  BasisMerkliste+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "BasisMerkliste.h"

@interface BasisMerkliste (ccm)
+(BasisMerkliste*)insertBasisMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteBasisMerkliste;
@end
