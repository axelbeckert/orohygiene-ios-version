//
//  MerklisteQuickSave.m
//  orochemie
//
//  Created by Axel Beckert on 09.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklisteQuickSave.h"
@interface MerklisteQuickSave()
@property (nonatomic,strong)NSMutableArray *produktUIDArray;
@property (nonatomic,strong)NSMutableArray *reinigerUIDArray;
@end

@implementation MerklisteQuickSave


-(NSMutableArray*)produktUIDArray{
    if(!_produktUIDArray) _produktUIDArray=[NSMutableArray new];
    return _produktUIDArray;
    
}

-(NSMutableArray*)reinigerUIDArray{
    if(!_reinigerUIDArray) _reinigerUIDArray=[NSMutableArray new];
    return _reinigerUIDArray;
    
}


-(void)addProduktUIDToMerkliste:(NSString*)produktUID{
    
    [self.produktUIDArray addObject:produktUID];
    
}
-(void)addReinigerUIDToMerkliste:(NSString*)reinigerUID{
    
    [self.reinigerUIDArray addObject:reinigerUID];
}

-(NSMutableArray*)getProduktUIDForMerkliste{
    return self.produktUIDArray;
}

-(NSMutableArray*)getReinigerUIDForMerkliste{
    return self.reinigerUIDArray;
}
@end
