//
//  oroTestTheme1.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "oroTestTheme1.h"

@implementation oroTestTheme1


+(UIImage*)backgroundImage
{
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        return [UIImage imageNamed:@"background_oro1136.png"]; //Iphone 5
    }
    return  [UIImage imageNamed:@"background_oro.png"];
    
}

+(void)cutomizeView: (UIView*)theView
{
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[oroTheme backgroundImage]];
    [theView setBackgroundColor:backgroundColor];
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    NSLog(@"aktuelle Bildschirmhöhe: %f",height);
}


+(void)customizeTableView:(UITableView*)theTableView
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[oroTheme backgroundImage]];
    [theTableView setBackgroundView:imageView];
}

+(void)customizeCollectionView:(UICollectionView*)theCollectionView
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[oroTheme backgroundImage]];
    [theCollectionView setBackgroundView:imageView];
    
}

+(void)customizeButtonBackgroundImage:(UIImageView*)theImageView
{
    [theImageView setImage:[UIImage imageNamed:@"button.png"]];
}

+(void)customizeAccessoryView:(UITableViewCell*)theTableViewCell
{
    CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:[UIColor whiteColor]];
    accessory.highlightedColor = [UIColor blackColor];
    theTableViewCell.accessoryView = accessory;
    
}

+(UIImage*) customizeImageforNavigationBar
{
    return [[UIImage imageNamed:@"navbar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 100.0, 0.0, 100.0)];
}
@end
