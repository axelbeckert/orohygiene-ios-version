//
//  ReinigungNavifationViewController.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReinigungNavigationViewController
    : UIViewController <UICollectionViewDataSource>

@property(strong, nonatomic)
    IBOutlet UICollectionView *navigationCollectionView;
@end
