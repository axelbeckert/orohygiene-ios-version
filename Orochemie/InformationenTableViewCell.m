//
//  InformationenTableViewCell.m
//  orochemie
//
//  Created by Axel Beckert on 19.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "InformationenTableViewCell.h"

@implementation InformationenTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
