//
//  Produkte+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkte+ccm.h"

@implementation Produkte (ccm)
+(Produkte*) insertProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Produkte" inManagedObjectContext:managedObjectContext];
}
-(void)deleteProdukt
{
    [self.managedObjectContext deleteObject:self];
}
@end
