//
//  AddKrankheitserregerToDataBase.h
//  orochemie 
//
//  Created by Axel Beckert on 19.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
@class KrankheitserregerTableViewController;

@interface AddKrankheitserregerToDataBase : NSObject
@property (nonatomic, strong) KrankheitserregerTableViewController *krankheitserregerTableViewController;
-(BOOL) addKrankheitserreger:(NSArray*)krankheitserreger;

@end
