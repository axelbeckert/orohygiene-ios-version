//
//  Wirksamkeit+ccm.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Wirksamkeit+ccm.h"

@implementation Wirksamkeit (ccm)
+(Wirksamkeit*) insertWirksamkeitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext{
    
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Wirksamkeit class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteWirksamkeit{
    
    [self.managedObjectContext deleteObject:self];
    
}

-(NSString *)description{
    return [NSString stringWithFormat:@"Bezeichnung: %@ - Reihenfolge: %@",self.bezeichnung, self.sorting];
}

@end
