//
//  InfoScreenViewController.h
//  orochemie
//
//  Created by Axel Beckert on 13.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoScreenViewController : UIViewController
-(void)showInfoScreenOnFirstStartUp;
@end
