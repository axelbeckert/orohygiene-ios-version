//
//  produktStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktStaticTableViewController.h"
#import "Produkte.h"
#import "Anwendung.h"
#import "JSMToolBox.h"
#import "ProduktStaticTabBarViewController.h"
#import "PdfViewViewController.h"
#import "Gefahrstoffsymbole.h"
#import "WebviewViewController.h"
#import "ProductToSavedMerklisteTableViewController.h"
#import "MerklistenTabbarController.h"
#import "Pictogramm.h"

#import "InfoScreenViewController.h"



#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "JSMLabel.h"



@interface ProduktStaticTableViewController () <ProductToSavedMerklisteTableViewControllerDelegate>
@property(nonatomic,strong) AppDelegate *appDelegate;
@property (nonatomic,strong)NSString *merklistenInfoScreen;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *pictogrammImageViewThree;



@end

@implementation ProduktStaticTableViewController
NSInteger currentPage = 0;
NSInteger zusatzHoehe = 25;




-(BOOL)shouldAutorotate
{
        return YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager  customizeTableView:self.tableView];
    [oroThemeManager customizeButtonImage:self.sicherheitsdatenblattButtonOutlet];
    [oroThemeManager customizeButtonImage:self.betriebsanweisungButtonOutlet];
    [oroThemeManager customizeButtonImage:self.hinweisButtonOutlet];
    [oroThemeManager customizeButtonImage:self.hygieneChecklistenButtonOutlet];


    //AppDelegate Initialisieren
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    
    
    //Einstellung des Abstandes zur Navigationbar
    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.contentInset = inset;
    
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc]init];
    paragraph.alignment= NSTextAlignmentJustified;
    
    paragraph.firstLineHeadIndent = 0.0;
    paragraph.paragraphSpacingBefore = 0.0;
    
    
    self.staticTableViewImageAndButtonCell.backgroundColor = [UIColor clearColor];


    if(self.produkt != nil){
        
                                                                  
        
        Produkte *p = self.produkt;
     
        //Anzeigen der Gefahrstoffsymbole
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortierung"
                                                     ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *gefahrstoffsymbole =[[NSArray arrayWithArray:[p.gefahrstoffsymbole allObjects]] sortedArrayUsingDescriptors:sortDescriptors];

        
        if([gefahrstoffsymbole count]==4)
        {
            Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
            self.gefahrstoffImageEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
            
            Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
            self.gefahrstoffImageZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
            
            Gefahrstoffsymbole *symbol3 = [gefahrstoffsymbole objectAtIndex:2];
            self.gefahrstoffImageDreiOutlet.image = [UIImage imageNamed:symbol3.bild];
        
            Gefahrstoffsymbole *symbol4 = [gefahrstoffsymbole objectAtIndex:3];
            self.gefahrstoffImageVierOutlet.image = [UIImage imageNamed:symbol4.bild];
            
            
        } else if([gefahrstoffsymbole count]==3)
        {
            Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
            self.gefahrstoffImageEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
            
            Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
            self.gefahrstoffImageZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
            
            Gefahrstoffsymbole *symbol3 = [gefahrstoffsymbole objectAtIndex:2];
            self.gefahrstoffImageDreiOutlet.image = [UIImage imageNamed:symbol3.bild];
            
            
        } else if([gefahrstoffsymbole count]==2)
        {
            Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
            self.gefahrstoffImageEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
            
            Gefahrstoffsymbole *symbol2 = [gefahrstoffsymbole objectAtIndex:1];
            self.gefahrstoffImageZweiOutlet.image = [UIImage imageNamed:symbol2.bild];
            
            
        } else if ([gefahrstoffsymbole count]==1)
        {
            Gefahrstoffsymbole *symbol1 = [gefahrstoffsymbole objectAtIndex:0];
            self.gefahrstoffImageEinsOutlet.image = [UIImage imageNamed:symbol1.bild];
        }
        
        //Anzeige der Pictogramme
        
        NSArray *pictrogramme = [p.pictogramm allObjects];
        
        if([pictrogramme count]==1){
            Pictogramm *pg = [pictrogramme objectAtIndex:0];
            self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
        
        } else if ([pictrogramme count]==2){
            Pictogramm *pg = [pictrogramme objectAtIndex:0];
            self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
            
            Pictogramm *pg2 = [pictrogramme objectAtIndex:1];
            self.pictogrammImageViewTwo.image = [UIImage imageNamed:pg2.bild];
        
        } else if([pictrogramme count]==3){
            Pictogramm *pg = [pictrogramme objectAtIndex:0];
            self.pictogrammImageViewOne.image = [UIImage imageNamed:pg.bild];
            
            Pictogramm *pg2 = [pictrogramme objectAtIndex:1];
            self.pictogrammImageViewTwo.image = [UIImage imageNamed:pg2.bild];
            
            Pictogramm *pg3 = [pictrogramme objectAtIndex:2];
            self.pictogrammImageViewThree.image = [UIImage imageNamed:pg3.bild];
       
        }
        

        

        
        self.produktNameLabelOutlet.text=[NSString stringWithFormat:@"%@ - %@",p.name,p.zusatz];
        self.produktNameLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
       
        self.produktZusatzLabelOutlet.text=[NSString stringWithFormat:@"%@",p.zusatz];
        self.produktZusatzLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
        
        self.produktImageViewOutlet.image = [UIImage imageNamed:p.bild];
        
        self.produktEinsatzbereichLabelOutlet.text= [NSString stringWithFormat:@"%@",p.einsatzbereich];
        self.produktEinsatzbereichLabelOutlet.verticalAlignment=JSMLabelAlignmentTop;
        
        self.produktListungLabelOutlet.text=[NSString stringWithFormat:@"%@",p.listung];
        self.produktListungLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;
        
        self.produktWirksamkeitLabelOutlet.text=[NSString stringWithFormat:@"%@",p.wirksamkeit];
        self.produktWirksamkeitLabelOutlet.verticalAlignment= JSMLabelAlignmentTop;
        
        self.produktDosierungLabelOutlet.text=[NSString stringWithFormat:@"%@",p.dosierung];
        self.produktDosierungLabelOutlet.verticalAlignment = JSMLabelAlignmentTop;

        if([p.pflichttext isEqualToString:@""])
        {
           self.produktPflichttextOutlet.text=@"keine Angabe";
        } else {
        self.produktPflichttextOutlet.text=p.pflichttext;
        }
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Produkt-Detail-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Produkt"
                                                              action:@"Show"
                                                               label:self.produkt.nameHauptbezeichnung
                                                               value:nil] build]];
        
    }
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //Prüfen ob der InfoScreen angezeigt werden soll
    NSUserDefaults *merklistenInfoScreenDefaults = [NSUserDefaults standardUserDefaults];
    
    // holen der Info
    self.merklistenInfoScreen = [merklistenInfoScreenDefaults valueForKey:@"merklistenInfoScreen"];


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    if(self.branchenName)
    {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.branchenName;
        self.tabBarController.navigationItem.titleView=tlabel;

    } else if (self.anwendungName){
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.anwendungName;
        self.tabBarController.navigationItem.titleView=tlabel;
    
    } else if(!self.branchenName && self.tabBarController){
        self.tabBarController.title=@"Information";
    }

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Desinfektions-Produkt-Detail-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];

    if(![self.merklistenInfoScreen isEqualToString:@"NO"]){
        
        [self showInfoScreenOnFirstStartUp];
    }
    
}


-(void)showInfoScreenOnFirstStartUp {

    //Setzen der Anzeige des InfoScreen auf NO
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:@"NO" forKey:@"merklistenInfoScreen"];
    [prefs synchronize];
    
    self.merklistenInfoScreen=@"NO";
    
    InfoScreenViewController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoScreenViewController"];
    [infoController showInfoScreenOnFirstStartUp];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   
    if(section == 1)
    {
        return nil;
    }
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Produktbezeichnung";
    }  else if (section == 2)
    {
        label.text=@"Einsatzbereich";
    } else if (section == 3)
    {
        label.text=@"Listung / Prüfung";
    } else if (section == 4)
    {
        label.text=@"Wirksamkeit";
    } else if (section == 5)
    {
        label.text=@"Dosierung / Einwirkzeit";
    } else if (section == 6)
    {
        label.text=@"Hinweise";
    } else if (section == 7)
    {
        label.text=@"Gefahrstoffsymbole";
    } else if (section == 8)
    {
        label.text=@"Checklisten";
    }
    
    
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 0;
    }
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - TableView Definitons

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {    // Produktname Label
        
        return [self calculateLabelHeightWithLabelAndText:self.produktNameLabelOutlet];
        
    }
  
    
    if (indexPath.section == 1 && indexPath.row == 0) {    // Bild
        
        if([UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPad)
        {

            return 144; //144
        } else {

            return  175;
        }
    }
    

    if (indexPath.section == 2 && indexPath.row == 0) {    // Einsatzbereich TextView
        
        return [self calculateLabelHeightWithLabelAndText:self.produktEinsatzbereichLabelOutlet];
        
    }

    if (indexPath.section == 2 && indexPath.row == 1) {    // Einsatzbereich Pictogramme
        
        return 50;
    }
    
    if (indexPath.section == 3 && indexPath.row == 0) {    // Listung TextView
        
        return [self calculateLabelHeightWithLabelAndText:self.produktListungLabelOutlet];

    }

    if (indexPath.section == 4 && indexPath.row == 0) {    // Wirksamkeit TextView
        
        return [self calculateLabelHeightWithLabelAndText:self.produktWirksamkeitLabelOutlet];

    }

    if (indexPath.section == 5 && indexPath.row == 0) {    // Dosierung TextView
       
         return [self calculateLabelHeightWithLabelAndText:self.produktDosierungLabelOutlet];

    }
    
    if (indexPath.section == 6 && indexPath.row == 0) {    // Plichttext TextView
        
         return [self calculateLabelHeightWithLabelAndText:self.produktPflichttextOutlet];
        
    }
    
    if (indexPath.section == 7 && indexPath.row == 0) {    // Gefahrstoffsymbole
        
        return 80;
    }
    
    if (indexPath.section == 8 && indexPath.row == 0) {    // Hygienechecklisten
        
        return 44;
    }
    
    
    return 23;

}

/**
 * calculateLabelHeightWithLabelAndText
 *
 * Calculates the label height from given label containing a text
 **/
-(int)calculateLabelHeightWithLabelAndText: (JSMLabel *)label {
    
    UIFont* originalFont = label.font;

    [label setFont:[UIFont systemFontOfSize:18.0]];
    CGRect bounds = label.bounds;
    CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
    UIFont* font = label.font;
    
    CGRect sizeOfText = [label.text boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];

    [label setFont:originalFont];
    
    return MAX(44, sizeOfText.size.height+zusatzHoehe);
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"infoScreenSegue"]){
        InfoScreenViewController *controller = segue.destinationViewController;
        controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
      //  self.navigationController.definesPresentationContext = YES;
        self.navigationController.navigationBar.layer.zPosition = -1;
        

    }
    
    if([segue.identifier isEqualToString:@"sicherheitsdatenblattSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Sicherheitsdatenblatt";
        controller.pdfFileName=[[NSString alloc]initWithFormat:@"datenblatt_%@.pdf",self.produkt.informationPdf];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/datenblatt_%@.pdf",self.produkt.informationPdf];
        controller.webViewToolbar=HideToolBar;

        
        
    } else if ([segue.identifier isEqualToString:@"betriebsanweisungSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Betriebsanweisung";
        controller.pdfFileName=[NSString stringWithFormat:@"betriebsanweisung_%@.pdf",self.produkt.informationPdf];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/betriebsanweisung_%@.pdf",self.produkt.informationPdf];

        
        

        
    } else if ([segue.identifier isEqualToString:@"produktinformationSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        controller.webViewTitle=@"Produktinformation";
        controller.pdfFileName=[[NSString alloc]initWithFormat:@"produktinformation_%@.pdf",self.produkt.informationPdf];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/de/download/produktinformation_%@.pdf",self.produkt.informationPdf];
        controller.webViewToolbar=HideToolBar;

        
    } else if ([segue.identifier isEqualToString:@"hygieneChecklistenSegue"]){
        
        WebviewViewController *controller=segue.destinationViewController;
        controller.webViewTitle=cWebsiteTitleFuerHygieneChecklisten;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneChecklisten;
        controller.urlString=cWebsiteFuerHygieneChecklisten;
    }
}



- (IBAction)sicherheitsDatenblattTouchUpInside:(id)sender {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (IBAction)betriebsanweisungToucUpInside:(id)sender {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
}

- (IBAction)hinweisTouchUpInside:(id)sender {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
}



- (IBAction)hygieneChecklistenButtonAction:(id)sender {
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)addToMerkliste {
    
    UIAlertController *actionSheet = [AlertHelper getUIActionSheetWithTitle:@"aktuelle Merkliste" andMessage:nil andCancelButtonWithCompletionHandler:nil];
    

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {

        CGRect sourceRect = CGRectZero;
        sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
        sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
        
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = sourceRect;
        [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"aktuelle Merkliste" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self saveProduktToAktuelleMerkliste];
    }]];

    //Wenn mehr als eine Merkliste vorhanden ist dann bitte den Button dem ActionSheet hinzufügen
    if([self checkIfSavedMerklistenAreThere]){
        
        [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"bereits vorhandene Merkliste" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self saveProduktToSavedMerkliste];
        }]];

    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Merklisten anzeigen" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self showMerklisten];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)saveProduktToAktuelleMerkliste {
    [self.appDelegate.merkliste addProdukteObject:self.produkt];
    [self.appDelegate saveContext];
    
    UIAlertController *showInfoAlertView = [AlertHelper showInfoAlertWithMessage:@"Produkt wurde der Merkliste hinzugefügt"];
    [self presentViewController:showInfoAlertView animated:YES completion:nil];
    [self.appDelegate saveMerklistenDataToMerklistenDB];
    [self performSelector:@selector(dismissSaveIsDoneInfoView:) withObject:showInfoAlertView afterDelay:2];
}

-(BOOL)checkIfSavedMerklistenAreThere {
    //Prüfen ob bereits mehr als eine Merkliste vorhanden ist (Basismerkliste ist immer vorhanden)
    //Fetchen der Merklisten
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSArray *merklistenArray = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if(merklistenArray.count > 1) return YES;
    return NO;
}

-(void)saveProduktToSavedMerkliste {
    
    ProductToSavedMerklisteTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductToSavedMerklisteTableViewController"];
    controller.produkt = self.produkt;
    controller.delegate = self;
    controller.showProductToSavedMerklisteFrom=DesinfektionChannel;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showMerklisten {
    
    MerklistenTabbarController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
    controller.showMerklistenTabBarControllerFrom= DesinfektionChannel;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)dismissSaveIsDoneInfoView:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - ProductSavedToMerkliste Delegate Methoden
-(void) informUserAboutProductSavedToMerkliste:(Merkliste*)merkliste{
    
    UIAlertController *infoAlert = [AlertHelper showInfoAlertWithMessage:[NSString stringWithFormat:@"Das Produkt wurde zur Merkliste %@ hinzugefügt", merkliste.bezeichnung]];
    
    [self.appDelegate saveMerklistenDataToMerklistenDB];
    [self presentViewController:infoAlert animated:YES completion:nil];
    [self performSelector:@selector(dismissSaveIsDoneInfoView:) withObject:infoAlert afterDelay:3];
    
}

@end
