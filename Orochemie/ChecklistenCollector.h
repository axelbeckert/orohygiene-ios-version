//
//  ChecklistenCollector.h
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JsonCollector.h"
@class HygieneChecklistenTableViewController;
@class KrankheitserregerTableViewController;

@interface ChecklistenCollector : JsonCollector
@property (nonatomic, strong) HygieneChecklistenTableViewController *hygienechecklistenTableViewController;
@property (nonatomic, strong) KrankheitserregerTableViewController *krankheitserregerTableViewController;
-(void) collectModelPlans;
-(void) collectFlyerPlans;
-(void) collectWebsiteFlyer;
-(void) startCollectingChecklists;

@end
