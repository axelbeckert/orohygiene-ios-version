//
//  PLZ.m
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "PLZ.h"
#import "Berater.h"


@implementation PLZ

@dynamic plz;
@dynamic ort;
@dynamic land;
@dynamic berater;

@end
