//
//  JSMLabel.m
//  orochemie
//
//  Created by Axel Beckert on 25.02.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JSMLabel.h"

@implementation JSMLabel

-(void)drawTextInRect:(CGRect)rect
{

    CGFloat lineHeight = self.font.lineHeight;
    CGFloat height;
    
    if(self.numberOfLines ==1){
        height = lineHeight;
    } else {
        CGRect boundsRect = [self.text boundingRectWithSize:self.bounds.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.font} context:nil];
        height = boundsRect.size.height;
        //NSLog(@"boundsRect.height= %f", boundsRect.size.height );
    }
    
    if(height < self.bounds.size.height){
        rect.origin.y = ((rect.size.height - height) / 2);
        rect.origin.y *= (self.verticalAlignment == JSMLabelAlignmentTop) ? - 1.0 : 1.0;
        
    }
    
    

    
    [super drawTextInRect:rect];
}

-(void)setVerticalAlignment:(JSMLabelAlignment)verticalAlignment
{
	_verticalAlignment = verticalAlignment;
	[self setNeedsDisplay];
}

@end
