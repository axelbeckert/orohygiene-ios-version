//
//  Checkliste+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 20.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Checkliste.h"


NS_ASSUME_NONNULL_BEGIN

@interface Checkliste (CoreDataProperties)

+ (NSFetchRequest<Checkliste *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *branchmodel;
@property (nullable, nonatomic, copy) NSString *kurzbezeichnung;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *sortierung;
@property (nullable, nonatomic, copy) NSNumber *typemodel;
@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *url;
@property (nullable, nonatomic, copy) NSNumber *websitemodel;
@property (nullable, nonatomic, copy) NSString *tstamp;
@property (nullable, nonatomic, retain) Checklistenkategorie *checklistenkategorie;
@property (nullable, nonatomic, retain) NSSet<Krankheitserreger *> *krankheitserreger;

@end

@interface Checkliste (CoreDataGeneratedAccessors)

- (void)addKrankheitserregerObject:(Krankheitserreger *)value;
- (void)removeKrankheitserregerObject:(Krankheitserreger *)value;
- (void)addKrankheitserreger:(NSSet<Krankheitserreger *> *)values;
- (void)removeKrankheitserreger:(NSSet<Krankheitserreger *> *)values;

@end

NS_ASSUME_NONNULL_END
