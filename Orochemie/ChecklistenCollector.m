//
//  ChecklistenCollector.m
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ChecklistenCollector.h"
#import "NewsletterTableViewController.h"
#import "Newsletter.h"
#import "NewsletterModel.h"
#import "NewsletterWebViewController.h"
#import "AppDelegate.h"
#import "ModelPlanChecklist.h"
#import "WebsiteChecklist.h"
#import "HygieneChecklistenTableViewController.h"
#import "KrankheitserregerTableViewController.h"


@interface ChecklistenCollector () < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >
@property(nonatomic,strong) AppDelegate *appDelegate;
@property int collectFlyerPlansIsActive;
@property int collectModelPlansIsActive;
@property int collectWebsiteFlyerIsActive;
@end

@implementation ChecklistenCollector   
NSString *const collectFlyerPlans=@"collectFlyerPlans";
NSString *const collectModelPlans=@"collectModelPlans";
NSString *const collectWebsiteFlyer=@"collectWebsiteFlyer";

-(void) startCollectingChecklists{
    
    if(self.hygienechecklistenTableViewController!=nil){
    
        if(self.hygienechecklistenTableViewController.musterplaene==1){
            self.collectModelPlansIsActive=1;
            [self collectModelPlans];
        } else {
            self.collectFlyerPlansIsActive=1;
            self.collectWebsiteFlyerIsActive=1;
            
            [self collectFlyerPlans];
            [self collectWebsiteFlyer];
        }
        
    } else {
        self.collectModelPlansIsActive=1;
        self.collectFlyerPlansIsActive=1;
        self.collectWebsiteFlyerIsActive=1;
        
        [self collectModelPlans];
        [self collectFlyerPlans];
        [self collectWebsiteFlyer];
    }

    
}


-(void) collectModelPlans{

    [self collectJsonWithUrl:urlStringModelPlan andTaskDescription:collectModelPlans];
}

-(void) collectFlyerPlans{
    
    [self collectJsonWithUrl:urlStringFlyerPlan andTaskDescription:collectFlyerPlans];
}

-(void) collectWebsiteFlyer{
    
    [self collectJsonWithUrl:urlStringWebsiteFlyer andTaskDescription:collectWebsiteFlyer];
}

-(void)checkIfDownloadProcessIsActive{
    int processActive = 0;
    if(self.collectWebsiteFlyerIsActive==1) processActive = 1;
    if(self.collectModelPlansIsActive==1) processActive = 1;
    if(self.collectFlyerPlansIsActive==1) processActive =1;
    
    if(processActive==0 && self.showSVProgressHUD==1)[SVProgressHUD dismiss];
}

-(void) checkDownloadTaskDescription:(NSString*)downloadTaskDescription andBuildChecklistsWithData:(NSArray*)data{
    
    if([downloadTaskDescription isEqualToString:collectModelPlans]){
        ModelPlanChecklist *modelplanChecklist = [ModelPlanChecklist new];
        modelplanChecklist.hygienechecklistenTableViewController = self.hygienechecklistenTableViewController;
        [modelplanChecklist addModelPlanChecklistToDatabase:data];
        self.collectModelPlansIsActive = 0;
    } else if ([downloadTaskDescription isEqualToString:collectFlyerPlans]){
        ModelPlanChecklist *modelplanChecklist = [ModelPlanChecklist new];
        modelplanChecklist.hygienechecklistenTableViewController = self.hygienechecklistenTableViewController;
        [modelplanChecklist addModelPlanChecklistToDatabase:data];
        self.collectFlyerPlansIsActive=0;
    } else if ([downloadTaskDescription isEqualToString:collectWebsiteFlyer]){
        WebsiteChecklist* websiteChecklist = [WebsiteChecklist new];
        websiteChecklist.hygienechecklistenTableViewController = self.hygienechecklistenTableViewController;
        [websiteChecklist addWebsiteChecklistToDatabase:data];
        self.collectWebsiteFlyerIsActive=0;
    }
    [self checkIfDownloadProcessIsActive];
    
    if([self.krankheitserregerTableViewController respondsToSelector:@selector(startCollectiongKrankheitserreger)]){
         [self.krankheitserregerTableViewController startCollectiongKrankheitserreger];
    }
    
    NSLog(@"Download Finished: %@", downloadTaskDescription);

}

@end
