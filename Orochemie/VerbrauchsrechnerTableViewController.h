//
//  VerbrauchsrechnerTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 04.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerbrauchsrechnerTableViewController : UITableViewController
@property(nonatomic,strong) Reiniger *reiniger;
@end
