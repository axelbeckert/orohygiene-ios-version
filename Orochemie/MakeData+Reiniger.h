//
//  MakeData+Reiniger.h
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MakeData.h"

@interface MakeData (Reiniger)
- (void)makeReinigerData;
@end
