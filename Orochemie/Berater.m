//
//  Berater.m
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Berater.h"


@implementation Berater

@dynamic image;
@dynamic name;
@dynamic telefon;
@dynamic gebiet;
@dynamic plz;

@end
