//
//  BestellungTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "BestellungTableViewController.h"
#import "ProduktAuflistungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "InformationenTableViewCell.h"
#import "BarcodeReader.h"
#import "KrankheitserregerTableViewController.h"
#import "BestellProdukteTableViewController.h"
#import "BestelluebersichtTableViewController.h"

@interface BestellungTableViewController ()
@property (nonatomic,strong) NSArray *navigationPoints;
@end

@implementation BestellungTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];
    
    
    self.title=@"Produkt-Bestellung";
    
    
    self.navigationPoints = @[@"Desinfektionsmittel + Hygienepräperate",@"Zubehör Desinfektionsmitel",@"Reinigungsmittel",@"Zubehör Reinigungsmittel",@"Gesamtbestellung prüfen und Adresse eingeben"];
    
    self.bestellMengenArray = [NSMutableArray new];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"BestellmengenArray: %@",self.bestellMengenArray);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.navigationPoints.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    // Configure the cell...
    cell.TitleLabelOutlet.text = [self.navigationPoints objectAtIndex:indexPath.row];
    
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
        controller.bestellMengenArray = self.bestellMengenArray;
        controller.bestellungTableViewController = self;
        [self.navigationController pushViewController:controller animated:YES];
        
        
    } else if (indexPath.row == 1)
    {
        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 2)
    {
        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }  else if (indexPath.row == 3)
    {
        BestellProdukteTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestellProdukteTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }  else if (indexPath.row == 4)
    {
        BestelluebersichtTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BestelluebersichtTableViewController"];
        controller.bestellMengenArray = self.bestellMengenArray;
        controller.bestellungTableViewController = self;
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    
}
@end
