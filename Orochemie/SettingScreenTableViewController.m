//
//  SettingScreenTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 25.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "SettingScreenTableViewController.h"

@interface SettingScreenTableViewController()
@property (strong, nonatomic) IBOutlet UITextView *googleAnalyticsTextViewOutlet;
@property (strong, nonatomic) IBOutlet UISwitch *googleAnalyticsSwitchOutlet;
@property (strong, nonatomic) IBOutlet UITextView *videoTextViewOutlet;
@property (strong, nonatomic) IBOutlet UISwitch *videoSwitchOutlet;
@end

@implementation SettingScreenTableViewController

NSInteger zusatzHoeheSettings = 60;

#pragma mark View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];

    NSArray *barButtonArray = [[NSArray alloc]initWithObjects:[oroThemeManager homeBarButton:self], nil];
    self.tabBarController.navigationItem.rightBarButtonItems = barButtonArray;
    
    self.title = @"Einstellungen";

    [self.googleAnalyticsSwitchOutlet addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    
    [self.videoSwitchOutlet addTarget:self action:@selector(setVideoState:) forControlEvents:UIControlEventValueChanged];
    
    NSUserDefaults *dbVersionDefaults = [NSUserDefaults standardUserDefaults];
    
    // Prpüfen wie der Status des Trackings aktuell ist:
    NSString* googleTracking = [dbVersionDefaults valueForKey:@"googleTracking"];
    //Prüfen wie der Status der Videos aktuell ist:
    NSString* videoDownload = [dbVersionDefaults valueForKey:@"DownloadingVideos"];

 
    if([googleTracking isEqualToString:@"YES"]){
        self.googleAnalyticsSwitchOutlet.on = YES;
    } else {
        self.googleAnalyticsSwitchOutlet.on = NO;

    }
    
    if([videoDownload isEqualToString:@"YES"]){
        self.videoSwitchOutlet.on=YES;
    } else {
        self.videoSwitchOutlet.on=NO;

    }
    //NSLog(@"Google Tracking: %@",googleTracking);
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
 
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"App-Settings-Screen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Definitons

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 &&
        indexPath.row == 0) { // Google Analytics Label
        
        CGRect bounds = self.googleAnalyticsTextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.googleAnalyticsTextViewOutlet.font;
        
        CGRect sizeOfText = [self.googleAnalyticsTextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zusatzHoeheSettings);
    }

    if (indexPath.section == 0 &&
        indexPath.row == 0) { // video Label
        
        CGRect bounds = self.videoTextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.videoTextViewOutlet.font;
        
        CGRect sizeOfText = [self.videoTextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zusatzHoeheSettings);
    }
    
    return 44;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Anwendungsvideos";
    } else if (section ==1)
    {
        label.text = @"Google Analytics";

    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section == 1)
//    {
//        return 0;
//    }
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}









-(void)logButtonPress:(UIButton *)button{
    
    //    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    //    
    //    [tracker set:kGAIScreenName value:@"ProduktDetailStaticTableViewController"];
    //    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
    //                                                          action:@"touch"
    //                                                           label:[button.titleLabel text]
    //                                                           value:nil] build]];
    //    [tracker set:kGAIScreenName value:nil];
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)setState:(id)sender
{
    BOOL state = [sender isOn];
    NSString *rez = state == YES ? @"YES" : @"NO";
    
    //Setzen der Datenbankversion
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:rez forKey:@"googleTracking"];
    [prefs synchronize];
    ////NSLog(@"Switch State: %@",rez);
}

-(void)setVideoState:(id)sender{
    BOOL state = [sender isOn];
    NSString *rez = state == YES ? @"YES" : @"NO";
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:rez forKey:@"DownloadingVideos"];
    
    if([rez isEqualToString:@"YES"]){
        [prefs setValue:@"NO" forKey:@"YouTubeVideos"];
    } else {
        [prefs setValue:@"YES" forKey:@"YouTubeVideos"];
    }

    [prefs synchronize];
}
@end
