//
//  Produkteinwirkzeit.m
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkteinwirkzeit.h"
#import "Produktdosierung.h"


@implementation Produkteinwirkzeit

@dynamic einwirkzeit;
@dynamic mischungsverhaeltnis;
@dynamic rechengroesse;
@dynamic sortierung;
@dynamic produktdosierung;

@end
