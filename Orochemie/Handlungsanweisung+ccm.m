//
//  Handlungsanweisung+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Handlungsanweisung+ccm.h"

@implementation Handlungsanweisung (ccm)
+(Handlungsanweisung*) insertHandlungsanweisungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Handlungsanweisung" inManagedObjectContext:managedObjectContext];
}
-(void)deleteHandlungsanweisung
{
    [self.managedObjectContext deleteObject:self];
}
@end
