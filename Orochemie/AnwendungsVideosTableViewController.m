//
//  AnwendungsVideosTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 25.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "AnwendungsVideosTableViewController.h"
#import "Video.h"
#import "VideoPlayerViewController.h"
#import "KrankheitserregerTableViewStandardCell.h"
#import "VideoDownloadViewController.h"
#import "WebviewViewController.h"
#import "VideoHelper.h"


@interface AnwendungsVideosTableViewController ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property BOOL noVideosToPlay;
@end

@implementation AnwendungsVideosTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Video class])];
    NSPredicate * videoPredicate;
    if(self.showVideosFrom == ReinigerChannel){
        videoPredicate = [NSPredicate predicateWithFormat:@"reinigerVideo == 1"];
    } else {
        videoPredicate = [NSPredicate predicateWithFormat:@"reinigerVideo == 0"];
    }
    
    
    fetch.predicate = videoPredicate;
    
    NSSortDescriptor *sortKategorie= [[NSSortDescriptor alloc] initWithKey:@"kategorie" ascending:YES];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierung" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= [NSArray arrayWithObjects:sortKategorie,sortReihenfolge, nil];
    fetch.sortDescriptors = sortArray;
    
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"kategorie"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

-(void) viewDidLoad

{

    
    //CoreData initalisieren
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = delegate.managedObjectContext;
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Anwendungsvideos";
    
  
    
    if (self.showVideosFrom == ReinigerChannel){
        //NSLog(@"Zeige Videos vom Reiniger Kanal");
        [oroThemeManager  customizeReinigerTableView:self.tableView];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reinigerVideo == 1"];
        
        self.fetchedResultsController.fetchRequest.predicate = predicate;
        
        
    } else {
        //NSLog(@"Zeige Videos vom Desinfektions Kanal");
        [oroThemeManager  customizeTableView:self.tableView];
        
    }
    
    
    [self.fetchedResultsController performFetch:nil];

    
    self.noVideosToPlay = ![self checkIfVideosAreThere];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"AWC -> viewWillAppear");
    self.watchVideoNow = 0;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Anwendungsvideos-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    if(self.watchVideoNow==1){
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *additionalSectionTitle =@"";
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Desinfektion allgemein";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"F"])
    {
        if(self.showVideosFrom==DesinfektionChannel){
            additionalSectionTitle = @"Flächendesinfektion";
        } else {
            additionalSectionTitle = @"Feinsteinzeug";
        }
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"I"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"S"])
    {
        if(self.showVideosFrom==DesinfektionChannel){
            additionalSectionTitle = @"Spezialanwendungen";
        } else {
            additionalSectionTitle = @"Sanitär";
        }
    }  else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"R"])
    {
        additionalSectionTitle = @"Reinigung und Desinfektion";
    
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"T"])
    { // Da nur ein Buchstabe geprüft werden kann steht die Prüfung auf T da das vor U kommt
        additionalSectionTitle = @"Unterhaltsreinigung";
    
    }else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"U"])
    {
        additionalSectionTitle = @"Unternehmen";
        
        
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"G"])
    {
        additionalSectionTitle = @"Garnpad";
    }
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    //Da ein T für den Index von Unterhaltsreinigung verwendet werden musste, muss hier auf T geprüft werden damit ein U angezeigt wird.
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"T"]){
    
        label.text = [NSString stringWithFormat:@"U - %@", additionalSectionTitle];
    
    } else {
       
        label.text = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    }
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoCell" forIndexPath:indexPath];
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    [oroThemeManager customizeAccessoryView:cell];
    
    //[oroThemeManager customizeTableViewCellBackground:cell];
    
    Video *video = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0]; //12
    cell.textLabel.numberOfLines=0;
    cell.textLabel.text = video.bezeichnung;
    return cell;
}



-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    self.noVideosToPlay = ![self checkIfVideosAreThere];
    
    NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
    NSString* youTubeVideo = [usingOroHygieneDefaults valueForKey:@"YouTubeVideos"];
  
    if(!self.noVideosToPlay && [youTubeVideo isEqualToString:@"YES"]){
        
     WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        
        Video *video=[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.urlString = video.youtube;
        controller.pdfFileName = @"video";
        controller.webViewTitle = @"YouTube-Video";
        
         [self.navigationController pushViewController:controller animated:YES];
        
        return NO;
    } else if (self.noVideosToPlay && [youTubeVideo isEqualToString:@"NO"]){
        return NO;
    }
    
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

    
        if([segue.identifier isEqualToString:@"VideoPlayer"]){
        VideoPlayerViewController *controller = segue.destinationViewController;
        controller.video = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.anwendungsVideosTableViewController = self;
        self.watchVideoNow = 1;

        
        
    }
}

#pragma mark- Check if Videos are there

-(BOOL)checkIfVideosAreThere{
    
    if(![VideoHelper checkVideoFiles]){
        VideoHelper *vh = [VideoHelper new];
        return [vh checkIfVideosAreThereWithStoryboard:self.storyboard andNavigationController:self.navigationController];
    }
    
    return YES;
}

#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
