//
//  Gefahrstoffsymbole.h
//  orochemie
//
//  Created by Axel Beckert on 19.05.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Reiniger;

@interface Gefahrstoffsymbole : NSManagedObject

@property (nonatomic, retain) NSString * bild;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * sortierung;
@property (nonatomic, retain) NSSet *produkte;
@property (nonatomic, retain) NSSet *reiniger;
@end

@interface Gefahrstoffsymbole (CoreDataGeneratedAccessors)

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet *)values;
- (void)removeProdukte:(NSSet *)values;

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

@end
