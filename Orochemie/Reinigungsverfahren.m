//
//  Reinigungsverfahren.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsverfahren.h"
#import "Reiniger.h"


@implementation Reinigungsverfahren

@dynamic bezeichnung;
@dynamic reiniger;

@end
