//
//  MengeDosierungViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "MengeDosierungViewController.h"
#import "DosierungStaticTableViewController.h"

@interface MengeDosierungViewController ()
@property (nonatomic,strong) NSString *mengeGebrauchsfertigerLoesung;
@end

@implementation MengeDosierungViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayPickerMengeDosierungPicker =@[@"1 Liter",
                                             @"2 Liter",
                                             @"3 Liter",
                                             @"4 Liter",
                                             @"5 Liter",
                                             @"6 Liter",
                                             @"7 Liter",
                                             @"8 Liter",
                                             @"9 Liter",
                                             @"10 Liter",
                                             ];
    

    self.topToolBarOutlet.layer.borderWidth = 0.5;
    self.topToolBarOutlet.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.4].CGColor;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrayPickerMengeDosierungPicker.count;
}



- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        
        label.text=[arrayPickerMengeDosierungPicker objectAtIndex:row];
        label.font=[UIFont boldSystemFontOfSize:30.0];
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 600, 40)];
        myView.backgroundColor = [UIColor redColor];
        [myView addSubview:label];
        
        return myView;
        
    } else {
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 40)];
        
        label.text=[arrayPickerMengeDosierungPicker objectAtIndex:row];
        label.font=[UIFont boldSystemFontOfSize:15.0];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350, 40)];
        
        [myView addSubview:label];
        
        
        return myView;
        
    }
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.mengeGebrauchsfertigerLoesung = [arrayPickerMengeDosierungPicker objectAtIndex:row];

}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return 90;
    }
    
    return 44;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //NSLog(@"Menge %@", self.mengeGebrauchsfertigerLoesung);
    if(!self.mengeGebrauchsfertigerLoesung){
        self.dosierungStaticTableViewController.mengeGebrauchsfertigerLoesungZusatzinformationOutlet.text = [arrayPickerMengeDosierungPicker objectAtIndex:0];
        self.dosierungStaticTableViewController.rechnen = @(1);
        [self.dosierungStaticTableViewController rechne2];
        //NSLog(@"Rechnen freigegeben? %@",self.dosierungStaticTableViewController.rechnen);
        
    } else {
        
        self.dosierungStaticTableViewController.mengeGebrauchsfertigerLoesungZusatzinformationOutlet.text = self.mengeGebrauchsfertigerLoesung;
        self.dosierungStaticTableViewController.rechnen = @(1);
        [self.dosierungStaticTableViewController rechne2];
        //NSLog(@"Rechnen freigegeben? %@",self.dosierungStaticTableViewController.rechnen);

    }
    
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)auswahlUebernehmenButtonAction:(id)sender
{
[self dismissViewControllerAnimated:YES completion:nil];
}

@end
