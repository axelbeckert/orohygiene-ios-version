//
//  ModelPlanChecklist.m
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ModelPlanChecklist.h"
#import "Checklistenkategorie+CoreDataProperties.h"
#import "Checkliste+CoreDataProperties.h"
#import "AppDelegate.h"
#import "HygieneChecklistenTableViewController.h"
#import "ChecklistenkategorieCheck.h"

@interface ModelPlanChecklist()
@property(nonatomic,strong)AppDelegate* appDelegate;
@property(nonatomic,strong)ChecklistenkategorieCheck *checklistenKategorieCheck;
@property (nonatomic,strong) NSMutableArray *mutableChecklistArray;
@property (nonatomic,strong) NSString *planUrl;
@property BOOL isBranchModel;
@property BOOL isTypeModel;
@property BOOL isDeleted;
@end

@implementation ModelPlanChecklist




-(BOOL) addModelPlanChecklistToDatabase:(NSArray*)modelPlanChecklist{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.checklistenKategorieCheck = [ChecklistenkategorieCheck new];
    self.checklistenKategorieCheck.appDelegate = self.appDelegate;
    self.mutableChecklistArray = [NSMutableArray new];
    
    for (NSDictionary* dictionary in modelPlanChecklist) {
        [self checkData:dictionary];
        [self addChecklist:dictionary];
    }
    if([self.hygienechecklistenTableViewController respondsToSelector:@selector(performTableViewRefresh)]){
        [self.hygienechecklistenTableViewController performTableViewRefresh];
    }
    
    [self compareDownloadedChecklistsWithStoredChecklists];

    [self.appDelegate saveContext];
    
    return YES;
}


-(void) addChecklist:(NSDictionary*)checklist{
    Checkliste* checkliste = nil;
    NSString* rawChecklistenUId =checklist[@"uid"];
    int checklistUid = [rawChecklistenUId intValue];

    checkliste = [self checkChecklisteWithChecklistenID:checklistUid];
    
    if(checkliste!=nil){
        checkliste = [self checkChecklistValues:checkliste ifTheyChangedSinceLastDownload:checklist];
    }
    
    if(checkliste==nil && !self.isDeleted){
        
        checkliste = [self getNewChecklisteWithUid:checklistUid];
        checkliste.kurzbezeichnung = checklist[@"firstHeadline"];
        checkliste.name = checklist[@"firstHeadline"];
        checkliste.tstamp= checklist[@"tstamp"];
        NSString* rawTranslatePlanCheck =checklist[@"translatePlan"];
        int translatePlanCheck = [rawTranslatePlanCheck intValue];
        
        if(translatePlanCheck==1){
            checkliste.kurzbezeichnung = checklist[@"firstHeadlineTranslation"];
            checkliste.name = checklist[@"firstHeadlineTranslation"];
        }
        
        checkliste.url= [NSString stringWithFormat:self.planUrl,checklistUid];
        
        
    }
    if(checkliste != nil) {
            [self.checklistenKategorieCheck addChecklistenCategorieToChecklisteWithData:checklist
                                                                        andCheckliste:checkliste ];
        
            [self addChecklistToChecklistStorage:checkliste];
    }

    
}

-(void) addChecklistToChecklistStorage:(Checkliste*)checkliste{

    [self.mutableChecklistArray addObject:checkliste];
    
}

-(void)compareDownloadedChecklistsWithStoredChecklists{

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
    NSPredicate *predicate = NULL;
    
    if(self.isTypeModel){
        predicate = [NSPredicate predicateWithFormat:@"typemodel = 1"];
    } else if (self.isBranchModel){
        predicate = [NSPredicate predicateWithFormat:@"branchmodel = 1"];
    }
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    
    if(results.count){
        NSMutableArray *finalResults = [NSMutableArray arrayWithArray:results];
        [finalResults removeObjectsInArray:self.mutableChecklistArray];
        
        for(Checkliste* checkliste in finalResults){
            [checkliste deleteCheckliste];
        }
      
    }
    
}

-(Checkliste*)checkChecklisteWithChecklistenID:(int)checklistenUid{
   
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
    NSPredicate *predicate = NULL;
    
    if(self.isTypeModel){
        predicate = [NSPredicate predicateWithFormat:@"uid ==%i AND typemodel = 1",checklistenUid];
    } else if (self.isBranchModel){
        predicate = [NSPredicate predicateWithFormat:@"uid ==%i AND branchmodel = 1",checklistenUid];
    }
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Checkliste* checkliste = NULL;
    
    if(results.count){
        checkliste =[results lastObject];
        return checkliste;
    }
    
    return nil;
    
}

-(Checkliste*) getNewChecklisteWithUid:(int)checklistenUid{
    
    Checkliste *checkliste = [Checkliste insertChecklisteInManagedObjectContext:self.appDelegate.managedObjectContext];
    checkliste.uid = [NSNumber numberWithInt:checklistenUid];
    if(self.isTypeModel) checkliste.typemodel = @(YES);
    if(self.isBranchModel) checkliste.branchmodel = @(YES);
    return checkliste;
    
}

-(void) checkData:(NSDictionary*)data
{
    NSString* rawDeletedCheck =data[@"deleted"];
    self.isDeleted =[rawDeletedCheck boolValue];
    if(data[@"branch"]!=nil ){
        self.planUrl = urlModelPlan;
        self.isBranchModel = @(YES);
    }
    else if (data[@"type"]!=nil){
        self.planUrl = urlFlyerPlan;
        self.isTypeModel = @(YES);

    }
}

-(Checkliste*) checkChecklistValues:(Checkliste*)checkliste ifTheyChangedSinceLastDownload:(NSDictionary*)data{
    

    if(self.isDeleted){
        [checkliste deleteCheckliste];
        [self.appDelegate saveContext];
        return nil;
    }
    
    if(![checkliste.kurzbezeichnung isEqualToString:data[@"firstHeadline"]]){
        checkliste.kurzbezeichnung = data[@"firstHeadline"];
        checkliste.name = data[@"firstHeadline"];
    }
    
    NSString* rawTranslatePlanCheck =data[@"translatePlan"];
    int translatePlanCheck = [rawTranslatePlanCheck intValue];
    
    if(translatePlanCheck==1){
        if(![checkliste.kurzbezeichnung isEqualToString:data[@"firstHeadlineTranslation"]]){
            checkliste.kurzbezeichnung = data[@"firstHeadlineTranslation"];
            checkliste.name = data[@"firstHeadlineTranslation"];
        }
    }
    
    return checkliste;
}

@end
