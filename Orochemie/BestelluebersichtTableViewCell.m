//
//  BestelluebersichtTableViewCell.m
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "BestelluebersichtTableViewCell.h"

@implementation BestelluebersichtTableViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    //self.stepperValue = 12.0;
    self.bestellMengenStepperOutlet.value = self.stepperValue;
    //NSLog(@"Value: %f",self.bestellMengenStepperOutlet.value);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.bestellMengenStepperOutlet.value = self.stepperValue;
        //NSLog(@"Value init: %f",self.bestellMengenStepperOutlet.value);
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteButtonAction:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DELETEBESTELLPOSITION" object:self];
}

- (IBAction)bestellMengenStepperAction:(UIStepper *)sender {
    
  self.mengeTextLabelOutlet.text=[NSString stringWithFormat:@"%d",(int)sender.value ];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"BESTELLMENGENAENDERUNG" object:self];
}


@end
