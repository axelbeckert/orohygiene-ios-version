//
//  Berater+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 16.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Berater.h"

@interface Berater (ccm)
+(Berater*) insertBeraterInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteBerater;
@end
