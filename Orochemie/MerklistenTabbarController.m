//
//  MerklistenTabbarController.m
//  orochemie
//
//  Created by Axel Beckert on 16.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenTabbarController.h"
#import "MerklisteTableViewController.h"
#import "SavedMerklistenTableViewController.h"

@interface MerklistenTabbarController()
//@property(nonatomic,strong) AppDelegate *appDelegate;

@end

@implementation MerklistenTabbarController
#pragma mark - View Lifecycle

-(void)viewDidLoad{
    [super viewDidLoad];
    
    //AppDelegate initalisieren
    //self.appDelegate = [[UIApplication sharedApplication] delegate];

    //View Controller Initalisieren
    MerklisteTableViewController *controller = [self.viewControllers objectAtIndex:0];

    SavedMerklistenTableViewController *controller2 = [self.viewControllers objectAtIndex:1];
    
    controller.savedMerklistenTableViewController = controller2;
    controller.merklistenTabbarController=self;
    
    //Optik
    if(self.showMerklistenTabBarControllerFrom == ReinigerChannel){
        controller.showMerklistenTableViewControllerFrom = ReinigerChannel;
        controller2.showSavedMerklistenFrom = ReinigerChannel;
    } else if(self.showMerklistenTabBarControllerFrom == DesinfektionChannel){
        controller.showMerklistenTableViewControllerFrom = DesinfektionChannel;
        controller2.showSavedMerklistenFrom = DesinfektionChannel;
    }
    
    self.parentViewController.automaticallyAdjustsScrollViewInsets=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;


    
    
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}



@end
