//
//  ReinigungsRechnerStaticTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerbrauchsRechnerStaticTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *flachenTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *produktTextLabelOutlet;
@property  (nonatomic,strong) Reiniger *reiniger;
@property (nonatomic,strong) Verbrauch* verbrauch;
@property (strong, nonatomic) IBOutlet UILabel *reinigungsartTextLabelOutlet;
@property(nonatomic,strong) NSNumber *flaeche;
@property (strong, nonatomic) IBOutlet UILabel *konzentratmengeTextLabelOutlet;
@property(nonatomic,strong) NSNumberFormatter *numberFormatter;
@property (strong, nonatomic) IBOutlet UILabel *zusatzinformatioTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *produktMainTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *reinigungsartMainTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *flaecheMainTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *wassermengeTextLabelOutlet;
-(void)rechneVerbrauch;
-(void)checkReiniger;
@end
