//
//  Oberflaeche+ccm.h
//  oro Reinigung
//
//  Created by Axel Beckert on 24.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Oberflaeche+CoreDataProperties.h"



@interface Oberflaeche (ccm) <JsonProtocol>
+(Oberflaeche*) insertOberflaecheInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteOberflaeche;
@end
