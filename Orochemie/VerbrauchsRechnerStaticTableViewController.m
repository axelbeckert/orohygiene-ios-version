//
//  ReinigungsRechnerStaticTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VerbrauchsRechnerStaticTableViewController.h"
#import "VerauchsrechnerFlaecheViewController.h"
#import "VerbrauchsrechnerreinigerAuswahlViewController.h"
#import "VerbrauchsrechnerReinungsartAuswahlViewController.h"
#import "Reinigungsart+ccm.h"
#import "Verbrauch+ccm.h"

@interface VerbrauchsRechnerStaticTableViewController ()
@property (nonatomic,strong) NSArray *reinigungsartenArray;
@end

@implementation VerbrauchsRechnerStaticTableViewController

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Optik
    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Verbrauchsrechner";
    
    //TabbarController
    if(!self.tabBarController){
        UIEdgeInsets inset = UIEdgeInsetsMake(-35, 0, 0, 0);
        self.tableView.contentInset = inset;
    } else{
        UIEdgeInsets inset = UIEdgeInsetsMake(28, 0, 20, 0);
        self.tableView.contentInset = inset;
        
    }

    //Wenn Reiniger von TabbarController
    if(self.reiniger){
        self.produktMainTextLabelOutlet.text=self.reiniger.bezeichnung;
        self.produktTextLabelOutlet.text=@"Produkt";
    }
    
    //Initalisierung NumberFormatter
    self.numberFormatter =[[NSNumberFormatter alloc]init];
    self.numberFormatter.locale = [NSLocale currentLocale];// this ensures the right separator behavior
    self.numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.numberFormatter.usesGroupingSeparator = YES;
    [self.numberFormatter setAllowsFloats:TRUE];
    [self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.numberFormatter setMaximumFractionDigits:2];
    [self.numberFormatter setMinimumFractionDigits:2];
   
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Reiniger-Verbrauchsrechner-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    if(self.reiniger){
    
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Reiniger"
                                                              action:@"Verbrauchsrechner"
                                                               label:self.reiniger.bezeichnung
                                                               value:nil] build]];
            [tracker send:[[GAIDictionaryBuilder createAppView] build]];

    }
    if(self.verbrauch){
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Reiniger"
                                                              action:@"Reinigungsart"
                                                               label:self.verbrauch.reinigungsart.bezeichnung2
                                                               value:nil] build]];
        [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    }
   

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     NSLog(@"viewWillAppear");
    [self checkReiniger];
    [self rechneVerbrauch];
}


-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    self.konzentratmengeTextLabelOutlet.text =@"";
    self.wassermengeTextLabelOutlet.text=@"";
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - TableView Definition

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    
    if (section == 0)
    {
        return nil;
    } else if (section == 1)
    {
        label.text=@"Mindest-Konzentratmenge in ml";
    } else if (section == 2)
    {
        label.text=@"Wassermenge in ml";
    } else if (section == 3)
    {
        label.text=@"Zusatzinformation";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section ==0) return 0;
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}


#pragma mark - Segue Methoden

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
   NSLog(@"shouldPerformSegueWithIdentifier");
    if(self.reiniger.mindestdosierung || !self.reiniger){
        if([identifier isEqualToString:@"flaechenSegue"]){
            
            if (self.reiniger){
              return YES;
            }
          
        } else if ([identifier isEqualToString:@"reinigungsartAuswahlSegue"]){
        
            
            if (self.reiniger){
                return YES;
            }

        } else if ([identifier isEqualToString:@"produktAuswahlSegue"]){
            
            if(!self.tabBarController) return YES;
        }
    }
    
    return NO;

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    NSLog(@"prepareForSegue");

    if([segue.identifier isEqualToString:@"flaechenSegue"]){
        VerauchsrechnerFlaecheViewController* controller = segue.destinationViewController;
        controller.verbrauchsRechnerStaticTableViewController = self;
        controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    } else if ([segue.identifier isEqualToString:@"produktAuswahlSegue"]){
    

        if(![self.produktTextLabelOutlet.text isEqualToString:@"Auswahl Produkt"]){
            self.verbrauch = nil;
            self.flaeche = nil;
            self.flaecheMainTextLabelOutlet.text=@"Zu reinigende Fläche in qm";
            self.flachenTextLabelOutlet.text = @"Fläche in qm";
            self.reinigungsartMainTextLabelOutlet.text=@"Reinigungsart";
            self.reinigungsartTextLabelOutlet.text=@"Auswahl Reinigungsart";
            self.zusatzinformatioTextLabelOutlet.text=@"";
            self.wassermengeTextLabelOutlet.text=@"-";
            self.konzentratmengeTextLabelOutlet.text=@"-";

            
        }

        VerbrauchsrechnerreinigerAuswahlViewController *controller = segue.destinationViewController;
        controller.verbrauchsRechnerStaticTableViewController = self;
        controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        
    } else if ([segue.identifier isEqualToString:@"reinigungsartAuswahlSegue"]){
        
        VerbrauchsrechnerReinungsartAuswahlViewController *controller = segue.destinationViewController;
        NSLog(@"reinigungsartAuswahlSegue");
        controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        controller.reinigungsartenArray = self.reinigungsartenArray;
        controller.verbrauchsRechnerStaticTableViewController = self;
        controller.reiniger=self.reiniger;
        
        
        
    }
}

    

#pragma mark - Überprüfen

-(void)checkReiniger {

    if(!self.reiniger.mindestdosierung && self.tabBarController){
        
        self.reinigungsartMainTextLabelOutlet.text = @"Produkt ist  gebrauchsfertig";
        self.reinigungsartTextLabelOutlet.text=@"";
        self.flachenTextLabelOutlet.text=@"";
        self.flaecheMainTextLabelOutlet.text=@"";
        self.wassermengeTextLabelOutlet.text=@"";
        self.zusatzinformatioTextLabelOutlet.textAlignment = NSTextAlignmentCenter;
        
    }
    
    else if(self.reiniger.mindestdosierung){
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"reinigungsart.sortierung"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        
        self.reinigungsartenArray = [[NSArray arrayWithArray:[self.reiniger.verbrauch allObjects]]sortedArrayUsingDescriptors:sortDescriptors];
        
        self.zusatzinformatioTextLabelOutlet.textAlignment = NSTextAlignmentLeft;
        
        if(self.reiniger.mindestdosierung && [self.reiniger.bezeichnung isEqualToString:@"oro® Grundreiniger universal"]){
            self.zusatzinformatioTextLabelOutlet.text = [NSString stringWithFormat:@"Die Mindestkonzentratmenge auf 10 Liter Wasser ist für dieses Produkt %@ ml. Hartnäckige Verschmutzungen bzw. die Entfernung einer Bodenbeschichtung erfordern eine höhere Dosierung mit entsprechend mehr Konzentratverbrauch als oben angegeben. Sehen Sie dazu die detailierte Produktinformation.",self.reiniger.mindestdosierung];
        } else if(self.reiniger.mindestdosierung){
            self.zusatzinformatioTextLabelOutlet.text = [NSString stringWithFormat:@"Die Mindestkonzentratmenge auf 10 Liter Wasser ist für dieses Produkt %@ ml. Hartnäckige Verschmutzungen erfordern eine höhere Dosierung mit entsprechend mehr Konzentratverbrauch als oben angegeben.",self.reiniger.mindestdosierung];
        }
        
        [self rechneVerbrauch];
        
    }
}

#pragma mark - rechnen

-(void)rechneVerbrauch {

    if(self.reiniger && self.verbrauch && self.flaeche){

        [self.numberFormatter setMinimumFractionDigits:0];
        [self.numberFormatter setMaximumFractionDigits:0];
        [self.numberFormatter setRoundingMode:NSNumberFormatterRoundUp];
        
        float ergebnis1 = self.flaeche.floatValue * self.verbrauch.verbrauchsmenge.floatValue;
        NSNumber *ergebnis2 = [NSNumber numberWithFloat:ergebnis1];
        self.konzentratmengeTextLabelOutlet.text = [self.numberFormatter stringFromNumber:ergebnis2];
        
        float wassermenge = self.flaeche.floatValue * self.verbrauch.reinigungsart.wassermenge.floatValue;
        wassermenge = wassermenge-ergebnis1;
        NSNumber *wasserMenge =[NSNumber numberWithFloat:wassermenge];
        [self.numberFormatter setRoundingMode:NSNumberFormatterRoundDown];
        self.wassermengeTextLabelOutlet.text = [self.numberFormatter stringFromNumber:wasserMenge];

        [self.numberFormatter setMinimumFractionDigits:2];
        [self.numberFormatter setMaximumFractionDigits:2];
        [self.numberFormatter setRoundingMode:NSNumberFormatterRoundHalfEven];
        
    }
}

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
