//
//  StartTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 12.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "StartTableViewController.h"
#import "StartTableViewCell.h"
#import "ProdukteViewController.h"
#import "newsTableViewController.h"
#import "DosierungViewController.h"
#import "HygieneTippsTableViewController.h"
#import "NavigationCollectionViewController.h"

#import "StandardCell.h"

#import "ProduktAuflistungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "KrankheitserregerTableViewController.h"
#import "BarcodeReader.h"
#import "DosierungStaticTableViewController.h"


@interface StartTableViewController ()
@property(nonatomic,strong) NSArray* datenModel;
@end

@implementation StartTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}




-(NSArray*) datenModel
{
    if (!_datenModel) _datenModel =[[NSArray alloc]init];
    return _datenModel;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.datenModel = @[@"",@"News",@"Hygienetipps",@"Produkte",@"Dosierrechner",@"Unternehmen",@"CollectionViewNaviTest",@"CollectionViewProduktDarstellung"];
    self.tableView.backgroundColor = [UIColor grayColor];
    self.tableView.separatorColor = [UIColor colorWithRed:171.0/255 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    
    self.datenModel = @[      @{@"section": @"",
                                @"inhalt" : @[@""]
                                },
                              @{@"section" : @"Infos",
                                @"inhalt": @[@"News",@"Hygienetipps",@"Aktionen für App-User"],
                                },
                              @{@"section" : @"Produkte",
                                @"inhalt": @[@"nach Bezeichnung",@"nach Anwendung",@"nach Branche"]
                                },
                              @{@"section" : @"Hilfsmittel",
                                @"inhalt": @[@"Dosierrechner",@"Barcode Reader",@"Krankheitserreger von A-Z"],
                                },
                              @{@"section" : @"Unternehmen",
                                @"inhalt" : @[@"Entdecken Sie orochemie",@"Kontakt",@"Impressum"]
                                },
                              @{@"section" : @"Test",
                                @"inhalt": @[@"Produktübersicht",@"Navigation"],
                                }];

    ////ccmLog(@"0-1: %@",[self.datenModel  );

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    ////ccmLog(@"Nummer der Sektionen: %i",self.datenModel.count);
       return self.datenModel.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    //return self.datenModel.count;
    return [self.datenModel[section][@"inhalt"] count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0 && indexPath.section==0)
    {
        StartTableViewCell* firstCell = [tableView dequeueReusableCellWithIdentifier:@"firstCell" forIndexPath:indexPath];
        return firstCell;
    }
     //backgroundColor = [UIColor blackColor];
    
        StandardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StandardCell" forIndexPath:indexPath];
    
    cell.contentView.backgroundColor = [UIColor colorWithRed:223.0/255 green:224.0/255 blue:225.0/255 alpha:1.0];
    cell.StandardCellLabelOutlet.text= [NSString stringWithFormat:@"%@",self.datenModel[indexPath.section][@"inhalt"][indexPath.row]];
    cell.StandardCellLabelOutlet.textColor=[UIColor blackColor];
    return cell;
    
    


}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    
//    NSString *title = [NSString stringWithFormat:@"%@",self.datenModel[section][@"section"]];
//    if(section==0) return 0;
//    
//    return title;
//}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];

        [headerView setBackgroundColor:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, tableView.bounds.size.width - 10, 18)];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.text = [NSString stringWithFormat:@"%@",self.datenModel[section][@"section"]];
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];

    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 & indexPath.row == 0 )
    {

        return 190;
    }
    return 44;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"News"])
        {
            newsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"newsTableViewController"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Hygienetipps"])
        {
            HygieneTippsTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HygieneTippsTableViewController"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"nach Bezeichnung"])
        {
               ProduktAuflistungTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktbezeichnungTableViewController"];
                [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"nach Anwendung"])
        {
            AnwendungTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AnwendungTableViewController"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"nach Branche"])
        {
            brancheTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"brancheTableViewController"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Dosierrechner"])
        {
            DosierungStaticTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DosierungStaticTableViewController"];
           
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Barcode Reader"])
        {
            BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Entdecken Sie orochemie"])
        {
//            DosierungViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DosierungViewController"];
//            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Kontakt"])
        {
//            DosierungViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DosierungViewController"];
//            [self.navigationController pushViewController:controller animated:YES];
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Impressum"])
        {
//            DosierungViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DosierungViewController"];
//            [self.navigationController pushViewController:controller animated:YES];
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Krankheitserreger von A-Z"])
        {
            KrankheitserregerTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VirusTableViewController"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Navigation"])
        {
            NavigationCollectionViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CollectionViewC"];
            [self.navigationController pushViewController:controller animated:YES];
            
        } else if ([self.datenModel[indexPath.section][@"inhalt"][indexPath.row] isEqualToString:@"Produktübersicht"])
        {
//            CollectionViewTestControllerViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CollectionViewTestControllerViewController"];
//            [self.navigationController pushViewController:controller animated:YES];
        }
    
    

}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    Produkte *produkt =[self.brancheProduktFetchedResultsController objectAtIndexPath:indexPath];
//    
//    
//    produktStaticTabBarViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
//    controller.produkt = produkt;
//    [self.navigationController pushViewController:controller animated:YES];
//    
//    
//}

-(NSUInteger)supportedInterfaceOrientations
{
    
    
    
    return UIInterfaceOrientationMaskAllButUpsideDown;
    
    
}

-(BOOL)shouldAutorotate
{
    return YES;
}

@end
