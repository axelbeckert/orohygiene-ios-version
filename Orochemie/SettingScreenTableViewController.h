//
//  SettingScreenTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 25.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingScreenTableViewController : UITableViewController

@end
