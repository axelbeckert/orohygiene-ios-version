//
//  Merkliste+ccm.m
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Merkliste+ccm.h"


@implementation Merkliste (ccm)

+(Merkliste*) insertMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Merkliste" inManagedObjectContext:managedObjectContext];
}
-(void)deleteMerkliste
{
    [self.managedObjectContext deleteObject:self];
}


@end
