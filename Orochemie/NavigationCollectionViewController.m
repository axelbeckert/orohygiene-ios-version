//
//  CollectionViewC.m
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "NavigationCollectionViewController.h"
#import "CollectionViewCell.h"
#import "ProduktAuflistungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "CollectionReusableView.h"
#import "AktuellesTableViewController.h"
#import "ProduktTableViewController.h"
#import "HilfsmittelTableViewController.h"
#import "UnternehmenTableViewController.h"
#import "BarcodeReader.h"
#import "KrankheitserregerTableViewController.h"
#import "AnwendungsVideosTableViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "DeviceCheck.h"

@interface NavigationCollectionViewController () <UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)IBOutlet NSLayoutConstraint *topConstraint;
@end

@implementation NavigationCollectionViewController





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self adjustTopLayoutConstraint];
    [oroThemeManager cutomizeView:self.view];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Desinfektionsmittel";
    
        
    navigationDataModel = @[@"Produkte",@"Krankheitserreger von A-Z",@"Anwendungsvideos",@"Hilfsmittel",@"Aktuelles",@"Unternehmen"]; //,@"Barcode Reader"


    
	// Do any additional setup after loading the view.
}

-(void)adjustTopLayoutConstraint{
    
    DeviceCheck *deviceCheck = [DeviceCheck new];
    
    if([[deviceCheck deviceName] rangeOfString:@"iPad"].location != NSNotFound){
            self.topConstraint.constant = 50;
    } else {
        if ([[deviceCheck deviceName] rangeOfString:@"Plus"].location != NSNotFound) {
            self.topConstraint.constant = 0;
        } else {
            self.topConstraint.constant = 50;
        }
    
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Navigations-Schirm-Desinfektionskanal"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return navigationDataModel.count;
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView* reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];
    

    //NSLog(@"reusableView vor dem Bild");
    //[reusableView.reusableViewHeaderImageOutlet setImage:[UIImage imageNamed:@"hintergrund_haendedesinfektion.jpg"]];
    
    //NSLog(@"reusableView nach dem Bild");
    
    
    return reusableView;
}
                                            
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
    
   [oroThemeManager customizeButtonBackgroundImage:cell.collectionViewImage2];

    cell.collectionViewImage2.layer.borderColor = [UIColor whiteColor].CGColor;
    
    CGFloat borderWidth = 1.0f;
    
    cell.collectionViewImage2.layer.borderWidth = borderWidth;
    
    cell.collectionViewBezeichnungLabelOutlet.text = [navigationDataModel objectAtIndex:indexPath.row];
    

    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Aktuelles"])
    {
        AktuellesTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"informationenTableViewController"];
        controller.showAktuellesFrom = DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];

    } else if ([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Produkte"])
    {
        ProduktTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Hilfsmittel"])
    {
        HilfsmittelTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HilfsmittelTableViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Unternehmen"])
    {
        UnternehmenTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"UnternehmenTableViewController"];
        controller.showUnternehmenFrom = DesinfektionChannel;
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Krankheitserreger von A-Z"])
    {
        KrankheitserregerTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VirusTableViewController"];
        controller.openendFromMainMenue=@YES;
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Anwendungsvideos"])
    {
        AnwendungsVideosTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AnwendungsVideosTableViewController"];
        
        controller.showVideosFrom = DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
}


#pragma mark - UICollectionViewDelegateFlowLayout


-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if([UIScreen mainScreen].currentMode.size.height==1136) //Iphone 5
    {
        return 3; //war 12
        
    } else if([UIScreen mainScreen].currentMode.size.height==2048 || [UIScreen mainScreen].currentMode.size.height==1024 || [UIScreen mainScreen].currentMode.size.height==768) //Ipad
    {
         return 25;
    }
    
    return 1;
}


#pragma mark - View Lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([self.navigationController.navigationBar isHidden])
    {
        [self.navigationController.navigationBar setHidden:NO];
    }
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
