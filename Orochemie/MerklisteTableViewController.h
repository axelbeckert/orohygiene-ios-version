//
//  MerklisteTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SavedMerklistenTableViewController;
@class MerklistenTabbarController;

@interface MerklisteTableViewController : UITableViewController
@property (nonatomic,assign)show showMerklistenTableViewControllerFrom;
@property (nonatomic,weak) SavedMerklistenTableViewController *savedMerklistenTableViewController;
@property (nonatomic,strong) MerklistenTabbarController *merklistenTabbarController;

@end
