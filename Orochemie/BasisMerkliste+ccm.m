//
//  BasisMerkliste+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "BasisMerkliste+ccm.h"

@implementation BasisMerkliste (ccm)
+(BasisMerkliste*)insertBasisMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"BasisMerkliste" inManagedObjectContext:managedObjectContext];
}
-(void)deleteBasisMerkliste
{
    [self.managedObjectContext deleteObject:self];
}
@end
