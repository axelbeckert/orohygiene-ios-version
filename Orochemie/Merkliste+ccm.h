//
//  Merkliste+ccm.h
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Merkliste.h"

@interface Merkliste (ccm)

+(Merkliste*) insertMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteMerkliste;

@end
