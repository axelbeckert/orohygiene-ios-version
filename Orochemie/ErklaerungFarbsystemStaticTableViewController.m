//
//  HilfsmittelStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ErklaerungFarbsystemStaticTableViewController.h"

@interface ErklaerungFarbsystemStaticTableViewController ()

@end

@implementation ErklaerungFarbsystemStaticTableViewController
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Erklärung-Farbsystem-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.font = [UIFont fontWithName:label.font.fontName size:13.0];
   
    if(section == 0)
    {
        label.text = @"Das Vier-Farben-System";
    }
    
    
    if (section == 1)
    {
        label.text = @"Das Vier-Farben-System für die Desinfektion";
    }
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
