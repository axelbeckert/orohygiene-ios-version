//
//  BestellProdukteTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMTableViewController.h"
@class BestellungTableViewController;

@interface BestellProdukteTableViewController : JSMTableViewController
@property (nonatomic,strong) NSMutableArray *bestellMengenArray;
@property (nonatomic,strong) BestellungTableViewController *bestellungTableViewController;
@end
