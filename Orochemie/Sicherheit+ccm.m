//
//  Sicherheit+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Sicherheit+ccm.h"

@implementation Sicherheit (ccm)
+(Sicherheit*) insertSicherheitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Sicherheit class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteSicherheit{
    [self.managedObjectContext deleteObject:self];
}
@end
