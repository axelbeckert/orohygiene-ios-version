//
//  ChecklistenkategorieCheck.h
//  orochemie 
//
//  Created by Axel Beckert on 30.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppDelegate;

@interface ChecklistenkategorieCheck : NSObject
@property (nonatomic,strong) AppDelegate* appDelegate;
-(Checkliste*) addChecklistenCategorieToChecklisteWithData:(NSDictionary*)data andCheckliste:(Checkliste*)checkliste;
@end
