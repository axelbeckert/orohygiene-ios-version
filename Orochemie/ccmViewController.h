//
//  ccmViewController.h
//  BarcodeScanner
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ccmViewController : UIViewController <ZBarReaderDelegate >

{
    UIImageView *resultImage;
    UITextView *resultText;
}

@property (strong, nonatomic) IBOutlet UIImageView *resultImageOutlet;
@property (strong, nonatomic) IBOutlet UITextField *resultTextOutlet;


- (IBAction)ScanButtonTouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *scanButtonOutlet;
@property (strong, nonatomic) IBOutlet UILabel *titleLabelOutlet;

@end
