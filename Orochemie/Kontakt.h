//
//  Kontakt.h
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Kontakt : NSManagedObject

@property (nonatomic, retain) NSString * ansprechpartner;
@property (nonatomic, retain) NSString * telefon;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * nachricht;

@end
