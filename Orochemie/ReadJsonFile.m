//
//  ReadJsonFile.m
//  orochemie 
//
//  Created by Axel Beckert on 27.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ReadJsonFile.h"

@implementation ReadJsonFile


- (NSDictionary *)JSONFromFile:(NSString*)file
{
    NSString *path=[[NSBundle mainBundle] pathForResource:file ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
   
    NSError *error;
   
    NSDictionary* jsonDict =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error) NSLog(@"error: %@", error.localizedDescription);
    
    return jsonDict;
}

@end
