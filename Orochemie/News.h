//
//  News.h
//  ReadWriteFormats
//
//  Created by Axel Beckert on 27.02.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject
@property (nonatomic, strong) NSString* newsId;
@property (nonatomic, strong) NSString* text1;
@property (nonatomic, strong) NSString* text2;
@property (nonatomic, strong) NSString* imageUrl;
@property (nonatomic, strong) UIImage* image;
-(NSDictionary*) asDictionary;
@end
