//
//  Anwendung+ccm.h
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Anwendung.h"

@interface Anwendung (ccm)
+(Anwendung*) insertAnwendungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteAnwendung;
@end
