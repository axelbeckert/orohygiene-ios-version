//
//  VirusTableViewControllerlViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "KrankheitserregerTableViewController.h"
#import "KrankheitserregerStaticTableViewController.h"
#import "KrankheitserregerTableViewStandardCell.h"
#import "KrankheitserregerCollector.h"
#import "ChecklistenCollector.h"

@interface KrankheitserregerTableViewController ()<UISearchBarDelegate>
@property(nonatomic,strong) NSArray* virusDatenModel;
@property(nonatomic,strong) NSMutableArray *virusDatenModel2;
@property(nonatomic,strong) NSMutableArray *sectionArray;
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) AppDelegate *delegate;
@end


@implementation KrankheitserregerTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Krankheitserreger class])];
    if(self.produkt)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produkte CONTAINS[c] %@",self.produkt];
        fetch.predicate=predicate;
    }
    
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedCompare:)];
    NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCompare:)];
    
    NSArray *sortArray= [NSArray arrayWithObjects:sortIndex,sortName,nil];
    fetch.sortDescriptors = sortArray;
    
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"index"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

     [oroThemeManager  customizeTableView:self.tableView];
    
    //CoreData
    self.delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = self.delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    
    [self checkIfKrankheitserregerAreThere];
    
    if(!self.openendFromMainMenue)
    {

        
        self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    }


    self.title = @"Krankheitserreger";
        
   
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    //SerachBar
    UISearchBar *searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
       [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performTableViewRefresh) name:UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];

}

#pragma mark - collect krankheitserreger
-(void) checkIfKrankheitserregerAreThere{
    
    if(self.fetchedResultsController.fetchedObjects.count==0){
        //First off all collect checklists after that start collecting the krankheitserreger
        self.delegate.checklistenCollector.krankheitserregerTableViewController = self;
        self.delegate.checklistenCollector.showSVProgressHUD = 1;
        [self.delegate.checklistenCollector startCollectingChecklists];

    }
    
}

-(void) startCollectiongKrankheitserreger{
    //Now the krankheitserreger
    self.delegate.krankheitserregerCollector.krankheitserregerTableViewController = self;
    self.delegate.krankheitserregerCollector.showSVProgressHUD = 1;
    [self.delegate.krankheitserregerCollector startCollectingKrankheitserreger];
}


#pragma mark - performTableViewRefresh

-(void) performTableViewRefresh{
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Barbutton Items for Toolbar
    UIBarButtonItem *itemOne = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *itemTwo = [[UIBarButtonItem alloc]initWithTitle:cHinweisWissenGewissenButton style:UIBarButtonItemStylePlain target:self action:@selector(hinweis:)];
    UIBarButtonItem *itemThree = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [itemTwo setTintColor:[UIColor whiteColor]];
    
    
    
    //Toolbar initialisieren und anzeigen
    
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    self.toolbarItems=@[itemOne,itemTwo,itemThree];
}



-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:@"Krankheitserreger-Auswahl-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    KrankheitserregerTableViewStandardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"standardCell" forIndexPath:indexPath];

    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    [oroThemeManager customizeAccessoryView:cell];
    
    Krankheitserreger *krankheitserreger = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    cell.textLabel.numberOfLines=0;
    cell.textLabel.text = krankheitserreger.name;
    return cell;
}


-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
  if([segue.identifier isEqualToString:@"virusStaticTableSegue"]){
  
         KrankheitserregerStaticTableViewController *controller = segue.destinationViewController;
      controller.krankheitserreger = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
  
      }   else if([segue.identifier isEqualToString:@"virusStaticTableSegueCustomCell"]){
  
          KrankheitserregerStaticTableViewController *controller = segue.destinationViewController;
          controller.krankheitserreger = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
      }

}



#pragma mark - SearchBarDelegate Methoden

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    

    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.name CONTAINS[c] %@", searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Hinweis zur Auflistung

-(void)hinweis:(id)sender
{
    
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:cHinweisWissenGewissenTitle andMessage:cHinweisWissenGewissen andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
    }];
    
    [AlertHelper showAlert:alert];
    
    
}

@end
