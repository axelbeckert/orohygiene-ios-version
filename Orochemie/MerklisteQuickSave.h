//
//  MerklisteQuickSave.h
//  orochemie
//
//  Created by Axel Beckert on 09.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MerklisteQuickSave : NSObject
@property (nonatomic, strong) NSString* bezeichnung;

-(void)addProduktUIDToMerkliste:(NSString*)produktUID;
-(void)addReinigerUIDToMerkliste:(NSString*)reinigerUID;
-(NSMutableArray*)getProduktUIDForMerkliste;
-(NSMutableArray*)getReinigerUIDForMerkliste;
@end
