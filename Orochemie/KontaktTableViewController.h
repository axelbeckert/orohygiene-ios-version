//
//  KontaktTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAnnotaion.h"

@interface KontaktTableViewController : UITableViewController <MKMapViewDelegate,MKAnnotation>

@property (strong, nonatomic) IBOutlet MKMapView *mapViewOutlet;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,assign) show showKontaktFrom;
@end
