//
//  VirusTableViewControllerlViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMTableViewController.h"
#import "Produkte.h"

@interface KrankheitserregerTableViewController : JSMTableViewController
@property(nonatomic,strong) Produkte *produkt;
@property(nonatomic,assign) BOOL openendFromMainMenue;
-(void) performTableViewRefresh;
-(void) startCollectiongKrankheitserreger;
@end
