//
//  WebsiteChecklist.h
//  orochemie 
//
//  Created by Axel Beckert on 29.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HygieneChecklistenTableViewController;

@interface WebsiteChecklist : NSObject
-(BOOL)addWebsiteChecklistToDatabase:(NSArray*)websiteChecklist;
@property (nonatomic, strong) HygieneChecklistenTableViewController *hygienechecklistenTableViewController;
@end
