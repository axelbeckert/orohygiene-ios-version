//
//  VariantenTableViewCell.h
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produktvarianten.h"

@interface VariantenTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titelLabelOutlet;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabelOutlet;
- (IBAction)bestellMengenStepperAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *bestellMengenAnzeigeOutlet;
@property(nonatomic,strong) Produktvarianten *produktVariante;
@end
