//
//  VariantenTableViewCell.m
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "VariantenTableViewCell.h"


@implementation VariantenTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)bestellMengenStepperAction:(UIStepper*)sender {
    
    
   self.bestellMengenAnzeigeOutlet.text=[NSString stringWithFormat:@"%d",(int)sender.value ];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BESTELLMENGEGEAENDERT" object:self];
    
    ////NSLog(@"Dass Array hat folgenden Inhalt: %@",self.produktVariantenTableViewController.bestellMengenArray);
}
@end
