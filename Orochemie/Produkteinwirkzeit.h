//
//  Produkteinwirkzeit.h
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produktdosierung;

@interface Produkteinwirkzeit : NSManagedObject

@property (nonatomic, retain) NSString * einwirkzeit;
@property (nonatomic, retain) NSString * mischungsverhaeltnis;
@property (nonatomic, retain) NSNumber * rechengroesse;
@property (nonatomic, retain) NSNumber * sortierung;
@property (nonatomic, retain) Produktdosierung *produktdosierung;

@end
