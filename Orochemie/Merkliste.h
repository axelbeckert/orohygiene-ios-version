//
//  Merkliste.h
//  orochemie
//
//  Created by Axel Beckert on 14.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Reiniger;

@interface Merkliste : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSSet *reiniger;
@property (nonatomic, retain) NSSet *produkte;
@end

@interface Merkliste (CoreDataGeneratedAccessors)

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet *)values;
- (void)removeProdukte:(NSSet *)values;

@end
