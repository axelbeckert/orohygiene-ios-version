//
//  ProduktDosierungViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktDosierungViewController.h"
#import "DosierungStaticTableViewController.h"
#import "Produkteinwirkzeit.h"
#import "Produktdosierung.h"
#import "Produkte.h"

@interface ProduktDosierungViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic,strong) NSString *ausgewaehltesProdukt;
@property (nonatomic,strong) NSString *untertitelZumAusgewaehltenProdukt;
@property(nonatomic,strong) Produktdosierung *produktdosierung;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@end

@implementation ProduktDosierungViewController

#pragma mark - fetched ResultsController
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Produktdosierung class])];
    if(self.uebergebenesProdukt){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"produkte.name CONTAINS[c] %@",self.uebergebenesProdukt.name];
        fetch.predicate=predicate;
    }
    
    NSSortDescriptor *sortByName =
    [NSSortDescriptor sortDescriptorWithKey:@"sortierung" ascending:YES];
    
    fetch.sortDescriptors = @[ sortByName ];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

- (IBAction)zurueckButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;

         [self.fetchedResultsController performFetch:nil];
    
    self.topToolBarDosierungAuswahlViewOutlet.layer.borderWidth = 0.5;
    self.topToolBarDosierungAuswahlViewOutlet.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.4].CGColor;

    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return [[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects];
 
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    Produktdosierung *dosierung =[self.fetchedResultsController objectAtIndexPath:indexPath];
    

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 650, 60)];
        
        label.text=dosierung.hauptbezeichnung;
        label.font=[UIFont boldSystemFontOfSize:30.0];
        label.backgroundColor = [UIColor clearColor];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 650, 40)];
        
        label1.text=dosierung.zusatzbezeichnung;
        label1.font=[UIFont italicSystemFontOfSize:18.0];
        label1.backgroundColor = [UIColor clearColor];
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 600, 100)];
        
        [myView addSubview:label];
        [myView addSubview:label1];
        
        return myView;
    
    } else {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 350, 30)];
    
    label.text=dosierung.hauptbezeichnung;
    label.font=[UIFont boldSystemFontOfSize:15.0];
    label.backgroundColor = [UIColor clearColor];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 350, 20)];
    
    label1.text=dosierung.zusatzbezeichnung;
    label1.font=[UIFont italicSystemFontOfSize:9.0];
    label1.backgroundColor = [UIColor clearColor];
    label1.numberOfLines=0;
    
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    
    [myView addSubview:label];
    [myView addSubview:label1];
    
    return myView;
        
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    Produktdosierung *dosierung = [self.fetchedResultsController objectAtIndexPath:indexPath];

    self.ausgewaehltesProdukt = dosierung.hauptbezeichnung;
    self.untertitelZumAusgewaehltenProdukt = dosierung.zusatzbezeichnung;
    self.produktdosierung = dosierung;
    
    
}


-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    return 100;
    }
    
    return 44;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if([[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects]!=0){

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        Produktdosierung *dosierung = [self.fetchedResultsController objectAtIndexPath:indexPath];



        if(!self.ausgewaehltesProdukt){
            self.dosierungStaticTableViewController.produkt = dosierung.hauptbezeichnung;
            self.dosierungStaticTableViewController.produktTextOutlet.text = dosierung.hauptbezeichnung;
            self.dosierungStaticTableViewController.produktZusatzinformationTextOutlet.text = [dosierung.zusatzbezeichnung stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            self.dosierungStaticTableViewController.produktdosierung = dosierung;
            [self.dosierungStaticTableViewController rechne2];
           

        } else {
            self.dosierungStaticTableViewController.produkt = self.ausgewaehltesProdukt;
            self.dosierungStaticTableViewController.produktTextOutlet.text = self.ausgewaehltesProdukt;
            self.dosierungStaticTableViewController.produktZusatzinformationTextOutlet.text =[self.untertitelZumAusgewaehltenProdukt stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            self.dosierungStaticTableViewController.produktdosierung = self.produktdosierung;
            [self.dosierungStaticTableViewController rechne2];
        }
   
    }
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}



- (IBAction)auswahlUebernehmenButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
