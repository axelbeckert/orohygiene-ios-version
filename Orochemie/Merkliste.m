//
//  Merkliste.m
//  orochemie
//
//  Created by Axel Beckert on 14.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Merkliste.h"
#import "Produkte.h"
#import "Reiniger.h"


@implementation Merkliste

@dynamic bezeichnung;
@dynamic reiniger;
@dynamic produkte;

@end
