//
//  Produktdosierung+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktdosierung.h"

@interface Produktdosierung (ccm)
+(Produktdosierung*) insertProduktdosierungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteProduktdosierung;
@end
