//
//  Reinigungsart.m
//  orochemie
//
//  Created by Axel Beckert on 25.02.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsart.h"
#import "Verbrauch.h"


@implementation Reinigungsart

@dynamic bezeichnung;
@dynamic bezeichnung2;
@dynamic kennung;
@dynamic sortierung;
@dynamic wassermenge;
@dynamic verbrauch;

@end
