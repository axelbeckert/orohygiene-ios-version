//
//  HygieneChecklistenTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 27.02.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "HygieneChecklistenTableViewController.h"
#import "Checkliste.h"
#import "Checklistenkategorie.h"
#import "WebviewViewController.h"
#import "ChecklistenCollector.h"

@interface HygieneChecklistenTableViewController () <UISearchBarDelegate>
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,strong) NSMutableArray *kategorieArray;
@end

@implementation HygieneChecklistenTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Checkliste class])];
    NSPredicate *predicate = nil;
    NSSortDescriptor *checklistenkategorie=nil;
    NSSortDescriptor *sortName = nil;
    NSString *sectionKeyPath = [[NSString alloc]init];
    
    if(self.musterplaene==1){
        predicate = [NSPredicate predicateWithFormat:@"checklistenkategorie.musterplan ==1 AND checklistenkategorie.branchmodel ==1 AND checklistenkategorie.typemodel ==0"];
        checklistenkategorie= [[NSSortDescriptor alloc] initWithKey:@"checklistenkategorie.sortierung" ascending:YES];
        sortName= [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        sectionKeyPath = @"checklistenkategorie.sortierung";
        
    } else {
        predicate = [NSPredicate predicateWithFormat:@"checklistenkategorie.musterplan ==0 AND checklistenkategorie.typemodel ==1 AND checklistenkategorie.branchmodel ==0"];
        checklistenkategorie= [[NSSortDescriptor alloc] initWithKey:@"checklistenkategorie.index" ascending:YES];
        sortName= [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        sectionKeyPath = @"checklistenkategorie.index";
    
    }
    
    fetch.predicate = predicate;
    NSArray *sortArray= [NSArray arrayWithObjects:checklistenkategorie,sortName,nil];
    fetch.sortDescriptors = sortArray;
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:sectionKeyPath
                                 cacheName:nil];

    

    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - ScreenRotation

-(BOOL)shouldAutorotate
{
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [oroThemeManager  customizeTableView:self.tableView];
    
    //Core Data initalisieren
    self.delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = self.delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    [self buildCategorieHeadlines];
    [self checkIfChecklistsThere];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    //SerachBar
    UISearchBar *searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
    [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    if(self.musterplaene == 1){
        self.title=@"Desinfektionspläne Muster";
    } else {
        self.title=@"Checklisten";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performTableViewRefresh) name:UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
    
//    [self instantiateRefreshControl];
    
}

-(void) instantiateRefreshControl{

    self.refreshControl = [[UIRefreshControl alloc]init];
    self.refreshControl = self.refreshControl;
    [self.refreshControl addTarget:self action:@selector(refreshChecklists:) forControlEvents:UIControlEventValueChanged];
    
}

-(void) refreshChecklists:(id)paramSender{
    [self.tableView setContentOffset:CGPointMake(0.0f, -60.0f) animated:YES];
    [self.refreshControl beginRefreshing];
 
    self.delegate.checklistenCollector.hygienechecklistenTableViewController = self;
    self.delegate.checklistenCollector.showSVProgressHUD = 1;
    [self.delegate.checklistenCollector startCollectingChecklists];
    [self.tableView layoutIfNeeded];
    [self.refreshControl endRefreshing];

}

-(void) checkIfChecklistsThere{

    if(self.fetchedResultsController.fetchedObjects.count==0){
        self.delegate.checklistenCollector.hygienechecklistenTableViewController = self;
        self.delegate.checklistenCollector.showSVProgressHUD = 1;
        [self.delegate.checklistenCollector startCollectingChecklists];
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"View will Appear");
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - fetch Checklistenkategories

-(void) buildCategorieHeadlines {
    
    NSMutableSet *kategorieSet = [NSMutableSet new];
    NSArray *checklisten = [self.fetchedResultsController fetchedObjects];
    
    for (Checkliste* checkliste in checklisten){
        [kategorieSet addObject:checkliste.checklistenkategorie];
        
    }
    
    NSString *sortWithKey=@"index";
    
    if(self.musterplaene==1) sortWithKey=@"sortierung";
    
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:sortWithKey ascending:YES];
    
    self.kategorieArray = [NSMutableArray arrayWithArray:[kategorieSet allObjects]];
    [self.kategorieArray sortUsingDescriptors:[NSArray arrayWithObject:sorter]];

}

-(NSString*)fetchChecklistenKategorieWithSection:(NSUInteger*)section{
 
    Checklistenkategorie* kategorie =[self.kategorieArray objectAtIndex:section];
    
    return kategorie.bezeichnung;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Checkliste *checkliste =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    
    cell.textLabel.text= checkliste.name;
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

#pragma mark - tableView indexTitle
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
//    return self.fetchedResultsController.sectionIndexTitles;
    return nil;
}
#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
   label.text =[self fetchChecklistenKategorieWithSection:section];
    
    return container;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:@"checklistenSegue"]){
        Checkliste *checkliste = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    
        WebviewViewController* controller = segue.destinationViewController;
        controller.urlString= checkliste.url;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneChecklisten;
        if(self.musterplaene==1){
            controller.webViewTitle=@"Musterplan";
        } else {
            controller.webViewTitle=cWebsiteTitleFuerHygieneChecklisten;
        }
        
        controller.loadPdfFile=1;
    }
    
}

#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - SearchBarDelegate Methoden

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    
    
    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        NSPredicate *predicate;
        if(self.musterplaene==1){
        predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.name CONTAINS[c] %@ AND SELF.checklistenkategorie.musterplan ==1 AND SELF.checklistenkategorie.branchmodel ==1", searchText];
        } else {
            predicate = [NSPredicate
                         predicateWithFormat:@"SELF.name CONTAINS[c] %@ AND SELF.checklistenkategorie.musterplan ==0 AND SELF.checklistenkategorie.typemodel ==1", searchText];
        }
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self buildCategorieHeadlines];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - performTableViewRefresh

-(void) performTableViewRefresh{

    [self.fetchedResultsController performFetch:nil];
    [self buildCategorieHeadlines];
   // NSLog(@"Results from fetch: %@", [self.fetchedResultsController fetchedObjects]);
    [self.tableView reloadData];
}
@end
