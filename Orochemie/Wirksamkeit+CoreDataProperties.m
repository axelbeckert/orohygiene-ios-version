//
//  Wirksamkeit+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 08.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Wirksamkeit+CoreDataProperties.h"

@implementation Wirksamkeit (CoreDataProperties)

@dynamic bezeichnung;
@dynamic tstamp;
@dynamic uid;
@dynamic wirksam;
@dynamic sorting;
@dynamic produkte;

@end
