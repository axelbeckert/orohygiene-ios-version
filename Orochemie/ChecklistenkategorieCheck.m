//
//  ChecklistenkategorieCheck.m
//  orochemie 
//
//  Created by Axel Beckert on 30.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ChecklistenkategorieCheck.h"
#import "Checklistenkategorie+CoreDataProperties.h"
#import "Checkliste+CoreDataProperties.h"
#import "AppDelegate.h"

@interface ChecklistenkategorieCheck()
@property (nonatomic,strong) NSDictionary* data;
@property int checklistenKategorieUid;
@property BOOL isBranchCategorie;
@property BOOL isTypeCategorie;
@end

@implementation ChecklistenkategorieCheck


-(Checkliste*) addChecklistenCategorieToChecklisteWithData:(NSDictionary*)data andCheckliste:(Checkliste*)checkliste{
    
    self.data = data;
    
    [self checkData];

    Checklistenkategorie* checklistencategorie = [self checkCategorieWithCategorieID];
    
    if(checklistencategorie){
        [self checkChecklistCategorieValuesIfTheyChangedSinceLastDownload:checklistencategorie];
        checkliste.checklistenkategorie = checklistencategorie;
        
    } else {
        checkliste.checklistenkategorie = [self getNewChecklistenCategorie];
    }
    
    
    return checkliste;
    
    
}

-(void) checkData
{
   
    if(self.data[@"branch"]!=nil ){
        if([self.data[@"branch"] objectForKey:@"uid"]!=nil){
            self.isBranchCategorie = @(YES);
            NSString* rawChecklistenCategorieUId = self.data[@"branch"][@"uid"];
            self.checklistenKategorieUid = [rawChecklistenCategorieUId intValue];
        }
    }
    else if (self.data[@"type"]!=nil){
        if([self.data[@"type"] objectForKey:@"uid"]!=nil){
            self.isTypeCategorie = @(YES);
            NSString* rawChecklistenCategorieUId = self.data[@"type"][@"uid"];
            self.checklistenKategorieUid = [rawChecklistenCategorieUId intValue];
        }
    }
}

-(Checklistenkategorie*) checkCategorieWithCategorieID{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checklistenkategorie"];
    NSPredicate *predicate = NULL;
    if(self.isBranchCategorie){
        predicate = [NSPredicate predicateWithFormat:@"uid ==%i AND branchmodel==1",self.checklistenKategorieUid];
    } else if(self.isTypeCategorie){
        predicate = [NSPredicate predicateWithFormat:@"uid ==%i AND typemodel==1",self.checklistenKategorieUid];
    }
    
    
    request.predicate = predicate;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    Checklistenkategorie* kategorie= NULL;
    
    if(results.count){
        
        kategorie =[results lastObject];
        
    }
    
    return kategorie;
}



-(Checklistenkategorie*) getNewChecklistenCategorie{
    
    Checklistenkategorie *checklistenkategorie = [Checklistenkategorie insertChecklistenkategorieInManagedObjectContext:self.appDelegate.managedObjectContext];
    
    checklistenkategorie.uid = [NSNumber numberWithInt:self.checklistenKategorieUid];
    
    [self setBranchValuesForChecklistenCategorie:checklistenkategorie];
    [self setTypeValuesForChecklistenCategorie:checklistenkategorie];
    
        
    return checklistenkategorie;
    
}

-(Checklistenkategorie*) setBranchValuesForChecklistenCategorie:(Checklistenkategorie*)checklistenCategorie{
    
    if(self.data[@"branch"]!=nil){
        checklistenCategorie.bezeichnung = self.data[@"branch"][@"description"];
        checklistenCategorie.index = [self.data[@"branch"][@"description"] substringToIndex:1];
        checklistenCategorie.branchmodel = @(YES);
        checklistenCategorie.typemodel = @(NO);
        checklistenCategorie.musterplan = @(YES);
        NSString* rawSortOrder = self.data[@"branch"][@"sortorder"];
        int sortOrder = [rawSortOrder intValue];
        checklistenCategorie.sortierung = @(sortOrder);
    }
    
    return checklistenCategorie;
}

-(Checklistenkategorie*) setTypeValuesForChecklistenCategorie:(Checklistenkategorie*)checklistenCategorie{

    if(self.data[@"type"]!=nil){
        checklistenCategorie.index = [self.data[@"type"][@"description"] substringToIndex:1];
        checklistenCategorie.bezeichnung = self.data[@"type"][@"description"];
        checklistenCategorie.typemodel = @(YES);
        checklistenCategorie.branchmodel = @(NO);
        checklistenCategorie = [self specialRuleForForeignModelFlyerCat:checklistenCategorie];
    }
    
    return checklistenCategorie;
}

-(Checklistenkategorie*) specialRuleForForeignModelFlyerCat: (Checklistenkategorie*) checklistenCategorie{
    
    if([checklistenCategorie.bezeichnung isEqualToString:@"Fremdsprachige Musterflyer"]){
        checklistenCategorie.index = @"Z";
    }
    
    return checklistenCategorie;
}

-(Checklistenkategorie*) checkChecklistCategorieValuesIfTheyChangedSinceLastDownload:(Checklistenkategorie*)checklistCategorie {
    
    if(self.isTypeCategorie){
        if(![checklistCategorie.bezeichnung isEqualToString:self.data[@"type"][@"description"]]){
            checklistCategorie.bezeichnung = self.data[@"type"][@"description"];
            checklistCategorie.index = [self.data[@"type"][@"description"] substringToIndex:1];
        }
        checklistCategorie = [self specialRuleForForeignModelFlyerCat:checklistCategorie];
        
    } else if(self.isBranchCategorie){
        if(![checklistCategorie.bezeichnung isEqualToString:self.data[@"branch"][@"description"]]){
            checklistCategorie.bezeichnung = self.data[@"branch"][@"description"];
            checklistCategorie.index = [self.data[@"branch"][@"description"] substringToIndex:1];
        }
    }

    
    return checklistCategorie;
}

@end
