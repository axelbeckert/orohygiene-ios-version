//
//  NSObject+JSM.m
//  AffirmationApp
//
//  Created by Frank Jüstel on 03.09.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+JSM.h"

@implementation NSObject (JSM)

static char staticAccociatedObjectKey;

+(NSString *)className
{
    return NSStringFromClass([self class]);
}

-(NSString *)className
{
    return NSStringFromClass([self class]);
}

-(BOOL)isNull
{
    return self == [NSNull null];
}

-(void)setAssociatedObject:(id)object forKey:(NSString *)key
{
    NSMutableDictionary* dict = objc_getAssociatedObject(self, &staticAccociatedObjectKey);
    if (!dict) {
        dict = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, &staticAccociatedObjectKey, dict, OBJC_ASSOCIATION_RETAIN);
    }
    [dict setObject:object forKey:key];
}

-(id)associatedObjectForKey:(NSString *)key
{
    NSMutableDictionary* dict = objc_getAssociatedObject(self, &staticAccociatedObjectKey);
    return [dict objectForKey:key];
}

@end
