//
//  Produktvarianten.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produktvarianten.h"
#import "Produkte.h"


@implementation Produktvarianten

@dynamic artikelnummer;
@dynamic barcode;
@dynamic bezeichnung1;
@dynamic bezeichnung2;
@dynamic matchcode;
@dynamic produkte;
@dynamic reiniger;

@end
