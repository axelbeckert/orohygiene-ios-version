//
//  Berater.h
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Berater : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * telefon;
@property (nonatomic, retain) NSString * gebiet;
@property (nonatomic, retain) NSSet *plz;
@end

@interface Berater (CoreDataGeneratedAccessors)

- (void)addPlzObject:(NSManagedObject *)value;
- (void)removePlzObject:(NSManagedObject *)value;
- (void)addPlz:(NSSet *)values;
- (void)removePlz:(NSSet *)values;

@end
