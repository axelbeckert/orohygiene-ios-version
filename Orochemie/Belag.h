//
//  Belag.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Reiniger;

@interface Belag : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) Reiniger *reiniger;

@end
