//
//  Newsletter.h
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Newsletter : NSObject
@property (nonatomic, strong) NSString* newsletterId;
@property (nonatomic, strong) NSString* newsletterText1;
@property (nonatomic, strong) NSString* newsletterText2;
@property (nonatomic, strong) NSString* newsletterUrl;

-(NSDictionary*) asNewsletterDictionary;
@end
