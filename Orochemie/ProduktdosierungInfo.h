//
//  ProduktdosierungInfo.h
//  Orochemie
//
//  Created by Axel Beckert on 07.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte;

@interface ProduktdosierungInfo : NSManagedObject

@property (nonatomic, retain) NSString * dosierung;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) Produkte *produkte;

@end
