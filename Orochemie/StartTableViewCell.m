//
//  StartTableViewCell.m
//  Orochemie
//
//  Created by Axel Beckert on 12.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "StartTableViewCell.h"

@implementation StartTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
