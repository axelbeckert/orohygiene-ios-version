//
//  VerauchsrechnerFlaecheViewController.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VerauchsrechnerFlaecheViewController.h"
#import "VerbrauchsRechnerStaticTableViewController.h"

@interface VerauchsrechnerFlaecheViewController ()
@property (strong, nonatomic) IBOutlet UILabel *qmEingabeLabelOutlet;
@property(nonatomic,strong) NSString *stringCollection;
- (IBAction)digitButtonTouchUpInside:(id)sender;
- (IBAction)decimalPointButtonTouchUpInside:(id)sender;
- (IBAction)eingabeuebernehmenButtonAction:(id)sender;
- (IBAction)deleteButtonAction:(id)sender;
@end

@implementation VerauchsrechnerFlaecheViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {

        NSLog(@"FlächenView geöffnet");
        
        
        
        NSString *title =@"Eingabe erforderlich:";
        NSString* message =@"Bitte geben Sie die Fläche in qm ein:";
        
        UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:title andMessage:message];
        
        [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
            UITextField *flaeche = alert.textFields.firstObject;
            NSLog(@"Fläche:%@", flaeche.text);
            [self setFlaechenValueAfterInput:flaeche.text];
        }]];
        
        [AlertHelper addTextFieldToUIAlertController:alert withPlaceHolderText:@"Geben Sie hier die Fläche ein!"];
        
        UITextField *tf = [alert.textFields firstObject];
        tf.keyboardType = UIKeyboardTypeDecimalPad;
        
        [alert addAction:[AlertHelper getCancelButtonWithCompletionHandler:nil]];
        [AlertHelper showAlert:alert];
        
    }
}

-(void) setFlaechenValueAfterInput: (NSString*)flaeche {

    NSNumber *numberString = [self.verbrauchsRechnerStaticTableViewController.numberFormatter numberFromString:flaeche];
    
    self.verbrauchsRechnerStaticTableViewController.flaecheMainTextLabelOutlet.text =[NSString stringWithFormat:@"%@ qm",[self.verbrauchsRechnerStaticTableViewController.numberFormatter stringFromNumber:numberString]];
    self.verbrauchsRechnerStaticTableViewController.flachenTextLabelOutlet.text =@"zu reinigende Fläche";
    
    self.verbrauchsRechnerStaticTableViewController.flaeche = numberString;
    [self.verbrauchsRechnerStaticTableViewController rechneVerbrauch];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewDidAppear:(BOOL)animated{
//  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (IBAction)digitButtonTouchUpInside:(UIButton *)sender {
   
    NSString* digit = sender.titleLabel.text;
    
    self.stringCollection = [self.stringCollection stringByAppendingString:digit];
    //NSLog(@"stringCollection: %@", self.stringCollection);
    
    self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text stringByAppendingString:digit];

}

- (IBAction)decimalPointButtonTouchUpInside:(id)sender {
    
    NSRange range = [self.qmEingabeLabelOutlet.text rangeOfString:@","];
    if (range.location == NSNotFound) {
        self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text stringByAppendingString:@","];
    }
    NSRange range2 = [self.stringCollection rangeOfString:@","];
    if (range2.location == NSNotFound) {
        self.stringCollection = [self.stringCollection stringByAppendingString:@","];
    }

}
- (IBAction)eingabeuebernehmenButtonAction:(id)sender {
   
        NSNumber *numberString = [self.verbrauchsRechnerStaticTableViewController.numberFormatter numberFromString:self.qmEingabeLabelOutlet.text];

        self.verbrauchsRechnerStaticTableViewController.flaecheMainTextLabelOutlet.text =[NSString stringWithFormat:@"%@ qm",[self.verbrauchsRechnerStaticTableViewController.numberFormatter stringFromNumber:numberString]];
        self.verbrauchsRechnerStaticTableViewController.flachenTextLabelOutlet.text =@"zu reinigende Fläche";

        self.verbrauchsRechnerStaticTableViewController.flaeche = numberString;
    [self.verbrauchsRechnerStaticTableViewController rechneVerbrauch];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)deleteButtonAction:(id)sender {
    
    self.qmEingabeLabelOutlet.text = [self.qmEingabeLabelOutlet.text substringToIndex:self.qmEingabeLabelOutlet.text.length-(self.qmEingabeLabelOutlet.text.length>0)];
    
    self.stringCollection = [self.stringCollection substringToIndex:self.stringCollection.length-(self.stringCollection.length>0)];
}
@end
