//
//  JSMTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface JSMTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>
- (NSFetchedResultsController *) fetchedResultsController;
@end
