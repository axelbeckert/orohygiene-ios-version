//
//  HilfsmittelTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "HilfsmittelTableViewController.h"
#import "DosierungStaticTableViewController.h"
#import "BarcodeReader.h"
#import "KrankheitserregerTableViewController.h"
#import "ErklaerungFarbsystemStaticTableViewController.h"
#import "InformationenTableViewCell.h"
#import "PdfViewViewController.h"
#import "BestellungTableViewController.h"
#import "WebviewViewController.h"
#import "MerklistenTabbarController.h"
#import "TechnischeAnfrageTableViewController.h"
#import "HygieneChecklistenTableViewController.h"

@interface HilfsmittelTableViewController ()
@property (nonatomic,strong) NSArray *navigationPoints;
@end

@implementation HilfsmittelTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeTableView:self.tableView];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.navigationPoints = @[@"Anwendungshinweise",
                              @"Barcode-Reader",
                              @"Checklisten für Hygieneaufgaben",
                              @"Desinfektionspläne erstellen",
                              @"Desinfektionspläne Muster",
                              @"Dosierrechner",
                              @"Erklärung Farbsystem",
                              @"Hygienewissen",
                              @"Krankheitserreger von A-Z",
                              @"Merklisten",
                              @"Produkt-Bestellung",
                              @"Technische Anfrage"
                            ];
    
    //aktivieren wenn die Bestllung freigeschaltet werden soll.
    //self.navigationPoints = @[@"Dosierrechner",@"Krankheitserreger von A-Z",@"Erklärung Farbsystem",@"Hygienewissen",@"Barcode-Reader",@"Produkt-Bestellung",@"APP-Bestellung-Test"];

}

-(void)viewDidAppear:(BOOL)animated{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Hilfsmittel-Schirm-Desinfektionskanal"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.navigationPoints.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
//    UIView *backgroundView = [[UIView alloc]init];
//    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
//    
//    cell.backgroundView=backgroundView;
    
    // Configure the cell...
    cell.TitleLabelOutlet.text = [self.navigationPoints objectAtIndex:indexPath.row];

    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 5)
    {

            DosierungStaticTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DosierungStaticTableViewController"];
            
            [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 8)
    {
        KrankheitserregerTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VirusTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];

        
    } else if (indexPath.row == 6)
    {
        
        ErklaerungFarbsystemStaticTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HilfsmittelStaticTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    
    } else if (indexPath.row == 2)
    {
        
        HygieneChecklistenTableViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"HygieneChecklistenTableViewController"];
        controller.musterplaene=0;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 0)
    {
        
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerBebilderteAnwendungshinweise;
        controller.pdfFileName=cWebsitePDFTitleFuerBebilderteAnwendungshinweise;
        controller.webViewTitle=cWebsiteTitleFuerBebilderteAnwendungshinweise;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 3)
    {
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerHygieneSystem;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneSystem;
        controller.webViewTitle=cWebsiteTitleFuerHygieneSystem;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 7)
    {        
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerHygieneWissen;
        controller.pdfFileName=cWebsitePDFTitleFuerHygieneWissen;
        controller.webViewTitle=cWebsiteTitleFuerHygieneWissen;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 1)
    {
    BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
        controller.showBarcodeReaderFrom=DesinfektionChannel;
    [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 9) {
        MerklistenTabbarController *controller = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
        controller.showMerklistenTabBarControllerFrom = DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 10)
    {
        WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        controller.urlString=cWebsiteFuerBestellung;
        controller.pdfFileName=cWebsitePDFTitleFuerBestellung;
        controller.webViewTitle=cWebsiteTitleFuerBestellung;
        [self.navigationController pushViewController:controller animated:YES];
    
    
    } else if (indexPath.row == 11)
    {
        TechnischeAnfrageTableViewController *controller = [self.storyboard
    instantiateViewControllerWithIdentifier:@"TechnischeAnfrageTableViewController"];
        controller.showTechnischeAnfrageFrom=DesinfektionChannel;
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    } else if (indexPath.row == 4)
    {
        HygieneChecklistenTableViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"HygieneChecklistenTableViewController"];
        controller.musterplaene=1;
        [self.navigationController pushViewController:controller animated:YES];
    }

    

}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end