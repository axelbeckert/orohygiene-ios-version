//
//  ProduktVariantenTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 06.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMTableViewController.h"
@class Produkte;
@class BestellProdukteTableViewController;

@interface ProduktVariantenTableViewController : JSMTableViewController
@property (nonatomic, strong) Produkte *produkt;
@property (nonatomic,strong) NSMutableArray *bestellMengenArray;
@property(nonatomic,strong) BestellProdukteTableViewController *bestellProdukteTableViewController;
@end
