//
//  PdfTest.h
//  orochemie
//
//  Created by Axel Beckert on 22.08.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PdfTest : NSObject
-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename;
-(NSData *)pdfDataWithTableView:(UITableView*)tableView;
@end
