//
//  Phwert.h
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Reiniger;

@interface Phwert : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSSet *reiniger;
@end

@interface Phwert (CoreDataGeneratedAccessors)

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

@end
