//
//  Video.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Reiniger;

@interface Video : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSString * datei;
@property (nonatomic, retain) NSString * filesize;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * kategorie;
@property (nonatomic, retain) NSNumber * reinigerVideo;
@property (nonatomic, retain) NSNumber * sortierung;
@property (nonatomic, retain) NSString * youtube;
@property (nonatomic, retain) NSSet *produkte;
@property (nonatomic, retain) NSSet *reiniger;
@end

@interface Video (CoreDataGeneratedAccessors)

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet *)values;
- (void)removeProdukte:(NSSet *)values;

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

@end
