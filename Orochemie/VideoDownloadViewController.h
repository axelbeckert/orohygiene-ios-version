//
//  VideoDownloadViewController.h
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoDownloadViewController :UIViewController
-(void)downloadVideosWithID:(int)id;
@property (nonatomic, strong) NSBlockOperation *blockOperation;
@property int errorFlag;
-(void)checkVideos;
@end
