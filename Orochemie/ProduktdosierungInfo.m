//
//  ProduktdosierungInfo.m
//  Orochemie
//
//  Created by Axel Beckert on 07.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktdosierungInfo.h"
#import "Produkte.h"


@implementation ProduktdosierungInfo

@dynamic dosierung;
@dynamic index;
@dynamic produkte;

@end
