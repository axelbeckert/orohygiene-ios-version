//
//  Reinigungsverfahren+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsverfahren.h"

@interface Reinigungsverfahren (ccm)
+(Reinigungsverfahren*) insertReinigungsverfahrenInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteReinigungsverfahren;
@end
