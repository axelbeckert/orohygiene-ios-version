//
//  ProduktbezeichnungTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktAuflistungTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "ProduktStaticTableViewController.h"
#import "ProduktStaticTabBarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "SectionHeaderTableViewCell.h"
#import "WebviewViewController.h"


@interface ProduktAuflistungTableViewController()<UISearchBarDelegate>
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@end

@implementation ProduktAuflistungTableViewController
#pragma mark - NSFechtedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Produkte class])];
    
    //    //Wenn vom AnwendungsController der Aufruf kommt dann Suche die Produkte die zur Anwendung gehören
    if(self.anwendungForProduktResult!=nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"anwendung.name CONTAINS[c] %@",self.anwendungForProduktResult.name];
        fetch.predicate=predicate;
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
        
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Anwendung-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Anwendung"
                                                              action:@"Show"
                                                               label:self.anwendungForProduktResult.name                                                         value:nil] build]];
    }
    //Wenn vom BranchenController der Aufruf kommt dann Suche die Produkte die zur Anwendung gehören
    if(self.brancheForProduktResult!=nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"branche.name CONTAINS[c] %@",self.brancheForProduktResult.name];
        fetch.predicate=predicate;
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
        
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Anwendung-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Branche"
                                                              action:@"Show"
                                                               label:self.brancheForProduktResult.name                                                         value:nil] build]];
    }
    
    if(self.wirksamkeitForProduktResult!=nil){
        NSLog(@"wirksakeit in liste: %@", self.wirksamkeitForProduktResult.bezeichnung);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wirksamkeiten.uid CONTAINS[c] %@",self.wirksamkeitForProduktResult.uid];
        fetch.predicate=predicate;
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Wirksamkeits-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Wirksamkeit"
                                                              action:@"Show"
                                                               label:self.brancheForProduktResult.name                                                         value:nil] build]];
    }
    
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
    fetch.sortDescriptors = sortArray;
    
    
    
    //fetch.sortDescriptors = @[ sortByName ];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"index"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle
-(void) viewDidLoad
{

    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    [oroThemeManager  customizeTableView:self.tableView];
    
    //[JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
 
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
       [self.fetchedResultsController performFetch:nil];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];

    
    //SerachBar
    UISearchBar *searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
       [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];

    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;


    //Title Label
    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
   
    if(self.brancheForProduktResult!=nil){
        
        tlabel.text = self.brancheForProduktResult.name;
    
    } else if (self.anwendungForProduktResult!=nil){
       
        tlabel.text = self.anwendungForProduktResult.name;

    } else {
        
        tlabel.text = @"Produkte";

    }
    
    if(self.wirksamkeitForProduktResult!=nil){
        tlabel.text = self.wirksamkeitForProduktResult.bezeichnung;
    }
    
    self.navigationItem.titleView=tlabel;
    

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self messageForUsingWirksamkeiten];
    [self messageForUsingAnwendungen];
    
}

-(void) messageForUsingAnwendungen{
    
    if(self.anwendungForProduktResult!=nil){

        UIBarButtonItem *anwendungsItem = [[UIBarButtonItem alloc]initWithTitle:@"bebilderte Anwendungshinweise" style:UIBarButtonItemStylePlain target:self action:@selector(anwendungsHinweise:)];

        [anwendungsItem setTintColor:[UIColor whiteColor]];
        
        [self activateToolbarWith:anwendungsItem];
        
    }

}

-(void)messageForUsingWirksamkeiten{
    
    
    if(self.wirksamkeitForProduktResult!=nil){
        UIBarButtonItem *wirksamkeitenItem = [[UIBarButtonItem alloc]initWithTitle:cHinweisWissenGewissenButton style:UIBarButtonItemStylePlain target:self action:@selector(hinweis:)];
        
        [self activateToolbarWith:wirksamkeitenItem];
    }
}

-(void)activateToolbarWith:(UIBarButtonItem*)toolbarItem{

    [toolbarItem setTintColor:[UIColor whiteColor]];

    //Barbutton Items for Toolbar
    UIBarButtonItem *itemOne = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *itemThree = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    //Toolbar initialisieren und anzeigen
    
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    
    
    self.toolbarItems=@[itemOne,toolbarItem,itemThree];
  
}



-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Desinfektion-Produkt-Auflistung-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:YES];
    
}

- (BOOL)shouldAutorotate {
    
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}



#pragma mark - TableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    }
    
    NSString *sectionTitle = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return sectionTitle;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"ProduktuebersichtTableViewCell";
    

    ProduktuebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Produkte *produkte =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    

    [oroThemeManager customizeTableCellText:cell];
    
    cell.titleLabelOutlet.text = produkte.nameHauptbezeichnung;
    cell.descriptionLabelOutlet.text = produkte.nameZusatz;
    cell.subtitleLabelOutlet.text = produkte.zusatz;
    cell.imageViewOutlet.image = [UIImage imageNamed:produkte.bild];
    
  
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}




-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"O"])
    {
        additionalSectionTitle = @"Vliestücher";
    }
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


#pragma mark - Segue Methoden

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.identifier isEqualToString:@"showProduktStatic"]){
        ProduktStaticTableViewController *controller = segue.destinationViewController;
        if(self.anwendungForProduktResult) controller.anwendungName = self.anwendungForProduktResult.name;
        if(self.brancheForProduktResult) controller.branchenName = self.brancheForProduktResult.name;
        controller.produkt = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
    if([segue.identifier isEqualToString:@"showProductStaticCustom"]){
        
        produktStaticTabBarViewController *controller = segue.destinationViewController;
        
        if(self.anwendungForProduktResult) controller.anwendungName = self.anwendungForProduktResult.name;
        if(self.brancheForProduktResult) controller.branchenName = self.brancheForProduktResult.name;
        controller.produkt = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
}

#pragma mark - Button Action Methoden

-(void)anwendungsHinweise:(id)sender
{
    WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
    controller.urlString=cWebsiteFuerBebilderteAnwendungshinweise;
    controller.webViewTitle=cWebsiteTitleFuerBebilderteAnwendungshinweise;
    controller.pdfFileName=cWebsitePDFTitleFuerBebilderteAnwendungshinweise;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - SearchBarDelegate Methoden

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    

    
    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.nameHauptbezeichnung CONTAINS[c] %@ OR Self.nameZusatz CONTAINS[c] %@", searchText,searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Hinweis zur Auflistung

-(void)hinweis:(id)sender
{
    
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:cHinweisWissenGewissenTitle andMessage:cHinweisWissenGewissen andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
    }];
    
    [AlertHelper showAlert:alert];
    
    
}
@end
