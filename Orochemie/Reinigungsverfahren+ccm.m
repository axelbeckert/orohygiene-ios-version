//
//  Reinigungsverfahren+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsverfahren+ccm.h"

@implementation Reinigungsverfahren (ccm)
+(Reinigungsverfahren*) insertReinigungsverfahrenInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Reinigungsverfahren class]) inManagedObjectContext:managedObjectContext];
}
-(void)deleteReinigungsverfahren{
    [self.managedObjectContext deleteObject:self];
}
@end
