//
//  DosierungStaticTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Produkte;
@class produktStaticTabBarViewController;
@class Produktdosierung;
@class Produkteinwirkzeit;

@protocol DosierungStaticTableViewControllerDelegate <NSObject>
@optional
-(void) dosierrechnerIsStarted;
-(void) dosierrechnerIsStopped;

@end

@interface DosierungStaticTableViewController : UITableViewController
@property (nonatomic,weak) id <DosierungStaticTableViewControllerDelegate> delegate;
@property (nonatomic,strong) NSString *produkt;
@property (nonatomic,strong) NSString *produktUntertitel;
@property (nonatomic,strong) NSString *einwirkzeit;
@property (nonatomic,strong) NSString *mengeGebrauchsfertigeLoesung;
@property (nonatomic,strong) NSNumber *rechnen;
@property (nonatomic,strong) NSNumber *ersterStart;
@property(nonatomic,strong) NSString *branchenName;
@property(nonatomic,strong) NSString *anwendungName;

@property (strong, nonatomic) IBOutlet UILabel *produktZusatzinformationTextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *produktTextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *einwirkzeitTextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *mengeGebrauchsfertigerLoesungtextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *einwirkzeitZustazinformationTextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *mengeGebrauchsfertigerLoesungZusatzinformationOutlet;
@property (strong, nonatomic) IBOutlet UILabel *kippanzahlDosierflascheOutlet;
@property (strong, nonatomic) IBOutlet UILabel *konzentratMengetextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *wasserMengeTextOutlet;
@property (strong, nonatomic) IBOutlet UITableViewCell *produktAuswahlCellOutlet;
@property (nonatomic,assign)  BOOL fixedProduct;
@property (nonatomic,strong) Produkte *uebergebenesProdukt;
@property (nonatomic,assign) BOOL tabBarViewController;
@property (nonatomic, strong) Produktdosierung *produktdosierung;
@property(nonatomic,strong) Produkteinwirkzeit *produkteinwirkzeit;
- (IBAction)printAction:(id)sender;

- (UIImage *) imageWithView:(UIView *)view;

-(void)rechne2;
@end
