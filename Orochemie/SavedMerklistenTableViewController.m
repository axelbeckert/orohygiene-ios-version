//
//  SavedMerklistenTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "SavedMerklistenTableViewController.h"
#import "SavedMerklistenDetailsTableViewController.h"
#import "MerklistenTableViewHelper.h"


@interface SavedMerklistenTableViewController () <MerklistenTableViewHelperDelegate>
@property(nonatomic, strong)
    NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) MerklistenTableViewHelper *merklistenTableViewHelper;
@end

@implementation SavedMerklistenTableViewController

#pragma mark - NSFechtedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
  if (_fetchedResultsController)
    return _fetchedResultsController;

  NSFetchRequest *fetch = [NSFetchRequest
      fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
  NSSortDescriptor *sortByName =
      [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];

  fetch.sortDescriptors = @[ sortByName ];

 NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:@"bezeichnung != %@",
                              @"basisMerkliste"];

    fetch.predicate = predicate;
    
  _fetchedResultsController = [[NSFetchedResultsController alloc]
      initWithFetchRequest:fetch
      managedObjectContext:self.managedObjectContext
        sectionNameKeyPath:nil
                 cacheName:nil];

  NSError *error;

  [_fetchedResultsController performFetch:&error];

  if (error) {
    //NSLog(@"Fehler: %@", error);
    abort();
  }

  return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
    
  //Optik
    if(self.showSavedMerklistenFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
    } else if(self.showSavedMerklistenFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
    }
  //AppDelegate initialisieren
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

  //Managed Objekt Context setzen
    self.managedObjectContext = appDelegate.managedObjectContext;

  //Insets anlegen
    if(self.tabBarController){
        
        UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 49, 0);
        self.tableView.contentInset = inset;
    }
    
    self.merklistenTableViewHelper = [MerklistenTableViewHelper new];
    self.merklistenTableViewHelper.delegate = self;
    

}
- (void)viewDidAppear:(BOOL)animated {

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Gespeicherte Merklisten anzeigen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
  self.tabBarController.title = @"gesp. Merklisten";
  self.tabBarController.navigationItem.rightBarButtonItems = nil;

  [self showRightBarButtonMenueItems];
    
  NSError *error;
  [self.fetchedResultsController performFetch:&error];
    [self.tableView reloadData];
    
    
}



- (void)viewDidDisappear:(BOOL)animated {
  if ([self.delegate
          respondsToSelector:@selector(showRightBarButtonMenueItems)]) {
    [self.delegate showRightBarButtonMenueItems];
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
  
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  id<NSFetchedResultsSectionInfo> info =
      self.fetchedResultsController.sections[section];
  return [info numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:@"cell"
                                      forIndexPath:indexPath];

  Merkliste *merkliste =
      [self.fetchedResultsController objectAtIndexPath:indexPath];

    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    cell.textLabel.text = merkliste.bezeichnung;

  return cell;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
       
        Merkliste *merkliste = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [merkliste deleteMerkliste];
        
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        
        [appDelegate saveContext];
        
        [self.fetchedResultsController performFetch:nil];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath]
                                withRowAnimation:UITableViewRowAnimationFade];
        [tableView endUpdates];
        
        [appDelegate saveMerklistenDataToMerklistenDB];
        
        
        id<NSFetchedResultsSectionInfo> info =
        self.fetchedResultsController.sections[0];
        
        
        if([info numberOfObjects]==0){
            [self stopMerklistenTableViewEditingMode];
        }

        
        
    }
    
}

- (void)merklistenActionSheet {
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *actionSheet = [AlertHelper getUIActionSheetWithTitle:@"Merkliste bearbeiten:" andMessage:@"Bitte wählen Sie eine Aktion aus!" andCancelButtonWithCompletionHandler:nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        CGRect sourceRect = CGRectZero;
        sourceRect.origin.x = CGRectGetMidX(self.view.bounds)-self.view.frame.origin.x/2.0;
        sourceRect.origin.y = CGRectGetMidY(self.view.bounds)-self.view.frame.origin.y/2.0;
        
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = sourceRect;
        [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    }
    
    [actionSheet addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Merkliste löschen"
                                                   andCompletionHandler:^(UIAlertAction *alertAction) {
                                                       [weakSelf switchMerklistenTableViewToEditingMode];
                                                   }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark - Merkliste Buttons
- (UIBarButtonItem *)callMerklistenActionSheetButton:(id)target {
    
    UIBarButtonItem *MerklistenActionSheetButton = [[UIBarButtonItem alloc]
                                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                    target:self
                                                    action:@selector(merklistenActionSheet)];
    
    return MerklistenActionSheetButton;
}

-(void)switchMerklistenTableViewToEditingMode {
    self.tableView.editing = YES;
    self.tabBarController.navigationItem.rightBarButtonItems = nil;
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(stopMerklistenTableViewEditingMode)];
    
    self.tabBarController.navigationItem.rightBarButtonItem = item;
}

-(void)stopMerklistenTableViewEditingMode{
    
    self.tableView.editing=NO;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
    
    [self showRightBarButtonMenueItems];

}

#pragma mark - SavedMerklistenTableViewControllerDelegate

-(void)showRightBarButtonMenueItems{
    
    self.tabBarController.title = @"aktuelle Merkliste";
    id<NSFetchedResultsSectionInfo> info =
    self.fetchedResultsController.sections[0];
    
    NSMutableArray *buttonArray = [NSMutableArray new];
    
    if([info numberOfObjects]>0){
       [buttonArray addObject:[self callMerklistenActionSheetButton:self]];    }
    
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.tabBarController.navigationItem.rightBarButtonItems = buttonArray;

}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Segue Navigation



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showMerklistenItems"]){
        SavedMerklistenDetailsTableViewController *controller = segue.destinationViewController;
        controller.merkliste = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        if(self.showSavedMerklistenFrom == ReinigerChannel){
            controller.showSavedMerklistenDetailsFrom = ReinigerChannel;
        } else if(self.showSavedMerklistenFrom == DesinfektionChannel){
            controller.showSavedMerklistenDetailsFrom = DesinfektionChannel;
        }

    }
  
    
}

-(void)presentAlertController: (UIAlertController*)alert animated:(BOOL)animated completion:(UITableViewControllerBlock)block {
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@"delegate was called with: present ViewController");
}

@end
