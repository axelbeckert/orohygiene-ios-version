//
//  TechnischeAnfrage+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 13.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "TechnischeAnfrage+ccm.h"

@implementation TechnischeAnfrage (ccm)
+(TechnischeAnfrage*) insertTechnischeAnfrageInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"TechnischeAnfrage" inManagedObjectContext:managedObjectContext];
}
-(void)deleteTechnischeAnfrage
{
    [self.managedObjectContext deleteObject:self];
}
@end
