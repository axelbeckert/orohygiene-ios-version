//
//  ConnectionCheck.h
//  Orochemie
//
//  Created by Axel Beckert on 17.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionCheck : NSObject
+(BOOL)hasConnectivity;
+(void)answer;
@end
