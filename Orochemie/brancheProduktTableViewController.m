//
//  brancheProduktTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.02.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "brancheProduktTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "produktStaticTabBarViewController.h"

@interface brancheProduktTableViewController()
@property (nonatomic, strong) NSFetchedResultsController *brancheProduktFetchedResultsController;
@end

@implementation brancheProduktTableViewController
@synthesize brancheProduktFetchedResultsController = _brancheProduktFetchedResultsController;
@synthesize brancheProdukteTableView = _brancheProdukteTableView;


-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(void) viewDidLoad
{
    [oroThemeManager  customizeTableView:self.tableView];
    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
    
    UIBarButtonItem* homeBarButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:self action:@selector(home:)];
    [homeBarButton setImage:[oroThemeManager setUiBarButtonImage]];
    
    self.navigationItem.rightBarButtonItem = homeBarButton;
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    //Title Label

    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
    tlabel.text = self.brancheProdukteTableView.name;
    self.navigationItem.titleView=tlabel;
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[[self.fetchedResultsController sections] objectAtIndex:section]numberOfObjects];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProduktuebersichtTableViewCell";
    
    ProduktuebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Produkte *produkte =[self.brancheProduktFetchedResultsController objectAtIndexPath:indexPath];

    [oroThemeManager customizeTableCellText:cell];
        [oroThemeManager customizeAccessoryView:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
    if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.titleLabelOutlet.text = [produkte.name substringToIndex:7];
        
    } else {
        cell.titleLabelOutlet.text = [produkte.name substringToIndex:4];
    }
    
    if([produkte.name isEqualToString:@"B 5 Wischdesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:4];
    } else if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:7];
    } else {
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:5];
    }
    
    cell.subtitleLabelOutlet.text = produkte.zusatz;
    
    cell.imageViewOutlet.image = [UIImage imageNamed:produkte.bild];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark - must be overloaded methods
-(NSFetchedResultsController *)fetchedResultsController
{
    if(self.brancheProduktFetchedResultsController !=nil){
        return self.brancheProduktFetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:cEntityProdukte inManagedObjectContext:[JSMCoreDataHelper managedObjectContext]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"branche.name CONTAINS[c] %@",self.brancheProdukteTableView.name];
    
    
    fetchRequest.entity=entityDescription;
    fetchRequest.fetchBatchSize=64;
    fetchRequest.predicate=predicate;
    
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];
    
    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    

    fetchRequest.sortDescriptors = sortArray;
    
    self.brancheProduktFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[JSMCoreDataHelper managedObjectContext] sectionNameKeyPath:@"index" cacheName:nil];
    
    self.brancheProduktFetchedResultsController.delegate=self;
    
    return self.brancheProduktFetchedResultsController;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"O"])
    {
        additionalSectionTitle = @"Vliestücher";
    }
    
    NSString *sectionTitle = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = sectionTitle;
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSArray *titles = [self.fetchedResultsController sectionIndexTitles];
//    
//    if(titles.count <=section) return @"Error";
//    return [titles objectAtIndex:section];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Produkte *produkt =[self.brancheProduktFetchedResultsController objectAtIndexPath:indexPath];
    
    
    produktStaticTabBarViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"produktStaticTabBarViewController"];
    controller.produkt = produkt;
    controller.branchenName=self.brancheProdukteTableView.name;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

@end
