//
//  SavedMerklistenDetailsTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 05.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedMerklistenDetailsTableViewController : UITableViewController
@property(nonatomic,strong) Merkliste *merkliste;
@property(nonatomic,assign) show showSavedMerklistenDetailsFrom;
@end
