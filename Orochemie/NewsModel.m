//
//  NewsModel.m
//  ReadWriteFormats
//
//  Created by Axel Beckert on 27.02.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import "NewsModel.h"


@interface NewsModel ();
@property (nonatomic, strong) NSMutableArray* news;
@end

@implementation NewsModel

-(NSMutableArray *)news
{
    if (!_news) _news = [[NSMutableArray alloc] init];
    return _news;
}

-(NSInteger)numberOfNews
{
    return self.news.count;
}

-(void)addNews:(News *)news
{
    [self.news addObject:news];
}

-(News *)newsAtIndexPath:(NSIndexPath *)indexPath
{
    return self.news [indexPath.row];
}

-(NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len
{
    return [self.news countByEnumeratingWithState:state objects:buffer count:len];
}

@end
