//
//  JsonProtocol.h
//  orochemie 
//
//  Created by Axel Beckert on 26.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#ifndef JsonProtocol_h
#define JsonProtocol_h
@protocol JsonProtocol <NSObject>
-(NSString*)tstamp;
@end

#endif /* JsonProtocol_h */
