//
//  MyAnnotaion.h
//  WoBinIch
//
//  Created by Frank Jüstel on 05.03.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface MyAnnotaion : NSObject <MKAnnotation>
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end
