//
//  Anwendung.h
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Reiniger;

@interface Anwendung : NSManagedObject

@property (nonatomic, retain) NSString * bild;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sortierung;
@property (nonatomic, retain) NSNumber * reinigerAnwendung;
@property (nonatomic, retain) NSString * zusatzbezeichnung;
@property (nonatomic, retain) NSSet *produkte;
@property (nonatomic, retain) NSSet *reiniger;
@end

@interface Anwendung (CoreDataGeneratedAccessors)

- (void)addProdukteObject:(Produkte *)value;
- (void)removeProdukteObject:(Produkte *)value;
- (void)addProdukte:(NSSet *)values;
- (void)removeProdukte:(NSSet *)values;

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet *)values;
- (void)removeReiniger:(NSSet *)values;

@end
