//
//  NavigationController.h
//  Orochemie
//
//  Created by Axel Beckert on 16.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController

@end
