//
//  ProduktVariantenStaticTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 06.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Produktvarianten;

@interface ProduktVariantenStaticTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *artikelnummerTextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *matchcodeTextOutelet;
@property (strong, nonatomic) IBOutlet UILabel *bezeichnung1TextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *bezeichnung2TextOutlet;
@property (strong, nonatomic) IBOutlet UILabel *barcodeTextOutlet;
@property (nonatomic,strong) Produktvarianten *produktvariante;
@end
