//
//  ProduktbezeichnungTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktbezeichnungTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "ProduktStaticTableViewController.h"
#import "produktStaticTabBarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "SectionHeaderTableViewCell.h"


@interface ProduktbezeichnungTableViewController()
@property (nonatomic, strong) NSFetchedResultsController *produkteFetchedResultsController;

@end

@implementation ProduktbezeichnungTableViewController
@synthesize produkteFetchedResultsController = _produkteFetchedResultsController;

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) viewDidLoad
{

    
    UIBarButtonItem* homeBarButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:self action:@selector(home:)];
    [homeBarButton setImage:[oroThemeManager setUiBarButtonImage]];
    
    self.navigationItem.rightBarButtonItem = homeBarButton;

    [oroThemeManager  customizeTableView:self.tableView];
    
    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexMinimumDisplayRowCount = 20;
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];

    

    
}

- (BOOL)shouldAutorotate {
    
    return NO;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

//-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//
//    return self.fetchedResultsController.sectionIndexTitles;
//
//}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    }
    
    NSString *sectionTitle = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return sectionTitle;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"ProduktuebersichtTableViewCell";
    

    ProduktuebersichtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Produkte *produkte =[self.produkteFetchedResultsController objectAtIndexPath:indexPath];
    
    

    [oroThemeManager customizeTableCellText:cell];
    //[oroThemeManager customizeAccessoryView:cell];
        

    
    if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.titleLabelOutlet.text = [produkte.name substringToIndex:7];
    
    } else {
    cell.titleLabelOutlet.text = [produkte.name substringToIndex:4];
    }
    
    if([produkte.name isEqualToString:@"B 5 Wischdesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:4];
    } else if([produkte.name isEqualToString:@"HD 410 Händedesinfektion"]){
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:7];
    } else {
        cell.descriptionLabelOutlet.text = [produkte.name substringFromIndex:5];
    } 
    
    //HD 410 Händedesinfektion
    
    cell.subtitleLabelOutlet.text = produkte.zusatz;
    cell.imageViewOutlet.image = [UIImage imageNamed:produkte.bild];
    
  
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
#pragma mark - must be overloaded methods
-(NSFetchedResultsController *)fetchedResultsController
{
    if(self.produkteFetchedResultsController !=nil){
        return self.produkteFetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:cEntityProdukte inManagedObjectContext:[JSMCoreDataHelper managedObjectContext]];
  
    fetchRequest.entity=entityDescription;
    fetchRequest.fetchBatchSize=64;

    
    //Wenn vom AnwendungsController der Aufruf kommt dann Suche die Produkte die zur Anwendung gehören
    if(self.anwendungForProduktResult!=nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"anwendung.name CONTAINS[c] %@",self.anwendungForProduktResult.name];
        fetchRequest.predicate=predicate;
    }
    
    if(self.brancheForProduktResult!=nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"branche.name CONTAINS[c] %@",self.brancheForProduktResult.name];
        fetchRequest.predicate=predicate;
    }

    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSSortDescriptor *sortReihenfolge= [[NSSortDescriptor alloc] initWithKey:@"sortierreihenfolge" ascending:YES selector:@selector(compare:)];

    
    NSArray *sortArray= @[sortIndex,sortReihenfolge];
    
    fetchRequest.sortDescriptors = sortArray;
    
    self.produkteFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[JSMCoreDataHelper managedObjectContext] sectionNameKeyPath:@"index" cacheName:nil];
    
    self.produkteFetchedResultsController.delegate=self;
    
    return self.produkteFetchedResultsController;
    

}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *additionalSectionTitle =@"";
    
    if([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"A"])
    {
        additionalSectionTitle = @"Instrumentendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"B"])
    {
        additionalSectionTitle = @"Flächendesinfektion";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"C"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"D"])
    {
        additionalSectionTitle = @"Spezialanwendung";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"H"])
    {
        additionalSectionTitle = @"Händehygiene";
    } else if ([[self.fetchedResultsController.sectionIndexTitles objectAtIndex:section] isEqualToString:@"O"])
    {
        additionalSectionTitle = @"Vliestücher";
    }
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [NSString stringWithFormat:@"%@ - %@", [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section], additionalSectionTitle];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.identifier isEqualToString:@"showProduktStatic"]){
        ProduktStaticTableViewController *controller = segue.destinationViewController;
        controller.produkt = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
    if([segue.identifier isEqualToString:@"showProductStaticCustom"]){
        ProduktStaticTableViewController *controller = segue.destinationViewController;
        controller.produkt = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
}

@end
