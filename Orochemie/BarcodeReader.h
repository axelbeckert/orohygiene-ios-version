//
//  ccmViewController.h
//  BarcodeScanner
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface BarcodeReader : UIViewController <AVCaptureMetadataOutputObjectsDelegate>//<ZBarReaderDelegate >
//
//{
//    UIImageView *resultImage;
//    UITextView *resultText;
//}

@property (strong, nonatomic) IBOutlet UIImageView *resultImageOutlet;
@property (strong, nonatomic) IBOutlet UITextField *resultTextOutlet;
@property(nonatomic,assign)show showBarcodeReaderFrom;

- (IBAction)ScanButtonTouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *scanButtonOutlet;
@property (strong, nonatomic) IBOutlet UILabel *titleLabelOutlet;

@end
