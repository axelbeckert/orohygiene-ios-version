//
//  MyNavControllerSC.m
//  Orochemie
//
//  Created by Axel Beckert on 23.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "MyNavControllerSC.h"

@interface MyNavControllerSC ()

@end

@implementation MyNavControllerSC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return [self.visibleViewController shouldAutorotate];
}
@end
