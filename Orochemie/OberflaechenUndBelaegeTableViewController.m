//
//  OberflaechenUndBelaegeTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "OberflaechenUndBelaegeTableViewController.h"
#import "ProdukteNachBezeichnungTableViewController.h"
#import "OberflaechenCollector.h"

@interface OberflaechenUndBelaegeTableViewController () <UISearchBarDelegate>
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) AppDelegate *delegate;
@end

@implementation OberflaechenUndBelaegeTableViewController
#pragma mark - NSFechtedResultsController

-(NSFetchedResultsController *) fetchedResultsController{
    if (_fetchedResultsController) return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Oberflaeche class])];
    NSSortDescriptor *sortByKategorie = [NSSortDescriptor sortDescriptorWithKey:@"kategorie" ascending:YES];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];
    
    fetch.sortDescriptors = @[sortByKategorie,sortByName];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetch
                                                                   managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"kategorie"
                                                                              cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if(error){
        //NSLog(@"Fehler: %@",error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Optik
    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    //Appdelegate & ManagedObjectContext zuweisen
    self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = self.delegate.managedObjectContext;
    
    //Titel
    self.title=@"Nach Oberflächen+Belägen";
    
    //Searchbar aufbauen
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
    [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    [self checkIfOberflaechenAreThere];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performTableViewRefresh) name:UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Barbutton Items for Toolbar
    UIBarButtonItem *itemOne = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *itemTwo = [[UIBarButtonItem alloc]initWithTitle:cHinweisWissenGewissenButton style:UIBarButtonItemStylePlain target:self action:@selector(hinweis:)];
    UIBarButtonItem *itemThree = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [itemTwo setTintColor:[UIColor whiteColor]];
    
    
    
    //Toolbar initialisieren und anzeigen
    
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    self.toolbarItems=@[itemOne,itemTwo,itemThree];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Nach-Oberflaechen-und-Belaegen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collect Oberflaechen
-(void) checkIfOberflaechenAreThere{
    
    if(self.fetchedResultsController.fetchedObjects.count==0){
        
        self.delegate.oberflaechenCollector.oberflaechenUndBelaegeTableViewController = self;
        self.delegate.oberflaechenCollector.showSVProgressHUD = 1;
        [self.delegate.oberflaechenCollector startCollectingOberflaechen];
        
    }
    
}

#pragma mark - TableViewRefresh
-(void)performTableViewRefresh{
    
    [self.fetchedResultsController performFetch:nil];
    self.delegate.oberflaechenCollector.oberflaechenUndBelaegeTableViewController = nil;
   
    [self.tableView reloadData];
    
}


#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> info = self.fetchedResultsController.sections[section];
    return [info numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Oberflaeche *oberflaeche = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    cell.textLabel.text = oberflaeche.bezeichnung;
    
    return cell;
}

#pragma mark - tableView indexTitle
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}
#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
       label.text = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
    return container;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


#pragma mark - SerachBar Delegate

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    
    
    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.bezeichnung CONTAINS[c] %@", searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Segue Methoden

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"produktNachBezeichnungSegue"]){
        ProdukteNachBezeichnungTableViewController *controller = segue.destinationViewController;
        controller.oberflaeche = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
    
    
}

#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Hinweis zur Auflistung

-(void)hinweis:(id)sender
{
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:cHinweisWissenGewissenTitle andMessage:cHinweisWissenGewissen andOKButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
    }];
    
    [AlertHelper showAlert:alert];
    
    
}

@end
