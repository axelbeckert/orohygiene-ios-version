//
//  DosierungStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "DosierungStaticTableViewController.h"
#import "ProduktDosierungViewController.h"
#import "EinwirkzeitDosierungViewController.h"
#import "MengeDosierungViewController.h"
#import <string.h>
#import "Produkte.h"
#import "Produktdosierung.h"
#import <QuartzCore/QuartzCore.h>
#import "Produktdosierung.h"
#import "Produkteinwirkzeit.h"



@interface DosierungStaticTableViewController ()
@property (nonatomic, assign) BOOL keineDosierung;
@property(nonatomic,strong) UIBarButtonItem* printActionButton;

-(void)rechne2;
@end

@implementation DosierungStaticTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [oroThemeManager  customizeTableView:self.tableView];
    
    //Title Label
    
    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
    tlabel.text = @"Dosierrechner";
    self.navigationItem.titleView=tlabel;
    
    if(!self.tabBarController)
    {
        
    UIEdgeInsets inset = UIEdgeInsetsMake(-35, 0, 0, 0);
    self.tableView.contentInset = inset;
        
    
    self.printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
    [self.printActionButton setEnabled:NO];
    
    NSArray *buttonArray = @[self.printActionButton,[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = buttonArray;

    }
    
    // Prüfung ob der Dosierrechner vom TabBarController aufgerufen wurde
    else{
        
        UIEdgeInsets inset = UIEdgeInsetsMake(28, 0, 20, 0);
        self.tableView.contentInset = inset;

    }
    
    //Delegate checken und erste Methode ausführen
    if([self.delegate respondsToSelector:@selector(dosierrechnerIsStarted)]){
        [self.delegate dosierrechnerIsStarted];
    }

    

    
    
    //NSLog(@"Übergebenes Produkt StaticTable: %@", self.uebergebenesProdukt);
    
    if(self.uebergebenesProdukt){
        NSArray *anzahlProduktDosierungen = [self.uebergebenesProdukt.produktdosierung allObjects];
        if(anzahlProduktDosierungen.count !=0){
            if(anzahlProduktDosierungen.count <2){
                Produktdosierung *dosierung = [anzahlProduktDosierungen objectAtIndex:0];
                
                self.produktdosierung = [anzahlProduktDosierungen objectAtIndex:0];
                self.produkt = dosierung.hauptbezeichnung;
                self.produktTextOutlet.text = dosierung.hauptbezeichnung;
                self.produktZusatzinformationTextOutlet.text = dosierung.zusatzbezeichnung;
                
            } else if (anzahlProduktDosierungen.count >1){
                self.produktTextOutlet.text = @"Bitte wählen Sie ein Produkt aus";
                self.produktZusatzinformationTextOutlet.text = @"Es stehen mehrere Varianten zur Verfügung";
            }
        } else {
            self.produktTextOutlet.text = @"Keine Dosierung notwendig";
            self.produktZusatzinformationTextOutlet.text = @"";
            self.einwirkzeitTextOutlet.text = @"";
            self.einwirkzeitZustazinformationTextOutlet.text =@"";
            self.mengeGebrauchsfertigerLoesungtextOutlet.text=@"";
            self.mengeGebrauchsfertigerLoesungZusatzinformationOutlet.text=@"";
            self.fixedProduct =YES;
            self.keineDosierung = YES;
        }
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
        
        [tracker set:kGAIScreenName value:@"Desinfektionsmittel-Dosierrechner-Detail-Information"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Produkt"
                                                              action:@"Rechnen"
                                                               label:self.uebergebenesProdukt.nameHauptbezeichnung
                                                               value:nil] build]];

    }

    
}

-(BOOL)shouldAutorotate
{
    return YES;
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Delegate checken und zweite Methode ausführen
    if([self.delegate respondsToSelector:@selector(dosierrechnerIsStarted)]){
        [self.delegate dosierrechnerIsStarted];
        
    }
    
 
    [self rechne2];
    

    
    
    if([self.navigationController.navigationBar isHidden])
    {
        [self.navigationController.navigationBar setHidden:NO];
    }
    
    if(self.branchenName)
    {
        
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.branchenName;
        self.tabBarController.navigationItem.titleView=tlabel;

        
    } else if (self.anwendungName){
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.anwendungName;
        self.tabBarController.navigationItem.titleView=tlabel;
    
    } else if (!self.branchenName && self.tabBarController)
    {
        self.tabBarController.title=@"Dosierung";
    }
    
    //NSLog(@"%s",__PRETTY_FUNCTION__);


}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Produkt-Dosierung-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];


}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //Delegate checken und zweite Methode ausführen
    if([self.delegate respondsToSelector:@selector(dosierrechnerIsStopped)]){
        [self.delegate dosierrechnerIsStopped];


    }
    
  }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    
    if (section == 0)
    {
        return nil;
    } else if (section == 1)
    {
        label.text=@"Konzentratmenge in ml";
    } else if (section == 2)
    {
        label.text=@"Kippanzahl Dosierflasche  (1 mal  = 20 ml)";
    } else if (section == 3)
    {
        label.text=@"Wassermenge in ml";
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section ==0) return 0;
    
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}

#pragma mark - Table view delegate

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{

    if(self.fixedProduct){
        return NO;
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
        if([segue.identifier isEqualToString:@"dosierungProduktSegue"]){
            ProduktDosierungViewController *controller = segue.destinationViewController;
            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            controller.dosierungStaticTableViewController = self;
            controller.uebergebenesProdukt = self.uebergebenesProdukt;
            self.einwirkzeitZustazinformationTextOutlet.text = @"Auswahl Einwirkzeit";
            self.mengeGebrauchsfertigerLoesungZusatzinformationOutlet.text = @"Auswahl Menge gewünschter Lösung";
            self.rechnen = nil;
            self.konzentratMengetextOutlet.text=@"-";
            self.kippanzahlDosierflascheOutlet.text=@"-";
            self.wasserMengeTextOutlet.text=@"-";
              [[self.navigationItem.rightBarButtonItems objectAtIndex:0] setEnabled:NO];
                        
        } else if ([segue.identifier isEqualToString:@"dosierungEinwirkzeitSegue"]){
         
            EinwirkzeitDosierungViewController *controller = segue.destinationViewController;
            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            controller.dosierungStaticTableViewController = self;
            controller.ausgewaehltesProdukt = self.produktTextOutlet.text;
            controller.ausgewaehltesProduktUntertitel = self.produktZusatzinformationTextOutlet.text;
            controller.produktdosierung = self.produktdosierung;
           
        } else if ([segue.identifier isEqualToString:@"dosierungMengeSegue"]){
            EinwirkzeitDosierungViewController *controller = segue.destinationViewController;
            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            controller.dosierungStaticTableViewController = self;
        }
    
    
}



-(void)rechne2
{
    
  if(self.produkteinwirkzeit!=nil){
   
    NSString *mengeLoesungString;
    if(!self.keineDosierung){
        mengeLoesungString = [self.mengeGebrauchsfertigerLoesungZusatzinformationOutlet.text substringToIndex:2];
    }
    

 
    double loesungsMenge = [mengeLoesungString doubleValue]*1000;
    double konzentratmenge = loesungsMenge*self.produkteinwirkzeit.rechengroesse.doubleValue;

    int wassermenge = (int)round(loesungsMenge)-(int)round(konzentratmenge);
      
    int modulo = (int)round(konzentratmenge) % 20;
    
    int kippanzahlDosierflasche = (int)round(konzentratmenge)/20;
    
    
    
    if(self.rechnen && konzentratmenge !=0)
    {
        UIBarButtonItem* printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
        [printActionButton setEnabled:YES];
        
        if(modulo == 0) self.kippanzahlDosierflascheOutlet.text = [NSString stringWithFormat:@"%i",kippanzahlDosierflasche];
        if(modulo != 0) self.kippanzahlDosierflascheOutlet.text = @"-";
        
        self.konzentratMengetextOutlet.text= [NSString stringWithFormat:@"%i",(int)round(konzentratmenge)];
        self.wasserMengeTextOutlet.text = [NSString stringWithFormat:@"%i",wassermenge];
        
        [[self.navigationItem.rightBarButtonItems objectAtIndex:0] setEnabled:YES];
        
    
    }
    
  }
    
}


- (void)printAction:(id)sender
{


	// Größe unseres Exportbildes festlegen
	CGRect rect = [UIScreen mainScreen].bounds;

	
	// Eine neue Session mit Transparenz, Retinaauflösung, wenn vorhanden!
    UIGraphicsBeginImageContextWithOptions(rect.size , NO, [UIScreen mainScreen].scale);
	
	// Context holen
    CGContextRef context = UIGraphicsGetCurrentContext();
	
	// Jetzt die Komponenten zeichnen lassen, die in das Bild sollen
    [self.tableView.layer renderInContext:context];


	
	// Ein "Photo" machen
    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSString *subject;
    NSString *string;
    
    if([self.produktTextOutlet.text isEqualToString:self.produktZusatzinformationTextOutlet.text])
    {
        subject = [NSString stringWithFormat:@"oro Hygine-App: %@",self.produktTextOutlet.text];
        
       string = [NSString stringWithFormat:@"Dosierung für Produkt: %@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.produktTextOutlet.text];
        
    }
    else {
        subject = [NSString stringWithFormat:@"oro Hygiene App: %@, %@",self.produktTextOutlet.text, self.produktZusatzinformationTextOutlet.text];
        
       string = [NSString stringWithFormat:@"Dosierung für Produkt: %@, %@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.produktTextOutlet.text, self.produktZusatzinformationTextOutlet.text];
    }
    

	
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[screenShot,string] applicationActivities:nil];
	
	

    controller.excludedActivityTypes = @[
                                      //UIActivityTypeCopyToPasteboard,
                                      UIActivityTypePostToWeibo,
                                      UIActivityTypePostToFacebook,
                                      UIActivityTypePostToTwitter,
                                      //UIActivityTypeSaveToCameraRoll,
                                      //UIActivityTypeMail,
                                      //UIActivityTypePrint,
                                      UIActivityTypeMessage,
                                      UIActivityTypeAssignToContact,
                                      ];
	

	[controller setValue:subject forKey:@"subject"];
    
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [sender valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
    
	[self presentViewController:controller animated:YES completion:nil];
}


- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}


@end
