//
//  KontaktAufnehmenTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "KontaktAufnehmenTableViewController.h"
#import "Kontakt+ccm.h"
#import "Berater+ccm.h"
#import "BEMCheckBox.h"
#import "DatenschutzTableViewController.h"

enum FormFieldFailure {
    
    ANSPRECHPARTNER_LEER,
    TELEFON_LEER,
    EMAIL_LEER,
    EMAIL_VALIDATION_FALSE,
    PRIVACY_POLICY_CHECKBOX
};

typedef enum FormFieldFailure formFieldFailure;

@interface KontaktAufnehmenTableViewController() <UITextViewDelegate>
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UITextField *ansprechpartnerTextFieldOutlet;
@property (strong, nonatomic) IBOutlet UITextField *telefonTextFieldOutlet;
@property (strong, nonatomic) IBOutlet UITextField *emailTextFieldOutlet;
@property (strong, nonatomic) IBOutlet UITextView *nachrichtTextViewOutlet;
@property (strong, nonatomic) IBOutlet UIButton *sendenButtonOutlet;

- (IBAction)sendenButtonTouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticSendenButtonCellOutlet;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticInformationZuKontaktAufnehmenCellOutlet;
@property (strong,nonatomic) IBOutlet BEMCheckBox *privacyCheckbox;
@property (strong, nonatomic) IBOutlet UIButton *readPrivacyPolicyButton;
@end

@implementation KontaktAufnehmenTableViewController
int zuatzHoeheKontaktAnfrage = 30;

#pragma mark - View Lifecycle

-(void)viewDidLoad{
    
    
    //Optik
    if(self.showBeraterInMeinerNaeheFrom == DesinfektionChannel){
        
        [oroThemeManager customizeTableView:self.tableView];
        
        
    } else if(self.showBeraterInMeinerNaeheFrom == ReinigerChannel){
        
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }

    [oroThemeManager customizeButtonImage:self.sendenButtonOutlet];
    self.staticInformationZuKontaktAufnehmenCellOutlet.backgroundColor=[UIColor clearColor];
    
    self.staticSendenButtonCellOutlet.backgroundColor=[UIColor clearColor];
    
    //BarButton
    [self prepareRightBarButtonItems];
    
    //TextView Delegate
    self.nachrichtTextViewOutlet.delegate=self;
    
    //AppDelegate für CoreData aktivieren
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    //Prüfen ob eine gespeicherte Anfrage vorhanden ist. Wenn ja, anzeigen
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Kontakt class])];
    
    NSArray *alteAnfragenArray = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if(alteAnfragenArray.count>0){
        
        Kontakt *alteAnfrage = [alteAnfragenArray lastObject];
        //NSLog(@"alte Anfrage vorhanden:%@",alteAnfrage);
        
        self.ansprechpartnerTextFieldOutlet.text = alteAnfrage.ansprechpartner;
        self.emailTextFieldOutlet.text = alteAnfrage.email;
        self.telefonTextFieldOutlet.text = alteAnfrage.telefon;
        self.nachrichtTextViewOutlet.text = alteAnfrage.nachricht;

        
        UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"voherige Anfrage:" andMessage:@"Eine alte Anfrage wurde noch nicht erfolgreich gesendet, soll sie nun gesendet oder gelöscht werden?"];
        
        [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Senden" andCompletionHandler:^(UIAlertAction *alertAction) {
                [self anfrageSenden];
        }]];
        
        [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Löschen" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self alteGespeicherteAnfrageLoeschen];
            [self formularLeeren];
        }]];
        
        [AlertHelper showAlert:alert];
        
    }
    
    alteAnfragenArray = nil;
    fetchRequest = nil;
    
}


-(void)prepareRightBarButtonItems{
    
    NSMutableArray *buttonArray = [NSMutableArray new];
    
    //BarButton
    [buttonArray addObject:[self clearFormButton:self]];
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Berater-In-meiner-Naehe-_Kontakt-Aufnehmen-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}
#pragma mark - ScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    [self.view endEditing:YES];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    
}
#pragma mark - TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.section == 3 && indexPath.row == 0) { // Nachricht TextView
        
        CGRect bounds = self.nachrichtTextViewOutlet.bounds;
        CGSize constraintSize = CGSizeMake(bounds.size.width, FLT_MAX);
        bounds.size = constraintSize;
        UIFont *font = self.nachrichtTextViewOutlet.font;
        
        CGRect sizeOfText = [self.nachrichtTextViewOutlet.text
                             boundingRectWithSize:constraintSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : font
                                          }
                             context:nil];
        
        return MAX(44, sizeOfText.size.height + zuatzHoeheKontaktAnfrage);
    } else if (indexPath.section ==4){
        return 88;
    }
    
    
    
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *container = [oroThemeManager customizeStaticTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    
    if (section == 0)
    {
        label.text = @"Ansprechpartner für Rückmeldung*";
    }  else if (section == 1)
    {
        label.text=@"Telefon*";
    } else if (section == 2)
    {
        label.text=@"Email*";
    } else if (section == 3)
    {
        label.text=@"Nachricht";
        
    } else if (section ==4 || section==5){
        return nil;
    }
    
    return container;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section==4 || section ==5){
        return 0;
    }
    return [oroThemeManager customizeStaticTableViewHeigtForHeaderView];
    
}


#pragma mark - UITextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    UITableViewCell *cell = (UITableViewCell*) [[textView superview] superview];
    [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}



#pragma mark - Email Validation

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}


#pragma mark - Button Definitions
- (UIBarButtonItem *)clearFormButton:(id)target {
    
    UIBarButtonItem *clearFormButton = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                        target:self
                                        action:@selector(clearForm)];
    
    return clearFormButton;
}

#pragma mark - Button Actions

-(void)clearForm{
    
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Anfrage zurücksetzen" andMessage:@"Möchten Sie wirklich die Daten im Formular löschen?" andCancelButtonWithCompletionHandler:nil];
    
    [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self formularLeeren];
        [self alteGespeicherteAnfrageLoeschen];
    
    }]];
    
   
    
    [AlertHelper showAlert:alert];
  
}



- (IBAction)sendenButtonTouchUpInside:(id)sender {
    
    [self anfrageSenden];
}

-(IBAction)privacyPolicyButtonToucUpInside:(id)sender{
    
    DatenschutzTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DatenschutzTableViewController"];
    
    if(self.showBeraterInMeinerNaeheFrom == DesinfektionChannel){
        controller.showDatenschutzFrom = DesinfektionChannel;
    } else {
        controller.showDatenschutzFrom = ReinigerChannel;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

#pragma mark - Anfrage Senden

-(void)anfrageSenden{
    
    [self alteGespeicherteAnfrageLoeschen];
    
    if([self checkFormFields]) {
        
        if([ConnectionCheck hasConnectivity]) {
            
            [SVProgressHUD showWithStatus:@"Sende die Anfrage" maskType:SVProgressHUDMaskTypeBlack];
            
            NSMutableDictionary *kontaktAnfrageDictionary = [[NSMutableDictionary alloc]init];
            
            [kontaktAnfrageDictionary setObject:self.berater.name forKey:@"berater"];
            [kontaktAnfrageDictionary setObject:  self.ansprechpartnerTextFieldOutlet.text forKey:@"ansprechpartner"];
            [kontaktAnfrageDictionary setObject:  self.telefonTextFieldOutlet.text forKey:@"telefon"];
            [kontaktAnfrageDictionary setObject:  self.emailTextFieldOutlet.text  forKey:@"email"];
            [kontaktAnfrageDictionary setObject:  self.nachrichtTextViewOutlet.text  forKey:@"nachricht"];
            [kontaktAnfrageDictionary setObject:  @"Datenschutzerklärung akzeptiert"  forKey:@"datenschutz"];
         
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:kontaktAnfrageDictionary options:0 error:nil];
            NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            NSString *post = [NSString stringWithFormat:@"%@", jsonString];
            NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
            
            [request setURL:[NSURL URLWithString:@"http://www.orohygienesystem.de/anfragen/json_kontakt_anfrage.php"]];
            [request setHTTPMethod:@"POST"];
            
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(((NSHTTPURLResponse *)response).statusCode==200){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [SVProgressHUD showSuccessWithStatus:@"Anfrage erfolgreich gesendet"];
                        [self formularLeeren];
                    });
                    
                } else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [SVProgressHUD showSuccessWithStatus:@"Anfrage nicht erfolgreich gesendet! Bitte wiederholen."];
                    });

                }
            }];
            
            [postDataTask resume];
            
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung"];
            
            
            //Aktuelle Anfrage speichern damit sie nicht verloren geht
            
            Kontakt *aktuelleAnfrage = [Kontakt insertKontaktInManagedObjectContext:self.appDelegate.managedObjectContext];
            aktuelleAnfrage.ansprechpartner = self.ansprechpartnerTextFieldOutlet.text;
            aktuelleAnfrage.email = self.emailTextFieldOutlet.text;
            aktuelleAnfrage.telefon = self.telefonTextFieldOutlet.text;
            aktuelleAnfrage.nachricht = self.nachrichtTextViewOutlet.text;

            //NSLog(@"neue Anfrage:%@",aktuelleAnfrage);
            [self.appDelegate saveContext];
            
            //NSLog(@"neue Anfrage wurde gespeichert");
            
            
        }
      
        
    }
    
    
    
    
}

-(BOOL)checkFormFields{

    if(self.ansprechpartnerTextFieldOutlet.text.length==0){
        [self notifyUserAboutFailure:ANSPRECHPARTNER_LEER];
        return NO;
    }
    else if(self.telefonTextFieldOutlet.text.length==0) {
        [self notifyUserAboutFailure:TELEFON_LEER];
        return NO;
    }
    else if (self.emailTextFieldOutlet.text.length==0){
        [self notifyUserAboutFailure:EMAIL_LEER];
        return NO;
    } else if (![self validateEmail:self.emailTextFieldOutlet.text]) {
        [self notifyUserAboutFailure:EMAIL_VALIDATION_FALSE];
        return NO;
    } else if(!self.privacyCheckbox.on){
        [self notifyUserAboutFailure:PRIVACY_POLICY_CHECKBOX];
        return NO;
    }
    
    return YES;
}

-(void)notifyUserAboutFailure:(formFieldFailure)failureCode{
    NSString *message;
    
    switch (failureCode) {
            
        case ANSPRECHPARTNER_LEER: // Fehler weil Ansprechpartner leer
            message =@"Bitte tragen Sie einen Ansprechpartner ein.";
            break;
            
        case TELEFON_LEER: //Fehler weil Telefon leer
            message =@"Bitte tragen Sie eine Telefonnummer ein.";
            break;

        case EMAIL_LEER: //Fehler weil E-Mail leer
            message =@"Bitte tragen Sie eine gültige E-Mail Adresse ein.";
            break;

        case EMAIL_VALIDATION_FALSE:
            message =@"Bitte tragen Sie eine gültige E-Mail Adresse ein.";
            break;
        case PRIVACY_POLICY_CHECKBOX:
            message =@"Bitte bestätigen Sie, dass Sie die Datenschutzerklärung akzeptieren.";
            break;
        default:
            break;
    }
    
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:message andOKButtonWithCompletionHandler:nil];
    
    [AlertHelper showAlert:alert];
    
}

-(void)alteGespeicherteAnfrageLoeschen{
    //Prüfen ob bereits eine alte Anfrage gespeichert wurde und eventuell löschen
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Kontakt class])];
    
    NSArray *anfragenArray = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if(anfragenArray.count>0){
        
        Kontakt *alteAnfrage = [anfragenArray lastObject];
        //NSLog(@"alte Anfrage:%@",alteAnfrage);
        [alteAnfrage deleteKontakt];
        [self.appDelegate saveContext];
        
        //NSLog(@"alte Anfrage wurde gelöscht");
    }
    
    anfragenArray = nil;
    fetchRequest = nil;
    
}

-(void)formularLeeren{
    
    
    self.ansprechpartnerTextFieldOutlet.text=@"";
    self.telefonTextFieldOutlet.text=@"";
    self.emailTextFieldOutlet.text=@"";
    self.nachrichtTextViewOutlet.text=@"";
    self.privacyCheckbox.on = false;
    
    [self.tableView reloadData];
}

@end
