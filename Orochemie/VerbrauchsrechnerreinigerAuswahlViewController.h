//
//  VerbrauchsrechnerreinigerAuswahlViewController.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VerbrauchsRechnerStaticTableViewController;

@interface VerbrauchsrechnerreinigerAuswahlViewController : UIViewController
<UIPickerViewDataSource, UIPickerViewDelegate>{
    __weak IBOutlet UIPickerView *reinigerAuswahlPickerViewOutlet;
    
    
}
@property (strong, nonatomic) IBOutlet UIView *topToolBarDosierungAuswahlViewOutlet;
@property(nonatomic,strong) VerbrauchsRechnerStaticTableViewController *verbrauchsRechnerStaticTableViewController;
@end
