//
//  DatenschutzTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 20.08.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatenschutzTableViewController : UITableViewController
@property (nonatomic,assign) show showDatenschutzFrom;

@property (strong, nonatomic) IBOutlet UITextView *allgemeierHinweisZumDatenschutz;
@property (strong, nonatomic) IBOutlet UITextView *datenschutzbeauftragter;
@property (strong, nonatomic) IBOutlet UITextView *voraussetzungDatenverarbeitung;
@property (strong, nonatomic) IBOutlet UITextView *datenschutzUndWebsitesDritter;
@property (strong, nonatomic) IBOutlet UITextView *datensicherheit;
@property (strong, nonatomic) IBOutlet UITextView *detailsZurDatenverarbeitung;
@property (strong, nonatomic) IBOutlet UITextView *detailsZurDatenverarbeitung2;
@property (strong, nonatomic) IBOutlet UITextView *betroffenenrechte;
@property (strong, nonatomic) IBOutlet UITextView *widerspruchsrecht;
@property (strong, nonatomic) IBOutlet UITextView *aktualisierungDatenschutzerklaerung;
@end

