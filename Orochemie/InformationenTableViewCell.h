//
//  InformationenTableViewCell.h
//  orochemie
//
//  Created by Axel Beckert on 19.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationenTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *TitleLabelOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *imageOutlet;

@end
