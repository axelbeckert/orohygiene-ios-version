//
//  BeraterInMeinerNaeheTableViewController.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "BeraterInMeinerNaeheTableViewController.h"
#import "Berater+ccm.h"
#import "PLZ+ccm.h"
#import "KontaktAufnehmenTableViewController.h"

enum searchForBeraterCode{
    ZIP_FAILURE,
    NO_SALESMAN_FOUND,
    NO_FAILURE
};

typedef enum searchForBeraterCode beraterCode;

@interface BeraterInMeinerNaeheTableViewController ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) Berater *berater;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *textLabelOutlet;

@property (strong, nonatomic) IBOutlet UIButton *kontaktButtonOutlet;
- (IBAction)kontaktButtonTouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticBeraterImageCellOutlet;
@property (strong, nonatomic) IBOutlet UITableViewCell *staticKontaktAufnehmenButtonCellOutlet;
@end
@implementation BeraterInMeinerNaeheTableViewController


#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Optik
    if(self.showBeraterInMeinerNaeheFrom == DesinfektionChannel){

        [oroThemeManager customizeTableView:self.tableView];
   
        
    } else if(self.showBeraterInMeinerNaeheFrom == ReinigerChannel){
        
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    [oroThemeManager customizeButtonImage:self.kontaktButtonOutlet];
    
    self.staticBeraterImageCellOutlet.backgroundColor = [UIColor clearColor];
    
    self.staticKontaktAufnehmenButtonCellOutlet.backgroundColor = [UIColor clearColor];
    
    [self prepareRightBarButtonItems];
    
    //AppDelegate initialisieren
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //ManagedObjectContext zuweisen
    self.managedObjectContext = appDelegate.managedObjectContext;

    self.title=@"Berater in meiner Nähe";
    
    [self searchForSalesmanWithCode:NO_FAILURE];
 }


-(void)prepareRightBarButtonItems{
    
    NSMutableArray *buttonArray = [NSMutableArray new];
    
    //BarButton
    [buttonArray addObject:[self searchAgainButton:self]];
    [buttonArray addObject:[oroThemeManager homeBarButton:self]];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
}

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Berater-In-meiner-Naehe-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = @"Ihr Berater:";
    
    return container;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


#pragma mark - Berater suchen

-(void) searchForSalesmanWithCode:(beraterCode)code{
    NSString *title;
    NSString *message;
    
    switch (code) {

        case ZIP_FAILURE: // Fehler weil PLZ zu kurz
            title =@"Fehler!";
            message =@"Die eingegebene PLZ war zu kurz. Bitte geben Sie Ihre PLZ erneut ein:";
            break;
            
        case NO_SALESMAN_FOUND: //Fehler weil kein Berater gefunden wurde
            title =@"Fehler!";
            message =@"Bitte prüfen Sie die eingegebende PLZ, es konnte kein Berater gefunden werden! Bitte geben Sie Ihre PLZ erneut ein:";
            break;
            
        default: //Standard Text
            title =@"Eingabe erfoderlich:";
            message =@"Bitte geben Sie Ihre PLZ ein:";
            break;
    }
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:title andMessage:message andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
         [self.navigationController popViewControllerAnimated:YES];
    }];
   

    
    [AlertHelper addTextFieldToUIAlertController:alert withPlaceHolderText:@"Geben Sie hier die PLZ ein!" andKeyboardType:UIKeyboardTypeNumberPad];
    
    [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
        [SVProgressHUD showWithStatus:@"Suche Ansprechpartner" maskType:SVProgressHUDMaskTypeBlack];
        
        UITextField *plzTextfield = [alert textFields].firstObject;
        
        NSString *erfasstePLZ = plzTextfield.text;
        
        //PLZ kleiner als 5 Stellen
        if(erfasstePLZ.length < 5) {
            [self searchForSalesmanWithCode:ZIP_FAILURE];
            [SVProgressHUD dismiss];
            return;
        }
        
        //Berater suchen
        NSError *error;
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([PLZ class])];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"plz == %@",erfasstePLZ];
        fetchRequest.predicate = predicate;
        NSArray *plzArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        //Fehler ausgeben, wenn vorhanden
        if(error){
            //NSLog(@"Error: %@", error.localizedDescription);
        }
        
        //Wenn Berater gefunden, anzeigen
        if(plzArray.count>0){
            PLZ *plz =[plzArray lastObject];
            self.berater = plz.berater;
            //NSLog(@"Berater gefunden: Name:%@ - PLZmin: - PLZmax:", self.berater.name);
            
            self.imageViewOutlet.image = [UIImage imageNamed:self.berater.image];
            self.textLabelOutlet.text = self.berater.name;
            [SVProgressHUD dismiss];
            
        }
        //wenn nicht erneuter Aufforderung zur Eingabe der PLZ
        else {
            //            [SVProgressHUD dismiss];
            //            [self searchForSalesmanWithCode:NO_SALESMAN_FOUND];
            
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Berater class])];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@",@"orochemie"];
            fetchRequest.predicate = predicate;
            
            NSArray *beraterArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            if(beraterArray.copy>0){
                self.berater = [beraterArray lastObject];
                self.imageViewOutlet.image = [UIImage imageNamed:self.berater.image];
                self.textLabelOutlet.text = self.berater.name;
                [SVProgressHUD dismiss];
                
            }
        }

    }]];
    
    [AlertHelper showAlert:alert];

}



#pragma mark - Button Definitions
- (UIBarButtonItem *)searchAgainButton:(id)target {
    
    UIBarButtonItem *searchAgainButton = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                        target:self
                                        action:@selector(searchAgainButtonAction)];
    
    return searchAgainButton;
}

#pragma mark - Button Actions

-(void)searchAgainButtonAction{
    [self searchForSalesmanWithCode:NO_FAILURE];
}

- (IBAction)kontaktButtonTouchUpInside:(id)sender {
}

#pragma  mark - Segue Naviagtion

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"kontaktAufnehmenSegue"]){
        
        KontaktAufnehmenTableViewController *controller = segue.destinationViewController;
        controller.showBeraterInMeinerNaeheFrom = self.showBeraterInMeinerNaeheFrom;
        controller.berater = self.berater;
    }
}
@end
