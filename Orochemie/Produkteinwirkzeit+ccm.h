//
//  Produkteinwirkzeit+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkteinwirkzeit.h"

@interface Produkteinwirkzeit (ccm)
+(Produkteinwirkzeit*) insertProdukteinwirkzeitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteProdukteinwirkzeit;
@end
