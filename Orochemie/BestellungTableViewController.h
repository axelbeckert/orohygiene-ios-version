//
//  BestellungTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BestellungTableViewController : UITableViewController
@property (nonatomic,strong) NSMutableArray *bestellMengenArray;
@end
