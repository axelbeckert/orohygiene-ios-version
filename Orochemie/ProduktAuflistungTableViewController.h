//
//  ProduktbezeichnungTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSMTableViewController.h"

@class Anwendung;
@class Branche;

@interface ProduktAuflistungTableViewController : UITableViewController
@property (nonatomic,strong) Anwendung *anwendungForProduktResult;
@property (nonatomic,strong) Branche *brancheForProduktResult;
@property (nonatomic,strong) Wirksamkeit *wirksamkeitForProduktResult;
@end
