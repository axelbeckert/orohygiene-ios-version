//
//  CollectionReusableView.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableView : UICollectionReusableView


@property (strong, nonatomic) IBOutlet UIImageView *reusableViewHeaderImageOutlet;
@property (strong, nonatomic) IBOutlet UILabel *resuableViewHeaderLabelOutlet;

@end
