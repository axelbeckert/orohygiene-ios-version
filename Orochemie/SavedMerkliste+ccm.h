//
//  SavedMerkliste+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "SavedMerkliste.h"

@interface SavedMerkliste (ccm)
+(SavedMerkliste*)insertSavedMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteSavedMerkliste;
@end
