//
//  BestellungViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 17.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BestellungViewController : UIViewController
//@property (nonatomic,strong)
@property (strong, nonatomic) IBOutlet UIWebView *bestellungWebViewOutlet;
@property (nonatomic,strong) NSString *branchenName;
@property (nonatomic,strong) NSString *anwendungName;
- (IBAction)refreshAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)forwardAction:(id)sender;
- (IBAction)stopLoadingAction:(id)sender;

@end
