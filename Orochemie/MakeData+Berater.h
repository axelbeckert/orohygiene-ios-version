//
//  MakeData+Berater.h
//  orochemie
//
//  Created by Axel Beckert on 16.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MakeData.h"

@interface MakeData (Berater)
- (void)makeBeraterData;
@end
