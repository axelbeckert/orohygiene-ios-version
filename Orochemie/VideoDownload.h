//
//  VideoDownload.h
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoDownload : NSObject < NSURLConnectionDataDelegate>
-(void)downloadVideos;
@end
