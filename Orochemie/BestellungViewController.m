//
//  BestellungViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 17.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "BestellungViewController.h"
#import "BNHtmlPdfKit.h"

@interface BestellungViewController () <UIWebViewDelegate,BNHtmlPdfKitDelegate>
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (nonatomic,strong) NSTimer *timer;
@property int loadingCounter;
@property (nonatomic,assign) BOOL videoIsOn;
@property(nonatomic,strong) BNHtmlPdfKit *pdfKit;
-(void)loadRequest;
@end

@implementation BestellungViewController
-(BOOL)shouldAutorotate
{
    return YES;
}

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"Bestellung";
    self.loadingCounter=0;
    self.bestellungWebViewOutlet.delegate = self;
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    

    
    UIBarButtonItem* printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
    
    NSArray *buttonArray = @[printActionButton, [oroThemeManager homeBarButton:self]];

    self.navigationItem.rightBarButtonItems = buttonArray;
    
    if(self.tabBarController){
       
        UIEdgeInsets inset = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height+20, 0, 0, 0);
        self.bestellungWebViewOutlet.scrollView.contentInset = inset;
    }
    
    [self loadRequest];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.branchenName)
    {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.branchenName;
        self.tabBarController.navigationItem.titleView=tlabel;
        
    } else if(self.branchenName)
    {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.anwendungName;
        self.tabBarController.navigationItem.titleView=tlabel;
        
    } else if (!self.branchenName && self.tabBarController)
    {
        self.tabBarController.title=@"Bestellung";
    }
}

-(void)loadRequest
{
    
  if([ConnectionCheck hasConnectivity]) {
    NSString *urlString = [NSString stringWithFormat:@"http://www.orochemie.de/de/bestellung_weiche.php"];
    
    NSURL *bestellungUrl = [NSURL URLWithString:urlString];
    
    [self.bestellungWebViewOutlet loadRequest:[NSURLRequest requestWithURL:bestellungUrl]];	
  } else {

      [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
  }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    
    NSString *string = [NSString stringWithFormat:@"%@",request ];
    NSRange range = [string rangeOfString:@"youtube.com"];
    //NSLog(@"shouldStartLoadWithRequest - Range Video: %i",range.length);
    if(!range.length==0)
    {
        self.videoIsOn = YES;
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        if([self.timer isValid]){
            [self.timer invalidate];
            
            //NSLog(@"shouldStartLoadWithRequest - Timer aus");
        }
        
    }
    
    
    return YES;
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde
    
    self.loadingCounter++;
    if(!self.videoIsOn){
        if(![self.timer isValid])
        {
            //NSLog(@"webViewDidStartLoad - Timer an (%i)",self.loadingCounter);
            self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
        }
    }
    
    
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:@"Fehler beim Laden"];
     self.loadingCounter=0;
    //NSLog(@"error:%@",error);
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadingCounter--;
    //NSLog(@"loadingCounter: %i",self.loadingCounter);
    
    
    
    if(self.loadingCounter==0)
    {
        [self.timer invalidate]; //Timer ausschalten
        self.timer=nil;
        self.videoIsOn=NO;
        //NSLog(@"webViewDidFinishLoad - Timer aus");
    }
    
    
    [SVProgressHUD dismiss];
}

#pragma mark - AlertView Delegate & Timer Selector
- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //NSLog(@"videoIsOn=%i",self.videoIsOn);
    if(!self.videoIsOn)
    {
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        self.bestellungWebViewOutlet.hidden=YES;
        [self.timer invalidate]; //Timer ausschalten
        
        GRAlertView *alert = [[GRAlertView alloc] initWithTitle:@"Fehler:"
                                                        message:@"Abruf momentan nicht möglich!"
                                                       delegate:self
                                              cancelButtonTitle:@"Abbruch"
                                              otherButtonTitles:@"Erneut laden", nil];
        
        
        [alert setTopColor:[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1]
               middleColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1]
               bottomColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]
                 lineColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
        
        alert.animation = GRAlertAnimationNone;    // set animation type
        
        
        [alert setImage:@"alert.png"];              // add icon image
        [alert show];
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){ [self.navigationController popViewControllerAnimated:YES]; }
    if(buttonIndex==1)
    {
        [self loadRequest];
        self.loadingCounter=0;
        
        self.bestellungWebViewOutlet.hidden=NO;
        [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
        
    }
}
- (IBAction)refreshAction:(id)sender
{
    [self.bestellungWebViewOutlet reload];
}

- (IBAction)backAction:(id)sender
{
    [self.bestellungWebViewOutlet goBack];
}

- (IBAction)forwardAction:(id)sender
{
    [self.bestellungWebViewOutlet goForward];
}

- (IBAction)stopLoadingAction:(id)sender
{
    [self.bestellungWebViewOutlet stopLoading];
}

- (void)printAction:(id)sender
{
    
    
    self.pdfKit = [[BNHtmlPdfKit alloc]initWithPageSize:BNPageSizeA4];
    self.pdfKit.delegate = self;
    
    
    [self.pdfKit saveUrlAsPdf:self.bestellungWebViewOutlet.request.URL toFile:nil];
    
	
    
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfData:(NSData *)data
{
    
    ////// - neu
    
    NSString *pdfFileName = @"oroWebsite.pdf";
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:pdfFileName];
    
    NSString *htmlContent = [NSMutableString stringWithString:[self.bestellungWebViewOutlet stringByEvaluatingJavaScriptFromString:@"document.body.outerHTML"]];
    
    NSCachedURLResponse* response = [[NSURLCache sharedURLCache]
                                     cachedResponseForRequest:[self.bestellungWebViewOutlet request]];
    
    
    
    if([htmlContent isEqualToString:@""])
    {
        NSData *pdfFile = [response data];
        [pdfFile writeToFile:filePath atomically:YES];
        
        
    } else {
        
        
        NSData *pdfFile = data;
        [pdfFile writeToFile:filePath atomically:YES];
        
    }
    
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    
    NSString *string = [NSString stringWithFormat:@"Link zur Seite: %@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.bestellungWebViewOutlet.request.URL];
    
    
    
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl,string] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                         //UIActivityTypeCopyToPasteboard,
                                         UIActivityTypePostToWeibo,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         //UIActivityTypeSaveToCameraRoll,
                                         //UIActivityTypeMail,
                                         //UIActivityTypePrint,
                                         UIActivityTypeMessage,
                                         UIActivityTypeAssignToContact,
                                         ];
    
    
    
    NSString *subject = [NSString stringWithFormat:@"oro Hygiene App: %@", pdfFileName];
	[controller setValue:subject forKey:@"subject"];
    
    
	controller.completionHandler = ^(NSString* type, BOOL complete) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){//NSLog(@"Löschen war nicht erfolgreich");
        
        };
        
	};
	
	[self presentViewController:controller animated:YES completion:nil];
    
    
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error;
{
    //NSLog(@"Error: %@", error.localizedDescription);
}
@end
