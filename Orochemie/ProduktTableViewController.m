//
//  ProduktTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "ProduktTableViewController.h"
#import "ProduktAuflistungTableViewController.h"
#import "AnwendungTableViewController.h"
#import "brancheTableViewController.h"
#import "InformationenTableViewCell.h"
#import "BarcodeReader.h"
#import "KrankheitserregerTableViewController.h"
#import "MerklistenTabbarController.h"
#import "WirksamkeitenTableViewController.h"

@interface ProduktTableViewController ()
@property (nonatomic,strong) NSArray *navigationPoints;
@end

@implementation ProduktTableViewController



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [oroThemeManager customizeTableView:self.tableView];
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Produkte";

    
    self.navigationPoints = @[@"nach Bezeichnung",@"nach Anwendung",@"nach Branche",@"nach Krankheitserreger",@"nach Desinfektionsmittelwirksamkeit",@"Barcode-Reader",@"Merklisten"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

       if(self.navigationController.toolbarHidden==NO) [self.navigationController setToolbarHidden:YES animated:YES];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Produktsortierungs-Schirm-Desinfektionskanal"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.navigationPoints.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InfoCell";

    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
   
    UIFont *font =cell.TitleLabelOutlet.font;
  
    cell.TitleLabelOutlet.text = [self.navigationPoints objectAtIndex:indexPath.row];
    [cell.TitleLabelOutlet setFont:[UIFont fontWithName:font.fontName size:19.0]];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        ProduktAuflistungTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProduktbezeichnungTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];

        
    } else if (indexPath.row == 1)
    {
        AnwendungTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AnwendungTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 2)
    {
        brancheTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"brancheTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }  else if (indexPath.row == 3)
    {
        KrankheitserregerTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VirusTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 4)
    {
        WirksamkeitenTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WirksamkeitenTableViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 5)
    {
        BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
        controller.showBarcodeReaderFrom= DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == 6) {
        MerklistenTabbarController *controller = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
        controller.showMerklistenTabBarControllerFrom = DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


#pragma mark - Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end