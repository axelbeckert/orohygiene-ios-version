//
//  ProductToSavedMerklisteTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 06.05.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProductToSavedMerklisteTableViewController.h"
#import "Merkliste+ccm.h"

@interface ProductToSavedMerklisteTableViewController ()
@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,strong) Merkliste *merkliste;

@end

@implementation ProductToSavedMerklisteTableViewController

#pragma mark - NSFechtedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSSortDescriptor *sortByName =
    [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];
    
    fetch.sortDescriptors = @[ sortByName ];
    
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:@"bezeichnung != %@",
                              @"basisMerkliste"];
    
    fetch.predicate = predicate;
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Optik
    if(self.showProductToSavedMerklisteFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
    } else if(self.showProductToSavedMerklisteFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
    }
    
    self.title =@"Merkliste wählen:";
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;
    

    
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Produkt auf vorhandene Merklisten speichern"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    self.tabBarController.title = @"Merklisten";
    self.tabBarController.navigationItem.rightBarButtonItems = nil;
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated {
    if(self.merkliste){
        if ([self.delegate
             respondsToSelector:@selector(informUserAboutProductSavedToMerkliste:)]) {
            [self.delegate informUserAboutProductSavedToMerkliste:self.merkliste];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    id<NSFetchedResultsSectionInfo> info =
    self.fetchedResultsController.sections[section];
    return [info numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"
                                    forIndexPath:indexPath];
    
    Merkliste *merkliste = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    cell.textLabel.text = merkliste.bezeichnung;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Merkliste *merkliste = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if(self.reiniger){
        [merkliste addReinigerObject:self.reiniger];
    } else if (self.produkt){
        [merkliste addProdukteObject:self.produkt];
    }
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [delegate saveContext];

    [delegate saveMerklistenDataToMerklistenDB];
    
    self.merkliste = merkliste;
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

@end
