//
//  NewsletterWebViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Newsletter;

@interface NewsletterWebViewController : UIViewController
@property (nonatomic,strong) Newsletter* newsletter;
@property (strong, nonatomic) IBOutlet UIWebView *newsletterWebViewOutlet;
- (IBAction)refershAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)forwardAction:(id)sender;
- (IBAction)stopLoadingAction:(id)sender;
@end
