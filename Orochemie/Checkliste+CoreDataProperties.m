//
//  Checkliste+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 20.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Checkliste+CoreDataProperties.h"

@implementation Checkliste (CoreDataProperties)

+ (NSFetchRequest<Checkliste *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
}

@dynamic branchmodel;
@dynamic kurzbezeichnung;
@dynamic name;
@dynamic sortierung;
@dynamic typemodel;
@dynamic uid;
@dynamic url;
@dynamic websitemodel;
@dynamic tstamp;
@dynamic checklistenkategorie;
@dynamic krankheitserreger;

@end
