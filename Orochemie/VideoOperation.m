//
//  VideoOperation.m
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VideoOperation.h"

@implementation VideoOperation

-(instancetype)initWithVideo:(NSString*)video{
    if(self = [super init]){
        self.video = video;
        
        __weak typeof(self) weakSelf = self;
        [self setCompletionBlock:^{
            NSLog(@"Habe fertig: iscanceled:%d ", weakSelf.isCancelled);
        }];
    }
    
    return  self;
}

-(void)main{

    [self downloadVideosWithID:1];
}

-(void)downloadVideosWithID:(int)id{
    
    NSLog(@"Download started");
    
    
//    
//    
//    self.geladeneVideos = self.geladeneVideos + 1;
//    self.videoLabelOutlet.text = [self.videoArray objectAtIndex:id];
//    self.aktuellGeladenesVideo = id;
//    self.videoNochZuLadenLabelOutlet.text = [NSString stringWithFormat:@"%i", self.videoArray.count-self.geladeneVideos];
//    self.videosGeladenLabelOutlet.text = [NSString stringWithFormat:@"%i",self.geladeneVideos];
    
    // Create the request.
    NSString *urlString = [NSString stringWithFormat:@"http://www.tpwebservice.com/Archiv/%@",self.video ];
    NSLog(@"URLString: %@", urlString);
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    self.receivedData = [NSMutableData dataWithCapacity: 0];
    
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSLog(@"theConnection: %@", theConnection);
    if (!theConnection) {
        // Release the receivedData object.
        self.receivedData = nil;
        NSLog(@"Download did fail");
        // Inform the user that the connection failed.
    }
    
    
    //Update Gesamtfortschritt
   // CGFloat progress = self.geladeneVideos / self.videoArray.count;
    
    
//    if(self.videoArray.count > 1){
//        
//        self.gesamtfortschrittProgressView.progress = progress;
//        self.gesamtfortschrittLabelOutlet.text = [NSString stringWithFormat:@"%u%%", (self.geladeneVideos / self.videoArray.count)*100];
//    }
//    
    
    
    
    
    
    
    
    
    
}


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.receivedResponseData = self.receivedResponseData + data.length;
    
    
   // CGFloat progress = (CGFloat)self.receivedResponseData / (CGFloat)self.responseContentLength;
    
//    self.progressViewOutlet.progress = progress;
//    
//    if(self.videoArray.count==1){
//        self.gesamtfortschrittProgressView.progress = progress;
//        self.gesamtfortschrittLabelOutlet.text = [NSString stringWithFormat:@"%0.0f%%",  progress*100];
//    }
    
    [self.receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.responseContentLength =[response expectedContentLength];
    
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.video];
    [self.receivedData writeToFile:filePath atomically:YES];
    
//    [self.activityIndicatorOutlet stopAnimating];
    if(self.geladeneVideos == self.videoArray.count){
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"Error: %@", error.localizedDescription);
}
@end
