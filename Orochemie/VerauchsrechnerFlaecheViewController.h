//
//  VerauchsrechnerFlaecheViewController.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VerbrauchsRechnerStaticTableViewController;

@interface VerauchsrechnerFlaecheViewController : UIViewController
@property(nonatomic,strong) VerbrauchsRechnerStaticTableViewController *verbrauchsRechnerStaticTableViewController;
@end
