//
//  Berater+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 16.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Berater+ccm.h"

@implementation Berater (ccm)
+(Berater*) insertBeraterInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Berater" inManagedObjectContext:managedObjectContext];
}
-(void)deleteBerater
{
    [self.managedObjectContext deleteObject:self];
}
@end
