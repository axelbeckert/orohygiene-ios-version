//
//  MerklistenTableViewHelper.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenTableViewHelper.h"
#import "EmailHelper.h"
#import "Produkte.h"
#import "Produkte+ccm.h"



@implementation MerklistenTableViewHelper


-(void)sendMerklisteByEmail {
    
    
    UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Eingabe erforderlich:"
                                                            andMessage:@"Bitte geben Sie Ihre E-Mail Adresse ein:"
                                  andCancelButtonWithCompletionHandler:nil];
    
    [AlertHelper addTextFieldToUIAlertController:alertView withPlaceHolderText:@"Tragen Sie hier die Emailadresse ein"];
    
    alertView.textFields.firstObject.keyboardType = UIKeyboardTypeEmailAddress;
    [self addOkButtonActionForMerklistenVersendenAlert:alertView];
    if([self.delegate respondsToSelector:@selector(presentAlertController:animated:completion:)]){
        [self.delegate presentAlertController:alertView animated:YES completion:nil];
    }
    
    
}

-(void) addOkButtonActionForMerklistenVersendenAlert:(UIAlertController *) alertController  {
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *firstTextfield = alertController.textFields.firstObject;
                                   if([self checkInternetConnectionBeforeSendingEmail]) [self validateEmail:firstTextfield.text];
                                   
                               }];
    
    [alertController addAction:okAction];
}

-(bool) checkInternetConnectionBeforeSendingEmail {
    
    if([ConnectionCheck hasConnectivity]) {
        return YES;
    } else {
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung"];
        return NO;
    }
    
}

-(void) validateEmail: (NSString*) email {
    
    if([EmailHelper validateEmail:email]){
        [self sendMerklisteToEmail:email];
    } else {
        [EmailHelper notifyUserAboutFailure:EMAIL_VALIDATION_FALSE];
    }
}

-(NSMutableArray *) addAllMerklistenProductsToDesinfektionsmittelArray {
    
    NSMutableArray *alleDesinfektionsArtikel = [NSMutableArray new];
    for(Produkte *produkt in self.desinfektionsArray)
    {
        NSMutableArray *artikel = [NSMutableArray new];
        NSString *bezeichnung = produkt.name;
        [artikel addObject:bezeichnung];
        
        [alleDesinfektionsArtikel addObject:artikel];
    }
    return alleDesinfektionsArtikel;
}

-(NSMutableArray *) addAllMerklistenReinigerToReinigerArray {
    
    NSMutableArray *alleReinigerArtikel = [NSMutableArray new];
    for(Reiniger *produkt in self.reinigerArray)
    {
        NSMutableArray *artikel = [NSMutableArray new];
        NSString *bezeichnung = produkt.bezeichnung;
        [artikel addObject:bezeichnung];
        
        [alleReinigerArtikel addObject:artikel];
    }
    return alleReinigerArtikel;
}


-(NSString*)prepareSendingMerklisteAndReturnJsonStringWithEmail:(NSString*) email{
    
    NSMutableArray *alleDesinfektionsArtikel = [self addAllMerklistenProductsToDesinfektionsmittelArray];
    NSMutableArray *alleReinigerArtikel = [self addAllMerklistenReinigerToReinigerArray];
    
    NSMutableDictionary *bestellungDict = [[NSMutableDictionary alloc]init];
    
    [bestellungDict setObject:  self.merklistenTitel forKey:@"merkliste"];
    [bestellungDict setObject:  email forKey:@"email"];
    [bestellungDict setObject:alleDesinfektionsArtikel forKey:@"desinfektionsartikel"];
    [bestellungDict setObject:alleReinigerArtikel forKey:@"reinigerartikel"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:bestellungDict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

-(void)sendMerklisteToEmail:(NSString*)email{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *post = [NSString stringWithFormat:@"%@", [self prepareSendingMerklisteAndReturnJsonStringWithEmail:email]];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setURL:[NSURL URLWithString:@"http://www.orohygienesystem.de/anfragen/json_aktuelle_merkliste.php"]];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(((NSHTTPURLResponse *)response).statusCode==200){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self openAlertInfoAlertDialogAsyncWithMessage:@"Merkliste erfolgreich gesendet"];
            });
            
        }
    }];
    
    [postDataTask resume];
    
}

-(void) openAlertInfoAlertDialogAsyncWithMessage: (NSString*) message {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showInfoAlertAndCloseItAfterTwoSecondsWithMessage:message];
    });
}

#pragma mark - Merkliste Action Sheet Common Actions


-(void) showInfoAlertAndCloseItAfterTwoSecondsWithMessage: (NSString *)infoMessage{
    
    UIAlertController *saveIsDoneInfoView = [UIAlertController
                                             alertControllerWithTitle:@"Info"
                                             message:infoMessage
                                             preferredStyle:UIAlertControllerStyleAlert];
    
    if([self.delegate respondsToSelector:@selector(presentAlertController:animated:completion:)]){
        [self.delegate presentAlertController:saveIsDoneInfoView animated:YES completion:nil];
    }
   
    [self performSelector:@selector(dismissInfoAlert:)
               withObject:saveIsDoneInfoView
               afterDelay:4];
}

- (void)dismissInfoAlert:(UIAlertController *)alertView {
    [alertView dismissViewControllerAnimated:false completion:nil];
}

+(BOOL) testDatabaseWithDelegate: (AppDelegate*)delegate {
    //Prüfen ob bereits mehr als eine Merkliste vorhanden ist (Basismerkliste ist immer vorhanden)
    //Fetchen der Merklisten
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSArray *merklistenArray = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    
    
    //Wenn mehr als eine Mekrliste vorhanden ist dann bitte den Button dem ActionSheet hinzufügen
    if(merklistenArray.count > 1){
        return YES;
    }
    return false;
}

@end
