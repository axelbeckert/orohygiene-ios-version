//
//  ZustimmungZurAppNutzung.m
//  orochemie 
//
//  Created by Axel Beckert on 08.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "ZustimmungZurAppNutzung.h"

@implementation ZustimmungZurAppNutzung

+(void)einholungZustimmung{
    
    NSLog(@"Zustimmungsprüfung");
    NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
    
    // holen der Zustimmung
    NSString* oroHygieneDefaults = [usingOroHygieneDefaults valueForKey:@"UsingOroHygieneForIndustrialPurposes"];
    
    if([oroHygieneDefaults isEqualToString:@"YES"]){
        return;
        
    } else {
        
        [ZustimmungZurAppNutzung aufforderungZurZustimmung];
        
    }
    
}

+(void) aufforderungZurZustimmung {
    
    NSString *title = @"Zustimmung erforderlich!";
    NSString *message = @"Diese Anwendung ist nur für gewerbliche Nutzung bestimmt. Für falschen Einsatz übernimmt die Firma orochemie GmbH & Co.KG keine Haftung";
    
    UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:title andMessage:message andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        //Setzen der Ablehnung
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setValue:@"NO" forKey:@"UsingOroHygieneForIndustrialPurposes"];
        [pref synchronize];
        [AlertHelper showInfoAlertAndCloseItAfterTwoSecondsWithMessage:@"Die App wird aufgrund der nicht erteilten Zustimmung beendet!" andAction:^{
            exit(0);
        }];
        
        
        
    }];
    
    
    [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Zustimmen" andCompletionHandler:^(UIAlertAction *alertAction) {
        //Setzen der Zustimmung
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setValue:@"YES" forKey:@"UsingOroHygieneForIndustrialPurposes"];
        [pref synchronize];
        return;
    }]];
    
    [AlertHelper showAlert:alert];
}
@end
