//
//  JsonCollector.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "JsonCollector.h"
#import "ReadJsonFile.h"

@interface JsonCollector () < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >

@property(nonatomic,strong) AppDelegate *appDelegate;
@property int downloadTaskIsActive;


@end

@implementation JsonCollector

-(void) collectJsonWithUrl: (NSString*) collectionUrl andTaskDescription:(NSString*)taskDescription{
    NSLog(@"Start Download with Task:%@", taskDescription);
    if([ConnectionCheck hasConnectivity]) {
        if(self.showSVProgressHUD==1)  [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
        
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:collectionUrl]
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval:60.0];
        int r = arc4random() % 100;
        NSString* sessionIdentifier = [NSString stringWithFormat:@"de.orochemie.desinfektion.%@.%i", taskDescription,r];
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:sessionIdentifier];
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionDownloadTask *dataTask = [session downloadTaskWithRequest:theRequest];
        [dataTask setTaskDescription:taskDescription];
        [dataTask resume];
        
    } else {
        if(self.showSVProgressHUD==1) [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
    }
    
}


-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSData *fileData = [NSData dataWithContentsOfURL:location];
    NSError* error;
    if(fileData!=nil){
        NSArray* jsonContainer = [NSJSONSerialization JSONObjectWithData:fileData options:0 error:&error];
        [self checkDownloadTaskDescription:downloadTask.taskDescription andBuildChecklistsWithData:jsonContainer];
    }
    if(error){
        NSLog(@"Error on jsonContainer: %@",error.localizedDescription);
    }
    
    
}
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
  
    
    if(error){
        if(self.showSVProgressHUD==1) {
            [SVProgressHUD dismiss];
            [SVProgressHUD showErrorWithStatus:@"Fehler beim Abholen der Daten!"];
        }
        NSLog(@"Error on session task: %@",error.localizedDescription);
    }
    
}

-(void)checkIfDownloadProcessIsActive{
    int processActive = 0;
    if(self.downloadTaskIsActive==1) processActive = 1;
    
    if(processActive==0 && self.showSVProgressHUD==1)[SVProgressHUD dismiss];
}

-(void) checkDownloadTaskDescription:(NSString*)downloadTaskDescription andBuildChecklistsWithData:(NSArray*)data{
    
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat: @"You must override %@ in a subclass",NSStringFromSelector(_cmd)] userInfo:nil];
}

/**
 * compareLatestTimestampFromDatabaseWithJsonTimestamp
 * Used to collect the latest timestamp from a given entity from database
 * IMPORTANT: The entity must implement the JsonProtocol
 **/
-(BOOL) compareLatestTimestampFromDatabaseWithJsonTimestamp:(NSString*)timestamp andEntitiy:(NSString*)entity{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSSortDescriptor *sortIndex= [[NSSortDescriptor alloc] initWithKey:@"tstamp" ascending:NO selector:@selector(localizedCompare:)];
    
    NSArray *sortArray= [NSArray arrayWithObjects:sortIndex,nil];
    request.sortDescriptors = sortArray;
    
    request.fetchLimit = 1;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(results.count){
        
        id<JsonProtocol> k = [results lastObject];
        
        if([k.tstamp isEqualToString:timestamp]){
            return true;
        }
    }
    
    
    return false;
    
}

/**
 * retrieveObjectsFromDatabaseForEntity
 * Checks if the database not empty for a given entity
 *
 **/
-(BOOL) retrieveObjectsFromDatabaseForEntity{
   
    if(self.appDelegate==nil){
        self.appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:self.entityName];
    
    request.fetchLimit = 1;
    
    NSError *error = nil;
    
    NSArray *results = [self.appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(results.count){
        return true;
    }
    return false;
    
}

/**
 * retrieveDataFromJsonFile
 * Reads the contents of a given json file which is locatetd in the main bundle
 **/
-(NSDictionary*)retrieveDataFromJsonFile:(NSString*)file{
    
    ReadJsonFile* readJsonFile = [ReadJsonFile new];
    return [readJsonFile JSONFromFile:file];
    
}

@end
