//
//  Handlungsanweisung.h
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte;

@interface Handlungsanweisung : NSManagedObject

@property (nonatomic, retain) NSString * anweisung;
@property (nonatomic, retain) Produkte *produkte;

@end
