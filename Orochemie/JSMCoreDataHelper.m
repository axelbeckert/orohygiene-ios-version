//
//  JSMCoreDataHelper.m
//  CoreDataProjekt
//
//  Created by trainer on 31.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSMCoreDataHelper.h"


@implementation JSMCoreDataHelper
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectContext = _managedObjectContext;

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"database.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        

        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

        //abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSString*) directoryForDatabaseFilename
{
     return [NSHomeDirectory() stringByAppendingString:@"/Documents"];
    //return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSString*) databaseFilename
{
    return @"database.sqlite";
}

//+ (NSManagedObjectContext*) managedObjectContext
//{
//    static NSManagedObjectContext *managedObjectContext;
//    
//    if (managedObjectContext != nil) {
//        return managedObjectContext;
//    }
//    
//    NSError *error;
////    [[NSFileManager defaultManager] createDirectoryAtPath:[JSMCoreDataHelper directoryForDatabaseFilename] withIntermediateDirectories:YES attributes:nil error:&error];
////    
////    if (error) {
////        //NSLog(@"Fehler: %@", error.localizedDescription);
////        return nil;
////    }
//    
//    NSString *path = [NSString stringWithFormat:@"%@/%@", [JSMCoreDataHelper directoryForDatabaseFilename],
//                      [JSMCoreDataHelper databaseFilename]];
//    
//    NSURL *url = [NSURL fileURLWithPath:path];
//    NSManagedObjectModel *managedModel = [NSManagedObjectModel mergedModelFromBundles:nil];
//    
//    NSPersistentStoreCoordinator *storeCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
//    
//    if (! [storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:nil error:&error]) {
//        //NSLog(@"Fehler: %@", error.localizedDescription);
//        return nil;
//    }
//    
//    managedObjectContext = [[NSManagedObjectContext alloc] init];
//    managedObjectContext.persistentStoreCoordinator = storeCoordinator;
//    
//    return managedObjectContext;
//}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc]  initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

+ (id) insertManagedObjectOfClass: (Class) aClass inManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    NSManagedObject *managedObject = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass (aClass) inManagedObjectContext:managedObjectContext];
    return managedObject;
}

+ (BOOL) saveManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    NSError *error;
    if (! [managedObjectContext save:&error] ) {
        //NSLog(@"Fehler: %@", error.localizedDescription);
        return NO;        
    }
    return YES;
}

+ (NSArray*) fetchEntitiesForClass: (Class) aClass withPredicate: (NSPredicate*) predicate inManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass(aClass) inManagedObjectContext:managedObjectContext];
    fetchRequest.entity = entityDescription;
    fetchRequest.predicate = predicate;
    
    NSArray *items = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        //NSLog(@"Fehler: %@", error.localizedDescription);
        return nil;        
    }
    return items;
}

+(BOOL) perfomFetchOnFetchedResultsController: (NSFetchedResultsController*) fetchedResultsController
{
    NSError *error;
    if (! [fetchedResultsController performFetch:&error] ) {
        //NSLog(@"Fehler: %@", error.localizedDescription);
        return NO;        
    }
    return YES;
}

@end
