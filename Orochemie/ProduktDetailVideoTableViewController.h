//
//  ProduktDetailVideoTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProduktDetailVideoTableViewController : UITableViewController
@property(nonatomic,strong) Reiniger *reiniger;
@end
