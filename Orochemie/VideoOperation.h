//
//  VideoOperation.h
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoOperation : NSOperation <NSURLConnectionDataDelegate>
@property long responseContentLength;
@property long receivedResponseData;
@property (nonatomic,strong) NSArray *videoArray;
@property int aktuellGeladenesVideo;
@property int geladeneVideos;
@property (nonatomic,strong) NSMutableData *receivedData;
@property (nonatomic,strong) NSString *video;

-(instancetype)initWithVideo:(NSString*)video;
@end
