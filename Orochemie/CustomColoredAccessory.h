//
//  CustomColoredAccessory.h
//  Orochemie
//
//  Created by Axel Beckert on 28.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomColoredAccessory : UIControl

{
	UIColor *_accessoryColor;
	UIColor *_highlightedColor;
}

@property (nonatomic, retain) UIColor *accessoryColor;
@property (nonatomic, retain) UIColor *highlightedColor;

+ (CustomColoredAccessory *)accessoryWithColor:(UIColor *)color;

@end