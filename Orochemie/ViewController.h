//
//  ViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *buttonNewsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *buttonHygienetippsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *buttonProdukteOutlet;
@property (weak, nonatomic) IBOutlet UIButton *buttonDosierrechnerOutlet;
@property (weak, nonatomic) IBOutlet UIButton *buttonKrankheitserregerOutlet;
@property (weak, nonatomic) IBOutlet UIButton *buttonUnternehmenOutlet;


@end
