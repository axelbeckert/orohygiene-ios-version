//
//  ProduktempfehlungTableViewControllerDatasourceandDelegate.h
//  Orochemie
//
//  Created by Axel Beckert on 22.04.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProduktempfehlungTableViewControllerDatasourceandDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray *produktEmpfehlungArray;
@end
