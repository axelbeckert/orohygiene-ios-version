//
//  MerklistenReiniger+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenReiniger.h"

@interface MerklistenReiniger (ccm)
+(MerklistenReiniger*)insertMerklistenProduktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteMerklistenReiniger;
@end
