//
//  InfoScreenViewController.m
//  orochemie
//
//  Created by Axel Beckert on 13.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "InfoScreenViewController.h"

@interface InfoScreenViewController ()
- (IBAction)removeFromView:(id)sender;
@property (nonatomic,strong) UIWindow* alertWindow;
@end

@implementation InfoScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)viewWillDisappear:(BOOL)animated {
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)removeFromView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showInfoScreenOnFirstStartUp {
    // precaution to insure window gets destroyed
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [[UIViewController alloc] init];
    
    // we inherit the main window's tintColor
    self.alertWindow.tintColor = [UIApplication sharedApplication].delegate.window.tintColor;
    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    self.alertWindow.windowLevel = topWindow.windowLevel + 1;
    
    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:self animated:YES completion:nil];
}

@end
