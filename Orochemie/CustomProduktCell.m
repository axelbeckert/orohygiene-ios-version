//
//  CustomProduktCell.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "CustomProduktCell.h"

@implementation CustomProduktCell




- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //NSLog(@"%s",__PRETTY_FUNCTION__);
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    UIView *container = [UIView new];
    container.backgroundColor = [UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 73, 69)]; //zur Darstellung des Produktbildes
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 2, 230, 20)]; //Produktbezeichnung
    UILabel *descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 24, 230, 20)]; //Produktzusatz
    UILabel *subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 46, 230, 20)]; //Produktuntertitel
    
    imageView.image = [UIImage imageNamed:@"a20.jpg"];
    
    titleLabel.font = [UIFont boldSystemFontOfSize:17];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"Title-Text";
    titleLabel.backgroundColor = [UIColor clearColor];
    
    descriptionLabel.font = [UIFont boldSystemFontOfSize:15];
    descriptionLabel.textColor = [UIColor whiteColor];
    descriptionLabel.backgroundColor = [UIColor clearColor];
    descriptionLabel.text = @"Description Label Text";
    
    subTitleLabel.font = [UIFont italicSystemFontOfSize:14];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.text = @"Subtitlelabel Text";
    
    [self addSubview:imageView];
    [self addSubview:titleLabel];
    [self addSubview:descriptionLabel];
    [self addSubview:subTitleLabel];
    
    
    //NSLog(@"titleLabel: %@", self.titleLabel);

    

    
}

@end
