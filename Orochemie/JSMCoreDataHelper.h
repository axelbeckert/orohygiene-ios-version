//
//  JSMCoreDataHelper.h
//  AutoScoutTest
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface JSMCoreDataHelper : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
+ (NSString*) directoryForDatabaseFilename;
+ (NSString*) databaseFilename;


-(NSManagedObjectContext*) managedObjectContext;

+(id) insertManagedObjectOfClass: (Class) aClass inManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;

+(BOOL) saveManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchEntitiesForClass: (Class) aClass withPredicate: (NSPredicate*) predicate inManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;

+(BOOL) perfomFetchOnFetchedResultsController: (NSFetchedResultsController*) fetchedResultsController;


@end
