//
//  SavedMerkliste+ccm.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "SavedMerkliste+ccm.h"

@implementation SavedMerkliste (ccm)
+(SavedMerkliste*)insertSavedMerklisteInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"SavedMerkliste" inManagedObjectContext:managedObjectContext];
}
-(void)deleteSavedMerkliste
{
    [self.managedObjectContext deleteObject:self];
}
@end
