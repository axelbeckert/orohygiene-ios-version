//
//  SavedMerklistenTableViewController+Private.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "MerklistenTableViewHelper.h"
#import "AlertHelper.h"

@interface SavedMerklistenDetailsTableViewController () <MerklistenTableViewHelperDelegate>
@property (nonatomic,strong) NSMutableArray *reinigerArray;
@property (nonatomic,strong) NSMutableArray *desinfektionsArray;
@property (nonatomic,strong) NSMutableDictionary *sections;
@property (nonatomic,strong) NSMutableArray *sectionTitles;
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (nonatomic,strong) MerklistenTableViewHelper *merklistenTableViewHelper;
@end
