//
//  Kontakt.m
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Kontakt.h"


@implementation Kontakt

@dynamic ansprechpartner;
@dynamic telefon;
@dynamic email;
@dynamic nachricht;

@end
