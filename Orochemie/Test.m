//
//  Test.m
//  orochemie
//
//  Created by Axel Beckert on 01.10.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import "Test.h"

@implementation Test
-(NSDictionary *)asHygieneTippDictionary
{
    return @{@"hygieneTippId" : self.hygieneTippId,  @"hygieneTippText1" : self.hygieneTippText1, @"Image" : self.image, @"hygieneTippUrl" : self.hygieneTippUrl};
}
@end
