//
//  SicherheitsanweisungStaticTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 13.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "SicherheitsanweisungStaticTableViewController.h"
#import "PdfViewViewController.h"
#import "Produkte.h"

@interface SicherheitsanweisungStaticTableViewController ()
@property (nonatomic,strong) Produkte *produkt;
@end

@implementation SicherheitsanweisungStaticTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [oroThemeManager  customizeTableView:self.tableView];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"sicherheitPdfSegue"]){
        
        //NSLog(@"sicherheitPdfSegue abgefeuert");
        
        PdfViewViewController *controller = segue.destinationViewController;
        controller.produkt = self.produkt;
        controller.senderController=cSicherheitSenderContoller;
    }
}


@end
