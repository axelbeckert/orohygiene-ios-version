//
//  NewsletterModel.m
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "NewsletterModel.h"
#import "Newsletter.h"

@interface NewsletterModel ()
@property (nonatomic, strong) NSMutableArray* newsletter;

@end

@implementation NewsletterModel

-(NSMutableArray *)newsletter
{
    if (!_newsletter) _newsletter = [[NSMutableArray alloc] init];
    return _newsletter;
}



-(void) addNewsletter: (Newsletter*) newsletter
{
    [self.newsletter addObject:newsletter];
}

-(NSInteger) numberOfNewsletter
{
     return  [self.newsletter count];
   
}

-(Newsletter*) newsletterAtIndexPath: (NSIndexPath*) indexPath
{
    return [self.newsletter objectAtIndex:indexPath.row];
}

-(NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len
{
    return [self.newsletter countByEnumeratingWithState:state objects:buffer count:len];
}


@end
