//
//  Pictogramm+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pictogramm+CoreDataProperties.h"

@implementation Pictogramm (CoreDataProperties)

@dynamic bild;
@dynamic produkt;
@dynamic reiniger;

@end
