//
//  ProdukteNachBezeichnungTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProdukteNachBezeichnungTableViewController.h"
#import "ProduktDetailTabbarViewController.h"
#import "ProduktuebersichtTableViewCell.h"
#import "Anwendung+ccm.h"



@interface ProdukteNachBezeichnungTableViewController () <UISearchBarDelegate, UISearchDisplayDelegate>
@property(nonatomic, strong)
    NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,strong) UISearchBar *searchBar;
@end

@implementation ProdukteNachBezeichnungTableViewController
int test;
#pragma mark - NSFechtedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
  if (_fetchedResultsController)
    return _fetchedResultsController;

  NSFetchRequest *fetch = [NSFetchRequest
      fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];
  NSSortDescriptor *sortByName =
      [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];
  NSSortDescriptor *sortByIndex =
    [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];

  fetch.sortDescriptors = @[ sortByIndex,sortByName ];

  _fetchedResultsController = [[NSFetchedResultsController alloc]
      initWithFetchRequest:fetch
      managedObjectContext:self.managedObjectContext
        sectionNameKeyPath:@"index"
                 cacheName:nil];

  NSError *error;

  [_fetchedResultsController performFetch:&error];

  if (error) {
    //NSLog(@"Fehler: %@", error);
    abort();
  }

  return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];

  self.title = @"nach Bezeichnung";

    [oroThemeManager customizeReinigerTableView:self.tableView];
    

  AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

  self.managedObjectContext = appDelegate.managedObjectContext;

  self.searchBar =
      [[UISearchBar alloc] initWithFrame:CGRectZero];
    [self.searchBar sizeToFit];
    self.searchBar.delegate = self;
    self.searchBar.translucent = YES;
    self.searchBar.backgroundImage = [UIImage new];
    self.searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    self.searchBar.placeholder = @"Suche";

    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];

    [searchBarView addSubview:self.searchBar];
    
    self.tableView.tableHeaderView = searchBarView;
    
   
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];


  if (self.pHWert) {
    
    //Predicate aufbauen
    NSPredicate *predicate = [NSPredicate
        predicateWithFormat:@"ANY phwert_relation.bezeichnung ==[c] %@",
                            self.pHWert.bezeichnung];
    //NSLog(@"pH-Wert");
    self.fetchedResultsController.fetchRequest.predicate = predicate;

    [self.fetchedResultsController performFetch:nil];
      
    //Titel setzen
      self.title = self.pHWert.bezeichnung;
      
      id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
       
      [tracker set:kGAIScreenName value:@"Reinigungsmittel-Information"];
      [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"PH-Wert"
                                                            action:@"Show"
                                                             label:self.pHWert.bezeichnung                                                        value:nil] build]];
  
  } else if (self.oberflaeche) {
    
    //Predicate aufbauen
    NSPredicate *predicate = [NSPredicate
        predicateWithFormat:@"ANY oberflaeche.bezeichnung ==[c] %@",
                            self.oberflaeche.bezeichnung];
    
    self.fetchedResultsController.fetchRequest.predicate = predicate;
     
    [self.fetchedResultsController performFetch:nil];
    
    //Titel setzen
    self.title = self.oberflaeche.bezeichnung;
      
      id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
       
      [tracker set:kGAIScreenName value:@"Reinigungsmittel-Information"];
      [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Oberflaeche"
                                                            action:@"Show"
                                                             label:self.oberflaeche.bezeichnung                                                        value:nil] build]];


  }  else if (self.reinigungsverfahren) {
      
    //Predicate aufbauen
    NSPredicate *predicate = [NSPredicate
        predicateWithFormat:@"ANY reinigungsverfahren.bezeichnung ==[c] %@",
                            self.reinigungsverfahren.bezeichnung];
    self.fetchedResultsController.fetchRequest.predicate = predicate;

    [self.fetchedResultsController performFetch:nil];
      
    //Titel setzen
    self.title = self.reinigungsverfahren.bezeichnung;
      
      id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
       
      [tracker set:kGAIScreenName value:@"Reinigungsmittel-Information"];
      [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Reinigungsverfahren"
                                                            action:@"Show"
                                                             label:self.reinigungsverfahren.bezeichnung                                                        value:nil] build]];
    
  } else if (self.anwendung) {

    //Predicate aufbauen
     NSPredicate *predicate = [NSPredicate
                                predicateWithFormat:@"ANY anwendung.name ==[c] %@",
                                self.anwendung.name];
      
     self.fetchedResultsController.fetchRequest.predicate = predicate;
      
     [self.fetchedResultsController performFetch:nil];
      
      
      
    //Titel setzen
      self.title = self.anwendung.name;
  }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Produkte-Nach-Bezeichnung"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    //NSLog(@"gefetchete Objekte: %i",[_fetchedResultsController fetchedObjects].count);
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [self.searchBar sizeToFit];
}


#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif  
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  id<NSFetchedResultsSectionInfo> info =
      self.fetchedResultsController.sections[section];
  return [info numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    ProduktuebersichtTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"ProduktuebersichtTableViewCell" forIndexPath:indexPath];
    Reiniger *reiniger =
      [self.fetchedResultsController objectAtIndexPath:indexPath];

    [oroThemeManager customizeTableCellText:cell];
    
    
    cell.titleLabelOutlet.text = [reiniger.bezeichnung substringToIndex:11];
      cell.descriptionLabelOutlet.text = [reiniger.bezeichnung substringFromIndex:11];
      cell.subtitleLabelOutlet.text = reiniger.zusatz;
      cell.imageViewOutlet.image = [UIImage
          imageNamed:[NSString stringWithFormat:@"%@.jpg", reiniger.image]];

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  return 80;
}
#pragma mark - tableView indexTitle
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}
#pragma mark -tableView header

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}


#pragma mark - Segue Methoden

// In a storyboard-based application, you will often want to do a little
// preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"produktDetailSegue"]) {
    ProduktDetailTabbarViewController *controller =
        segue.destinationViewController;
    controller.reiniger = [self.fetchedResultsController
        objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    //NSLog(@"produktDetailSegue");
  }
}

#pragma mark - SerachBar Delegate

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {

  //NSLog(@"SearchBar: %@", searchText);
  if (!searchText.length) {
    self.fetchedResultsController.fetchRequest.predicate = nil;
  } else {

    NSPredicate *predicate = [NSPredicate
        predicateWithFormat:@"SELF.bezeichnung CONTAINS[c] %@", searchText];
    self.fetchedResultsController.fetchRequest.predicate = predicate;
  }

  [self.fetchedResultsController performFetch:nil];
  [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
  [self.view endEditing:YES];
}



#pragma mark Button Actions
-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}




@end
