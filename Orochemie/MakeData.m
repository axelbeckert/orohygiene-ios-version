//
//  MakeData.m
//  Orochemie
//
//  Created by Axel Beckert on 13.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "MakeData.h"
#import "Produkte+ccm.h"
#import "Video+ccm.h"
#import "Produktvarianten+ccm.h"
#import "Branche+ccm.h"
#import "Anwendung+ccm.h"
#import "Krankheitserreger.h"
#import "Gefahrstoffsymbole+ccm.h"
#import "Produktdosierung+ccm.h"
#import "Produkteinwirkzeit+ccm.h"
#import "Checkliste.h"
#import "Checklistenkategorie.h"
#import "Pictogramm+ccm.h"

@interface MakeData ()


-(void)makeProducts;
@end

@implementation MakeData
-(void)makeData
{
    [self makeProducts];
    [self readCSVFileForGettingBarcodes];
//    [self readCSVFileForGettingMusterplaene];
    

}


#pragma mark - Erstellung Produkte
-(void)makeProducts
{
#pragma mark - Produkte

    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    Produkte *a20=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    a20.name = @"A 20 Instrumentendesinfektion";
    a20.nameHauptbezeichnung=@"A 20";
    a20.nameZusatz=@"Instrumentendesinfektion";
    a20.zusatz=@"Vielseitig & schnell wirksam";
    a20.bild =@"a20_gruppe.png";
    a20.einsatzbereich =@"Konzentrat zur Reinigung und Desinfektion allgemeiner und chirurgischer Instrumente";
    a20.listung=@"VAH-gelistet, IHO-Viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624,EN 14348, EN 14476, EN 14561, EN 14562,EN 14563";
    a20.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tb-Bakterien), Pilze und alle Viren (viruzid gegen behüllte und unbehüllte Viren, z. B. Polio-, Noro-, Adeno-, Vaccinia-, HBV, HCV, HIV, Polyoma-Viren SV40)";
    a20.dosierung=@"Nach VAH: 2%ige Lösung - 5 Min.  (2 Min. im Ultraschallgerät)\n\nSiehe dazu auch Produktinformation";
    a20.pflichttext=@"CE 0124";
    a20.informationPdf = @"a20";
    a20.index=@"A";
    a20.sortierreihenfolge=@(20);
    a20.uid = @"B363353F-E0DA-4204-8CEE-9D76099DD58E";
    

    
    Produkte *b5=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b5.name = @"B 5 Wischdesinfektion";
    b5.nameHauptbezeichnung=@"B 5";
    b5.nameZusatz=@"Wischdesinfektion";
    b5.zusatz=@"Bei Ausbruchsgeschehen & Clostridium difficile";
    b5.bild =@"b5_gruppe.png";
    b5.einsatzbereich=@"Konzentrat zur Reinigung und Desinfektion abwaschbarer Flächen und Gegenstände";
    b5.listung=@"VAH-gelistet. RKI-gelistet Wirkungsbereich A und B. ÖGHMP-Expertisenverzeichnis. Geprüft gemäß EN 14348, EN 13702. IHO-Viruzidie-Liste.";
    b5.wirksamkeit=@"Umfassend wirksam gegen Bakterien (inkl. MRSA und Tb-Bakterien), Hefepilze, Sporen (inkl. Clostridium difficile) und alle Viren (viruzid gegen behüllte und unbehüllte Viren, z. B. Vaccinia-Viren, inkl. HBV, HCV, Adeno-, Polyoma-, Polio-, Noro- Viren)";
    b5.dosierung=@"Nach VAH:\n0,5%ige Lösung, 1 Std. Einwirkzeit/1%ige Lösung,30 Min. Einwirkzeit/1,5%ige Lösung,15 Min. Einwirkzeit.\n\nNach RKI:\nfür den Seuchenfall: 7%ige Lösung, 4 Std. Einwirkzeit (Wirkungsbereich A – Bakterien und Pilze)/2%ige Lösung, 4 Std. Einwirkzeit (Wirkungsbereich B – Viren). \n\nWirksam gegen Sporen (Clostridium difficile) bei 2%iger Lösung und 30 Min. Einwirkzeit.\n\nSiehe dazu auch Produktinformation";
    b5.pflichttext=@"B 5 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b5.informationPdf=@"b5";
    b5.index=@"B";
    b5.sortierreihenfolge=@(5);
    b5.uid=@"AF6EEFB5-EA0A-406F-B6AE-3813DC43899B";

    
    Produkte *b10=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b10.name = @"B 10 Wischdesinfektion";
    b10.nameHauptbezeichnung=@"B 10";
    b10.nameZusatz=@"Wischdesinfektion";
    b10.zusatz=@"Schaumarm";
    b10.bild =@"b10_gruppe.png";
    b10.einsatzbereich=@"Konzentrat zur Reinigung und Desinfektion abwaschbarer Oberflächen von nicht-invasiven Medizinprodukten";
    b10.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476 und im 4-Felder-Test gemäß VAH und EN 16615.";
    b10.wirksamkeit=@"Wirkt gegen Bakterien (incl. MRSA und Tuberkulose-Bakterien), Pilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren inkl. Vaccinia-, HBV, HCV und HIV sowie unbehüllte Viren wie Adeno- und Noro-Viren)";
    b10.dosierung=@"Nach VAH: 2 %ige Lösung, 5 Min. Einwirkzeit; 30 Min. Einwirkzeit gegen Noro-Viren\n\nSiehe dazu auch Produktinformation";
    b10.pflichttext=@"CE 0124";
    b10.informationPdf=@"b10";
    b10.index=@"B";
    b10.sortierreihenfolge=@(10);
    b10.uid=@"88997826-BBA2-42B5-81F4-74CA518C653E";
    
    Produkte *b15=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b15.name = @"B 15 Wischdesinfektion";
    b15.nameHauptbezeichnung=@"B 15";
    b15.nameZusatz=@"Wischdesinfektion";
    b15.zusatz=@"Hochwirksam gegen alle Viren";
    b15.bild =@"b15_gruppe.png";
    b15.einsatzbereich=@"Konzentrat zur Reinigung und Desinfektion der Oberflächen von nicht-invasiven Medizinprodukten sowie von Flächen und Gegenständen";
    b15.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. IHO-Desinfektionsmittelliste für die Lebensmittelherstellung. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476, EN 14563, EN 1276, EN 1650, EN 13697.";
    b15.wirksamkeit=@"Wirkt gegen Bakterien (incl. MRSA und Tb-Bakterien), Pilze und alle Viren (viruzid gegen behüllte und unbehüllte Viren, z. B. Vaccinia-, HBV, HCV, HIV, Adeno-, Noro-, Polio- und Polyoma SV 40-Viren)";
    b15.dosierung=@"Nach VAH: 1%ige Lösung, 15 Minuten Einwirkzeit/2%ige Lösung, 5 Minuten/3%ige Lösung, 2 Minuten Einwirkzeit.  Wirkt gegen alle Viren in 5 Min. bei 2%iger Lösung.\n\nNach EN 1276/1650/13697 (Lebensmittelbereich):\n5 Min / 0,5% hohe und niedrige Belastung 20°C\nSiehe dazu auch Produktinformation";
    b15.pflichttext=@"CE 0124. B 15 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b15.informationPdf=@"b15";
    b15.index=@"B";
    b15.sortierreihenfolge=@(15);
    b15.uid=@"2FF3ADCA-9D1C-49BD-9BAE-1646B6AB14FF";
    
    Produkte *b20=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b20.name = @"B 20 Wischdesinfektion";
    b20.nameHauptbezeichnung=@"B 20";
    b20.nameZusatz=@"Wischdesinfektion";
    b20.zusatz=@"Universell & kostensparend";
    b20.bild =@"b20_gruppe.png";
    b20.einsatzbereich=@"Konzentrat zur Reinigung und Desinfektion der Oberflächen von nicht-invasiven Medizinprodukten sowie von Flächen und Gegenständen";
    b20.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624, EN 14476.";
    b20.wirksamkeit=@"Wirkt gegen Bakterien (incl. MRSA), Hefepilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren, inkl. Vaccinia-, HBV, HCV, HIV und unbehüllte Viren wie Noro-Viren)";
    b20.dosierung=@"Nach VAH: 1 %ige Lösung, 15 Minuten Einwirkzeit/ 2 %ige Lösung, 5 Minuten Einwirkzeit\n\nSiehe dazu auch Produktinformation";
    b20.pflichttext=@"CE 0124. B 20 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b20.informationPdf=@"b20";
    b20.index=@"B";
    b20.sortierreihenfolge=@(20);
    b20.uid=@"7D87F7D8-C4B4-4DBE-BBB4-A145DE876ABE";
    
//    Produkte *b25=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
//
//    
//    b25.name = @"B 25 Flächendesinfektion";
//    b25.nameHauptbezeichnung=@"B 25";
//    b25.nameZusatz=@"Flächendesinfektion";
//    b25.zusatz=@"Für den Lebensmittelbereich";
//    b25.bild =@"img_des_b25.png";
//    b25.einsatzbereich=@"Konzentrat zur Desinfektion und Reinigung abwaschbarer Flächen und Gegenstände";
//    b25.listung=@"DVG-gelistet";
//    b25.wirksamkeit=@"Wirkt gegen Bakterien (inkl. Salmonellen und Tb-Bakterien), Pilze, HBV und HIV";
//    b25.dosierung=@"Nach DVG: 1 %ige Lösung, 30 Minuten Einwirkzeit (2%ige Lösung in belasteten Bereichen bei 10°C)\n\nSiehe dazu auch Produktinformation";
//    b25.pflichttext=@"B 25 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
//    b25.informationPdf=@"b25";
//    b25.index=@"B";
//    b25.sortierreihenfolge=@(25);
//    b25.uid=@"3DA7456C-8EE7-4208-A487-0EA2FA2BA84F";
    
//    Produkte *b30=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
//
//
//    b30.name = @"B 30 Schnelldesinfektion";
//    b30.nameHauptbezeichnung=@"B 30";
//    b30.nameZusatz=@"Schnelldesinfektion";
//    b30.zusatz=@"Gebrauchsfertig & hochwirksam gegen alle Viren";
//    b30.bild =@"img_des_b30.png";
//    b30.einsatzbereich=@"Gebrauchslösung zur Wisch- oder Sprühdesinfektion kleiner Oberflächen von nicht-invasiven Medizinprodukten und anderer Flächen, insbesondere auch an schwer zugänglichen Stellen";
//    b30.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476 und im 4-Felder-Test gemäß VAH und EN 16615.";
//    b30.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tb-Bakterien), Pilze und alle Viren (viruzid gegen behüllte und unbehüllte Viren, z. B. Polio-, Polyoma- SV40, Noro-, Adeno-, SARS-Corona-, Vaccinia-, HBV, HCV, HIV)";
//    b30.dosierung=@"Gebrauchsfertig;  1 Min.nach VAH, gegen Noroviren in 1 Min., gegen alle Viren in 30 Sek.-1 Std.\n\nSiehe dazu auch Produktinformation";
//    b30.pflichttext=@"CE 0124. B 30 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
//    b30.informationPdf=@"b30";
//    b30.index=@"B";
//    b30.sortierreihenfolge=@(30);
//    b30.uid=@"1BE0FBB0-AB42-453F-A639-12E91EEAEE14";
    
    
    Produkte *b33=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    
    b33.name = @"B 33 Schnelldesinfektion";
    b33.nameHauptbezeichnung=@"B 33";
    b33.nameZusatz=@"Schnelldesinfektion";
    b33.zusatz=@"Gebrauchsfertig & hochwirksam gegen alle Viren";
    b33.bild =@"b33_gruppe.png";
    b33.einsatzbereich=@"Gebrauchslösung für die Wischdesinfektion alkoholbeständiger Oberflächen von nicht invasiven und invasiven - inkl. semikritischen - Medizinprodukten und medizinischem Inventar sowie für die Sprühdesinfektion schwer zugänglicher Stellen";
    b33.listung=@"B 33 - VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 13727,EN 13624, EN 14348, EN 14476 sowie im 4-Felder-Test gemäß VAH und EN 16615.";
    b33.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Pilze und alle Viren (viruzid gemäß DVV/RKI-Leitlinie und EN 14476 gegen alle behüllten und unbehüllten Viren, z. B. Polio-, Polyoma- SV40, Noro-, Adeno-, Vaccinia-, Hepatitis-B-, Hepatitis-C-Viren, HIV).";
    b33.dosierung=@"Gebrauchsfertig, Einwirkzeit gemäß VAH 1 Min., gegen alle Viren  1 Min.\n\nSiehe dazu auch Produktinformation.";
    b33.pflichttext=@"CE 0124. B 33 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformation lesen";
    b33.informationPdf=@"b33";
    b33.index=@"B";
    b33.sortierreihenfolge=@(35);
    b33.uid=@"55D24C3B-98C3-4DF0-A4C5-06762A68BE71";
    

    
    Produkte *b40=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b40.name = @"B 40 Schnelldesinfektion";
    b40.nameHauptbezeichnung=@"B 40";
    b40.nameZusatz=@"Schnelldesinfektion";
    b40.zusatz=@"Gebrauchsfertig & rückstandsfrei";
    b40.bild =@"b40_gruppe.png";
    b40.einsatzbereich=@"Gebrauchslösung zur Wisch- oder Sprühdesinfektion kleiner Oberflächen von nicht-invasiven Medizinprodukten und anderer Flächen, insbesondere auch an schwer zugänglichen Stellen; parfüm- und farbstofffrei, trocknet ohne jegliche Rückstände auf";
    b40.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. IHO-Desinfektionsmittelliste für die Lebensmittelherstellung. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476,EN 1276, EN 1650, EN 13697.";
    b40.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tb-Bakterien), Pilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren inkl. Vaccinia-, HBV, HCV und HIV sowie unbehüllte Viren wie Adeno-, Noro- und Polyoma-Viren SV 40).";
    b40.dosierung=@"Gebrauchsfertig. Einwirkzeit nach VAH 1 Min., im Lebensmittelbereich 5 Min. (Nach EN 1276/EN 1650/EN 13697), gegen Noroviren nur 1 Min.\n\nSiehe dazu auch Produktinformation";
    b40.pflichttext=@"CE 0124. B 40 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b40.informationPdf=@"b40";
    b40.index=@"B";
    b40.sortierreihenfolge=@(40);
    b40.uid=@"44D74C53-728C-4ED6-9FA2-B5F796132EAC";
    
    Produkte *b45=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b45.name = @"B 45 Schnelldesinfektion";
    b45.nameHauptbezeichnung=@"B 45";
    b45.nameZusatz=@"Schnelldesinfektion";
    b45.zusatz=@"Gebrauchsfertig & für empfindliche Flächen";
    b45.bild =@"b45_gruppe.png";
    b45.einsatzbereich=@"Gebrauchslösung zur Wisch- oder Sprühdesinfektion kleiner Oberflächen von nicht-invasiven Medizinprodukten und anderer Flächen, insbesondere auch an schwer zugänglichen Stellen; für empfindliche Oberflächen (Acrylglas, Kunstleder etc.). Auch geeignet für Displays und Bedienfelder empfindlicher Geräte (Mobiltelefone, Tablets, Monitore etc.). Herstellerangaben jeweils beachten.";
    b45.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 14348, EN 14476 und im 4-Felder-Test gemäß VAH und EN 16615";
    b45.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Hefepilze und viele Viren (begrenzt viruzid gegen behüllte Viren, z. B. Vaccinia-Viren, BVDV, inkl. HBV, HCV, HIV)";
    b45.dosierung=@"Gebrauchsfertig, Einwirkzeit gemäß VAH 1 Min., gegen Noro-Viren 5 Min.\n\nSiehe dazu auch Produktinformation";
    b45.pflichttext=@"CE 0124. B 45 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b45.informationPdf=@"b45";
    b45.index=@"B";
    b45.sortierreihenfolge=@(45);
    b45.uid=@"C02304AD-2194-404A-AEB3-83A2497E5E34";
    
    Produkte *b15t=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    b15t.name = @"B 15 Desinfektionstücher";
    b15t.nameHauptbezeichnung=@"B 15";
    b15t.nameZusatz=@"Desinfektionstücher";
    b15t.zusatz=@"Schnell wirksam & ohne Alkohol";
    b15t.bild =@"b15_flowpack.png";
    b15t.einsatzbereich=@"Gebrauchsfertige Feuchttücher zur schnellen Reinigung und Wischdesinfektion von kleinen Flächen, auch von Oberflächen nicht-invasiver Medizinprodukte; beste Materialverträglichkeit bei alkoholempfindlichen Flächen wie Acrylglas.";
    b15t.listung=@"B 15 ist VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. IHO-Desinfektionsmittelliste für die Lebensmittelherstellung. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476, EN 14563, EN 1276, EN 1650, EN 13697.";
    b15t.wirksamkeit=@"Wirken gegen Bakterien (inkl. MRSA und Tb- Bakterien), Pilze und alle Viren (viruzid gegen behüllte und unbehüllte Viren, z. B. Vaccinia-Viren, BVDV, inkl. HBV, HCV, HIV, Adeno-, Polyoma- SV 40, Noro-, Polio-Viren)";
    b15t.dosierung=@"Gebrauchsfertig, Einwirkzeit nach VAH nur 2 Min., im Lebensmittelbereich 5 Min (nach EN1276/EN 1650/EN 13697). Wirken gegen alle Viren in 5 Min. \n\nSiehe dazu auch Produktinformation";
    b15t.pflichttext=@"CE 0124. B 15  vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b15t.informationPdf=@"b15_dt";
    b15t.index=@"B";
    b15t.sortierreihenfolge=@(50);
    b15t.uid=@"8AB7EC05-360A-4394-BC72-EE1A22C6067C";
    
    Produkte *b33t=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    
    b33t.name = @"B 33 Desinfektionstücher";
    b33t.nameHauptbezeichnung=@"B 33";
    b33t.nameZusatz=@"Desinfektionstücher";
    b33t.zusatz=@"Schnell & hochwirksam";
    b33t.bild =@"b33_dt_gruppe.png";
    b33t.einsatzbereich=@"Gebrauchsfertige, parfümfreie, alkoholische Feuchttücher zur schnellen Wischdesinfektion alkoholbeständiger Oberflächen von nicht invasiven und invasiven - inkl. semikritischen - Medizinprodukten und medizinischem Inventar.";
    b33t.listung=@"B 33 - VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 13727,EN 13624, EN 14348, EN 14476 sowie im 4-Felder-Test gemäß VAH und EN 16615.";
    b33t.wirksamkeit=@"Wirken gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Pilze und alle Viren (viruzid gemäß DVV/RKI-Leitlinie und EN 14476 gegen alle behüllten und unbehüllten Viren, z. B. Polio-, Polyoma- SV40, Noro-, Adeno-, Vaccinia-, Hepatitis-B-, Hepatitis-C-Viren, HIV).";
    b33t.dosierung=@"Gebrauchsfertig, Einwirkzeit gemäß VAH 1 Min., gegen alle Viren  1 Min.\n\nSiehe dazu auch Produktinformation.";
    b33t.pflichttext=@"CE 0124. B 33 Desinfektionstücher vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformation lesen";
    b33t.informationPdf=@"b33_dt";
    b33t.index=@"B";
    b33t.sortierreihenfolge=@(55);
    b33t.uid=@"B7E7B27F-7225-4A70-8BF5-089C9B437CF6";
    
    Produkte *b40t=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    
    b40t.name = @"B 40 Desinfektionstücher";
    b40t.nameHauptbezeichnung=@"B 40";
    b40t.nameZusatz=@"Desinfektionstücher";
    b40t.zusatz=@"rückstandsfrei";
    b40t.bild =@"b40_dt_gruppe.png";
    b40t.einsatzbereich=@"Gebrauchsfertige aldehydfreie Desinfektionstücher zur Schnelldesinfektion alkoholbeständiger Oberflächen von nicht invasiven Medizinprodukten und anderen Flächen. Parfüm- und farbstofffrei. Trocknet ohne jegliche Rückstände auf.";
    b40t.listung=@"B 40 ist VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Desinfektionsmittelliste für die Lebensmittelherstellung. Geprüft gemäß EN 13727, EN 13624, EN 13697, EN 14348, EN 1276, EN 1650, EN 14476 sowie im 4-Felder-Test gemäß VAH und EN 16615.";
    b40t.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Pilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren inkl. Hepatitis-B-, Hepatitis-C-Viren und HIV sowie unbehüllte Viren wie Adeno-, Noro- und Polyoma-Viren SV 40).";
    b40t.dosierung=@"Gebrauchsfertig. Einwirkzeit gemäß VAH 1 Min., im Lebensmittelbereich 5 Min (nach EN 1276/EN 1650/EN 13697), gegen Noroviren 1 Min.\n\nSiehe dazu auch Produktinformation.";
    b40t.pflichttext=@"CE 0124. B 40 Desinfektionstücher vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformation lesen.";
    b40t.informationPdf=@"b40_dt";
    b40t.index=@"B";
    b40t.sortierreihenfolge=@(56);
    b40t.uid=@"74A488DE-4B19-47F9-AF21-AEBC29C221A6";
    

    Produkte *b45t=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    b45t.name = @"B 45 Desinfektionstücher";
    b45t.nameHauptbezeichnung=@"B 45";
    b45t.nameZusatz=@"Desinfektionstücher";
    b45t.zusatz=@"für empfindliche Flächen";
    b45t.bild =@"b45_dt_gruppe.png";
    b45t.einsatzbereich=@"Gebrauchsfertige aldehydfreie Desinfektionstücher zur Schnelldesinfektion empfindlicher Oberflächen von nicht invasiven Medizinprodukten und anderen empfindlichen Flächen. Parfüm- und farbstofffrei. Auch geeignet für Displays und Bedienfelder empfindlicher Geräte (Mobiltelefone, Tablets, Monitore etc.). Herstellerangaben jeweils beachten.";
    b45t.listung=@"B 45 ist VAH-gelistet. ÖGHMP-Expertisenverzeichnis. Geprüft gemäß EN 14348, EN 14476 und im 4-Felder-Test gemäß VAH und EN 16615.";
    b45t.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Hefepilze und viele Viren (begrenzt viruzid gegen behüllte Viren inkl. Hepatitis-B-, Hepatitis- C-Viren, HIV, und Noro-Viren)";
    b45t.dosierung=@"Gebrauchsfertig: Einwirkzeit gemäß VAH nur 1 Min., gegen Noro-Viren 5 Min.\n\nSiehe dazu auch Produktinformation.";
    b45t.pflichttext=@"CE 0124. B 45 Desinfektionstücher vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformation lesen.";
    b45t.informationPdf=@"b45_dt";
    b45t.index=@"B";
    b45t.sortierreihenfolge=@(57);
    b45t.uid=@"72A60BE3-EA76-4CB6-BEA6-5A51C2059DD3";
    
    Produkte *b60=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    b60.name = @"B 60 Desinfektionstücher";
    b60.nameHauptbezeichnung=@"B 60";
    b60.nameZusatz=@"Desinfektionstücher";
    b60.zusatz=@"Praktisch für kleinere Flächen";
    b60.bild =@"img_des_b60.png";
    b60.einsatzbereich=@"Gebrauchsfertige alkoholhaltige Tücher zur schnellen Reinigung und Wischdesinfektion von kleinen Flächen, auch von Oberflächen nicht invasiver Medizinprodukte";
    b60.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14476.";
    b60.wirksamkeit=@"Wirken gegen Bakterien (incl. MRSA und Tb-Bakterien), Pilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren inkl. Vaccinia-, HBV, HCV und HIV sowie unbehüllte Viren wie Adeno-, Noro- und Polyoma-Viren SV 40)";
    b60.dosierung=@"Gebrauchsfertig, Einwirkzeit gemäß VAH sowie für Noroviren nur 1 Min.\n\nSiehe dazu auch Produktinformation";
    b60.pflichttext=@"CE 0124. B 60 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    b60.informationPdf=@"b60";
    b60.index=@"B";
    b60.sortierreihenfolge=@(60);
    b60.uid=@"4232F71D-280C-4699-95E0-C04ECCE6B527";
    
    Produkte *oroVliesTuecher=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    oroVliesTuecher.name = @"oro Vliestücher";
    oroVliesTuecher.nameHauptbezeichnung=@"oro ";
    oroVliesTuecher.nameZusatz=@"Vliestücher";
    oroVliesTuecher.zusatz=@"Zum Selbsttränken";
    oroVliesTuecher.bild =@"img_des_oro_vliestuecher.png";
    oroVliesTuecher.einsatzbereich=@"Trockentücher in wieder befüllbarer Spenderbox zum Selbsttränken mit flüssigen orochemie-Desinfektionsmitteln für die Oberflächen von Medizinprodukten und anderen Oberflächen (s. Produktinformation)";
    oroVliesTuecher.listung=@"-";
    oroVliesTuecher.wirksamkeit=@"-";
    oroVliesTuecher.dosierung=@"-";
    oroVliesTuecher.pflichttext=@"CE \nSiehe Produktinformation bzgl. Tränkung und hygienischer Aufbereitung.";
    oroVliesTuecher.informationPdf=@"oro_vliestuecher";
    oroVliesTuecher.index=@"B";
    oroVliesTuecher.sortierreihenfolge=@(70);
    oroVliesTuecher.uid=@"4DE47458-CB69-43DC-9424-F16B185AF5D6";
    
    Produkte *hd410=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    hd410.name = @"HD 410 Händedesinfektion";
    hd410.nameHauptbezeichnung=@"HD 410";
    hd410.nameZusatz=@"Händedesinfektion";
    hd410.zusatz=@"Hautpflegend & rückfettend";
    hd410.bild =@"img_des_hd410.png";
    hd410.einsatzbereich=@"Für die hygienische und chirurgische Händedesinfektion";
    hd410.listung=@"VAH-gelistet. RKI-gelistet (Wirkungsbereich A). ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 1500, EN 12791, EN 13727, EN 13624, EN 14348, EN 14476";
    hd410.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA), Hefepilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren wie Vaccinia-Viren, BVDV, inkl. HBV, HCV, HIV, Herpes simplex- und SARS-Corona- Viren sowie unbehüllte Viren wie Rota-, Adeno-, Polyoma- SV 40 und Noro-Viren)";
    hd410.dosierung=@"Gebrauchsfertig, 15 Sek. Einwirkzeit bei der hyg. Händedesinfektion, 1,5 Min. bei der chirurg. Händedesinfektion.\n\nSiehe dazu auch Produktinformation";
    hd410.pflichttext=@"HD 410 - Zusammensetzung: 100 g Lösung enthalten als Wirkstoffe 2-Propanol 47 g und 1-Propanol 26 g. Sonstige Bestandteile: Poly(oxyethylen)-6-glycerol(mono, di)alkanoat(C8 - C10), Farbstoff E 131, Geruchsstoffe, gereinigtes Wasser. Anwendungsgebiete: Hygienische und chirurgische Händedesinfektion. Gegenanzeigen: HD 410 ist nicht geeignet für die Desinfektion von Schleimhäuten und zur Anwendung auf der verletzten Haut. Bei Über-empfindlichkeit gegenüber einem der Inhaltsstoffe darf HD 410 nicht angewendet werden. Nebenwirkungen: Ins-besondere bei häufiger Anwendung kann es zu Hautirritationen wie z. B. Hauttrockenheit kommen. Hinweise: Bei Raumtemperatur lagern. Vor Wärme, Licht und Feuchtigkeit schützen. Arzneimittel für Kinder unzugänglich aufbewahren. Das Arzneimittel soll nach Ablauf des Verfalldatums nicht mehr angewendet werden. Pharmazeutischer Unternehmer und Hersteller: orochemie GmbH + Co. KG, Max-Planck-Straße 27, 70806 Kornwestheim";
    hd410.informationPdf=@"hd410";
    hd410.index=@"C";
    hd410.sortierreihenfolge=@(10);
    hd410.uid=@"A39F27E4-9059-4EE3-A2ED-6863EFE5A3EA";
    
    Produkte *c20=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c20.name = @"C 20 Hände + Haut Desinfektion";
    c20.nameHauptbezeichnung=@"C 20";
    c20.nameZusatz=@"Hände + Haut Desinfektion";
    c20.zusatz=@"Parfüm- & farbstofffrei";
    c20.bild =@"img_des_c20.png";
    c20.einsatzbereich=@"Für die hygienische und chirurgische Händedesinfektion, zur Hautdesinfektion vor Injektionen, Punktionen und  Operationen";
    c20.listung=@"VAH- und RKI-gelistet (Wirkungsbereich A). ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gem. EN 12054, EN 1500, EN 12791";
    c20.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tb-Bakterien), Hefepilze und viele Viren (begrenzt viruzid PLUS inkl.Vaccinia-, HBV, HCV, HIV und SARS-Corona-Viren sowie unbehüllte Viren wie Adeno-, Rota- und Noroviren)";
    c20.dosierung=@"Gebrauchsfertig,  30 Sek. Einwirkzeit (auch bei Noro-Viren) bei der hyg. Händedesinfektion, 5 Min. bei der chirurg. Händedesinfektion\n\nSiehe dazu auch Produktinformation";
    c20.pflichttext=@"C 20 - 2-Propanol 70 % (V/V) - Zusammensetzung: Arzneilich wirksame Bestandteile: 100 g Lösung enthalten 2-Propanol 63,1 g. Sonstige Bestandteile: Gereinigtes Wasser. Anwendungsgebiete: Hygienische und chirurgische Händedesinfektion, Hautdesinfektion vor einfachen Injektionen und Punktionen peripherer Gefäße, Hautdesinfektion vor Operationen und vor Punktionen von Gelenken, Desinfektion talgdrüsenreicher Haut, Kühlumschläge. Gegenanzeigen: C 20 ist nicht zur Desinfektion offener Wunden geeignet. Bei Überempfindlichkeit gegenüber einem der Inhaltsstoffe darf C 20 nicht angewendet werden. Nebenwirkungen: Bei Einreibungen der Haut mit C 20 können Rötungen und leichtes Brennen auftreten. Hinweise: Dicht verschlossen lagern. Arzneimittel für Kinder unzugänglich aufbewahren. Das Arzneimittel soll nach Ablauf des Verfalldatums nicht mehr angewendet werden. Pharmazeutischer Unternehmer und Hersteller: orochemie GmbH + Co. KG, Max-Planck-Straße 27, 70806 Kornwestheim.\n";
    c20.informationPdf=@"c20";
    c20.index=@"C";
    c20.sortierreihenfolge=@(20);
    c20.uid=@"B8218F11-C506-4F85-8147-C4F945A01F49";
 
    Produkte *c25=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c25.name = @"C 25 Händedesinfektionsgel";
    c25.nameHauptbezeichnung=@"C 25";
    c25.nameZusatz=@"Händedesinfektionsgel";
    c25.zusatz=@"Tropffrei im Spender & praktisch für unterwegs";
    c25.bild =@"c25_gruppe.png";
    c25.einsatzbereich=@"Für die hygienische Händedesinfektion";
    c25.listung=@"VAH-Liste. IHO-Viruzidie-Liste. Geprüft gem. EN 12054, EN 1500, EN 12791";
    c25.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tuberkulose-Bakterien), Hefepilze und viele Viren (begrenzt viruzid PLUS inkl.Vaccinia-, HBV, HCV, HIV und SARS-Corona-Viren sowie unbehüllte Viren wie Adeno-, Rota- und Noroviren)";
    c25.dosierung=@"Gebrauchsfertig,  30 Sek. Einwirkzeit (auch bei Noro-Viren) bei der hyg. Händedesinfektion";
    c25.pflichttext=@"C 25 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformation lesen.";
    c25.informationPdf=@"c25";
    c25.index=@"C";
    c25.sortierreihenfolge=@(25);
    c25.uid=@"B69318C0-6370-46E4-9C41-2AB2E21A9604";
    
    Produkte *chirosyn = [Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];
    
    chirosyn.name = @"Chirosyn Händedesinfektion";
    chirosyn.nameHauptbezeichnung = @"Chirosyn";
    chirosyn.nameZusatz =  @"Händedesinfektion";
    chirosyn.zusatz = @"Viruzid & rückfettend";
    chirosyn.bild = @"chirosyn_gruppe.png";
    chirosyn.einsatzbereich = @"Für die hygienische und chirurgische Händedesinfektion. Geeignet für behördlich angeordnete Desinfektionen gem. § 18 IfSG bei Ausbrüchen.";
    chirosyn.listung = @"VAH- und RKI-gelistet (Wirkungsbereich A + B). ÖGHMP-Expertisenverzeichnis. IHO Viruzidie-Liste. Geprüft gemäß EN 1500, EN 12791, EN 13727, EN 13624, EN 14348, EN 14476 sowie DVV/RKI-Leitlinie.";
    chirosyn.wirksamkeit = @"Wirkt gegen Bakterien (inkl. Tuberkulose-Bakterien), Hefepilze und alle Viren (viruzid, d.h. gegen behüllte Viren inkl. Hepatitis-B-, Hepatitis-C-Viren und HIV sowie unbehüllte Viren wie Adeno-Viren, Polyoma-Viren SV 40, Noro-Viren und Polio-Viren).";
    chirosyn.dosierung = @"Gebrauchsfertig, Einwirkzeit für die hygienische Händedesinfektion 30 Sek., für die chirurgische Händedesinfektion sowie Virusinaktivierung 1,5 Min. Siehe dazu auch Produktinformation.";
    chirosyn.pflichttext = @"Chirosyn Händedesinfektion - Zusammensetzung: 100 g Lösung enthalten als Wirkstoffe 57,6 g Ethanol 96% (v/v) und 10 g 1-Propanol. Sonstige Bestandteile: Gereinigtes Wasser, Propylenglykol, Butan-1,3-diol, Glycerol, 2-Butanon, Phosphorsäure 85 %, Lanolin-poly(oxyethylen)-75, Parfum fresh. Anwendungsgebiete: Hygienische und chirurgische Händedesinfektion. Gegenanzeigen: Chirosyn Händedesinfektion darf nicht auf den Schleimhäuten und offenen Wunden angewendet werden. Bei Überempfindlichkeit gegenüber einem der Inhaltsstoffe darf Chirosyn Händedesinfektion nicht angewendet werden. Nebenwirkungen: Insbesondere bei mehrfacher Anwendung kann es zu Irritationserscheinungen der Haut (z. B. Austrocknung, Schuppung, Rötung, Spannung, Juckreiz) und bei hochfrequenter Anwendung auch zu weiter gehenden Hautreizungen mit oberflächlichen Defekten kommen. Das Ausmaß und der Schweregrad dieser Erscheinungen hängen direkt mit der Häufigkeit der Anwendung und der Durchführung angemessener Hautpflegemaßnahmen zusammen. Bei dem ersten Auftreten von Irritationserscheinungen sind die Hautpflegemaßnahmen zu intensivieren. Informieren Sie Ihren Arzt oder Apotheker, wenn Sie Nebenwirkungen bemerken, die nicht in dieser Gebrauchsinformation aufgeführt sind. Hinweise: Bewahren Sie dieses Arzneimittel für Kinder unzugänglich auf. Sie dürfen dieses Arzneimittel nach dem auf dem Behältnis angegebenen Verfalldatum nicht mehr verwenden. Das Verfalldatum bezieht sich auf den letzten Tag des Monats. Pharmazeutischer Unternehmer und Hersteller: orochemie GmbH + Co. KG, Max-Planck-Straße 27, 70806 Kornwestheim.";
    chirosyn.informationPdf=@"chirosyn";
    chirosyn.index = @"C";
    chirosyn.sortierreihenfolge = @(28);
    chirosyn.uid=@"5309FB45-FA57-4A2C-9941-2B7E2371ED67";
    
    Produkte *c30=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c30.name = @"C 30 Hygienische Waschlotion";
    c30.nameHauptbezeichnung=@"C 30";
    c30.nameZusatz=@"Hygienische Waschlotion";
    c30.zusatz=@"Auch zur Körperwaschung bei MRSA";
    c30.bild =@"c30_gruppe.png";
    c30.einsatzbereich=@"Hautmilde Waschlotion für die desinfizierende hygienische Händewaschung; auch zur Ganzkörperwaschung (inkl. Kopfhaar) bei Besiedlung mit MRSA bzw. MRE";
    c30.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. Geprüft gemäß EN 1499, EN 14476";
    c30.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA), Hefepilze und viele Viren (begrenzt viruzid gegen behüllte Viren wie Vaccinia- Viren, BVDV, inkl. HBV, HCV, HIV)";
    c30.dosierung=@"Gebrauchsfertig, Einwirkzeit: 30 Sek.\n\nSiehe dazu auch Produktinformation";
    c30.pflichttext=@"C 30 vorsichtig verwenden. Vor Gebrauch stets Etikett und Produktinformationen lesen.";
    c30.informationPdf=@"c30";
    c30.index=@"C";
    c30.sortierreihenfolge=@(30);
    c30.uid=@"AEA18DEE-2D7F-4753-9B46-3117CE9AB3E6";
    
    Produkte *c45=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c45.name = @"C 45 Waschlotion";
    c45.nameHauptbezeichnung=@"C 45";
    c45.nameZusatz=@"Waschlotion";
    c45.zusatz=@"Ergiebig  & hautschonend";
    c45.bild =@"c45_gruppe.png";
    c45.einsatzbereich=@"Milde Waschlotion zum häufigen Waschen von Haut und Händen, zum Duschen und Baden";
    c45.listung=@"-";
    c45.wirksamkeit=@"-";
    c45.dosierung=@"Ergiebig im Verbrauch: 500 ml reichen für mehr als 250 Anwendungen\n\nSiehe dazu auch Produktinformation";
    c45.pflichttext=@"-";
    c45.informationPdf=@"c45";
    c45.index=@"C";
    c45.sortierreihenfolge=@(45);
    c45.uid=@"714D8603-257D-4612-AF7B-A3A9F97800B0";
    
    Produkte *c50=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c50.name = @"C 50 Pflegelotion";
    c50.nameHauptbezeichnung=@"C 50";
    c50.nameZusatz=@"Pflegelotion";
    c50.zusatz=@"Zieht schnell ein & fettet nicht";
    c50.bild =@"c50_gruppe.png";
    c50.einsatzbereich=@"Pflegt die beanspruchte und empfindliche Haut, schützt und beugt Entzündungen vor";
    c50.listung=@"-";
    c50.wirksamkeit=@"-";
    c50.dosierung=@"Äußerst sparsam: 500 ml für bis zu 1.000 Anwendungen\n\nSiehe dazu auch Produktinformation";
    c50.pflichttext=@"-";
    c50.informationPdf=@"c50";
    c50.index=@"C";
    c50.sortierreihenfolge=@(50);
    c50.uid=@"6D1CD345-9E49-43B4-B12F-158028A0DB1E";
    
    Produkte *c60=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    c60.name = @"C 60 Hautschutzschaum";
    c60.nameHauptbezeichnung=@"C 60";
    c60.nameZusatz=@"Hautschutzschaum";
    c60.zusatz=@"Lang anhaltender Schutz";
    c60.bild =@"img_des_c60.png";
    c60.einsatzbereich=@"Schützt stark beanspruchte Haut und Hände; verhindert das Eindringen von Schad- und Reizstoffen; schützt beim Tragen von Einmalhandschuhen.";
    c60.listung=@"-";
    c60.wirksamkeit=@"-";
    c60.dosierung=@"Äußerst sparsam: 200 ml reichen für 400 Anwendungen\n\nSiehe dazu auch Produktinformation";
    c60.pflichttext=@"-";
    c60.informationPdf=@"c60";
    c60.index=@"C";
    c60.sortierreihenfolge=@(60);
    c60.uid=@"68C2A3FB-01CC-45A6-90B9-5F5769C3F296";
    
    Produkte *d10=[Produkte insertProduktInManagedObjectContext:delegate.managedObjectContext];

    
    d10.name = @"D 10 Absauggerätedesinfektion";
    d10.nameHauptbezeichnung=@"D 10";
    d10.nameZusatz=@"Absauggerätedesinfektion";
    d10.zusatz=@"Auch für Inhaliergeräte";
    d10.bild =@"d10_gruppe.png";
    d10.einsatzbereich=@"Konzentrat zur Reinigung, Desinfektion und Pflege von Absauggeräten und Inhaliergeräten.";
    d10.listung=@"VAH-gelistet. ÖGHMP-Expertisenverzeichnis. IHO-Viruzidie-Liste. Geprüft gemäß EN 13727, EN 13624, EN 14348, EN 14561,EN 14562, EN 14563, EN 14476 und im 4-Felder-Test gemäß VAH und EN 16615.";
    d10.wirksamkeit=@"Wirkt gegen Bakterien (inkl. MRSA und Tb-Bakterien), Pilze und viele Viren (begrenzt viruzid PLUS gegen behüllte Viren inkl.Vaccinia-, HBV, HCV und HIV sowie unbehüllte Viren wie Adeno- und Noro-Viren)";
    d10.dosierung=@"2 %ige Lösung, 1 Stunde Einwirkzeit\n\nSiehe dazu auch Produktinformation";
    d10.pflichttext=@"CE 0124";
    d10.informationPdf=@"d10";
    d10.index=@"D";
    d10.sortierreihenfolge=@(10);
    d10.uid=@"B4D43FF7-8897-4CED-9EFF-CF1C61AE0D72";
    
  
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Video Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - DesinfektionsVideo
    //Erstellung
    
    Video *kittelflasche=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    kittelflasche.datei = @"orochemie_kitteltaschenflasche";
    kittelflasche.bezeichnung=@"Die Händedesinfektion mit der Kitteltaschenflasche";
    kittelflasche.kategorie=@"H";
    kittelflasche.sortierung=@(60);
    kittelflasche.filesize=@"4378443";
    kittelflasche.youtube=@"https://www.youtube.com/watch?v=3Y8DL_1r7T4";

    
    Video *absaugGeraeteDesinfektion=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    absaugGeraeteDesinfektion.datei = @"orochemie_absauggeraetedesinfektion";
    absaugGeraeteDesinfektion.bezeichnung=@"Absauggerätedesinfektion";
    absaugGeraeteDesinfektion.kategorie=@"S";
    absaugGeraeteDesinfektion.sortierung=@(100);
    absaugGeraeteDesinfektion.filesize=@"5669361";
    absaugGeraeteDesinfektion.youtube=@"https://www.youtube.com/watch?v=CR7T9kafSws";

    Video *alkoholischeSchnellDesinfektion=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    alkoholischeSchnellDesinfektion.datei = @"orochemie_alkoholische_schnelldesinfektion";
    alkoholischeSchnellDesinfektion.bezeichnung=@"Alkoholische Schnelldesinfektion";
    alkoholischeSchnellDesinfektion.kategorie=@"F";
    alkoholischeSchnellDesinfektion.sortierung=@(30);
    alkoholischeSchnellDesinfektion.filesize=@"6356897";
    alkoholischeSchnellDesinfektion.youtube=@"https://www.youtube.com/watch?v=qUUKPs8KD18";
    
    Video *flaechenDesinfektion=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    flaechenDesinfektion.datei = @"orochemie_flaechendesinfektion";
    flaechenDesinfektion.bezeichnung=@"Flächendesinfektion";
    flaechenDesinfektion.kategorie=@"F";
    flaechenDesinfektion.sortierung=@(40);
    flaechenDesinfektion.filesize=@"11090307";
    flaechenDesinfektion.youtube = @"https://www.youtube.com/watch?v=FVm0F6kzKis";
    
    Video *haendeRichtigDesinfizieren=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    haendeRichtigDesinfizieren.datei = @"orochemie_haende_richtig_desinfizieren";
    haendeRichtigDesinfizieren.bezeichnung=@"Hände richtig desinfizieren";
    haendeRichtigDesinfizieren.kategorie=@"H";
    haendeRichtigDesinfizieren.sortierung=@(50);
    haendeRichtigDesinfizieren.filesize=@"8315347";
    haendeRichtigDesinfizieren.youtube=@"https://www.youtube.com/watch?v=AJFnDFNh71I";
    
    Video *haendeRichtigWaschen=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    haendeRichtigWaschen.datei = @"orochemie_haende_richtig_waschen";
    haendeRichtigWaschen.bezeichnung=@"Hände richtig waschen";
    haendeRichtigWaschen.kategorie=@"H";
    haendeRichtigWaschen.sortierung=@(70);
    haendeRichtigWaschen.filesize=@"4418529";
    haendeRichtigWaschen.youtube=@"https://www.youtube.com/watch?v=1uyCo656J68";
    
    Video *haendePflegeUndHautschutz=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    haendePflegeUndHautschutz.datei = @"orochemie_haendepflege_und_hautschutz";
    haendePflegeUndHautschutz.bezeichnung=@"Händepflege und Hautschutz";
    haendePflegeUndHautschutz.kategorie=@"H";
    haendePflegeUndHautschutz.sortierung=@(80);
    haendePflegeUndHautschutz.filesize=@"3844104";
    haendePflegeUndHautschutz.youtube=@"https://www.youtube.com/watch?v=HJqYSy-4TYk";
    
    Video *inhalierGeraeteDesinfektion=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    inhalierGeraeteDesinfektion.datei = @"orochemie_inhaliergeraetedesinfektion";
    inhalierGeraeteDesinfektion.bezeichnung=@"Inhaliergerätedesinfektion";
    inhalierGeraeteDesinfektion.kategorie=@"S";
    inhalierGeraeteDesinfektion.sortierung=@(110);
    inhalierGeraeteDesinfektion.filesize=@"4560954";
    inhalierGeraeteDesinfektion.youtube=@"https://www.youtube.com/watch?v=eVwmKaAe6vM";

    Video *instrumentenDesinfektion=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    instrumentenDesinfektion.datei = @"orochemie_instrumentendesinfektion";
    instrumentenDesinfektion.bezeichnung=@"Instrumentendesinfektion";
    instrumentenDesinfektion.kategorie=@"I";
    instrumentenDesinfektion.sortierung=@(90);
    instrumentenDesinfektion.filesize=@"5785856";
    instrumentenDesinfektion.youtube=@"https://www.youtube.com/watch?v=O2ga4PN39ps";
    
    Video *materialVertraeglichkeit=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    materialVertraeglichkeit.datei = @"orochemie_materialvertraeglichkeit";
    materialVertraeglichkeit.bezeichnung=@"Besonderheiten und Materialverträglichkeit von Desinfektionswirkstoffen";
    materialVertraeglichkeit.kategorie=@"D";
    materialVertraeglichkeit.sortierung=@(10);
    materialVertraeglichkeit.filesize=@"9113729";
    materialVertraeglichkeit.youtube=@"https://www.youtube.com/watch?v=DXYhxDAgyD0";
    
    Video *richtigDosierenWirksamDesinfizieren=[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    richtigDosierenWirksamDesinfizieren.datei = @"orochemie_richtig_dosieren_wirksam_desinfizieren";
    richtigDosierenWirksamDesinfizieren.bezeichnung=@"Richtig dosieren – wirksam desinfizieren";
    richtigDosierenWirksamDesinfizieren.kategorie = @"D";
    richtigDosierenWirksamDesinfizieren.sortierung=@(20);
    richtigDosierenWirksamDesinfizieren.filesize=@"8129311";
    richtigDosierenWirksamDesinfizieren.youtube=@"https://www.youtube.com/watch?v=NIyGLi5ZTiw";
    
    
    Video *orochemieImageFilm =[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];    orochemieImageFilm.datei=@"orochemie_desinfektionsmittel_unternehmensfilm_v5_mobile";
    orochemieImageFilm.bezeichnung=@"orochemie Unternehmensfilm";
    orochemieImageFilm.kategorie=@"U";
    orochemieImageFilm.sortierung=@(120);
    orochemieImageFilm.filesize=@"10645993"; //9508900
    orochemieImageFilm.youtube=@"https://www.youtube.com/watch?v=u_yv3o1xTRQ";
    
    Video *hygienetage =[Video insertVideoInManagedObjectContext:delegate.managedObjectContext];    hygienetage.datei=@"orochemie_hygienetag_v2_mobile";
    hygienetage.bezeichnung=@"orochemie Hygienetage";
    hygienetage.kategorie=@"U";
    hygienetage.sortierung=@(130);
    hygienetage.filesize=@"5411819";
    hygienetage.youtube=@"https://www.youtube.com/watch?v=Et6S_iI1KV0";
    
    Video *reinigungsWagenVideo = [Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    reinigungsWagenVideo.bezeichnung =
    @"Der Reinigungswagen im Gesundheitswesen";
    reinigungsWagenVideo.datei=@"reiniger_reinigungswagen_v3_270p";
    reinigungsWagenVideo.kategorie=@"R";
    reinigungsWagenVideo.sortierung=@(160);
    reinigungsWagenVideo.filesize=@"13849356";
    reinigungsWagenVideo.youtube=@"https://www.youtube.com/watch?v=XFOUey5eJsI";
    
    Video *spezialReiniger = [Video insertVideoInManagedObjectContext:delegate.managedObjectContext];
    spezialReiniger.bezeichnung =
    @"orochemie Reiniger - verträglich mit desinfizierten Flächen";
    spezialReiniger.datei=@"orochemie_hygienereiniger_schonreiniger_v3_mobil";
    spezialReiniger.kategorie=@"R";
    spezialReiniger.sortierung=@(159);
    spezialReiniger.filesize=@"4863958";
    spezialReiniger.youtube=@"https://www.youtube.com/watch?v=RN8uCca1e2c";
    
    //Zuordnung
    NSSet *richtigDosierenSet = [NSSet setWithObjects:a20,b5,b10,b15,b20,d10, nil];
    [richtigDosierenWirksamDesinfizieren addProdukte:richtigDosierenSet];

    NSSet *haendeRichtigDesinfizierenSet = [NSSet setWithObjects:c20,c25,chirosyn,hd410, nil];
    [haendeRichtigDesinfizieren addProdukte:haendeRichtigDesinfizierenSet];
    
    NSSet *haendeRichtigWaschenSet = [NSSet setWithObjects:c45, nil];
    [haendeRichtigWaschen addProdukte:haendeRichtigWaschenSet];
    
    NSSet *haendePflegeUndHautschutzSet = [NSSet setWithObjects:c50,c60, nil];
    [haendePflegeUndHautschutz addProdukte:haendePflegeUndHautschutzSet];
    
    NSSet *haendeDesinfektionMitKittelflasche = [NSSet setWithObjects:c20,chirosyn,hd410, nil];
    [kittelflasche addProdukte:haendeDesinfektionMitKittelflasche];
    
    NSSet *flaechenDesinfektionSet = [NSSet setWithObjects:b5,b10,b15,b20,b15t,oroVliesTuecher, nil];
    [flaechenDesinfektion addProdukte:flaechenDesinfektionSet];
    
    NSSet *alkoholischeSchnelldesinfektionSet =[NSSet setWithObjects:b40,b45, nil];
    [alkoholischeSchnellDesinfektion addProdukte:alkoholischeSchnelldesinfektionSet];
    
    NSSet *absaugGeraeteDesinfektionSet = [NSSet setWithObjects:d10,b15t, nil];
    [absaugGeraeteDesinfektion addProdukte:absaugGeraeteDesinfektionSet];
    
    NSSet *inhalierGeraeteDesinfektionSet = [NSSet setWithObjects:d10,b15t, nil];
    [inhalierGeraeteDesinfektion addProdukte:inhalierGeraeteDesinfektionSet];
    
    NSSet *instrumentenDesinfektionSet =[NSSet setWithObjects:a20, nil];
    [instrumentenDesinfektion addProdukte:instrumentenDesinfektionSet];
    
    NSSet *materialvertraeglichkeitSet = [NSSet setWithObjects:a20,b5,b10,b15,b15t,b20,b40,b45,b60,d10, nil];
    [materialVertraeglichkeit addProdukte:materialvertraeglichkeitSet];
    
    NSSet *reinigungsWagenSet = [NSSet setWithObjects:b15,b15t,b40,c20,chirosyn,hd410,c45, nil];
    [reinigungsWagenVideo addProdukte:reinigungsWagenSet];
    
    


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Branchen Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
 #pragma mark - Branchen   
    Branche *dialyseenrichtungen = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    dialyseenrichtungen.name = @"Dialyseenrichtungen";
    dialyseenrichtungen.index = @"D";
    dialyseenrichtungen.bild = @"img_produktintro_dialyse.jpg";
    
    
    Branche *rettungsdienst = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    rettungsdienst.name = @"Rettungsdienst";
    rettungsdienst.index = @"R";
    rettungsdienst.bild =@"img_produktintro_rettung.jpg";
    
    Branche *feuerwehr = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    feuerwehr.name = @"Feuerwehr";
    feuerwehr.index = @"F";
    feuerwehr.bild =@"img_produktintro_rettung.jpg";
    
    
    //Podologie/Fußpflege
    Branche *podologie = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    podologie.name=@"Podologie";
    podologie.index=@"P";
    podologie.bild=@"img_produktintro_fusspflege.jpg";
    
    
    Branche *fusspflege = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    fusspflege.name=@"Fußpflege";
    fusspflege.index=@"F";
    fusspflege.bild=@"img_produktintro_fusspflege.jpg";
    
    
    //Bestattungsunternehmen
    Branche *bestattungsunternehmen = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    bestattungsunternehmen.name=@"Bestattungsunternehmen";
    bestattungsunternehmen.index=@"B";
    bestattungsunternehmen.bild=@"img_produktintro_bestattung.jpg";
    
    Branche *lebensmittel = [Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    lebensmittel.name=@"Lebensmittel";
    lebensmittel.index=@"L";
    lebensmittel.bild=@"img_produktintro_food.jpg";
    
    Branche *pflege=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];    pflege.name=@"Pflege";
    pflege.bild=@"branche_pflege.jpg";
    pflege.index=@"P";
    
    
    Branche *humanmedizin=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    humanmedizin.name=@"Humanmedizin";
    humanmedizin.bild=@"branche_humanmed.jpg";
    humanmedizin.index=@"H";
    
    Branche *gebaeudereinigung=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    gebaeudereinigung.name=@"Gebäudereinigung";
    gebaeudereinigung.bild=@"img_produktintro_gebaeudereinigung.jpg";
    gebaeudereinigung.index=@"G";
    
    Branche *waescherei=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    waescherei.name=@"Wäscherei";
    waescherei.bild=@"img_produktintro_waescherei.jpg";
    waescherei.index=@"W";
    
    Branche *kindergarten=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    kindergarten.name=@"Kindergarten";
    kindergarten.bild=@"img_produktintro_schule.jpg";
    kindergarten.index=@"K";
    
    Branche *schule=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    schule.name=@"Schule";
    schule.bild=@"img_produktintro_schule.jpg";
    schule.index=@"S";
    
    
    Branche *friseur=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    friseur.name=@"Friseur";
    friseur.bild=@"img_produktintro_kosmetik.jpg";
    friseur.index=@"F";
    
    Branche *kosmetik=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    kosmetik.name=@"Kosmetiksalons";
    kosmetik.bild=@"img_produktintro_kosmetik.jpg";
    kosmetik.index=@"K";
    
    Branche *wellness=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    wellness.name=@"Wellness";
    wellness.bild=@"img_produktintro_wellness.jpg";
    wellness.index=@"W";
    
    Branche *sport=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    
    sport.name=@"Sport";
    sport.bild=@"img_produktintro_wellness.jpg";
    sport.index=@"S";
    
    Branche *tiermedizin=[Branche insertBrancheInManagedObjectContext:delegate.managedObjectContext];
    tiermedizin.name=@"Tiermedizin";
    tiermedizin.bild=@"img_produktintro_tiermed.jpg";
    tiermedizin.index=@"T";
    
    //Zurodnung
    
   NSSet *pflegeSet = [NSSet setWithObjects:a20,b5,b10,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60,d10,nil];
   [pflege addProdukte:pflegeSet];
 
   NSSet *humanMedizinSet = [NSSet setWithObjects:a20,b5,b10,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60,d10,nil];
   [humanmedizin addProdukte:humanMedizinSet];
    
   NSSet *dialyseSet = [NSSet setWithObjects:a20,b5,b10,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60,d10,nil];
   [dialyseenrichtungen addProdukte:dialyseSet];
   
    NSSet *rettungSet = [NSSet setWithObjects:a20,b5,b10,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60,d10,nil];
    [rettungsdienst addProdukte:rettungSet];
    
    NSSet *feuerwehrSet = [NSSet setWithObjects:a20,b5,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60,d10,nil];

    [feuerwehr addProdukte:feuerwehrSet];
    
    NSSet *podologieSet = [NSSet setWithObjects:a20,b15,b15t,b33,b33t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60, nil];
    [podologie addProdukte:podologieSet];
    [fusspflege addProdukte:podologieSet];
    
    NSSet *bestattungSet = [NSSet setWithObjects:a20,b5,b15,b15t,b20,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60, nil];
    [bestattungsunternehmen addProdukte:bestattungSet];
    
    NSSet *lebensmittelSet = [NSSet setWithObjects:b15,b15t,b20,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,hd410,c30,c45,c50,c60, nil];
    [lebensmittel addProdukte:lebensmittelSet];
    
    NSSet *gebaeudereinigungSet = [NSSet setWithObjects:b5,b15,b15t,b20,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c45,c50,c60, nil];
    [gebaeudereinigung addProdukte:gebaeudereinigungSet];
    
    NSSet *waeschereiSet = [NSSet setWithObjects:b5,b15,b15t,b20,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c30,c45,c50,c60, nil];
    [waescherei addProdukte:waeschereiSet];

    NSSet *kindergartenSet = [NSSet setWithObjects:b5,b15,b15t,b20,b40,b40t,b60,c20,c25,chirosyn,hd410,c30,c45,c50,c60, nil];
    [kindergarten addProdukte:kindergartenSet];
    [schule addProdukte:kindergartenSet];
 
    NSSet *friseurSet = [NSSet setWithObjects:a20,b15,b15t,b20,b40,b40t,b45,b45t,b60,c20,c25,hd410,c45,c50,c60, nil];
    [friseur addProdukte:friseurSet];
    [kosmetik addProdukte:friseurSet]; 
    
    NSSet *wellnessSet = [NSSet setWithObjects:b15,b15t,b20,b40,b40t,b45,b45t,b60,c20,c25,hd410,c45,c50,c60, nil];
    [wellness addProdukte:wellnessSet];
    [sport addProdukte:wellnessSet];   

    NSSet *tiermedizinSet = [NSSet setWithObjects:a20,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher,c20,c25,chirosyn,hd410,c45,c50,c60,d10, nil];
    [tiermedizin addProdukte:tiermedizinSet];

    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Anwendung Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
#pragma mark - Anwendung
    Anwendung *instrumentendesinfektion =[Anwendung insertAnwendungInManagedObjectContext:delegate.managedObjectContext];
    
    instrumentendesinfektion.name=@"Instrumentendesinfektion";
    instrumentendesinfektion.bild=@"instrumentendesinfektion.jpg";
    instrumentendesinfektion.sortierung=@"10";
    
    Anwendung *flaechendesinfektion =[Anwendung insertAnwendungInManagedObjectContext:delegate.managedObjectContext];
    
    flaechendesinfektion.name=@"Flächendesinfektion";
    flaechendesinfektion.bild=@"flaechendesinfektion.jpg";
    flaechendesinfektion.sortierung=@"20";
    
    Anwendung *haendehygiene =[Anwendung insertAnwendungInManagedObjectContext:delegate.managedObjectContext];
    haendehygiene.name=@"Händehygiene";
    haendehygiene.bild=@"haendedesinfektion.jpg";
    haendehygiene.sortierung=@"30";
    
    Anwendung *spezialanwendung =[Anwendung insertAnwendungInManagedObjectContext:delegate.managedObjectContext];
    spezialanwendung.name=@"Spezialanwendung";
    spezialanwendung.bild=@"specialdesinfektion.jpg";
    spezialanwendung.sortierung=@"40";
    
    NSSet *instrumentendesinfektionSet = [NSSet setWithObjects:a20, nil];
    [instrumentendesinfektion addProdukte:instrumentendesinfektionSet];
    
    NSSet *flaechendesinfektionsSet = [NSSet setWithObjects:b5,b10,b15,b15t,b20,b33,b33t,b40,b40t,b45,b45t,b60,oroVliesTuecher, nil];
    [flaechendesinfektion addProdukte:flaechendesinfektionsSet];
    
    NSSet *haendehygieneSet = [NSSet setWithObjects:c20,c25,chirosyn,hd410,c30,c45,c50,c60, nil];
    [haendehygiene addProdukte:haendehygieneSet];
    
    NSSet *spezialanwendungSet = [NSSet setWithObjects:d10, nil];
    [spezialanwendung addProdukte:spezialanwendungSet];
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Gefahrstoffsymbole Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //Erstellung
#pragma mark - Gefahrstoffsymbole    
    
    Gefahrstoffsymbole *ghs_achtung = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:delegate.managedObjectContext];
    
    ghs_achtung.bild=@"ghs_ausrufezeichen.png";
    ghs_achtung.name=@"ghs achtung";
    ghs_achtung.sortierung=@(50);
    
    Gefahrstoffsymbole *ghs_entzuendlich = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:delegate.managedObjectContext];
    
    ghs_entzuendlich.bild=@"ghs_flamme.png";
    ghs_entzuendlich.name=@"ghs entzündlich";
    ghs_entzuendlich.sortierung=@(40);
    
    Gefahrstoffsymbole *ghs_aetzend = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:delegate.managedObjectContext];
    
    ghs_aetzend.bild=@"ghs_aetzend.png";
    ghs_aetzend.name=@"ghs ätzend";
    ghs_aetzend.sortierung=@(20);

    Gefahrstoffsymbole *ghs_gesundheitsgefahr = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:delegate.managedObjectContext];
    
    ghs_gesundheitsgefahr.bild=@"ghs_gesundheitsgefahr.png";
    ghs_gesundheitsgefahr.name=@"ghs gesundheitsgefahr";
    ghs_gesundheitsgefahr.sortierung=@(10);
    
    Gefahrstoffsymbole *ghs_umweltgefahr = [Gefahrstoffsymbole insertGefahrstoffsymboleInManagedObjectContext:delegate.managedObjectContext];
    
    ghs_umweltgefahr.bild=@"ghs_umweltgefahr.png";
    ghs_umweltgefahr.name=@"ghs umweltgefahr";
    ghs_umweltgefahr.sortierung=@(30);
    
    
    //Zuordnung
    
    [a20 addGefahrstoffsymboleObject:ghs_gesundheitsgefahr];
    [a20 addGefahrstoffsymboleObject:ghs_aetzend];
    [a20 addGefahrstoffsymboleObject:ghs_umweltgefahr];
    [a20 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b5 addGefahrstoffsymboleObject:ghs_gesundheitsgefahr];
    [b5 addGefahrstoffsymboleObject:ghs_aetzend];
    [b5 addGefahrstoffsymboleObject:ghs_umweltgefahr];
    [b5 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b10 addGefahrstoffsymboleObject:ghs_aetzend];
    
    [b15 addGefahrstoffsymboleObject:ghs_gesundheitsgefahr];
    [b15 addGefahrstoffsymboleObject:ghs_aetzend];
    [b15 addGefahrstoffsymboleObject:ghs_umweltgefahr];
    [b15 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b20 addGefahrstoffsymboleObject:ghs_aetzend];
    [b20 addGefahrstoffsymboleObject:ghs_umweltgefahr];
    
//    [b30 addGefahrstoffsymboleObject:ghs_entzuendlich];
//    [b30 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b33 addGefahrstoffsymboleObject:ghs_entzuendlich];
    [b33 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b33t addGefahrstoffsymboleObject:ghs_entzuendlich];
    [b33t addGefahrstoffsymboleObject:ghs_achtung];
    
    [b40 addGefahrstoffsymboleObject:ghs_entzuendlich];
    [b40 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b40t addGefahrstoffsymboleObject:ghs_entzuendlich];
    [b40t addGefahrstoffsymboleObject:ghs_achtung];
    
    [b45 addGefahrstoffsymboleObject:ghs_entzuendlich];
    [b45 addGefahrstoffsymboleObject:ghs_achtung];
    
    [b45t addGefahrstoffsymboleObject:ghs_achtung];
    
    [b60 addGefahrstoffsymboleObject:ghs_entzuendlich];
    
    [c20 addGefahrstoffsymboleObject:ghs_entzuendlich];

    [c25 addGefahrstoffsymboleObject:ghs_entzuendlich];
    [c25 addGefahrstoffsymboleObject:ghs_achtung];

    [c30 addGefahrstoffsymboleObject:ghs_achtung];
    
    [hd410 addGefahrstoffsymboleObject:ghs_entzuendlich];
    
    [d10 addGefahrstoffsymboleObject:ghs_aetzend];

    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Dosierungen Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Dosierung    
    //Erstellung
    
    Produktdosierung *dA20 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dA20.hauptbezeichnung = cA20Instrumentendesinfektion;
    dA20.sortierung=@"10";
    
    [a20 addProduktdosierungObject:dA20];
    
    Produktdosierung *dB5_1 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB5_1.hauptbezeichnung = cB5WischdesinfektionVAH;
    dB5_1.zusatzbezeichnung=cB5WischdesinfektionVAHUntertitel;
    dB5_1.sortierung=@"20";
    
    [b5 addProduktdosierungObject:dB5_1];
    
    Produktdosierung *dB5_2 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB5_2.hauptbezeichnung = cB5WischdesinfektionRKI;
    dB5_2.zusatzbezeichnung= cB5WischdesinfektionRKIUntertitel;
    dB5_2.sortierung=@"30";
    
    [b5 addProduktdosierungObject:dB5_2];
    
    Produktdosierung *dB10 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB10.hauptbezeichnung = cB10Wischdesinfektion;
    dB10.sortierung=@"40";
    
    [b10 addProduktdosierungObject:dB10];
    
    Produktdosierung *dB15 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB15.hauptbezeichnung = cB15Wischdesinfektion;
    dB15.zusatzbezeichnung=cB15WischdesinfektionUntertitel;
    dB15.sortierung=@"50";
    
    [b15 addProduktdosierungObject:dB15];
    
    Produktdosierung *dB15Dvg = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB15Dvg.hauptbezeichnung = cB15Wischdesinfektion;
    dB15Dvg.zusatzbezeichnung = @"- gem. EN 1276/1650/13697 (Lebensmittelbereich)";
    dB15Dvg.sortierung=@"51";
    
    [b15 addProduktdosierungObject:dB15Dvg];
    
    Produktdosierung *dB20 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dB20.hauptbezeichnung = cB20Wischdesinfektion;
    dB20.zusatzbezeichnung=cB20WischdesinfektionUntertitel;
    dB20.sortierung=@"60";
    
    [b20 addProduktdosierungObject:dB20];
    
    
    Produktdosierung *dD10 = [Produktdosierung insertProduktdosierungInManagedObjectContext:delegate.managedObjectContext];
    
    dD10.hauptbezeichnung = cD10Absauggeraetedesinfektion;
    dD10.zusatzbezeichnung=cD10AbsauggeraetedesinfektionUntertitel;
    dD10.sortierung=@"80";
    
    [d10 addProduktdosierungObject:dD10];
 
#pragma mark - Einwirkzeit
    
    Produkteinwirkzeit *eA201 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eA201.einwirkzeit = @"5 min / 2,0%";
    eA201.rechengroesse = @0.02;
    eA201.sortierung=@10;
    
    [dA20 addProdukteinwirkzeitObject:eA201];
    
    Produkteinwirkzeit *eA202 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eA202.einwirkzeit = @"240 min / 8,0%";
    eA202.rechengroesse = @0.08;
    eA202.sortierung=@20;
    
    [dA20 addProdukteinwirkzeitObject:eA202];
    
    
    Produkteinwirkzeit *eB5Vah1 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah1.einwirkzeit = @"60 min / 0,5%";
    eB5Vah1.rechengroesse = @0.005;
    eB5Vah1.sortierung=@30;
    
    Produkteinwirkzeit *eB5Vah2 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah2.einwirkzeit = @"30 min / 1,0%";
    eB5Vah2.rechengroesse = @0.01;
    eB5Vah2.sortierung=@40;
    
    Produkteinwirkzeit *eB5Vah3 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah3.einwirkzeit = @"60 min / 1,0% Noroviren";
    eB5Vah3.rechengroesse = @0.01;
    eB5Vah3.sortierung=@50;
    
    Produkteinwirkzeit *eB5Vah4 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah4.einwirkzeit = @"15 min / 1,5%";
    eB5Vah4.rechengroesse = @0.015;
    eB5Vah4.sortierung=@60;
    
    Produkteinwirkzeit *eB5Vah5 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah5.einwirkzeit = @"30 min / 2,0% sporizid";
    eB5Vah5.rechengroesse = @0.02;
    eB5Vah5.sortierung=@70;
    
    Produkteinwirkzeit *eB5Vah6 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Vah6.einwirkzeit = @"60 min / 2,0% viruzid";
    eB5Vah6.rechengroesse = @0.02;
    eB5Vah6.sortierung=@80;
    
    
    NSSet *B5VahSet = [NSSet setWithObjects:eB5Vah1,eB5Vah2,eB5Vah3,eB5Vah4,eB5Vah5,eB5Vah6, nil];
    
    [dB5_1 addProdukteinwirkzeit:B5VahSet];
    
    
     Produkteinwirkzeit *eB5Rki1 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
   
    eB5Rki1.einwirkzeit = @"240 min / 2,0% Viren Wirkungsbereich B";
    eB5Rki1.rechengroesse = @0.02;
    eB5Rki1.sortierung=@90;
    
    Produkteinwirkzeit *eB5Rki2 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB5Rki2.einwirkzeit = @"240 min / 7,0% Bakterien & Pilze Wirkungsbereich A";
    eB5Rki2.rechengroesse = @0.07;
    eB5Rki2.sortierung=@100;
    
    NSSet *B5RkiSet = [NSSet setWithObjects:eB5Rki1,eB5Rki2, nil];
    
    [dB5_2 addProdukteinwirkzeit:B5RkiSet];
    
    Produkteinwirkzeit *eB101 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB101.einwirkzeit = @"5 min / 2,0%";
    eB101.rechengroesse = @0.02;
    eB101.sortierung=@110;
    
    Produkteinwirkzeit *eB102 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB102.einwirkzeit = @"30 min / 2,0% Noroviren";
    eB102.rechengroesse = @0.02;
    eB102.sortierung=@120;
    
    NSSet *b10Set = [NSSet setWithObjects:eB101, eB102, nil];
    
    [dB10 addProdukteinwirkzeit:b10Set];


//    Produkteinwirkzeit *eB151 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
//    eB151.einwirkzeit = @"60 min / 0,5%";
//    eB151.rechengroesse = @0.005;
//    eB151.sortierung=@130;
    
    Produkteinwirkzeit *eB152 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB152.einwirkzeit = @"15 min / 1,0%";
    eB152.rechengroesse = @0.01;
    eB152.sortierung=@140;
    
    Produkteinwirkzeit *eB153 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB153.einwirkzeit = @"5 min / 2,0% (inkl.Noroviren)";
    eB153.rechengroesse = @0.02;
    eB153.sortierung=@150;
    
    Produkteinwirkzeit *eB154 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    
    eB154.einwirkzeit = @"5 min / 2,0% viruzid";
    eB154.rechengroesse = @0.02;
    eB154.sortierung=@160;
    
    Produkteinwirkzeit *eB155 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB155.einwirkzeit = @"2 min / 3,0%";
    eB155.rechengroesse = @0.03;
    eB155.sortierung=@170;
    
//    NSSet *b15Set = [NSSet setWithObjects:eB151,eB152,eB153,eB154,eB155, nil];
    NSSet *b15Set = [NSSet setWithObjects:eB152,eB153,eB154,eB155, nil];
    
    [dB15 addProdukteinwirkzeit:b15Set];
    
    Produkteinwirkzeit *eB151dvg =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB151dvg.einwirkzeit = @"5 Min. / 0,5% - hohe und niedrige Belastung 20° C";
    eB151dvg.rechengroesse = @0.005;
    eB151dvg.sortierung=@130;

    NSSet *b15SetDvg = [NSSet setWithObjects:eB151dvg, nil];
    
    [dB15Dvg addProdukteinwirkzeit:b15SetDvg];
    
    Produkteinwirkzeit *eB201 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB201.einwirkzeit = @"15 min / 1,0%";
    eB201.rechengroesse = @0.01;
    eB201.sortierung=@180;

    Produkteinwirkzeit *eB202 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB202.einwirkzeit = @"5 min / 2,0%";
    eB202.rechengroesse = @0.02;
    eB202.sortierung=@190;
    
    Produkteinwirkzeit *eB203 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eB203.einwirkzeit = @"30 min / 4,0%";
    eB203.rechengroesse = @0.04;
    eB203.sortierung=@200;
    
    NSSet *b20Set = [NSSet setWithObjects:eB201,eB202,eB203, nil];
    
    [dB20 addProdukteinwirkzeit:b20Set];
    
    
    Produkteinwirkzeit *eD10 =[Produkteinwirkzeit insertProdukteinwirkzeitInManagedObjectContext:delegate.managedObjectContext];
    eD10.einwirkzeit = @"60 min / 2,0%";
    eD10.rechengroesse = @0.02;
    eD10.sortierung=@230;
    
    [dD10 addProdukteinwirkzeitObject:eD10];
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Pictogramm Objekte erstellen und zuordnen
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Pictogramme

    Pictogramm *absaugungPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    absaugungPictogramm.bild = @"picto_absaugung_sw.png";
   
    Pictogramm *einscheibenmaschinePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    einscheibenmaschinePictogramm.bild = @"picto_einscheibenmaschine_sw.png";
    
    Pictogramm *flaechePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    flaechePictogramm.bild = @"picto_flaeche_sw.png";
    
    Pictogramm *fliesenPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    fliesenPictogramm.bild = @"picto_fliesen_sw.png";

    Pictogramm *glasPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    glasPictogramm.bild = @"picto_glas_sw.png";
    
    Pictogramm *handPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    handPictogramm.bild = @"picto_hand_sw.png";
  
    Pictogramm *instrumentePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    instrumentePictogramm.bild = @"picto_instrumente_sw.png";

    Pictogramm *kuechePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    kuechePictogramm.bild = @"picto_kueche_sw.png";
    
    Pictogramm *maschinenreinigerPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    maschinenreinigerPictogramm.bild = @"picto_maschinenreiniger_sw.png";
    
    Pictogramm *spenderPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    spenderPictogramm.bild = @"picto_spender_sw.png";
    
    Pictogramm *spruehflaschePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    spruehflaschePictogramm.bild = @"picto_spruehflasche_sw.png";
    
    Pictogramm *teppichPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    teppichPictogramm.bild = @"picto_teppich_sw.png";
    
    Pictogramm *waschbeckenPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    waschbeckenPictogramm.bild = @"picto_waschbecken_sw.png";
    
    Pictogramm *wcFlaschePictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    wcFlaschePictogramm.bild = @"picto_wc_flasche_sw.png";

    Pictogramm *wcPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    wcPictogramm.bild = @"picto_wc_sw.png";
    
    Pictogramm *wischenHandPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    wischenHandPictogramm.bild = @"picto_wischen_hand_sw.png";
    
    Pictogramm *wischerPictogramm = [Pictogramm insertPictogrammInManagedObjectContext:delegate.managedObjectContext];
    wischerPictogramm.bild = @"picto_wischer_sw.png";
    
    [a20 addPictogrammObject:instrumentePictogramm];
    
    [b5 addPictogrammObject:fliesenPictogramm];
    
    [b10 addPictogrammObject:fliesenPictogramm];
    
    [b15 addPictogrammObject:fliesenPictogramm];
    
    [b15t addPictogrammObject:wischenHandPictogramm];
    
    [b20 addPictogrammObject:fliesenPictogramm];
    
//    [b30 addPictogrammObject:wischenHandPictogramm];
    
    [b33t addPictogrammObject:wischenHandPictogramm];
    
    [b33 addPictogrammObject:wischenHandPictogramm];
    
    [b40 addPictogrammObject:wischenHandPictogramm];
    [b40t addPictogrammObject:wischenHandPictogramm];
    
    [b45 addPictogrammObject:wischenHandPictogramm];
    [b45t addPictogrammObject:wischenHandPictogramm];
    
    [b60 addPictogrammObject:wischenHandPictogramm];
    
    [oroVliesTuecher addPictogrammObject:wischenHandPictogramm];
    
    [c20 addPictogrammObject:handPictogramm];
    
    [hd410 addPictogrammObject:handPictogramm];
    
    [c25 addPictogrammObject:handPictogramm];
    
    [chirosyn addPictogrammObject:handPictogramm];
    
    [c30 addPictogrammObject:handPictogramm];
    
    [c45 addPictogrammObject:handPictogramm];
    
    [c50 addPictogrammObject:handPictogramm];
    
    [c60 addPictogrammObject:handPictogramm];
    
    [d10 addPictogrammObject:absaugungPictogramm];
    
    [delegate saveContext];

}




-(void)readCSVFileForGettingBarcodes{

    
    //Fehlervariable
    NSError *errorReading;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    NSFetchRequest *fetchReuquest = [[NSFetchRequest alloc]initWithEntityName:NSStringFromClass([Produkte class])];
    NSArray* produktArray = [appDelegate.managedObjectContext executeFetchRequest:fetchReuquest error:&errorReading];
    
    
    //Zugang zur Datei
    NSString *path=[[NSBundle mainBundle] pathForResource:@"orochemie_barcodes" ofType:@"csv"];
    NSURL *csvUrl = [NSURL fileURLWithPath:path];
	
    //Lesen der Datei
    NSString *contents = [NSString stringWithContentsOfURL:csvUrl encoding:NSUTF8StringEncoding error:&errorReading];
    
    //if(errorReading) NSLog(@"error: %@", errorReading.localizedDescription);
    
    //Array mit den einzelnen Zeilen
    NSArray* lines = [contents componentsSeparatedByString:@"\n"];
    
    //Array mit den einzelnen Werten innerhalb der Zeilen
    for (NSString* line in lines)
    {
        NSArray* fields = [line componentsSeparatedByString:@";"];
        if ([fields count]==1) continue;

        //Zuweisung der Felder zum Barcode Objekt
        Produktvarianten *produtVariante = [Produktvarianten insertProduktvariantenInManagedObjectContext:appDelegate.managedObjectContext];        produtVariante.artikelnummer = fields[0];
        produtVariante.matchcode = fields[1];
        produtVariante.bezeichnung1 = fields[2];
        produtVariante.bezeichnung2 = fields[3];
        produtVariante.barcode = fields[4];
        
        
        
        NSString * bezeichnung_1=[produtVariante.bezeichnung1 substringWithRange:NSMakeRange(0, 8)];
        
        //Suchen des passenden Produkt-Objekt
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", bezeichnung_1];
        NSArray *filteredArr = [produktArray filteredArrayUsingPredicate:pred];
        
        Produkte *p = [filteredArr lastObject];
        
        
        
        if(p!=nil){
           
            //Holen des Produkt Objektes aus dem Ursprünglichen Produkt-Array
            int index = (int)[produktArray indexOfObject:p];
            p = [produktArray objectAtIndex:index];
            
            //hinzufügen des Barcodes zum Produkt
            [p addProduktvariantenObject:produtVariante];
            
        
        } else {
            
            [produtVariante deleteProduktvarianten];
            
        }
        
        
    }
    
    [appDelegate saveContext];
}


-(void)readCSVFileForGettingMusterplaene{
    
    
    //Fehlervariable
    NSError *errorReading;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSFetchRequest *fetchReuquest = [[NSFetchRequest alloc]initWithEntityName:NSStringFromClass([Checklistenkategorie class])];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"musterplan ==%i", 1];
    fetchReuquest.predicate = predicate;
    NSArray* kategorieArray = [appDelegate.managedObjectContext executeFetchRequest:fetchReuquest error:&errorReading];
    
    
    //Zugang zur Datei
    NSString *path=[[NSBundle mainBundle] pathForResource:@"orochemie_musterplaene" ofType:@"csv"];
    NSURL *csvUrl = [NSURL fileURLWithPath:path];
    
    //Lesen der Datei
    NSString *contents = [NSString stringWithContentsOfURL:csvUrl encoding:NSUTF8StringEncoding error:&errorReading];
    
    //if(errorReading) NSLog(@"error: %@", errorReading.localizedDescription);
    
    //Array mit den einzelnen Zeilen
    NSArray* lines = [contents componentsSeparatedByString:@"\n"];
    
    NSString* urlStringOne = @"http://www.hygienesystem.de/index.php?id=31&type=199&tx_desinfektionsplan_desinfektionsplan%5Bplan%5D=";
    NSString* urlStringTwo = @"&tx_desinfektionsplan_desinfektionsplan%5Binstitution%5D=&tx_desinfektionsplan_desinfektionsplan%5Boutput%5D=I&tx_desinfektionsplan_desinfektionsplan%5Baction%5D=print&tx_desinfektionsplan_desinfektionsplan%5Bcontroller%5D=Pdf";
    
    //Array mit den einzelnen Werten innerhalb der Zeilen
    for (NSString* line in lines)
    {
        NSArray* fields = [line componentsSeparatedByString:@";"];
        
        //Zuweisung der Felder zum Checklisten Objekt
        Checkliste *musterplan = [Checkliste insertChecklisteInManagedObjectContext:appDelegate.managedObjectContext];
        musterplan.kurzbezeichnung = fields[0];
        musterplan.name = fields[1];
        musterplan.url = [NSString stringWithFormat:@"%@%@%@",urlStringOne,fields[2],urlStringTwo];


        //Suchen des passenden Checklistenkategorie-Objekt
        NSString* cat = [NSString stringWithFormat:@"%@", fields[3]];
        cat = [cat substringToIndex:[cat length]-1];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"bezeichnung BEGINSWITH[c] %@", cat];
        NSArray *filteredArr = [kategorieArray filteredArrayUsingPredicate:pred];
        
        Checklistenkategorie *c = [filteredArr lastObject];
        
        if(c!=nil){
                        //Holen der Checklistenkategorie Objektes aus dem Ursprünglichen Checklistenkategorie-Array
            int index = (int)[kategorieArray indexOfObject:c];
            c = [kategorieArray objectAtIndex:index];
            
            //hinzufügen der Kazegorie zur Checkliste
            musterplan.checklistenkategorie = c;
            
        }
        
    }
    
    [appDelegate saveContext];
}

- (NSString *)createUUID
{
    // Create universally unique identifier (object)
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    
    // Get the string representation of CFUUID object.
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
    
    //CFUUIDBytes bytes = CFUUIDGetUUIDBytes(uuidObject);
    
    
    CFRelease(uuidObject);
    
    return uuidStr;
}




@end
