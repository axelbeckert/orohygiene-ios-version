//
//  NSObject+JSM.h
//  AffirmationApp
//
//  Created by Frank Jüstel on 03.09.13.
//  Copyright (c) 2013 JSM Arts Webservices GbR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (JSM)

+ (NSString*) className;
- (NSString*) className;
- (BOOL) isNull;

-(id) associatedObjectForKey: (NSString*) key;
-(void) setAssociatedObject: (id) object forKey: (NSString*) key;

@end
