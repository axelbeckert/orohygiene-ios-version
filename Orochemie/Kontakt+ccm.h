//
//  Kontakt+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 17.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Kontakt.h"

@interface Kontakt (ccm)
+(Kontakt*) insertKontaktInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteKontakt;
@end
