//
//  NewsletterTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "NewsletterTableViewController.h"
#import "Newsletter.h"
#import "NewsletterModel.h"
#import "NewsletterWebViewController.h"
#import "AppDelegate.h"
#import "NewsTableCell.h"
#import "WebviewViewController.h"

@interface NewsletterTableViewController ()
@property (nonatomic, strong) NewsletterModel* nModel;
@property (nonatomic,strong) NSTimer *timer;
@property(nonatomic,strong) AppDelegate *appDelegate;
@end

@implementation NewsletterTableViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(NewsletterModel *)nModel
{
    if(!_nModel) _nModel = [[NewsletterModel alloc] init];
    return _nModel;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.showNewsletterFrom == DesinfektionChannel){
        [oroThemeManager customizeTableView:self.tableView];
        
    } else if(self.showNewsletterFrom == ReinigerChannel){
        [oroThemeManager customizeReinigerTableView:self.tableView];
        
    }
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    //Timer einschalten
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];

    
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
    self.title=@"Newsletter";
    
    self.nModel = nil;
    
 if([ConnectionCheck hasConnectivity]) {
     [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
     
    NSString* urlString = @"http://www.orohygienesystem.de/json_newsLetter.php";
    NSURL* url = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:10.0];
    [request setCachePolicy:NSURLCacheStorageAllowed];
    [request setValue:@"text/json" forHTTPHeaderField:@"Content-type"];
    

    
     NSURLSession *session = [NSURLSession sharedSession];
     
     NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
         if (error) {
             //NSLog(@"Fehler: %@", error);
         };
         
         
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
         
         if (httpResponse.statusCode != 200) {
             return;
         }

         error = nil;
         NSArray* jsonContainer = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
         if (error) {
             //NSLog(@"Fehler: %@", error);
         };
         NSLog(@"Musterpläne: %@", jsonContainer);
         
         for (NSDictionary* dictionary in jsonContainer) {
             Newsletter* newsletter = [[Newsletter alloc] init];
             newsletter.newsletterId = dictionary[@"id"];
             newsletter.newsletterText1 = dictionary[@"text1"];
             newsletter.newsletterText2 = dictionary[@"text3"];
             newsletter.newsletterUrl = dictionary[@"text2"];
             
             [self.nModel addNewsletter:newsletter];
         }
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [self.timer invalidate]; //Timer ausschalten

             [self.tableView reloadData];
         });
         
     }];
     
     [dataTask resume];
     
 } else {
    [self.timer invalidate]; //Timer ausschalten
     [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
    }
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Newsletter-Uebersicht-Screen"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.nModel.numberOfNewsletter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    NewsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
    
    Newsletter* newsletter = [[Newsletter alloc] init];
    
    newsletter = [self.nModel newsletterAtIndexPath:indexPath];
    

    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    cell.backgroundView=backgroundView;
    
        
        cell.titleOutlet.text = newsletter.newsletterText1;
        cell.subTitleOutlet.text = newsletter.newsletterText2;
        
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
        [SVProgressHUD dismiss];
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"newsletterWebViewSegue"]){
        
        WebviewViewController *controller = segue.destinationViewController;
        Newsletter* newsletter=[self.nModel newsletterAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.urlString=[NSString stringWithFormat:@"https://www.orochemie.de/newsletter_archiv/%@",newsletter.newsletterUrl];
        controller.webViewTitle=@"Newsletter";
        controller.pdfFileName=[NSString stringWithFormat:@"%@",newsletter.newsletterUrl];
        
    }
}



    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - AlertView Delegate & Timer Selector

- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [self.timer invalidate]; //Timer ausschalten
    
    UIAlertController* alert = [AlertHelper getUIAlertControllerWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [AlertHelper showAlert:alert];
    
    [SVProgressHUD dismiss];
    
}
@end
