//
//  ProdukteNachBezeichnungTableViewController.h
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProdukteNachBezeichnungTableViewController : UITableViewController
@property (nonatomic,strong) Phwert* pHWert;
@property (nonatomic,strong) Oberflaeche* oberflaeche;
@property (nonatomic,strong) Reinigungsverfahren* reinigungsverfahren;
@property (nonatomic,strong) Anwendung* anwendung;
@end
