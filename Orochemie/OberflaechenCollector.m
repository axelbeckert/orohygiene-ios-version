//
//  OberflaechenCollector.m
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//

#import "OberflaechenCollector.h"
#import "OberflaechenUndBelaegeTableViewController.h"
#import "AppDelegate.h"
#import "AddOberflaechenToDatabase.h"

@interface OberflaechenCollector() < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >
@property(nonatomic,strong) AppDelegate *appDelegate;
@property int collectOberflaechenIsActive;
@end


@implementation OberflaechenCollector

NSString *const collectOberflaechen=@"collectOberflaechen";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.entityName = @"Oberflaeche";
        self.collectorType = ONLY_TSTAMP;
    }
    return self;
}

-(void) startCollectingOberflaechen{
    self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self collectOberflaechen];
}

-(void) collectOberflaechen{
    
    if (self.collectorType==ONLY_TSTAMP){
        
        [self collectJsonWithUrl:urlOberflaechenTstamp andTaskDescription:collectOberflaechen];
        
    } else if(self.collectorType==COMPLETE_OBJECTS) {
        
        [self collectJsonWithUrl:urlOberflaechen andTaskDescription:collectOberflaechen];
        
    } else if (self.collectorType==FROM_FILE){
        
        NSDictionary* jsonContents = [self retrieveDataFromJsonFile:self.entityName];
        
        self.collectorType = COMPLETE_OBJECTS;
        [self checkDownloadTaskDescription:collectOberflaechen andBuildChecklistsWithData:(NSArray*)jsonContents];
        
    }
   
}


-(void)checkIfDownloadProcessIsActive{
    int processActive = 0;
    if(self.collectOberflaechenIsActive==1) processActive = 1;
    
    if(processActive==0 && self.showSVProgressHUD==1)[SVProgressHUD dismiss];
}

-(void) checkDownloadTaskDescription:(NSString*)downloadTaskDescription andBuildChecklistsWithData:(NSArray*)data{
    
    if([downloadTaskDescription isEqualToString:collectOberflaechen] && self.collectorType==COMPLETE_OBJECTS){

        AddOberflaechenToDatabase *addOberflaecheToDatabase = [AddOberflaechenToDatabase new];
        addOberflaecheToDatabase.oberflaechenUndBelaegeTableViewController = self.oberflaechenUndBelaegeTableViewController;
        [addOberflaecheToDatabase addOberflaechen:data];
        self.collectorType = ONLY_TSTAMP;
        self.collectOberflaechenIsActive = 0;
    } else if ([downloadTaskDescription isEqualToString:collectOberflaechen] && self.collectorType==ONLY_TSTAMP){
        [self compareOberflaechenTimestampsWithData:data];
        
    }
    [self checkIfDownloadProcessIsActive];
    
    NSLog(@"Download Finished: %@", downloadTaskDescription);
    
}

-(void) compareOberflaechenTimestampsWithData:(NSArray*)data{
    NSDictionary*dict = (NSDictionary*)data;
    bool result;
    
    result = [self compareLatestTimestampFromDatabaseWithJsonTimestamp:dict[@"tstamp"] andEntitiy:self.entityName];
    
    if(!result){
        self.collectorType = COMPLETE_OBJECTS;
        [self collectOberflaechen];
    }
}

@end
