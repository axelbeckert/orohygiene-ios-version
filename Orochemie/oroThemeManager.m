//
//  oroThemeManager.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "oroThemeManager.h"
#import "CustomColoredAccessory.h"
#import "AnwendungTableViewCell.h"

@implementation oroThemeManager
static id<oroTheme> _theme = nil;

+(id<oroTheme>)sharedTheme
{
    return _theme;
}

+(void)setSharedTheme:(id<oroTheme>)inTheme
{
    _theme = inTheme;
    [self applyTheme];
}

+(void)applyTheme
{
    id<oroTheme> theme = [self sharedTheme];
    NSLog(@"aktuelles Theme: %@", theme);
}


#pragma mark - TableView Customization

+(void)customizeTableView:(UITableView*)theTableView
{
    id<oroTheme> theme = [self sharedTheme];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:[theme backgroundImage]];
    [theTableView setBackgroundView:imageView];
    [theTableView setSeparatorInset:UIEdgeInsetsZero];

    
}

+(void)customizeReinigerTableView:(UITableView*)theTableView
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[theme backgroundImageForCleanerChanel]];
    [theTableView setBackgroundView:imageView];
    [theTableView setSeparatorInset:UIEdgeInsetsZero];
    [theTableView setSectionIndexColor:[UIColor whiteColor]];
    [theTableView setSectionIndexBackgroundColor:[UIColor clearColor]];
    [theTableView setSectionIndexTrackingBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
    
}

+(void)customizeAccessoryView:(UITableViewCell*)theTableViewCell
{
    id<oroTheme> theme = [self sharedTheme];
    
    CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:[theme accessoryColor]];
    accessory.highlightedColor = [UIColor blackColor];
    theTableViewCell.accessoryView = accessory;
    
}

+(void)customizeTableCellText:(ProduktuebersichtTableViewCell*)tableCell
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIColor *color = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    tableCell.backgroundColor = color;
    
    tableCell.titleLabelOutlet.textColor = [theme tableCellTextColor];
    tableCell.subtitleLabelOutlet.textColor = [theme tableCellTextColor];
    tableCell.descriptionLabelOutlet.textColor = [theme tableCellTextColor];

}




+(void)customizeTableViewCellBackground:(UITableViewCell*)tableCell
{

    

    UIColor *color = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    tableCell.backgroundColor = color;
        
    
}



+(void)customizeTableViewStandardTableViewCellText:(UITableViewCell*)tableCell
{
    id<oroTheme> theme = [self sharedTheme];

    //UIColor *color = [[UIColor alloc] initWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    UIColor *color = [UIColor colorWithWhite:0.0 alpha:0.3];
    tableCell.backgroundColor = [UIColor clearColor];
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];

    tableCell.backgroundView=backgroundView;

    tableCell.layoutMargins = UIEdgeInsetsZero;
    tableCell.preservesSuperviewLayoutMargins = NO;
    
    tableCell.textLabel.textColor=[theme tableCellTextColor];
    tableCell.detailTextLabel.textColor=[theme tableCellTextColor];
}

+(void)customizeTableViewAnwendungTableViewCellText:(AnwendungTableViewCell *)tableCell
{
    id<oroTheme> theme = [self sharedTheme];
    
    //UIColor *color = [[UIColor alloc] initWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    UIColor *color = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    tableCell.backgroundColor = color;
    tableCell.textLabel.textColor=[theme tableCellTextColor];
    tableCell.detailTextLabel.textColor=[theme tableCellTextColor];
}


+(UIView*)customizeTableViewViewForHeaderView
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIView* container = [[UIView alloc] init];
    container.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-1, 0, 1024, 33)];
    imageView.image = [theme tableViewBackgroundPictureForHeaderView];

  
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 280, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [theme tableViewTextColorForHeaderView];//UIColor white color
    label.font = [theme tableViewFontForHeaderView]; //[UIFont boldSystemFontOfSize:14.0];
    
    [container addSubview:imageView];
    [container addSubview:label];
    
    
    return container;
}

+(UIView*)customizeStaticTableViewViewForHeaderView
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIView* container = [[UIView alloc] init];
    container.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-1, 0, 1024, 33)];
    imageView.image = [theme tableViewBackgroundPictureForHeaderView];
    //imageView.layer.borderColor = [[UIColor whiteColor]CGColor];
    //imageView.layer.borderWidth=1.0;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 280, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [theme tableViewTextColorForHeaderView];//UIColor white color
    label.font = [theme tableViewFontForHeaderView]; //[UIFont boldSystemFontOfSize:14.0];
    
    [container addSubview:imageView];
    [container addSubview:label];
    
    
    return container;
}



+(UIView*)customizeStaticTableViewViewForDatenschutzHeaderView
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIView* container = [[UIView alloc] init];
    container.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-1, 0, 1024, 33)];
    imageView.image = [theme tableViewBackgroundPictureForHeaderView];
    //imageView.layer.borderColor = [[UIColor whiteColor]CGColor];
    //imageView.layer.borderWidth=1.0;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, 280, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [theme tableViewTextColorForHeaderView];//UIColor white color
    label.font = [theme tableViewFontForDatenschutzHeaderView]; //[UIFont boldSystemFontOfSize:14.0];
    label.numberOfLines = 0;
    
    [container addSubview:imageView];
    [container addSubview:label];
    
    
    return container;
}

+(UIView*)customizeStaticTableViewViewForDatenschutzHeaderViewHeight
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIView* container = [[UIView alloc] init];
    container.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-1, 0, 1024, 73)];
    imageView.image = [theme tableViewBackgroundPictureForHeaderView];
    //imageView.layer.borderColor = [[UIColor whiteColor]CGColor];
    //imageView.layer.borderWidth=1.0;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, 280, 60)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [theme tableViewTextColorForHeaderView];//UIColor white color
    label.font = [theme tableViewFontForDatenschutzHeaderView]; //[UIFont boldSystemFontOfSize:14.0];
    label.numberOfLines = 0;
    
    [container addSubview:imageView];
    [container addSubview:label];
    
    
    return container;
}

+(CGFloat)customizeTableViewHeigtForHeaderView
{
    id<oroTheme> theme = [self sharedTheme];
    
    return [theme tableViewHeightForHeaderView];
}

+(CGFloat)customizeStaticTableViewHeigtForHeaderView
{
    id<oroTheme> theme = [self sharedTheme];


        return [theme staticTableViewHeightForHeaderView];

    
    
}

#pragma mark - View Customization

+(void)cutomizeView: (UIView*)theView
{
    id<oroTheme> theme = [self sharedTheme];
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
 

    
    
    
    //Screen Size
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    //CGFloat screenHeight = screenRect.size.height;
    //
    
    //UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background_oro_theme2_1136@2x.png"]];
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[theme backgroundImage]];
    ////NSLog(@"Hintergrund:%@",backgroundColor);
    [theView setBackgroundColor:backgroundColor];
    
    
    //NSLog(@"aktuelle Bildschirmhöhe im -view: %f",screenHeight);

    

}

+(void)customizeSelectView: (UIView*)theView
{
    id<oroTheme> theme = [self sharedTheme];
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    
    
    //Screen Size
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    //CGFloat screenHeight = screenRect.size.height;
    //
    
    //UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background_oro_theme2_1136@2x.png"]];
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[theme backgroundImageForSelectChanel]];
    ////NSLog(@"Hintergrund:%@",backgroundColor);
    [theView setBackgroundColor:backgroundColor];
    
    
    //NSLog(@"aktuelle Bildschirmhöhe im -view: %f",screenHeight);
    
    
    
}

+(void)customizeCleanerView: (UIView*)theView
{
    id<oroTheme> theme = [self sharedTheme];
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    
    
    //Screen Size
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    //CGFloat screenHeight = screenRect.size.height;
    //
    
    //UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background_oro_theme2_1136@2x.png"]];
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[theme backgroundImageForCleanerChanel]];
    ////NSLog(@"Hintergrund:%@",backgroundColor);
    [theView setBackgroundColor:backgroundColor];
    
    
    //NSLog(@"aktuelle Bildschirmhöhe im -view: %f",screenHeight);
    
    
    
}



#pragma mark - Button Customization

+(UIImage*)setUiBarButtonImage
{
    return [UIImage imageNamed:@"home.png"];
}

+(UIImage*)setUiToolBarButtonImage
{
    id<oroTheme> theme = [self sharedTheme];
    
    
    return [theme buttonBackgroundImage];
}

+(void)customizeButtonBackgroundImage:(UIImageView*)theImageView
{
    id<oroTheme> theme = [self sharedTheme];
    
    [theImageView setImage:[theme buttonBackgroundImage]];
}

+(void) customizeButtonImage: (UIButton*)theButton
{
    id<oroTheme> theme = [self sharedTheme];
    
    [theButton setBackgroundImage:
        [theme imageForButtonNormal]?
     [theme imageForButtonNormal]: nil 
                         forState:UIControlStateNormal];
    
    [theButton setBackgroundImage:
     [theme imageForButtonHighlited]?
     [theme imageForButtonHighlited]: nil
                         forState:UIControlStateHighlighted];
    theButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    theButton.layer.borderWidth = 1.0;
    
    [theButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    

}

+(UIBarButtonItem*) homeBarButton :(id)target{
    
        UIBarButtonItem* homeBarButton = [[UIBarButtonItem alloc] initWithImage:[oroThemeManager setUiBarButtonImage] style:UIBarButtonItemStyleBordered target:target action:@selector(home:)];
    
    return homeBarButton;
}

+(UIBarButtonItem*) addToMerklisteBarButton :(id)target{
    
    UIBarButtonItem *addToMerklisteBarButton = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"179-notepad.png"]
                                             style:UIBarButtonItemStyleBordered
                                             target:target
                                             action:@selector(addToMerkliste)];
    
    return addToMerklisteBarButton;
}

+(UIBarButtonItem*) printActionButton :(id)target{
    
    UIBarButtonItem* printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:target action:@selector(printAction:)];
    
    return printActionButton;
}



#pragma mark - Label Customization
+(void) customizeLabel:(UILabel*)theLabel
{
    id<oroTheme> theme = [self sharedTheme];
    [theLabel setTextColor:[theme labelTextColor]];
    
}


#pragma mark - Navbar Customization

+(UIImage*) customizeImageforNavigationBar
{
    id<oroTheme> theme = [self sharedTheme];
    
    return [[theme imageforNavigationBar] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 100.0, 0.0, 100.0)];
}


#pragma mark - Title Customization

+(UILabel*)customizeTitleLabel
{
    id<oroTheme> theme = [self sharedTheme];
    
    UIFont *font= [theme fontForLabelInTitleView]; //[UIFont boldSystemFontOfSize:17];
    
    UILabel* tlabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0, 100, 50)];
    
    tlabel.textColor=[theme colorForLabelInTitleView];
    tlabel.backgroundColor =[UIColor clearColor];
    tlabel.font=font;
    tlabel.adjustsFontSizeToFitWidth=YES;
    [tlabel setTextAlignment:NSTextAlignmentCenter];
    return tlabel;
}

-(UIColor*)colorForLabelInTitleView
{
    return [UIColor whiteColor];
}

-(UIFont*)fontForLabelInTitleView
{
    return [UIFont boldSystemFontOfSize:17];
}
@end
