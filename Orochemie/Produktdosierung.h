//
//  Produktdosierung.h
//  orochemie
//
//  Created by Axel Beckert on 10.03.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Produkteinwirkzeit;

@interface Produktdosierung : NSManagedObject

@property (nonatomic, retain) NSString * hauptbezeichnung;
@property (nonatomic, retain) NSString * sortierung;
@property (nonatomic, retain) NSString * zusatzbezeichnung;
@property (nonatomic, retain) Produkte *produkte;
@property (nonatomic, retain) NSSet *produkteinwirkzeit;
@end

@interface Produktdosierung (CoreDataGeneratedAccessors)

- (void)addProdukteinwirkzeitObject:(Produkteinwirkzeit *)value;
- (void)removeProdukteinwirkzeitObject:(Produkteinwirkzeit *)value;
- (void)addProdukteinwirkzeit:(NSSet *)values;
- (void)removeProdukteinwirkzeit:(NSSet *)values;

@end
