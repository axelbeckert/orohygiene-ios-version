//
//  TemporaryFilesInformation.h
//  orochemie
//
//  Created by Axel Beckert on 17.12.13.
//  Copyright (c) 2013 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TemporaryFilesInformation : NSObject
+(void)deleteContentOfLastTemporaryFolder;
+(void)clearTempSystemFolder;
+(NSString*)getLastFilePathFromLastTempFolderFile;
@end
