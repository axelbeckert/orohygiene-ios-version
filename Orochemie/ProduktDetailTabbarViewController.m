//
//  ProduktDetailTabbarViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProduktDetailTabbarViewController.h"
#import "ProduktDetailStaticTableViewController.h"
#import "ProduktDetailVideoTableViewController.h"
#import "WebviewViewController.h"
#import "VideoTableViewController.h"
#import "VerbrauchsRechnerStaticTableViewController.h"


@interface ProduktDetailTabbarViewController ()

@end

@implementation ProduktDetailTabbarViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
        [self.tabBarController.tabBar setTintColor:[UIColor lightGrayColor]];
    
    if(self.reiniger){
        //NSLog(@"Reiniger in Tab: %@", self.reiniger);
        
        ProduktDetailStaticTableViewController *controller = [[self viewControllers] objectAtIndex:0];
        controller.reiniger = self.reiniger;
     
         VideoTableViewController *controller2 = [[self viewControllers] objectAtIndex:1];
        controller2.reiniger = self.reiniger;
        controller2.showVideoTableViewFrom = ReinigerChannel;
        
        VerbrauchsRechnerStaticTableViewController *controller3 =[[self viewControllers] objectAtIndex:2];
        controller3.reiniger = self.reiniger;
        
        WebviewViewController* controller4 = [[self viewControllers]objectAtIndex:3];
        controller4.urlString=cWebsiteFuerBestellung;
        controller4.pdfFileName=cWebsitePDFTitleFuerBestellung;
        controller4.webViewTitle=cWebsiteTitleFuerBestellung;
        
    }
    
    [self.tabBarController.tabBar setTintColor:[UIColor lightGrayColor]];
    
    self.parentViewController.automaticallyAdjustsScrollViewInsets=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"ProduktTabBarController"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}



@end
