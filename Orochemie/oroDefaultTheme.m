//
//  oroDefaultTheme.m
//  Orochemie
//
//  Created by Axel Beckert on 29.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "oroDefaultTheme.h"

@implementation oroDefaultTheme

-(UIImage*)backgroundImage
{
    
    return nil;
    
}

-(UIColor*)accessoryColor
{
    return nil;
}

-(UIColor*)tableCellTextColor
{
    return [UIColor blackColor];
}

-(UIImage*)tableViewBackgroundPictureForHeaderView
{
    return nil;
}

-(UIColor*)tableViewTextColorForHeaderView
{
    return [UIColor blackColor];
}

-(UIFont*)tableViewFontForHeaderView
{
    return [UIFont boldSystemFontOfSize:14];
}

-(UIFont*)tableViewFontForDatenschutzHeaderView
{
    return [UIFont boldSystemFontOfSize:13];
}

-(CGFloat)tableViewHeightForHeaderView
{
    return  0;
}

-(UIImage*)imageforNavigationBar
{
   return nil;
}

-(UIImage*)buttonBackgroundImage;
{
   return nil;
}

-(UIImage*)imageForButtonNormal
{
    return nil;
}

-(UIImage*)imageForButtonHighlited
{
    return nil;
}

-(UIColor*)labelTextColor
{
    return [UIColor blackColor];
}


-(CGFloat)staticTableViewHeightForHeaderView
{
    return 30;
}

-(UIColor*)colorForLabelInTitleView
{
    return [UIColor whiteColor];
}

-(UIFont*)fontForLabelInTitleView
{
    return [UIFont boldSystemFontOfSize:17];
}

-(UIImage*)backgroundImageForCleanerChanel
{
    
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        //NSLog(@"Iphone 5 Auflösung");
        return [UIImage imageNamed:@"background_cleaners_1136.png"]; //Iphone 5 background_oro_theme2_1136.png
        
    }
    if (d==2)
        
    {
        //NSLog(@"Ipad Auflösung");
        return [UIImage imageNamed:@"background_cleaners_ipad.png"]; //Ipad
    } else if (d==3)
        
    {
        //NSLog(@"Ipad Retina Auflösung");
        return [UIImage imageNamed:@"background_cleaners_ipad.png"];//IPad Retina
        
    }
    
    //NSLog(@"Iphone 4 & IPhone 4 Retina Auflösung");
    return  [UIImage imageNamed:@"background_cleaners.png"]; //background_oro_theme2.png
    
}

-(UIImage*)backgroundImageForSelectChanel
{
    
    //Screen Size
    int d = 0; // standard display
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        d = 1; // is retina display
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        d += 2;
    }
    
    //Returns the following figures:
    //     0 for standard resolution iPhone/iPod touch,
    //     1 for retina iPhone,
    //     2 for standard resolution iPad,
    //     3 for retina iPad
    
    
    
    CGFloat height = [UIScreen mainScreen].currentMode.size.height;
    if(height == 1136){
        //NSLog(@"Iphone 5 Auflösung");
        return [UIImage imageNamed:@"background_select_1136.png"]; //Iphone 5 background_oro_theme2_1136.png
        
    }
    if (d==2)
        
    {
        //NSLog(@"Ipad Auflösung");
        return [UIImage imageNamed:@"background_select_ipad.png"]; //Ipad
    } else if (d==3)
        
    {
        //NSLog(@"Ipad Retina Auflösung");
        return [UIImage imageNamed:@"background_select_ipad.png"];//IPad Retina
        
    }
    
    //NSLog(@"Iphone 4 & IPhone 4 Retina Auflösung");
    return  [UIImage imageNamed:@"background_select.png"]; //background_oro_theme2.png
    
}

@end
