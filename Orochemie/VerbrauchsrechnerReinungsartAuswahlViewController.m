//
//  VerbrauchsrechnerReinungsartAuswahlViewController.m
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VerbrauchsrechnerReinungsartAuswahlViewController.h"
#import "Reinigungsart+ccm.h"
#import "Verbrauch+ccm.h"


@interface VerbrauchsrechnerReinungsartAuswahlViewController ()
@property(nonatomic,strong) Verbrauch *verbrauch;
@end

@implementation VerbrauchsrechnerReinungsartAuswahlViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Reiniger-VerbrauchsrechnerReinigungsartAuswahl-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];

    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(self.reinigungsartenArray.count>0){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        Verbrauch *verbrauch = [self.reinigungsartenArray objectAtIndex:indexPath.row];
        
        if(!self.verbrauch){
            
            self.verbrauchsRechnerStaticTableViewController.verbrauch = verbrauch;
            self.verbrauchsRechnerStaticTableViewController.reinigungsartMainTextLabelOutlet.text = [NSString stringWithFormat:@"%@ %@",verbrauch.reinigungsart.bezeichnung,verbrauch.reinigungsart.bezeichnung2];
            self.verbrauchsRechnerStaticTableViewController.reinigungsartTextLabelOutlet.text=@"Reinigungsart";
            
        } else {
            self.verbrauchsRechnerStaticTableViewController.verbrauch = self.verbrauch;
            self.verbrauchsRechnerStaticTableViewController.reinigungsartMainTextLabelOutlet.text = [NSString stringWithFormat:@"%@ %@",self.verbrauch.reinigungsart.bezeichnung,self.verbrauch.reinigungsart.bezeichnung2];
            self.verbrauchsRechnerStaticTableViewController.reinigungsartTextLabelOutlet.text=@"Reinigungsart";
        
        }
        
        [self.verbrauchsRechnerStaticTableViewController rechneVerbrauch];
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif  
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return [self.reinigungsartenArray count];
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
     Verbrauch *verbrauch =[ self.reinigungsartenArray objectAtIndex:row];
    
      int calculatedSize = self.view.bounds.size.width /2 - 65 ;
    
    
    //NSLog(@"Test = %i", test);
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 650, 60)];
        
        label.text=verbrauch.reinigungsart.bezeichnung;
        label.font=[UIFont boldSystemFontOfSize:25.0];
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
      
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 650, 60)];

        label1.text=verbrauch.reinigungsart.bezeichnung2;
        label1.font=[UIFont italicSystemFontOfSize:18.0];
        label1.backgroundColor = [UIColor whiteColor];
        label1.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 600, 100)];
       
        
        [myView addSubview:label];
        [myView addSubview:label1];
        
        return myView;
        
    } else {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(calculatedSize, 25, 110, 30)];
        
        label.text=verbrauch.reinigungsart.bezeichnung;
        label.font=[UIFont boldSystemFontOfSize:15.0];
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(calculatedSize, 50, 110, 20)];
        
        label1.text=verbrauch.reinigungsart.bezeichnung2;
        label1.font=[UIFont italicSystemFontOfSize:9.0];
        label1.backgroundColor = [UIColor whiteColor];
        label1.textAlignment = NSTextAlignmentCenter;
        
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
         myView.backgroundColor = [UIColor whiteColor];
        [myView addSubview:label];
        [myView addSubview:label1];
        
        return myView;
        
    }
    
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return 100;
    }
    
    return 44;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.verbrauch = [self.reinigungsartenArray objectAtIndex:row];


}

- (IBAction)auswahlUebernehmenButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
