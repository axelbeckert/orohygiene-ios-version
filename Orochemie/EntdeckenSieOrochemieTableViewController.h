//
//  EntdeckenSieOrochemieTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntdeckenSieOrochemieTableViewController : UITableViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic,assign) show showEntdeckenSieOrochemieFrom;
@end
