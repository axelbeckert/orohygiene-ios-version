//
//  BestelluebersichtTableViewCell.h
//  orochemie
//
//  Created by Axel Beckert on 26.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BestelluebersichtTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *textLabelOutlet;

@property (strong, nonatomic) IBOutlet UIButton *deleteButtonOutlet;
@property(nonatomic,strong) NSIndexPath *indexPathForCell;
- (IBAction)deleteButtonAction:(id)sender;
- (IBAction)bestellMengenStepperAction:(UIStepper *)sender;
@property (strong, nonatomic) IBOutlet UILabel *mengeTextLabelOutlet;
@property (strong, nonatomic) IBOutlet UIStepper *bestellMengenStepperOutlet;
@property  double stepperValue;

@end
