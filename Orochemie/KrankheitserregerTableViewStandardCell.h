//
//  VirusTableViewStandardCell.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KrankheitserregerTableViewStandardCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *BezeichnungLabelTextOutlet;

@end
