//
//  Verbrauch.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Reiniger, Reinigungsart;

@interface Verbrauch : NSManagedObject

@property (nonatomic, retain) NSNumber * verbrauchsmenge;
@property (nonatomic, retain) NSNumber * sortierung;
@property (nonatomic, retain) Reiniger *reiniger;
@property (nonatomic, retain) Reinigungsart *reinigungsart;

@end
