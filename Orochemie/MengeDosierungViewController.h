//
//  MengeDosierungViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DosierungStaticTableViewController;

@interface MengeDosierungViewController : UIViewController
<UIPickerViewDataSource, UIPickerViewDelegate>{
    __weak IBOutlet UIPickerView *MengePickerViewOutlet;
    
    NSArray *arrayPickerMengeDosierungPicker;
    
}
@property (strong, nonatomic) IBOutlet UIToolbar *dosierungToolbar;
@property (nonatomic,strong) DosierungStaticTableViewController *dosierungStaticTableViewController;
@property (strong, nonatomic) IBOutlet UIView *topToolBarOutlet;
@end
