//
//  VideoDownloadViewController.m
//  orochemie
//
//  Created by Axel Beckert on 14.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VideoDownloadViewController.h"



@interface VideoDownloadViewController() < NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate >
@property (nonatomic,strong) NSMutableData *receivedData;
@property (strong, nonatomic) IBOutlet UIProgressView *progressViewOutlet;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorOutlet;
@property (strong, nonatomic) IBOutlet UILabel *videoLabelOutlet;
@property (nonatomic,strong) NSArray *videoArray;
@property int aktuellGeladenesVideo;
@property int geladeneVideos;
@property (strong, nonatomic) IBOutlet UIProgressView *gesamtfortschrittProgressView;
@property (nonatomic,strong) NSMutableArray *geladeneVideoFiles;
@property (strong, nonatomic) IBOutlet UILabel *gesamtfortschrittLabelOutlet;
@property (nonatomic,strong) NSMutableArray *loadVideosAgainArray;
@property int loadVideosAgainFlag;

@end

@implementation VideoDownloadViewController

-(NSMutableArray*)loadVideosAgainArray {
    if(!_loadVideosAgainArray) _loadVideosAgainArray = [[NSMutableArray alloc]init];
    return _loadVideosAgainArray;
}

#pragma mark - View Lifecycle
-(void)viewDidLoad{
    NSError *error;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.geladeneVideoFiles = [[NSMutableArray alloc]init];
    self.videoArray = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    self.view.backgroundColor = [UIColor clearColor];

    self.errorFlag = 0;

    
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];

    self.progressViewOutlet.progressTintColor = [UIColor blueColor];
    self.gesamtfortschrittProgressView.progressTintColor = [UIColor blueColor];
    [self.activityIndicatorOutlet startAnimating ];
    
    [self checkVideos];
    
   }

-(void)checkVideos{
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    for(int i=0;i<self.videoArray.count;i++){
        Video *video =[self.videoArray objectAtIndex:i];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:video.datei];
        filePath = [filePath stringByAppendingString:@".mp4"];

            NSFileManager * fileManager = [NSFileManager new];
        NSError * error = 0;
        NSDictionary * attr = [fileManager attributesOfItemAtPath:filePath error:&error];
        unsigned long long size = [attr fileSize];

        if(!(size == video.filesize.longLongValue)){
            NSString *id = [NSString stringWithFormat:@"%i", i];
            [self.loadVideosAgainArray addObject:id];

        }

    }
    
    if(self.loadVideosAgainArray.count>0){
    
        self.loadVideosAgainFlag=1;
        self.geladeneVideos=0;
        [self downloadVideosWithID:0];
    } else {
        
        [self executeBlockOperation];
        
        self.progressViewOutlet.progress=1;
        self.gesamtfortschrittProgressView.progress=1;
        
        UIAlertController *alert = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:@"Alle Videos wurden richtig geladen"];
        
        [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [AlertHelper showAlert:alert];
//        [self presentViewController:alert animated:YES completion:nil];

    }
    
}



-(void)downloadVideosWithID:(int)id{

    if(self.errorFlag == 1) {
        NSLog(@"errorFlag==1");
        return;
    }
    
    int videoID  = (int)[[self.loadVideosAgainArray objectAtIndex:id] integerValue];
    self.aktuellGeladenesVideo = videoID;
      Video *video =[self.videoArray objectAtIndex:videoID];
    self.videoLabelOutlet.text = video.bezeichnung;
    // Create the request.
    NSString *urlString = [NSString stringWithFormat:@"http://www.orohygienesystem.de/videos/%@.mp4", video.datei];
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];


    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
    NSURLSessionDownloadTask *dataTask = [session downloadTaskWithRequest:theRequest];
    [dataTask resume];
    
    
    if(self.loadVideosAgainArray.count > 1){
        CGFloat video;
        
        if(self.geladeneVideos == 0){
            video = 1;
        } else {
            video = self.geladeneVideos;
        }
        
        //Update Gesamtfortschritt
        CGFloat progress = video / self.loadVideosAgainArray.count;
        self.gesamtfortschrittProgressView.progress = progress;
        self.gesamtfortschrittLabelOutlet.text = [NSString stringWithFormat:@"%0.0f%%", (video / self.loadVideosAgainArray.count)*100];
    }
    

    self.geladeneVideos = self.geladeneVideos + 1;


}



-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{

    // Create a NSFileManager instance
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    // Get the documents directory URL
    NSArray *documentURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [documentURLs firstObject];
    
    // Get the file name and create a destination URL
    NSString *sendingFileName = [downloadTask.originalRequest.URL lastPathComponent];
    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:sendingFileName];
    
    // Hold this file as an NSData and write it to the new location
    NSData *fileData = [NSData dataWithContentsOfURL:location];
    [fileData writeToURL:destinationUrl atomically:YES];
    
    NSLog(@"aktuell fertig geladenes Video:%@",sendingFileName);
    
    if(self.geladeneVideos < self.loadVideosAgainArray.count){
        
        [self downloadVideosWithID:(self.geladeneVideos)];
        
    } else {
        
        [self executeBlockOperation];
        
        self.gesamtfortschrittProgressView.progress = 1;
        self.gesamtfortschrittLabelOutlet.text = @"100%";
        
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Video-Download" andMessage:@"Es wurden alle Videos geladen!"];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self.activityIndicatorOutlet stopAnimating];
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [AlertHelper showAlert:alertView];
//        [self presentViewController:alertView animated:YES completion:nil];
    }

}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    CGFloat progress = (CGFloat)totalBytesWritten / (CGFloat)totalBytesExpectedToWrite;
    
    self.progressViewOutlet.progress = progress;
    
    if(self.loadVideosAgainArray.count==1){
        self.gesamtfortschrittProgressView.progress = progress;
        self.gesamtfortschrittLabelOutlet.text = [NSString stringWithFormat:@"%0.0f%%",  progress*100];
    }
    
    
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{

    
    if(error){
        self.errorFlag =1;
        NSLog(@"Error URL Session: %@", error.description);
        
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Video-Download" andMessage:error.localizedDescription];
    
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self.activityIndicatorOutlet stopAnimating];
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];

        [self presentViewController:alertView animated:YES completion:nil];
    }
    

}

//Nutzung zum UnitTest zur Übergabe der Expectation
-(void) executeBlockOperation {
    if(self.blockOperation != nil){
        NSOperationQueue * queue = [[NSOperationQueue alloc]init];
        [queue addOperation:self.blockOperation];
    }
}

@end
