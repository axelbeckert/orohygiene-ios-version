//
//  Anwendung+ccm.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Anwendung+ccm.h"

@implementation Anwendung (ccm)
+(Anwendung*) insertAnwendungInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Anwendung" inManagedObjectContext:managedObjectContext];
}
-(void)deleteAnwendung
{
    [self.managedObjectContext deleteObject:self];
}

-(void)setName:(NSString *)name{

    [self willChangeValueForKey:@"name"];
    [self setPrimitiveValue:name forKey:@"name"];
    self.index = [[name substringToIndex:1] uppercaseString];
    
    [self didChangeValueForKey:@"name"];
}
@end
