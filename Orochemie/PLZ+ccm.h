//
//  PLZ+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "PLZ.h"

@interface PLZ (ccm)
+(PLZ*)insertPLZInManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;
-(void)deletePLZ;
@end
