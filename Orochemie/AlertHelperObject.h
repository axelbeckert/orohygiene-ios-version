//
//  AlertHelperObject.h
//  orochemie 
//
//  Created by Axel Beckert on 08.06.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertHelperObject : NSObject
@property (nonatomic,strong) UIAlertController *alertController;
@property (nonatomic,copy) void(^block)(void);
@end
