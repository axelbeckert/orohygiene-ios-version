//
//  Oberflaeche+CoreDataClass.h
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Reiniger;

NS_ASSUME_NONNULL_BEGIN

@interface Oberflaeche : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Oberflaeche+CoreDataProperties.h"
