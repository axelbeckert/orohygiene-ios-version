//
//  Belag.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Belag.h"
#import "Reiniger.h"


@implementation Belag

@dynamic bezeichnung;
@dynamic reiniger;

@end
