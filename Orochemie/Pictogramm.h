//
//  Pictogramm.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkte, Reiniger;

NS_ASSUME_NONNULL_BEGIN

@interface Pictogramm : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Pictogramm+CoreDataProperties.h"
