//
//  VideoHelper.m
//  orochemie
//
//  Created by Axel Beckert on 28.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "VideoHelper.h"
#import "VideoDownloadViewController.h"

@interface VideoHelper ()
@property (nonatomic,strong) UIStoryboard *storyboard;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,strong) NSUserDefaults *standardUserDefaults;
@end

@implementation VideoHelper
#pragma mark- Check if Videos are there

-(BOOL)checkIfVideosAreThereWithStoryboard:(UIStoryboard*)storyboard andNavigationController:(UINavigationController*)navigationController{
    
    self.storyboard = storyboard;
    self.navigationController = navigationController;
    
    self.standardUserDefaults = [NSUserDefaults standardUserDefaults];

    NSString* videoDownload = [self.standardUserDefaults valueForKey:@"DownloadingVideos"];
    
    BOOL missingVideo = ![VideoHelper checkVideoFiles];
    
    if(missingVideo && videoDownload == nil ){
        
        [self decideToDownloadVideosOrWatchThemOnYouTube];

        return NO;
        
    } else if (missingVideo && [videoDownload isEqualToString:@"YES"]){
        
        [self notFinishedVideoDownloadAlert];
        
        return NO;
    
    }
    
    return YES;
}

-(void) decideToDownloadVideosOrWatchThemOnYouTube {
    
    UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:@"Um die Anwendungsvideos sehen zu können müssen diese einmalig geladen werden (ca. 50MB). Als Option können Sie die Videos auch bei YouTube sehen (benötigt immer eine Online-Verbindung). Bitte treffen Sie eine Auswahl:"];
    
    [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Downloaden" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self downloadVideos];
    }]];
    
    [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"YouToube" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self watchVideosOnYouTube];
    }]];
    
    
    [AlertHelper showAlert:alertView];
}

-(void)notFinishedVideoDownloadAlert {
    
    UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:@"Die Anwendungsvideos wurden nicht vollständig geladen. Soll der Download fortgesetzt werden?"];
    
    [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Downloaden" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self downloadVideos];
    }]];
    
    [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Jetzt nicht" andCompletionHandler:^(UIAlertAction *alertAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    
    [AlertHelper showAlert:alertView];
    
}

-(void) watchVideosOnYouTube {

    //Setzen der Entscheidung für YouTube
    [self.standardUserDefaults setValue:@"YES" forKey:@"YouTubeVideos"];
    [self.standardUserDefaults setValue:@"NO" forKey:@"DownloadingVideos"];
    [self.standardUserDefaults synchronize];
    
}

-(void)downloadVideos {

    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    VideoDownloadViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoDownloadViewController"];
    controller.view.backgroundColor = [UIColor clearColor];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.navigationController.definesPresentationContext = YES;
    
    [self.navigationController presentViewController:controller animated:YES completion:nil];
    
    //Setzen der Entscheidung für den Download
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setValue:@"YES" forKey:@"DownloadingVideos"];
    [pref setValue:@"NO" forKey:@"YouTubeVideos"];
    [pref synchronize];
    
}

+(BOOL)checkVideoFiles {

    NSError *error;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSArray *videoArray = [[delegate managedObjectContext]executeFetchRequest:fetchRequest error:&error];
    
    if(error){
        [AlertHelper showErrorAlert:error];
    }
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    for(int i=0;i<videoArray.count;i++){
        Video *video =[videoArray objectAtIndex:i];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:video.datei];
        filePath = [filePath stringByAppendingString:@".mp4"];
        
        NSFileManager * fileManager = [NSFileManager new];
        NSError * error = 0;
        NSDictionary * attr = [fileManager attributesOfItemAtPath:filePath error:&error];
        unsigned long long size = [attr fileSize];
        
        if(!(size == video.filesize.longLongValue)){
            return NO;
        }
        
    }
    
    return YES;
}

@end
