//
//  BestellPosition.h
//  orochemie
//
//  Created by Axel Beckert on 25.09.13.
//  Copyright (c) 2013 ccmagnus Mediendesign. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Produktvarianten.h"

@interface BestellPosition : NSObject
@property(nonatomic,strong) Produktvarianten *produktVariante;
@property(nonatomic,strong) NSString *bestellMenge;
@end
