//
//  Reinigungsart.h
//  orochemie
//
//  Created by Axel Beckert on 25.02.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Verbrauch;

@interface Reinigungsart : NSManagedObject

@property (nonatomic, retain) NSString * bezeichnung;
@property (nonatomic, retain) NSString * bezeichnung2;
@property (nonatomic, retain) NSString * kennung;
@property (nonatomic, retain) NSNumber * sortierung;
@property (nonatomic, retain) NSNumber * wassermenge;
@property (nonatomic, retain) NSSet *verbrauch;
@end

@interface Reinigungsart (CoreDataGeneratedAccessors)

- (void)addVerbrauchObject:(Verbrauch *)value;
- (void)removeVerbrauchObject:(Verbrauch *)value;
- (void)addVerbrauch:(NSSet *)values;
- (void)removeVerbrauch:(NSSet *)values;

@end
