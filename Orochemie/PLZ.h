//
//  PLZ.h
//  orochemie
//
//  Created by Axel Beckert on 17.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Berater;

@interface PLZ : NSManagedObject

@property (nonatomic, retain) NSString * plz;
@property (nonatomic, retain) NSString * ort;
@property (nonatomic, retain) NSString * land;
@property (nonatomic, retain) Berater *berater;

@end
