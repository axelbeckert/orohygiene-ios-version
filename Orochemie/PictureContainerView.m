//
//  PictureContainerView.m
//  Orochemie
//
//  Created by Axel Beckert on 03.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "PictureContainerView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PictureContainerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    self.scrollView.layer.borderColor = [[UIColor clearColor]CGColor];
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event {
    NSLog(@"%s",__PRETTY_FUNCTION__);

    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        return _scrollView;
        NSLog(@"%s",__PRETTY_FUNCTION__);
    }

    return view;
}



@end
