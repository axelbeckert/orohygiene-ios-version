//
//  AppDelegate.m
//  Orochemie
//
//  Created by Axel Beckert on 22.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "AppDelegate.h"
#import "MakeData+Reiniger.h"
#import "MakeData+Berater.h"
#import "Produkte+ccm.h"
#import "Reiniger+ccm.h"
#import "MerklisteQuickSave.h"
#import "BasisMerkliste+ccm.h"
#import "MerklistenCoreDataHelper.h"
#import "SavedMerkliste+ccm.h"
#import "MerklistenProdukt+ccm.h"
#import "MerklistenReiniger+ccm.h"
#import <Pushbots/Pushbots.h>
#import "ChecklistenCollector.h"
#import "WirksamkeitenCollector.h"
#import "Logger.h"
#import "KrankheitserregerCollector.h"
#import "OberflaechenCollector.h"

@interface AppDelegate()
@property (nonatomic,strong)NSMutableArray *savedMerklistenArray;
@property (nonatomic,strong)MerklisteQuickSave *savedBasisMerkliste;
@property (nonatomic,strong) id<GAITracker> tracker;
@end



@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

//lazy instatiation des Cache
-(NSCache *)cache
{
    if (!_cache) _cache = [[NSCache alloc] init];
    return _cache;
}

//lazy instatiation des Loggers
-(Logger *)logger
{
    if (!_logger) {
        _logger = [Logger new];
        _logger.isLoggingActive = 0;
    }
    return _logger;
}

////lazy instatiation saveMerklistenArray
//
-(NSMutableArray*)savedMerklistenArray
{
    if(!_savedMerklistenArray) _savedMerklistenArray = [NSMutableArray new];
    return _savedMerklistenArray;
}

-(MerklisteQuickSave*)savedBasisMerkliste{

    if(!_savedBasisMerkliste) _savedBasisMerkliste = [MerklisteQuickSave new];
    return _savedBasisMerkliste;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions

{
    [self.logger logMessage:@"Application starts"];
    
    //Google Analytics
    [self startGoogleAnalytics];
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    ////NSLog(@"Version: %@", version);

    [oroThemeManager setSharedTheme:[oroTheme2 new]];

    //Zeit setzen nach deren Ablauf das Laden von Inhalten aus dem Internet abgebrochen werden soll
    self.timeToStopLoading = 25.0;
    
    NSUserDefaults *dbVersionDefaults = [NSUserDefaults standardUserDefaults];
    
    // holen der Versionsnummer
    NSString* oroDbVersionNumber = [dbVersionDefaults valueForKey:@"dBVersionNumber"];

    //NSLog(@"oroDBVersionNumber = %@", oroDbVersionNumber);


    
//    NSString* file = [[JSMCoreDataHelper directoryForDatabaseFilename] stringByAppendingPathComponent:[JSMCoreDataHelper databaseFilename]];

    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"database.sqlite"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]];
    
//    //NSLog(@"fileExists == %i", fileExists);
    //  

    if(![oroDbVersionNumber isEqualToString:version] && fileExists)
    {
     
        //Löschen der Datenbank und dann direktes neu erstellen um den aktuellen Datenstand herzustellen
        NSError *error;

        NSLog(@"DB gelöscht");
        
        BOOL deleteDb=[[NSFileManager defaultManager] removeItemAtPath:[storeURL path] error:&error];;
        
        if(deleteDb) //NSLog(@"DB Gelöscht");
        if(error){
            NSLog(@"Löschen war nicht erfolgreich");
        };
        
        [self checkIfChecklistsAreThere];
        
        [self populateAppDataWithVersionNumber:version];

        //Restore der Merkslisten nach DB löschen
        [self makeMerklistenData];
        [self restoreMerklistenDataFromMerklistenDB];
        
    }
    
    


    
    if(!fileExists)
    {

        
        NSError *error;
        //NSLog(@"Die Datei wird erstellt, weil sie nicht existiert");
        if (error) {
            //NSLog(@"Fehler: %@", error.localizedDescription);
            return nil;
        }
        [self checkIfChecklistsAreThere];

        [self populateAppDataWithVersionNumber:version];
        
        //Basis-Merksliste erstellen
        [self makeMerklistenData];

        
    }
    [self makeMerklistenData];
    
    //UDID Generator
    
//     MakeData *testUiID = [MakeData new];
//    for(int i = 0; i<2;i++){
//       
//        NSLog(@"UDID Test: %@", [testUiID createUUID]);
//    }
    
    [Pushbots getInstance];
    
    // set Badge to 0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    // reset badge on the server
    [[Pushbots getInstance] resetBadgeCount];
    
//    NSDictionary * userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//    if(userInfo) {
//        // Notification Message
//        NSString* notificationMsg = [userInfo valueForKey:@"message"];
//        // Custom Field
//        NSString* title = [userInfo valueForKey:@"title"];
//        NSLog(@"Notification Msg is %@ and Custom field title = %@", notificationMsg , title);
//    }
//NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);

    
    [self checkIfChecklistsAreThere];
    [self collectWirksamkeitenFromServer];
    [self retrieveKrankheisterreger];
    [self retrieveOberflaechen];
    
    return YES;
}

-(void)populateAppDataWithVersionNumber:(NSString*)versionNumber{

    MakeData *data = [MakeData new];
    [data makeData];
    [data makeReinigerData];
    [data makeBeraterData];
    
    //Setzen der Datenbankversion
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:versionNumber forKey:@"dBVersionNumber"];
    [prefs synchronize];
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //NSLog(@"applicationDidBecomeActive");
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveMerklistenDataToMerklistenDB];
    
    // set Badge to 0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    // reset badge on the server
    [[Pushbots getInstance] resetBadgeCount];
    
    [self.checklistenCollector startCollectingChecklists];
    [self.krankheitserregerCollector startCollectingKrankheitserreger];
    [self.oberflaechenCollector startCollectingOberflaechen];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //[self saveMerklistenDataToMerklistenDB];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[self saveMerklistenDataToMerklistenDB];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
    [self saveMerklistenDataToMerklistenDB];

}

#pragma mark - Desinfktionsmittelwirksamkeiten

-(void) collectWirksamkeitenFromServer{
    self.wirksamkeitenCollector = [WirksamkeitenCollector new];
    self.wirksamkeitenCollector.showSVProgressHUD = 0;
    [self.wirksamkeitenCollector startCollectingWirksamkeiten];
}

#pragma mark - Checklists

-(void) checkIfChecklistsAreThere{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    self.checklistenCollector = [ChecklistenCollector new];
    self.checklistenCollector.showSVProgressHUD = 0;
    [SVProgressHUD dismiss];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Checkliste"];
        NSError *error = nil;
    
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if(results.count==0){
        [self.checklistenCollector startCollectingChecklists];
    }
}

#pragma mark - Krankheitserreger
-(void) retrieveKrankheisterreger {
    self.krankheitserregerCollector = [KrankheitserregerCollector new];
    
    if(![self.krankheitserregerCollector retrieveObjectsFromDatabaseForEntity]){
     self.krankheitserregerCollector.collectorType = FROM_FILE;
    }

    [self.krankheitserregerCollector startCollectingKrankheitserreger];
    
}

#pragma mark - Oberflächen und Beläge
-(void) retrieveOberflaechen {
    
    self.oberflaechenCollector = [OberflaechenCollector new];
    if(![self.oberflaechenCollector retrieveObjectsFromDatabaseForEntity]){
        self.oberflaechenCollector.collectorType = FROM_FILE;
    }
    
    [self.oberflaechenCollector startCollectingOberflaechen];
    
}


#pragma mark - Drehbewegungen


#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{

#else
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
#endif

  NSUInteger orientations = UIInterfaceOrientationMaskPortrait;
    
    if(self.window.rootViewController){
        UIViewController *presentedViewController ;
        if ([self.window.rootViewController isKindOfClass:([UINavigationController class])])
        {
            presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        }
        else if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]){
            UITabBarController *controller = (UITabBarController*)self.window.rootViewController;
            
            id selectedController =  [controller presentedViewController];
            
            if (!selectedController) {
                selectedController = [controller selectedViewController];
            }
            
            if ([selectedController isKindOfClass:([UINavigationController class])])
            {
                presentedViewController = [[(UINavigationController *)selectedController viewControllers] lastObject];
            }
            else{
                presentedViewController = selectedController;
            }
        }
        else
        {
            presentedViewController = self.window.rootViewController;
        }
        
        if ([presentedViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
            orientations = [presentedViewController supportedInterfaceOrientations];
        }
    }
    
    return orientations;
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"database.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {

        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Merklisten Data
-(void)makeMerklistenData {
    //globale Merkliste allocieren
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSPredicate * merklistenPredicate = [NSPredicate predicateWithFormat:@"bezeichnung CONTAINS[c] %@",@"basisMerkliste" ];
    fetchRequest.predicate = merklistenPredicate;
    
    NSArray *merklistenArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    self.merkliste = [merklistenArray lastObject];

}

-(void)saveMerklistenDataToMerklistenDB{

    //Globale Merkliste
    NSFetchRequest *globaleMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSPredicate * globaleMerklistePredicate = [NSPredicate predicateWithFormat:@"bezeichnung CONTAINS[c] %@",@"basisMerkliste" ];
    globaleMerklisteFetchRequest.predicate = globaleMerklistePredicate;
    
    NSArray *globaleMerklisteArray = [self.managedObjectContext executeFetchRequest:globaleMerklisteFetchRequest error:nil];
    
    Merkliste *globaleMerkliste = [globaleMerklisteArray lastObject];
    
    //Merklisten DB öffnen und BasisMerkliste auslesen
    NSError *error;
    MerklistenCoreDataHelper *merklistenHelper = [MerklistenCoreDataHelper new];

    NSString *filePath = [merklistenHelper applicationDocumentsDirectory].path;
    filePath = [filePath stringByAppendingString:@"/merklisten_database.sqlite"];
    
    BOOL dbFileExists = [[NSFileManager defaultManager]fileExistsAtPath:filePath];
    
    if(dbFileExists){
    
    NSFetchRequest *basisMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([BasisMerkliste class])];
    
    NSArray *basisMerklistenArray =[merklistenHelper.managedObjectContext executeFetchRequest:basisMerklisteFetchRequest error:&error];
    
    //Objekte der Basismerkliste löschen
    if(basisMerklistenArray.count>0){
        for(int i = 0;i<basisMerklistenArray.count;i++){
            
            BasisMerkliste *basisMerkliste = [basisMerklistenArray objectAtIndex:i];
            [basisMerkliste deleteBasisMerkliste];
            basisMerkliste = nil;
            
        }
    }
    
    [merklistenHelper saveContext];
    }
    
    //BasisMerkliste neu erstellen
    for(int i = 0;i<[globaleMerkliste.produkte allObjects].count;i++){
        Produkte *p = [[globaleMerkliste.produkte allObjects]objectAtIndex:i];
        BasisMerkliste *basisMerkliste = [BasisMerkliste insertBasisMerklisteInManagedObjectContext:merklistenHelper.managedObjectContext];
        basisMerkliste.uid = p.uid;
        basisMerkliste.produktart=@"P";
        
    }
    
    for(int i = 0;i<[globaleMerkliste.reiniger allObjects].count;i++){
        Reiniger *r = [[globaleMerkliste.reiniger allObjects]objectAtIndex:i];
        BasisMerkliste *basisMerkliste = [BasisMerkliste insertBasisMerklisteInManagedObjectContext:merklistenHelper.managedObjectContext];
        basisMerkliste.uid = r.uid;
         basisMerkliste.produktart=@"R";
    }
    

    
    NSFetchRequest *savedMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([SavedMerkliste class])];
    
    if(dbFileExists){
        NSArray *savedMerklistenArray = [merklistenHelper.managedObjectContext executeFetchRequest:savedMerklisteFetchRequest error:&error];
        
        //Objekte der SavedMerkliste löschen
        if(savedMerklistenArray.count>0){
            for(int i = 0;i<savedMerklistenArray.count;i++){
                
                SavedMerkliste *savedMerkliste = [savedMerklistenArray objectAtIndex:i];
                [savedMerkliste deleteSavedMerkliste];
                savedMerkliste = nil;
                
            }
        }
    
    }
    
    NSFetchRequest *alleMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSPredicate * alleMerklistePredicate = [NSPredicate predicateWithFormat:@"bezeichnung != %@",@"basisMerkliste" ];
    alleMerklisteFetchRequest.predicate = alleMerklistePredicate;
    
    NSArray *alleMerklisteArray = [self.managedObjectContext executeFetchRequest:alleMerklisteFetchRequest error:nil];
    
    
    for(int f = 0;f<alleMerklisteArray.count;f++){
        Merkliste *m = [alleMerklisteArray objectAtIndex:f];
        
        SavedMerkliste *merkliste = [SavedMerkliste insertSavedMerklisteInManagedObjectContext:merklistenHelper.managedObjectContext];
        merkliste.bezeichnung = m.bezeichnung;
        
        for(int d = 0;d<[m.produkte allObjects].count;d++){
            Produkte *p1 = [[m.produkte allObjects]objectAtIndex:d];
            MerklistenProdukt *p2 = [MerklistenProdukt insertMerklistenProduktInManagedObjectContext:merklistenHelper.managedObjectContext];
            p2.uid = p1.uid;
            [merkliste addMerklistenProduktObject:p2];
            
        }
        
        
        for(int e = 0;e<[m.reiniger allObjects].count;e++){
            Reiniger *r1 = [[m.reiniger allObjects]objectAtIndex:e];
            MerklistenReiniger *r2 = [MerklistenReiniger insertMerklistenProduktInManagedObjectContext:merklistenHelper.managedObjectContext];
            
            r2.uid = r1.uid;
            
            [merkliste addMerklistenReinigerObject:r2];
            
        }
        
       
    }
    
    [merklistenHelper saveContext];
    merklistenHelper = nil;

    //NSLog(@"Merklisten wurden gesichert");

}

-(void)restoreMerklistenDataFromMerklistenDB{

    //Hole gesicherte BasisMerklistenDaten aus der Merklisten-DB
    MerklistenCoreDataHelper *merklistenHelper = [MerklistenCoreDataHelper new];
    
    NSString *filePath = [merklistenHelper applicationDocumentsDirectory].path;
    filePath = [filePath stringByAppendingString:@"/merklisten_database.sqlite"];
    
    BOOL dbFileExists = [[NSFileManager defaultManager]fileExistsAtPath:filePath];
    
    


   
    if(dbFileExists){
        
        NSFetchRequest *basisMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([BasisMerkliste class])];
        
        NSArray *basisMerklisteArray = [merklistenHelper.managedObjectContext executeFetchRequest:basisMerklisteFetchRequest error:nil];
        
        if(basisMerklisteArray.count>0){
            for(int i=0; i<basisMerklisteArray.count;i++){
                BasisMerkliste *bM = [basisMerklisteArray objectAtIndex:i];
                if([bM.produktart isEqualToString:@"P"]){
                   
                    NSFetchRequest *produktFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Produkte class])];
                    NSPredicate *produktPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",bM.uid];
                    produktFetch.predicate = produktPredicate;
                    
                    
                    NSArray *produktArray = [self.managedObjectContext executeFetchRequest:produktFetch error:nil];
                    
                    if(produktArray.count>0){
                        Produkte *p = [produktArray lastObject];
                        [self.merkliste addProdukteObject:p];
                    }

                } else {
                    NSFetchRequest *reinigerFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];
                    NSPredicate *reinigerPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",bM.uid];
                    reinigerFetch.predicate = reinigerPredicate;
                    
                    
                    NSArray *reinigerArray = [self.managedObjectContext executeFetchRequest:reinigerFetch error:nil];
                    
                    if(reinigerArray.count>0){
                        Reiniger *r = [reinigerArray lastObject];
                        [self.merkliste addReinigerObject:r];
                    }
                    
                }
                
            }
            
            NSFetchRequest *savedMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([SavedMerkliste class])];
            
            NSArray *savedMerklisteArray = [merklistenHelper.managedObjectContext executeFetchRequest:savedMerklisteFetchRequest error:nil];
            
            if(savedMerklisteArray.count>0){
                for(int i=0; i<savedMerklisteArray.count;i++){
                    
                    SavedMerkliste *savedMerkliste = [savedMerklisteArray objectAtIndex:i];
                    Merkliste *merkliste = [Merkliste insertMerklisteInManagedObjectContext:self.managedObjectContext];
                    
                    merkliste.bezeichnung = savedMerkliste.bezeichnung;
                    
                    for(int f = 0;f<[savedMerkliste.merklistenProdukt allObjects].count;f++){
                        
                        
                        MerklistenProdukt *mP = [[savedMerkliste.merklistenProdukt allObjects]objectAtIndex:f];
                        NSFetchRequest *produktFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Produkte class])];
                        NSPredicate *produktPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",mP.uid];
                        produktFetch.predicate = produktPredicate;
                        
                        
                        NSArray *produktArray = [self.managedObjectContext executeFetchRequest:produktFetch error:nil];
                        
                        if(produktArray.count>0){
                            Produkte *p = [produktArray lastObject];
                            [merkliste addProdukteObject:p];
                        }
                        
                    }
                    
                    for(int f = 0;f<[savedMerkliste.merklistenReiniger allObjects].count;f++){
                        
                        
                        MerklistenReiniger *mR = [[savedMerkliste.merklistenReiniger allObjects]objectAtIndex:f];
                        NSFetchRequest *reinigerFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];
                        NSPredicate *reinigerPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",mR.uid];
                        reinigerFetch.predicate = reinigerPredicate;
                        
                        //NSLog(@"Reiniger UID: %@", mR.uid);
                        
                        
                        NSArray *reinigerArray = [self.managedObjectContext executeFetchRequest:reinigerFetch error:nil];
                        
                        if(reinigerArray.count>0){
                            Reiniger *r = [reinigerArray lastObject];
                            [merkliste addReinigerObject:r];
                        }
                        
                    }
                    
                    
                }
            
            }

            
        }
        
    }
    
    
    
    //NSLog(@"Merklisten wurden wieder hergestellt");

    
    merklistenHelper = nil;
    

}
-(void)getMerklistenDataBeforeDeletingTheStore{
    //Globale Merkliste
    NSFetchRequest *globaleMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSPredicate * globaleMerklistePredicate = [NSPredicate predicateWithFormat:@"bezeichnung CONTAINS[c] %@",@"basisMerkliste" ];
    globaleMerklisteFetchRequest.predicate = globaleMerklistePredicate;
    
    NSArray *globaleMerklisteArray = [self.managedObjectContext executeFetchRequest:globaleMerklisteFetchRequest error:nil];
    
    Merkliste *globaleMerkliste = [globaleMerklisteArray lastObject];

 
    
    for(int i = 0;i<[globaleMerkliste.produkte allObjects].count;i++){
        Produkte *p = [[globaleMerkliste.produkte allObjects]objectAtIndex:i];
        [self.savedBasisMerkliste addProduktUIDToMerkliste:p.uid];

    }
    
    for(int i = 0;i<[globaleMerkliste.reiniger allObjects].count;i++){
        Reiniger *r = [[globaleMerkliste.reiniger allObjects]objectAtIndex:i];
        [self.savedBasisMerkliste addReinigerUIDToMerkliste:r.uid];

        
    }
    

    
    //Alle weiteren Merklisten

    
    NSFetchRequest *alleMerklisteFetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Merkliste class])];
    NSPredicate * alleMerklistePredicate = [NSPredicate predicateWithFormat:@"bezeichnung != %@",@"basisMerkliste" ];
    alleMerklisteFetchRequest.predicate = alleMerklistePredicate;
    
    NSArray *alleMerklisteArray = [self.managedObjectContext executeFetchRequest:alleMerklisteFetchRequest error:nil];
    
    
    for(int f = 0;f<alleMerklisteArray.count;f++){
        Merkliste *m = [alleMerklisteArray objectAtIndex:f];
     
        MerklisteQuickSave *merkliste = [MerklisteQuickSave new];
        merkliste.bezeichnung = m.bezeichnung;
        
            for(int d = 0;d<[m.produkte allObjects].count;d++){
                Produkte *p1 = [[m.produkte allObjects]objectAtIndex:d];
                [merkliste addProduktUIDToMerkliste:p1.uid];
                
            }
       
       
            for(int e = 0;e<[m.reiniger allObjects].count;e++){
                Reiniger *r1 = [[m.reiniger allObjects]objectAtIndex:e];
                [merkliste addReinigerUIDToMerkliste:r1.uid];
                
            }
        
        [self.savedMerklistenArray addObject:merkliste];
        
    }
    
    

}
-(void)saveMerklistenDataAfterDeletingTheStore{

    //Basismerkliste
    
    //Hole die Produkte aus der DB und Speichere diese in der BasisMekliste
    for(int i=0;i<[self.savedBasisMerkliste getProduktUIDForMerkliste].count;i++){
        
        NSString *produktUID = [[self.savedBasisMerkliste getProduktUIDForMerkliste]objectAtIndex:i];
        NSFetchRequest *produktFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Produkte class])];
        NSPredicate *produktPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",produktUID];
        produktFetch.predicate = produktPredicate;
        
        
        NSArray *produktArray = [self.managedObjectContext executeFetchRequest:produktFetch error:nil];
        
        if(produktArray.count>0){
            Produkte *p = [produktArray lastObject];
            [self.merkliste addProdukteObject:p];
        }
        
    }
    
    //Hole die Reiniger aus der DB und Speichere diese in der BasisMekliste
    for(int i=0;i<[self.savedBasisMerkliste getReinigerUIDForMerkliste].count;i++){
        
        NSString *reinigerUID = [[self.savedBasisMerkliste getReinigerUIDForMerkliste]objectAtIndex:i];
        NSFetchRequest *reinigerFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];
        NSPredicate *reinigerPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",reinigerUID];
        reinigerFetch.predicate = reinigerPredicate;
        
        
        NSArray *reinigerArray = [self.managedObjectContext executeFetchRequest:reinigerFetch error:nil];
        
        if(reinigerArray.count>0){
            Reiniger *r= [reinigerArray lastObject];
            [self.merkliste addReinigerObject:r];
        }
        
    }
    
    //Alle weiteren Merklisten
    
    for(int a=0; a<self.savedMerklistenArray.count;a++)
    {
        
        MerklisteQuickSave *merklisteQuickSave = [self.savedMerklistenArray objectAtIndex:a];
    
        Merkliste *merkliste = [Merkliste insertMerklisteInManagedObjectContext:self.managedObjectContext];
        merkliste.bezeichnung = merklisteQuickSave.bezeichnung;
        
            for(int f = 0;f<[merklisteQuickSave getProduktUIDForMerkliste].count;f++){
                
                
                NSString *produktUID = [[merklisteQuickSave getProduktUIDForMerkliste]objectAtIndex:f];
                NSFetchRequest *produktFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Produkte class])];
                NSPredicate *produktPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",produktUID];
                produktFetch.predicate = produktPredicate;
                
                
                NSArray *produktArray = [self.managedObjectContext executeFetchRequest:produktFetch error:nil];
                
                if(produktArray.count>0){
                    Produkte *p = [produktArray lastObject];
                    [merkliste addProdukteObject:p];
                }
                
            }
        
        for(int f = 0;f<[merklisteQuickSave getReinigerUIDForMerkliste].count;f++){
            
            
            NSString *reinigerUID = [[merklisteQuickSave getReinigerUIDForMerkliste]objectAtIndex:f];
            NSFetchRequest *reinigerFetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Reiniger class])];
            NSPredicate *reinigerPredicate = [NSPredicate predicateWithFormat:@"uid CONTAINS[c] %@",reinigerUID];
            reinigerFetch.predicate = reinigerPredicate;
            
            
            NSArray *reinigerArray = [self.managedObjectContext executeFetchRequest:reinigerFetch error:nil];
            
            if(reinigerArray.count>0){
                Reiniger *r= [reinigerArray lastObject];
                [merkliste addReinigerObject:r];
            }
            
        }
        
    }
    [self saveContext];

}

-(void)startGoogleAnalytics {
    
    
    NSUserDefaults *dbVersionDefaults = [NSUserDefaults standardUserDefaults];
    
    // Prpüfen wie der Status des Trackings aktuell ist:
    NSString* googleTracking = [dbVersionDefaults valueForKey:@"googleTracking"];
   
    if([googleTracking isEqualToString:@"YES"] || !googleTracking){
        
        // 1
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        
        // 2
        [[GAI sharedInstance].logger setLogLevel:kGAILogLevelError];
        
        // 3
        [GAI sharedInstance].dispatchInterval = 20;
        
        
        // 4
        //id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-50691592-1"];
        self.tracker =[[GAI sharedInstance] trackerWithTrackingId:@"UA-1651419-6"];
        [self.tracker setAllowIDFACollection:NO];
      
        
        [[GAI sharedInstance] setOptOut:NO];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:@"YES" forKey:@"googleTracking"];
        [prefs synchronize];

    }



}

-(void)onReceivePushNotification:(NSDictionary *) pushDict andPayload:(NSDictionary *)payload {
//    [payload valueForKey:@"title"];
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"New Alert !" message:[pushDict valueForKey:@"alert"] delegate:self cancelButtonTitle:@"Thanks !" otherButtonTitles: @"Open",nil];
//    [message show];
}


//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//    if([title isEqualToString:@"Open"]) {
//        [[Pushbots getInstance] OpenedNotification];
//        // set Badge to 0
//        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//        // reset badge on the server
//        [[Pushbots getInstance] resetBadgeCount];
//    }
//}

    
@end
