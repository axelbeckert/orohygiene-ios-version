//
//  Wirksamkeit+ccm.h
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Wirksamkeit.h"

@interface Wirksamkeit (ccm)
+(Wirksamkeit*) insertWirksamkeitInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteWirksamkeit;
@end
