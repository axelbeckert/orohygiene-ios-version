//
//  Oberflaeche+CoreDataProperties.h
//  orochemie 
//
//  Created by Axel Beckert on 22.04.18.
//  Copyright © 2018 orochemie GmbH & Co.KG. All rights reserved.
//
//

#import "Oberflaeche+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Oberflaeche (CoreDataProperties)

+ (NSFetchRequest<Oberflaeche *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *bezeichnung;
@property (nullable, nonatomic, copy) NSString *index;
@property (nullable, nonatomic, copy) NSString *kategorie;
@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *tstamp;
@property (nullable, nonatomic, retain) NSSet<Reiniger *> *reiniger;

@end

@interface Oberflaeche (CoreDataGeneratedAccessors)

- (void)addReinigerObject:(Reiniger *)value;
- (void)removeReinigerObject:(Reiniger *)value;
- (void)addReiniger:(NSSet<Reiniger *> *)values;
- (void)removeReiniger:(NSSet<Reiniger *> *)values;

@end

NS_ASSUME_NONNULL_END
