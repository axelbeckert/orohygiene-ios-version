//
//  Oberflaeche+ccm.m
//  oro Reinigung
//
//  Created by Axel Beckert on 24.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "Oberflaeche+ccm.h"

@implementation Oberflaeche (ccm)

+(Oberflaeche*) insertOberflaecheInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Oberflaeche" inManagedObjectContext:managedObjectContext];
}
-(void)deleteOberflaeche
{
    [self.managedObjectContext deleteObject:self];
}

-(void)setBezeichnung:(NSString*)bezeichnung
{
    [self willChangeValueForKey:@"bezeichnung"];
    [self setPrimitiveValue:bezeichnung forKey:@"bezeichnung"];
    self.index = [[bezeichnung substringToIndex:1] uppercaseString];
    [self didChangeValueForKey:@"bezeichnung"];
}
@end
