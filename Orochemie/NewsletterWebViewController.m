//
//  NewsletterWebViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "NewsletterWebViewController.h"
#import "Newsletter.h"
#import "BNHtmlPdfKit.h"

@interface NewsletterWebViewController () <UIWebViewDelegate, UIAlertViewDelegate>
@property (nonatomic,strong) NSTimer *timer;
@property(nonatomic,strong) AppDelegate *appDelegate;
@property(nonatomic,assign) BOOL videoIsOn;
@property int loadingCounter;
@property(nonatomic,strong) BNHtmlPdfKit *pdfCreator;
@property(nonatomic,strong)  UIBarButtonItem* printActionButton;
@end

@implementation NewsletterWebViewController

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    //Title Label
    
    UILabel* tlabel=[oroThemeManager customizeTitleLabel];
    tlabel.text = @"Newsletter";
    self.navigationItem.titleView=tlabel;
    
    
        self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

   
    
    self.newsletterWebViewOutlet.delegate = self;

    
    self.printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
    
    NSArray *buttonArray = @[self.printActionButton, [oroThemeManager homeBarButton:self]];
    //self.navigationItem.rightBarButtonItem = homeBarButton;
    self.navigationItem.rightBarButtonItems = buttonArray;
    
    [self loadRequest];


    
	// Do any additional setup after loading the view.
}


-(void) loadRequest
{
    if([ConnectionCheck hasConnectivity]) {
        NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/newsletter_archiv/%@",self.newsletter.newsletterUrl];
        NSLog(@"newsletterUrl: %@", urlString);
        NSURL *newsletterUrl = [NSURL URLWithString:urlString];
        


        
        [self.newsletterWebViewOutlet loadRequest:[NSURLRequest requestWithURL:newsletterUrl]];
        
        
        
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung verfügbar"];
    }
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    
    NSString *string = [NSString stringWithFormat:@"%@",request ];
    NSRange range = [string rangeOfString:@"youtube.com"];
    //NSLog(@"shouldStartLoadWithRequest - Range Video: %i",range.length);
    if(range.length!=0)
    {
        self.videoIsOn = YES;
        //NSLog(@"videoIsOn=%i",self.videoIsOn);
        
        if([self.timer isValid]){
            [self.timer invalidate];
            
            //NSLog(@"shouldStartLoadWithRequest - Timer aus");
        }
        
    }
    
    
    return YES;
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde
    
    self.loadingCounter++;
    if(!self.videoIsOn){
        if(![self.timer isValid])
        {
            //NSLog(@"webViewDidStartLoad - Timer an (%i)",self.loadingCounter);
            self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
        }
    }
    
    
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:@"Fehler beim Laden"];
        self.loadingCounter=0;
    //NSLog(@"error:%@",error);
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadingCounter--;
    //NSLog(@"loadingCounter: %i",self.loadingCounter);
    
    
    
    if(self.loadingCounter==0)
    {
        [self.timer invalidate]; //Timer ausschalten
        self.timer=nil;
        self.videoIsOn=NO;
        //NSLog(@"webViewDidFinishLoad - Timer aus");
    }
    
    
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)cancelWeb
{
    //NSLog(@"%s",__PRETTY_FUNCTION__);

    self.newsletterWebViewOutlet.hidden=YES;
     [self.timer invalidate]; //Timer ausschalten
    
    UIAlertController *alert = [AlertHelper getUIActionSheetWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Erneut laden" andCompletionHandler:
    ^(UIAlertAction *alertAction) {
        [self loadRequest];
        [self.timer invalidate];
        self.timer=nil;
        self.newsletterWebViewOutlet.hidden=NO;
        [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    }]];
    
    [AlertHelper showAlert:alert];
    
    [SVProgressHUD dismiss];

}


- (IBAction)refershAction:(id)sender
{
    [self.newsletterWebViewOutlet reload];
}

- (IBAction)backAction:(id)sender
{
    [self.newsletterWebViewOutlet goBack];
}
- (IBAction)forwardAction:(id)sender
{
    [self.newsletterWebViewOutlet goForward];
}

- (IBAction)stopLoadingAction:(id)sender
{
    [self.newsletterWebViewOutlet stopLoading];
}

- (void)printAction:(id)sender
{
   
    
    
    NSString *pdfString = [self.newsletter.newsletterUrl stringByReplacingOccurrencesOfString:@".htm" withString:@".pdf"];
    
    //Suche den Punkt innerhalb des Strings
    
    NSCharacterSet* searchCharacters = [NSCharacterSet characterSetWithCharactersInString:@"."];
    //an welchem Teil des Strings befindet sich der Punkt
    NSRange resultRange = [pdfString rangeOfCharacterFromSet:searchCharacters];
    
    
    if(resultRange.length != 0)
    {
        // Suche die Jahreszahl aus dem URL String raus
        NSString * newsletterStringPart1 = [pdfString substringWithRange:NSMakeRange(resultRange.location-4,4)];
        //Ersetze die Jahrtausend-Anagbe durch nichts
        NSString * newsletterStringPart2 = [newsletterStringPart1 stringByReplacingOccurrencesOfString:@"20" withString:@""];
        //Ersetze innerhalb des pdfUrlStrings die Jahreszahl und füge nur das jahr ohne Jahrtausend wieder ein
        pdfString = [pdfString stringByReplacingOccurrencesOfString:newsletterStringPart1 withString:newsletterStringPart2];
        
    }
    

    NSString *urlString = [NSString stringWithFormat:@"https://www.orochemie.de/newsletter_archiv/download/orochemie_%@",pdfString];
    //NSLog(@"URLString:%@",urlString);
    NSURL *pdfNewsletterUrl = [NSURL URLWithString:urlString];
    

    NSError* error = nil;
    NSData* pdfFile = [NSData dataWithContentsOfURL:pdfNewsletterUrl options:NSDataReadingUncached error:&error];
   
    if (error) {
        //NSLog(@"%@", [error localizedDescription]);
      
    }

    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* filePath = [documentsPath stringByAppendingPathComponent:pdfString];
    [pdfFile writeToFile:filePath atomically:YES];
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    
    
        NSString *string = [NSString stringWithFormat:@"Link zur Seite: https://www.orochemie.de/newsletter_archiv/%@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.newsletter.newsletterUrl];
    
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl,string] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                      UIActivityTypeCopyToPasteboard,
                                      UIActivityTypePostToWeibo,
                                      //UIActivityTypePostToFacebook,
                                      //UIActivityTypePostToTwitter,
                                      //UIActivityTypeSaveToCameraRoll,
                                      //UIActivityTypeMail,
                                      //UIActivityTypePrint,
                                      UIActivityTypeMessage,
                                      UIActivityTypeAssignToContact,
                                      ];
    
    NSString *subject = [NSString stringWithFormat:@"oro Hygiene App: %@", pdfString];
	[controller setValue:subject forKey:@"subject"];
    
	controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
		//NSLog(@"Der User hat %@ gemacht und es war %@ erfolgreich", type, complete ? @"" : @"nicht");
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){//NSLog(@"Löschen war nicht erfolgreich");
        };
        
        
	};
	
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [self.printActionButton valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
    
	[self presentViewController:controller animated:YES completion:nil];
}
@end
