//
//  NewsletterModel.h
//  Orochemie
//
//  Created by Axel Beckert on 05.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Newsletter;

@interface NewsletterModel : NSObject

-(void) addNewsletter: (Newsletter*) newsletter;

-(NSInteger) numberOfNewsletter;

-(Newsletter*) newsletterAtIndexPath: (NSIndexPath*) indexPath;
@end
