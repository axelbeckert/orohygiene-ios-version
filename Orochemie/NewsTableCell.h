//
//  NewsTableCell.h
//  Orochemie
//
//  Created by Axel Beckert on 17.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *titleOutlet;
@property (strong, nonatomic) IBOutlet UILabel *subTitleOutlet;

@end
