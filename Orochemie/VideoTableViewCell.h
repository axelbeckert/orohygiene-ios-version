//
//  VideoTableViewCell.h
//  orochemie
//
//  Created by Axel Beckert on 24.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titelLabelOutlet;

@end
