//
//  Anwendung.m
//  orochemie
//
//  Created by Axel Beckert on 12.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Anwendung.h"
#import "Produkte.h"
#import "Reiniger.h"


@implementation Anwendung

@dynamic bild;
@dynamic index;
@dynamic name;
@dynamic sortierung;
@dynamic reinigerAnwendung;
@dynamic zusatzbezeichnung;
@dynamic produkte;
@dynamic reiniger;

@end
