//
//  WebviewViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 04.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "WebviewViewController.h"
#import "News.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BNHtmlPdfKit.h"
#import "TemporaryFilesInformation.h"

enum WebViewLoadingStatus{
    isLoading,
    finishedLoading,
};
typedef enum WebViewLoadingStatus webViewLoadingStatus;

@interface WebviewViewController () <UIWebViewDelegate, BNHtmlPdfKitDelegate,UIGestureRecognizerDelegate, UIScrollViewDelegate>
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic,strong) NSTimer *toolbarTimer;
@property (nonatomic,assign) NSTimeInterval timeToStopLoadingForToolBarTimer;
@property (strong, nonatomic) IBOutlet UIWebView *newsWebviewOutlet;
@property(nonatomic,assign) BOOL videoIsOn;
@property(nonatomic,assign) BOOL isVideo;
@property(nonatomic,strong) BNHtmlPdfKit *pdfKit;
@property(nonatomic,strong) NSString* documentsPath;
@property (nonatomic,assign) webViewLoadingStatus webViewLoadingStatus;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webViewVerticalSpaceConstraintOutlet;
@property(nonatomic,strong) UIBarButtonItem* printActionButton;

@property int loadingCounter;
-(void)loadRequest;
@end

@implementation WebviewViewController

-(void) home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Interface orientation

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    
    self.newsWebviewOutlet.delegate=self;
    self.loadingCounter=0;
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.documentsPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    NSString *pdfPath = [self.documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/pdfFiles"]];

    BOOL isDir;
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:pdfPath isDirectory:&isDir];
    if (!exists) {
//        NSLog(@"Pfad existiert nicht");
        [[NSFileManager defaultManager] createDirectoryAtPath:pdfPath withIntermediateDirectories:NO attributes:nil error:nil];
    }

    
    self.printActionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(printAction:)];
    
    NSArray *buttonArray = @[self.printActionButton, [oroThemeManager homeBarButton:self]];

    self.navigationItem.rightBarButtonItems = buttonArray;

    //Loading Status von WebView setzen - wenn der Pfad der PDf Datei leer ist wird dieser benötigt um eine relaod durchzuführen.
    self.webViewLoadingStatus = finishedLoading;
    
    //Toolbar ausblenden wenn so gesetzt
    self.webViewToolbar==HideToolBar ? (self.webViewToolbarOutlet.hidden=YES) : (self.webViewToolbarOutlet.hidden=NO);
    
    //Toolbar Timer setzen
    self.timeToStopLoadingForToolBarTimer=5;
    
    //Wenn die Toolbar angezeigt wird dann:
    if(self.webViewToolbar!=HideToolBar)
    {
        //TabRecognizer erstellen
        UITapGestureRecognizer *tabRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideToolbar)];
        //Tab Delegate setzen
        tabRecognizer.delegate=self;
        //Anzahl Tab die benötigt werden um zu reagieren
        tabRecognizer.numberOfTapsRequired=2;
        //TabRecognizer der Toolbar zuweisen
        [self.webViewToolbarOutlet addGestureRecognizer:tabRecognizer];
        //Timer starten zum Ausblenden der Toolbar
        self.toolbarTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeToStopLoadingForToolBarTimer target:self selector:@selector(hideToolbar) userInfo:nil repeats:NO];
    }
    
    //ScrollView delegate setzen
    self.newsWebviewOutlet.scrollView.delegate=self;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
    [tracker set:kGAIScreenName value:@"Webview-Information"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"URL"
                                                          action:@"Show"
                                                           label:self.urlString                                                          value:nil] build]];


    [self loadRequest];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Title Label

    if(self.tabBarController){
        self.tabBarController.title = self.webViewTitle;
        self.webViewVerticalSpaceConstraintOutlet.constant = 64;
    } else {
        UILabel* tlabel=[oroThemeManager customizeTitleLabel];
        tlabel.text = self.webViewTitle;
        self.navigationItem.titleView=tlabel;
        self.webViewVerticalSpaceConstraintOutlet.constant = (self.loadPdfFile==1) ? 64 : 0;
//        NSLog(@"loadPDFFile: %i", self.loadPdfFile);
//        NSLog(@"Constraint: %f",self.webViewVerticalSpaceConstraintOutlet.constant);
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Webview-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)viewDidDisappear:(BOOL)animated

{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - ScrollViewDelegate Methoden

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
     if(self.webViewToolbar!=HideToolBar)
    if(self.webViewToolbarOutlet.hidden == YES) self.webViewToolbarOutlet.hidden=NO;

}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

if(self.webViewToolbar!=HideToolBar)
 {
     if([self.toolbarTimer isValid])
    {
        [self.toolbarTimer invalidate];
        
    }
     self.toolbarTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeToStopLoadingForToolBarTimer target:self selector:@selector(hideToolbar) userInfo:nil repeats:NO];
 }

}

#pragma mark - Tab Selecor Methoden
-(void)hideToolbar
{
    self.webViewToolbarOutlet.hidden=YES;
}


#pragma mark - WebView Delegate Methoden

-(void)loadRequest
{
    
    if([ConnectionCheck hasConnectivity]) {
        //Leerezichen aus dem URL-String entfernen. Trifft speziell dort zu wo URL aus dem oro Backend geladen werden.
        NSString *urlWithoutSpace = [self.urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
//        NSLog(@"URL: %@", self.urlString);
//        NSLog(@"URLwithoutSpace: %@", urlWithoutSpace);
        NSURL *newsUrl = [NSURL URLWithString:urlWithoutSpace];
        [self.newsWebviewOutlet loadRequest:[NSURLRequest requestWithURL:newsUrl]];
        
    } else {
        
        [SVProgressHUD showErrorWithStatus:@"keine Internetverbindung"];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

//    NSLog(@"starte Laden");
    [TemporaryFilesInformation getLastFilePathFromLastTempFolderFile];


    NSString *string = [NSString stringWithFormat:@"%@",request ];
    NSRange range = [string rangeOfString:@"youtube.com"];
       if(range.length!=0)
    {
        self.videoIsOn = YES;
        
        if([self.timer isValid]){
            [self.timer invalidate];

        }
        
    } else {
     [TemporaryFilesInformation deleteContentOfLastTemporaryFolder];
    }


    return YES;
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // Erst einmal die GUI sperren, bis die Daten vorliegen, oder ein Fehler gemeldet wurde

    
    self.loadingCounter++;
    if(!self.videoIsOn){
       if(![self.timer isValid])
       {

        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.appDelegate.timeToStopLoading target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
       }
            }
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.timer invalidate];
    self.loadingCounter=0;
    NSLog(@"Fehler WebView: %@", error.localizedDescription);
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:@"Fehler beim Laden"];
    

}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{

       
    self.loadingCounter--;


    if(self.loadingCounter==0)
    {
        [self.timer invalidate]; //Timer ausschalten
        self.timer=nil;
        self.videoIsOn=NO;

    }

    if(self.webViewLoadingStatus == isLoading)
    {
        self.webViewLoadingStatus = finishedLoading;
        [self printAction:nil];
    }

    [SVProgressHUD dismiss];
}




#pragma mark - AlertView Delegate & Timer Selector
- (void)cancelWeb
{

    if(!self.videoIsOn)
    {

        
    self.newsWebviewOutlet.hidden=YES;
    [self.timer invalidate]; //Timer ausschalten
 
    UIAlertController *alert = [AlertHelper getUIActionSheetWithTitle:@"Achtung" andMessage:@"Abruf momentan nicht möglich!" andCancelButtonWithCompletionHandler:^(UIAlertAction *alertAction) {
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
    }];
        
    [AlertHelper addUIAlertActionToAlertWithTitle:@"Erneut laden" andCompletionHandler:^(UIAlertAction *alertAction) {
        [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
        [self loadRequest];
        self.loadingCounter=0;
        [self.timer invalidate];
        [self.newsWebviewOutlet reload];
        self.newsWebviewOutlet.hidden=NO;
    }];
        
    
    [AlertHelper showAlert:alert];
    
    }
    
}

#pragma mark - Button Actions

- (IBAction)refreshAction:(id)sender
{
    [self.newsWebviewOutlet reload];
}

- (IBAction)backAction:(id)sender
{
    [self.newsWebviewOutlet goBack];
}

- (IBAction)forwardAction:(id)sender {
    
    [self.newsWebviewOutlet goForward];
}
- (IBAction)stopLoadingAction:(id)sender
{
    [self.newsWebviewOutlet stopLoading];
}


#pragma mark - PDF Action

- (void)printAction:(id)sender
{
    
    [SVProgressHUD showWithStatus:@"Lade Daten..." maskType:SVProgressHUDMaskTypeBlack];
    
    NSString *pdfFileName;
    NSString* filePath;
    NSURL *fileUrl;
    
    
    NSString *word = @"pdf";
    if ([self.newsWebviewOutlet.request.URL.absoluteString rangeOfString:word].location != NSNotFound) {
                
        //NSData *pdfFile = [NSData dataWithContentsOfURL:self.newsWebviewOutlet.request.URL];
        
        pdfFileName = [[self.newsWebviewOutlet.request.URL.absoluteString componentsSeparatedByString:@"/"] lastObject];
        filePath = [self.documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/pdfFiles/%@",pdfFileName ]];
        
        NSData *pdfFile =[[NSFileManager defaultManager] contentsAtPath: [TemporaryFilesInformation getLastFilePathFromLastTempFolderFile]];
        [pdfFile writeToFile:filePath atomically:YES];

        
        
    fileUrl = [NSURL fileURLWithPath:filePath];

    NSString *string = [NSString stringWithFormat:@"Link zur Seite: %@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.urlString];
    
    
    [SVProgressHUD dismiss];
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl,string] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                         //UIActivityTypeCopyToPasteboard,
                                         UIActivityTypePostToWeibo,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         //UIActivityTypeSaveToCameraRoll,
                                         //UIActivityTypeMail,
                                         //UIActivityTypePrint,
                                         UIActivityTypeMessage,
                                         UIActivityTypeAssignToContact,
                                         ];
    
    
    
    NSString *subject = [NSString stringWithFormat:@"oro Hygiene App: %@", pdfFileName];
	[controller setValue:subject forKey:@"subject"];
    
        controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSError *error;
            [fileManager removeItemAtPath:filePath error:&error];
            if(error){
                NSLog(@"Löschen war nicht erfolgreich");
            };
            
        
        
        };

        
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [self.printActionButton valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
	
	[self presentViewController:controller animated:YES completion:nil];

    
    } else {
    
    
    
    self.pdfKit = [[BNHtmlPdfKit alloc]initWithPageSize:BNPageSizeA4];
    self.pdfKit.delegate = self;
    
  
    [self.pdfKit saveUrlAsPdf:self.newsWebviewOutlet.request.URL toFile:nil];
    }
	
    
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfData:(NSData *)data
{

    
    
    NSString *pdfFileName = [self.pdfFileName stringByAppendingString:@".pdf"];
    //NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* filePath = [self.documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/pdfFiles/%@",pdfFileName ]];
    NSURL *fileUrl;
   
    
    
        NSString *htmlContent = [NSMutableString stringWithString:[self.newsWebviewOutlet stringByEvaluatingJavaScriptFromString:@"document.body.outerHTML"]];

        //NSCachedURLResponse* response = [[NSURLCache sharedURLCache]cachedResponseForRequest:[self.newsWebviewOutlet request]];
   
    
        if([htmlContent isEqualToString:@""])
        {
            //NSData *pdfFile = [response data];
            NSData *pdfFile =[[NSFileManager defaultManager] contentsAtPath: [TemporaryFilesInformation getLastFilePathFromLastTempFolderFile]];

            [pdfFile writeToFile:filePath atomically:YES];
            
            
        } else {
        
            
            NSData *pdfFile = data;
            [pdfFile writeToFile:filePath atomically:YES];
        
        }
    
    
    
    fileUrl = [NSURL fileURLWithPath:filePath];
    //https://www.orochemie.de/de/,%@.php
    NSString *string = [NSString stringWithFormat:@"Link zur Seite: %@\n\n\n oro Hygiene App\n zur Verfügung gestellt von:\n orochemie GmbH + Co. KG\n Max-Planck-Str. 27\nD-70806 Kornwestheim",self.urlString];
    

    [SVProgressHUD dismiss];
    
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl,string] applicationActivities:nil];
	
	controller.excludedActivityTypes = @[
                                      //UIActivityTypeCopyToPasteboard,
                                      UIActivityTypePostToWeibo,
                                      UIActivityTypePostToFacebook,
                                      UIActivityTypePostToTwitter,
                                      //UIActivityTypeSaveToCameraRoll,
                                      //UIActivityTypeMail,
                                      //UIActivityTypePrint,
                                      UIActivityTypeMessage,
                                      UIActivityTypeAssignToContact,
                                      ];
    
    
    
    NSString *subject = [NSString stringWithFormat:@"oro Hygiene App: %@", pdfFileName];
	[controller setValue:subject forKey:@"subject"];
    
    controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){
            NSLog(@"Löschen war nicht erfolgreich");
        };
        
    };
 
//Austausch gg. neuen CompletionHandler am 28.02.15 wegen iOS8 Update
//	controller.completionHandler = ^(NSString* type, BOOL complete) {
//        
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        NSError *error;
//        [fileManager removeItemAtPath:filePath error:&error];
//        if(error){
//            NSLog(@"Löschen war nicht erfolgreich");
//        };
//        
//	};
	
    if ( [controller respondsToSelector:@selector(popoverPresentationController)] ) {
        //iOS8
        UIView *barButtonView = [self.printActionButton valueForKey:@"view"];
        controller.popoverPresentationController.sourceView = barButtonView;
    }
    
	[self presentViewController:controller animated:YES completion:nil];
    
    
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file
{
//    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error;
{
    NSLog(@"Error: %@", error.localizedDescription);
}


@end
