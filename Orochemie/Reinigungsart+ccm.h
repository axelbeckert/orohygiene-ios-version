//
//  Reinigungsart+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 11.08.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reinigungsart.h"

@interface Reinigungsart (ccm)
+(Reinigungsart*) insertReinigungsartInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteReinigungsart;
@end
