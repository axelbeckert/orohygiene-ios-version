//
//  VirusTableViewControllerlViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 19.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "VirusTableViewController.h"
#import "VirusStaticTableViewController.h"
#import "VirusTableViewStandardCell.h"

@interface VirusTableViewController ()
@property(nonatomic,strong) NSArray* virusDatenModel;
@property(nonatomic,strong) NSMutableArray *virusDatenModel2;
@property(nonatomic,strong) NSMutableArray *sectionArray;
@property (nonatomic, strong) NSFetchedResultsController *ErregerfetchedResultsController;



@end


@implementation VirusTableViewController




- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(BOOL)shouldAutorotate
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [JSMCoreDataHelper perfomFetchOnFetchedResultsController:self.fetchedResultsController];

    self.title = @"Krankheitserreger";
        
    [oroThemeManager  customizeTableView:self.tableView];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - must be overloaded methods
-(NSFetchedResultsController *)fetchedResultsController
{
    if(self.ErregerfetchedResultsController !=nil){
        return self.ErregerfetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Krankheitserreger class]) inManagedObjectContext:[JSMCoreDataHelper managedObjectContext]];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"anwendung.name CONTAINS[c] %@",@"fläche"];
    
    
    fetchRequest.entity=entityDescription;
    fetchRequest.fetchBatchSize=64;
    //fetchRequest.predicate=predicate;
    
    NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    NSArray *sortArray= [NSArray arrayWithObject:sortName];
    fetchRequest.sortDescriptors = sortArray;
    
    self.ErregerfetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[JSMCoreDataHelper managedObjectContext] sectionNameKeyPath:@"index" cacheName:nil];
    
    self.ErregerfetchedResultsController.delegate=self;
    
    return self.ErregerfetchedResultsController;
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    VirusTableViewStandardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"standardCell" forIndexPath:indexPath];
    //cell.contentView.backgroundColor = [UIColor colorWithRed:223.0/255 green:224.0/255 blue:225.0/255 alpha:1.0];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    [oroThemeManager customizeAccessoryView:cell];
    Krankheitserreger *krankheitserreger = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = krankheitserreger.name;
    return cell;
}


-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}

//-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView®
//{
//    NSMutableArray *sectionIndexTitles = [[NSMutableArray alloc]init];
//    for(int i = 0; i < [self.virusDatenModel2 count];i++)
//    {
//        NSDictionary *dict = self.virusDatenModel2[i];
//        NSString *title = [dict objectForKey:@"section"];
//        [sectionIndexTitles addObject: title];
//    }
//    return sectionIndexTitles;
//}
//
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//
//    NSDictionary *dict = self.virusDatenModel2[section];
//    NSString *title = [dict objectForKey:@"section"];
//    
//   // NSString *title = [NSString stringWithFormat:@"%@",self.virusDatenModel2[section][@"section"]];
//  
//
//    return title;
//}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
  if([segue.identifier isEqualToString:@"virusStaticTableSegue"]){
  
         VirusStaticTableViewController *controller = segue.destinationViewController;
      controller.krankheitserreger = [self.ErregerfetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
  
      }   else if([segue.identifier isEqualToString:@"virusStaticTableSegueCustomCell"]){
  
          VirusStaticTableViewController *controller = segue.destinationViewController;
          controller.krankheitserreger = [self.ErregerfetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
      }

}


//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//    
//    [headerView setBackgroundColor:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1]];
//    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, tableView.bounds.size.width - 10, 18)];
//    label.font = [UIFont boldSystemFontOfSize:14];
//    label.text = [NSString stringWithFormat:@"%@",self.virusDatenModel2[section][@"section"]];
//    label.textColor = [UIColor blackColor];
//    label.backgroundColor = [UIColor clearColor];
//    [headerView addSubview:label];
//    
//    
//    return headerView;
//}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

@end
