//
//  TechnischeAnfrage+ccm.h
//  orochemie
//
//  Created by Axel Beckert on 13.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "TechnischeAnfrage.h"

@interface TechnischeAnfrage (ccm)
+(TechnischeAnfrage*) insertTechnischeAnfrageInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteTechnischeAnfrage;
@end
