//
//  ProduktDetailVideoTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ProduktDetailVideoTableViewController.h"
#import "WebviewViewController.h"
#import "VideoDownloadViewController.h"
#import "VideoPlayerViewController.h"

@interface ProduktDetailVideoTableViewController ()
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property BOOL noVideosToPlay;
@end

@implementation ProduktDetailVideoTableViewController

#pragma mark - NSFechtedResultsController

-(NSFetchedResultsController *) fetchedResultsController{
    if (_fetchedResultsController) return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"bezeichnung" ascending:YES];
    
    fetch.sortDescriptors = @[sortByName];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetch managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if(error){
        //NSLog(@"Fehler: %@",error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.contentInset = inset;
    

    
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.managedObjectContext = appDelegate.managedObjectContext;

    if(self.reiniger) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reiniger.bezeichnung CONTAINS[c] %@", self.reiniger.bezeichnung];
        //NSLog(@"Reiniger");
        self.fetchedResultsController.fetchRequest.predicate = predicate;
        
        [self.fetchedResultsController performFetch:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if(self.tabBarController){
        self.tabBarController.title = @"Videoauswahl";
    }
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Reiniger-Produkt-Detail-Video"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> info = self.fetchedResultsController.sections[section];
    return [info numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Video *video = [self.fetchedResultsController objectAtIndexPath:indexPath];
    // Configure the cell...
    cell.textLabel.text = video.bezeichnung;
    
    return cell;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    if(![self checkIfVideosAreThere]) {self.noVideosToPlay = YES;}
    else {self.noVideosToPlay = NO;}
    
    
    NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
    NSString* youTubeVideo = [usingOroHygieneDefaults valueForKey:@"YouTubeVideos"];
    
    
    if(!self.noVideosToPlay && [youTubeVideo isEqualToString:@"YES"]){
        
        //        AlertViewHelper *alertView = [[AlertViewHelper alloc]initWithTitle:@"Information" message:@"Sie haben sich entschieden die Videos mit YouTube anzusehen. Diese Funktion ist aktuell noch nicht umgesetzt. Sie können diese Einstellung im Setting Screen wieder ändern."];
        //
        //        [alertView addButtonWithTitle:@"OK" withBlock:^(UIAlertView *alertView) {
        //            //NSLog(@"Missing Videos && YouTube");
        //        }];
        //        [alertView show];
        WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
        
        Video *video=[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        controller.urlString = video.youtube;
        controller.pdfFileName = @"video";
        controller.webViewTitle = @"YouTube-Video";
        
        [self.navigationController pushViewController:controller animated:YES];
        
        return NO;
    }
    
    
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier isEqualToString:@"VideoPlayer"]){
        
        VideoPlayerViewController *controller = segue.destinationViewController;
        controller.video = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];

        ////NSLog(@"gewähltes Video:%@",controller.video.bezeichnung);
        //[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        
    }
}


#pragma mark- Check if Videos are there

-(BOOL)checkIfVideosAreThere{
    
    NSError *error;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Video class])];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSArray *videoArray = [[delegate managedObjectContext]executeFetchRequest:fetchRequest error:&error];
    
    if(error){
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:error.localizedDescription];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"OK" andCompletionHandler:nil]];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    BOOL missingVideo = NO;
    
    for(int i=0;i<videoArray.count;i++){
        Video *video =[videoArray objectAtIndex:i];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:video.datei];
        filePath = [filePath stringByAppendingString:@".mp4"];
        
        NSFileManager * fileManager = [NSFileManager new];
        NSError * error = 0;
        NSDictionary * attr = [fileManager attributesOfItemAtPath:filePath error:&error];
        unsigned long long size = [attr fileSize];
        
        if(!(size == video.filesize.longLongValue)){
            
            missingVideo=YES;
            
        }
        
    }
    
    
    
    NSUserDefaults *usingOroHygieneDefaults = [NSUserDefaults standardUserDefaults];
    
    // holen der Zustimmung
    NSString* videoDownload = [usingOroHygieneDefaults valueForKey:@"DownloadingVideos"];
    
    if(missingVideo && videoDownload == nil ){
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:@"Um die Anwendungsvideos sehen zu können müssen diese einmalig geladen werden (ca. 50MB). Als Option können Sie die Videos auch bei YouTube sehen (benötigt immer eine Online-Verbindung). Bitte treffen Sie eine Auswahl:"];

        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Downloaden" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self downloadVideos];
        }]];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"YouToube" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self watchVideosOnYouTube];
        }]];

        
        [self presentViewController:alertView animated:YES completion:nil];
        
        return NO;
        
    } else if (missingVideo && [videoDownload isEqualToString:@"YES"]){
        
        UIAlertController *alertView = [AlertHelper getUIAlertControllerWithTitle:@"Information" andMessage:@"Die Anwendungsvideos wurden nicht vollständig geladen. Soll der Download fortgesetzt werden?"];
    
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Downloaden" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self downloadVideos];
        }]];
        
        [alertView addAction:[AlertHelper addUIAlertActionToAlertWithTitle:@"Jetzt nicht" andCompletionHandler:^(UIAlertAction *alertAction) {
            [self.navigationController popViewControllerAnimated:YES];
        }]];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
        return NO;
        
        
    }
    
    return YES;
}

-(void)downloadVideos {
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    VideoDownloadViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoDownloadViewController"];
    [self.navigationController presentViewController:controller animated:YES completion:nil];
    //NSLog(@"Videos downloaden - bitte noch ausprogrammieren");
    
    //Setzen der Entscheidung für den Download
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setValue:@"YES" forKey:@"DownloadingVideos"];
    [pref setValue:@"NO" forKey:@"YouTubeVideos"];
    [pref synchronize];
}

-(void)watchVideosOnYouTube {

    //Setzen der Entscheidung für YouTube
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setValue:@"YES" forKey:@"YouTubeVideos"];
    [pref setValue:@"NO" forKey:@"DownloadingVideos"];
    [pref synchronize];
    
    //NSLog(@"Videos künftig bei YouToube sehen - bitte noch ausprogrammieren");
}

@end
