//
//  Video.m
//  orochemie
//
//  Created by Axel Beckert on 21.07.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Video.h"
#import "Produkte.h"
#import "Reiniger.h"


@implementation Video

@dynamic bezeichnung;
@dynamic datei;
@dynamic filesize;
@dynamic index;
@dynamic kategorie;
@dynamic reinigerVideo;
@dynamic sortierung;
@dynamic youtube;
@dynamic produkte;
@dynamic reiniger;

@end
