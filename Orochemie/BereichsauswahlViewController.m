//
//  BereichsauswahlViewController.m
//  orochemie
//
//  Created by Axel Beckert on 28.05.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import "BereichsauswahlViewController.h"
#import "CollectionReusableView.h"
#import "CollectionViewCell.h"
#import "NavigationCollectionViewController.h"
#import "ReinigungNavigationViewController.h"
#import "BarcodeReader.h"
#import "SettingScreenTableViewController.h"
#import "VideoDownloadViewController.h"
#import "ZustimmungZurAppNutzung.h"
#import "UnternehmenTableViewController.h"
#import "ImpressumTableViewController.h"
#import "DatenschutzTableViewController.h"
#import "DeviceCheck.h"



@interface BereichsauswahlViewController ()
@property (nonatomic,strong)IBOutlet NSLayoutConstraint *topConstraint;
@end

@implementation BereichsauswahlViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self adjustTopLayoutConstraint];
    
    [oroThemeManager customizeSelectView:self.view];
    self.title=@"orochemie Hygiene";
    
    
    self.navigationDataModel = @[@"Desinfektionsmittel",
                                 @"Reinigungsmittel",
                                 @"Produkt-Barcode scannen",
                                 @"Impressum",
                                 @"Datenschutz"
                                 ];
    
        UIBarButtonItem* settingButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"20-gear-2.png"] style:UIBarButtonItemStylePlain target:self action:@selector(openSettingScreen)];
    
    self.navigationItem.rightBarButtonItem = settingButton;
	// Do any additional setup after loading the view.
    
    [ZustimmungZurAppNutzung einholungZustimmung];
}

-(void)adjustTopLayoutConstraint{
    
    DeviceCheck *deviceCheck = [DeviceCheck new];
    
    if([[deviceCheck deviceName] rangeOfString:@"iPad"].location != NSNotFound){
        if ([[deviceCheck deviceName] rangeOfString:@"Plus"].location != NSNotFound) {
            self.topConstraint.constant = 0;
        } else {
            self.topConstraint.constant = 50;
        }
    } else {
        self.topConstraint.constant = 50;
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"App Startbildschirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.navigationDataModel.count;
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionReusableView* reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];

        
    return reusableView;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
    
    [oroThemeManager customizeButtonBackgroundImage:cell.collectionViewImage2];
    
    cell.collectionViewImage2.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    CGFloat borderWidth = 1.0f;
    
    cell.collectionViewImage2.layer.borderWidth = borderWidth;

    cell.collectionViewBezeichnungLabelOutlet.text = [self.navigationDataModel objectAtIndex:indexPath.row];
    
    
    return cell;
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([[self.navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Desinfektionsmittel"])
    {
        NavigationCollectionViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NavigationCollectionView"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if([[self.navigationDataModel objectAtIndex:indexPath.item]isEqualToString:@"Reinigungsmittel"])
    {
        ReinigungNavigationViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReinigungNavigationViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if ([[self.navigationDataModel objectAtIndex:indexPath.item]
               isEqualToString:@"Produkt-Barcode scannen"]) {
        BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
        controller.showBarcodeReaderFrom=DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Impressum"]) {
        ImpressumTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ImpressumTableViewController"];
        controller.showImpressumFrom=DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([[self.navigationDataModel objectAtIndex:indexPath.item]
                isEqualToString:@"Datenschutz"]) {
        DatenschutzTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DatenschutzTableViewController"];
        controller.showDatenschutzFrom=DesinfektionChannel;
        [self.navigationController pushViewController:controller animated:YES];
        
    }

    
    
}


#pragma mark - UICollectionViewDelegateFlowLayout


-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if([UIScreen mainScreen].currentMode.size.height==1136) //Iphone 5
    {
        return 3; //war 12
        
    } else if([UIScreen mainScreen].currentMode.size.height==2048 || [UIScreen mainScreen].currentMode.size.height==1024 || [UIScreen mainScreen].currentMode.size.height==768) //Ipad
    {
        return 25;
    }
    
    return 6;
}


#pragma mark - View Lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([self.navigationController.navigationBar isHidden])
    {
        [self.navigationController.navigationBar setHidden:NO];
    }
}

    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark BarButton Actions
-(void)openSettingScreen{
    SettingScreenTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingScreenTableViewController"];
    //controller.showBarcodeReaderFrom=DesinfektionChannel;
    [self.navigationController pushViewController:controller animated:YES];

}



@end
