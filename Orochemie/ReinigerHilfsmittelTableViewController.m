//
//  HilfsmittelTableViewController.m
//  oro Reinigung
//
//  Created by Axel Beckert on 25.03.14.
//  Copyright (c) 2014 Orochemie GmbH & Co. KG. All rights reserved.
//

#import "ReinigerHilfsmittelTableViewController.h"
#import "OberflaechenUndBelaegeTableViewController.h"
#import "MerklisteTableViewController.h"
#import "InformationenTableViewCell.h"
#import "BarcodeReader.h"
#import "WebviewViewController.h"
#import "MerklistenTabbarController.h"
#import "VerbrauchsRechnerStaticTableViewController.h"
#import "TechnischeAnfrageTableViewController.h"

@interface ReinigerHilfsmittelTableViewController ()
@property(nonatomic, strong) NSArray *hilfsmittelArray;
@end

@implementation ReinigerHilfsmittelTableViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
  
    [super viewDidLoad];

    [oroThemeManager customizeReinigerTableView:self.tableView];
    
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    
      self.hilfsmittelArray = [NSArray new];
      self.hilfsmittelArray = @[
        @"Barcode-Reader",
        @"Hygienewissen",
        @"Merklisten",
        @"Oberflächen und Beläge",
        @"Produktbestellung",
        @"Reinigungspläne erstellen",
        @"Technische Anfrage",
        @"Verbrauchsrechner",
      ];
}

- (void)viewDidAppear:(BOOL)animated {

  [super viewDidAppear:animated];
  id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         
  [tracker set:kGAIScreenName value:@"Reiniger-Hilfsmittel-Schirm"];
  [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Interface Orientation
    #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000  
    - (NSUInteger)supportedInterfaceOrientations  
    #else  
    - (UIInterfaceOrientationMask)supportedInterfaceOrientations  
    #endif
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {

  return self.hilfsmittelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"InfoCell";
    
    InformationenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    UIFont *font =cell.TitleLabelOutlet.font;
    
    cell.TitleLabelOutlet.text = [self.hilfsmittelArray objectAtIndex:indexPath.row];
    [cell.TitleLabelOutlet setFont:[UIFont fontWithName:font.fontName size:19.0]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
          isEqualToString:@"Oberflächen und Beläge"]) {
    OberflaechenUndBelaegeTableViewController *controller =
        [self.storyboard instantiateViewControllerWithIdentifier:
                             @"OberflaechenUndBelaegeTableViewController"];

    [self.navigationController pushViewController:controller animated:YES];

  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
              isEqualToString:@"Verbrauchsrechner"]) {
      VerbrauchsRechnerStaticTableViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReinigungsRechnerStaticTableViewController"];

      [self.navigationController pushViewController:controller animated:YES];
      
  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
              isEqualToString:@"Barcode-Reader"]) {
      BarcodeReader* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ccmViewController"];
      controller.showBarcodeReaderFrom=ReinigerChannel;
      [self.navigationController pushViewController:controller animated:YES];
      
  }  else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
                 isEqualToString:@"Merklisten"]) {
    MerklistenTabbarController *controller = [self.storyboard
        instantiateViewControllerWithIdentifier:@"MerklistenTabbarController"];
      controller.showMerklistenTabBarControllerFrom = ReinigerChannel;
    [self.navigationController pushViewController:controller animated:YES];

  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
              isEqualToString:@"Produktbestellung"]) {
      WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
      controller.urlString=cWebsiteFuerBestellung;
      controller.pdfFileName=cWebsitePDFTitleFuerBestellung;
      controller.webViewTitle=cWebsiteTitleFuerBestellung;
      [self.navigationController pushViewController:controller animated:YES];
      
  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
              isEqualToString:@"Hygienewissen"]) {
      WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
      controller.urlString=cWebsiteFuerHygieneWissen;
      controller.pdfFileName=cWebsitePDFTitleFuerHygieneWissen;
      controller.webViewTitle=cWebsiteTitleFuerHygieneWissen;
      [self.navigationController pushViewController:controller animated:YES];
      
  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.row]
              isEqualToString:@"Reinigungspläne erstellen"]) {
      WebviewViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
      controller.urlString=cWebsiteFuerHygieneSystem;
      controller.pdfFileName=cWebsitePDFTitleFuerHygieneSystem;
      controller.webViewTitle=cWebsiteTitleFuerHygieneSystem;
      [self.navigationController pushViewController:controller animated:YES];
      
  } else if ([[self.hilfsmittelArray objectAtIndex:indexPath.item]
                isEqualToString:@"Technische Anfrage"]) {
      TechnischeAnfrageTableViewController *controller = [self.storyboard
                                                          instantiateViewControllerWithIdentifier:@"TechnischeAnfrageTableViewController"];
      
      controller.showTechnischeAnfrageFrom = ReinigerChannel;
      [self.navigationController pushViewController:controller animated:YES];
  }
}

#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



@end
