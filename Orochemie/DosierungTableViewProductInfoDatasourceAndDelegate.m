//
//  DosierungTableViewProductInfoDatasourceAndDelegate.m
//  Orochemie
//
//  Created by Axel Beckert on 07.05.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "DosierungTableViewProductInfoDatasourceAndDelegate.h"
#import "ProduktdosierungInfo.h"

@implementation DosierungTableViewProductInfoDatasourceAndDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.produktDosierungArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    ProduktdosierungInfo *dosierung = [self.produktDosierungArray objectAtIndex:indexPath.row];
    cell.textLabel.text = dosierung.dosierung;

    return cell;
    
}

@end
