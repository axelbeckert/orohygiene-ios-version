//
//  WebviewViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 04.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class News;

enum webViewToolbar{
    ShowToolBar,
    HideToolBar,
};
typedef enum webViewToolbar webViewToolbar;

@interface WebviewViewController : UIViewController
@property (nonatomic,strong) News* news;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *webViewTitle;
@property(nonatomic,strong) NSString *pdfFileName;
- (IBAction)refreshAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)forwardAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButtonOutlet;
- (IBAction)stopLoadingAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIToolbar *webViewToolbarOutlet;
@property (nonatomic,assign) webViewToolbar webViewToolbar;
@property(nonatomic,assign)show showWebviewFrom;
@property int loadPdfFile;
@end
