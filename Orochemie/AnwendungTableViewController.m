//
//  AnwendungTableViewController.m
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import "AnwendungTableViewController.h"
#import "Produkte.h"
#import "Branche.h"
#import "Anwendung.h"
#import "Constants.h"
#import "JSMToolBox.h"
#import "AnwendungTableViewCell.h"
#import "WebviewViewController.h"
#import "ProduktAuflistungTableViewController.h"

@interface AnwendungTableViewController() <UISearchBarDelegate>
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@end


@implementation AnwendungTableViewController

#pragma mark - fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest
                             fetchRequestWithEntityName:NSStringFromClass([Anwendung class])];
    
    
    NSPredicate * anwendungPredicate = [NSPredicate predicateWithFormat:@"reinigerAnwendung == NO"];
    fetch.predicate = anwendungPredicate;
    
    NSSortDescriptor *sortName= [[NSSortDescriptor alloc] initWithKey:@"sortierung" ascending:YES];
    
    NSArray *sortArray= [NSArray arrayWithObject:sortName];
    fetch.sortDescriptors = sortArray;
    
    
    fetch.sortDescriptors = @[ sortName ];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetch
                                 managedObjectContext:self.managedObjectContext
                                 sectionNameKeyPath:@"index"
                                 cacheName:nil];
    
    NSError *error;
    
    [_fetchedResultsController performFetch:&error];
    
    if (error) {
        //NSLog(@"Fehler: %@", error);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - ScreenRotation

-(BOOL)shouldAutorotate
{
    return NO;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
    
}


#pragma mark - ViewLifeCycle

-(void) viewDidLoad
{
    [oroThemeManager  customizeTableView:self.tableView];
   
    //Core Data initalisieren
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.managedObjectContext = delegate.managedObjectContext;
    
    [self.fetchedResultsController performFetch:nil];
    
    self.navigationItem.rightBarButtonItem = [oroThemeManager homeBarButton:self];
    self.tableView.sectionIndexColor = [UIColor whiteColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    self.tableView.sectionIndexTrackingBackgroundColor=[UIColor colorWithWhite:0.0 alpha:0.3];

    //SerachBar
    //SerachBar
    UISearchBar *searchBar =
    [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 44)];
     [searchBar sizeToFit];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [UIImage new];
    searchBar.scopeBarBackgroundImage = [UIImage imageNamed:@"headline_3d.png"];
    searchBar.placeholder = @"Suche";
    
    UIView *searchBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 44)];
    
    [searchBarView addSubview:searchBar];
    
    self.tableView.tableHeaderView = searchBarView;

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Barbutton Items for Toolbar
    UIBarButtonItem *itemOne = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *itemTwo = [[UIBarButtonItem alloc]initWithTitle:@"bebilderte Anwendungshinweise" style:UIBarButtonItemStylePlain target:self action:@selector(anwendungsHinweise:)];
    UIBarButtonItem *itemThree = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [itemTwo setTintColor:[UIColor whiteColor]];

    
    
    //Toolbar initialisieren und anzeigen
    
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    self.toolbarItems=@[itemOne,itemTwo,itemThree];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
     
    [tracker set:kGAIScreenName value:@"Desinfektion-Anwendung-Schirm"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:YES];

}



#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"anwendungCell";
    
    

    AnwendungTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Anwendung *anwendung =[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    [oroThemeManager customizeAccessoryView:cell];
    [oroThemeManager customizeTableViewStandardTableViewCellText:cell];
    
    
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    //cell.backgroundView=backgroundView;
    [oroThemeManager customizeTableViewAnwendungTableViewCellText:cell];

    cell.textLabelOutlet.text= anwendung.name;
    cell.imageOutlet.image = [UIImage imageNamed:anwendung.bild];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *container = [oroThemeManager customizeTableViewViewForHeaderView];
    UILabel *label= container.subviews.lastObject;
    label.text = [self.fetchedResultsController.sectionIndexTitles objectAtIndex:section];
    
    return container;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [oroThemeManager customizeTableViewHeigtForHeaderView];
}








#pragma mark - Segue Methoden

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.identifier isEqualToString:@"showProdukteForAnwendung"]){
        
        ProduktAuflistungTableViewController *controller = segue.destinationViewController;
        controller.anwendungForProduktResult =[self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];

    }
    
}

#pragma mark - Button Actions

-(void)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)anwendungsHinweise:(id)sender
{
    WebviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebviewViewController"];
    controller.urlString=cWebsiteFuerBebilderteAnwendungshinweise;
    controller.webViewTitle=cWebsiteTitleFuerBebilderteAnwendungshinweise;
    controller.pdfFileName=cWebsitePDFTitleFuerBebilderteAnwendungshinweise;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - SearchBarDelegate Methoden

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    
    
    if (!searchText.length) {
        self.fetchedResultsController.fetchRequest.predicate = nil;
    } else {
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.name CONTAINS[c] %@", searchText];
        self.fetchedResultsController.fetchRequest.predicate = predicate;
    }
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}

#pragma mark - scrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
@end
