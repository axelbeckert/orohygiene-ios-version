//
//  EmailHelper.h
//  orochemie
//
//  Created by Axel Beckert on 25.06.14.
//  Copyright (c) 2014 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
enum FormFieldFailure {
    
    EMAIL_VALIDATION_FALSE,
};

typedef enum FormFieldFailure formFieldFailure;

@interface EmailHelper : NSObject
+ (BOOL) validateEmail: (NSString *) candidate;
+(void)notifyUserAboutFailure:(formFieldFailure)failureCode;
@end
