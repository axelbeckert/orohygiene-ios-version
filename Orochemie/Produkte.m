//
//  Produkte.m
//  orochemie 
//
//  Created by Axel Beckert on 07.06.17.
//  Copyright © 2017 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Produkte.h"
#import "Anwendung.h"
#import "Branche.h"
#import "Gefahrstoffsymbole.h"
#import "Handlungsanweisung.h"
#import "Krankheitserreger.h"
#import "Merkliste.h"
#import "Pictogramm.h"
#import "Produktdosierung.h"
#import "Produktvarianten.h"
#import "Sicherheit.h"
#import "Video.h"
#import "Wirksamkeit.h"

@implementation Produkte

// Insert code here to add functionality to your managed object subclass

@end
