//
//  Hygienetipp.h
//  Orochemie
//
//  Created by Axel Beckert on 05.03.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hygienetipp : NSObject
@property (nonatomic, strong) NSString* hygieneTippId;
@property (nonatomic, strong) NSString* hygieneTippText1;
@property (nonatomic, strong) NSString* hygieneTippText2;
@property (nonatomic, strong) NSString* hygieneTippUrl;

-(NSDictionary*) asHygieneTippDictionary;
@end
