//
//  AnwendungsVideosTableViewController.h
//  orochemie
//
//  Created by Axel Beckert on 25.06.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMTableViewController.h"


@interface AnwendungsVideosTableViewController : JSMTableViewController
@property (nonatomic,assign)show showVideosFrom;
@property int watchVideoNow;
@end
