//
//  Reiniger.m
//  orochemie
//
//  Created by Axel Beckert on 01.03.15.
//  Copyright (c) 2015 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Reiniger.h"
#import "Anwendung.h"
#import "Belag.h"
#import "Gefahrstoffsymbole.h"
#import "Merkliste.h"
#import "Oberflaeche+ccm.h"
#import "Phwert.h"
#import "Produktvarianten.h"
#import "Reinigungsverfahren.h"
#import "Verbrauch.h"
#import "Video.h"


@implementation Reiniger

@dynamic anwendbar;
@dynamic bezeichnung;
@dynamic dosierung;
@dynamic ecoLabel;
@dynamic ecolabelPDF;
@dynamic eigenschaften;
@dynamic einsatzbereich;
@dynamic hinweise;
@dynamic image;
@dynamic index;
@dynamic informationPDF;
@dynamic konzentrat;
@dynamic mindestdosierung;
@dynamic nachhaltigkeit;
@dynamic phwert;
@dynamic sortierreihenfolge;
@dynamic uid;
@dynamic zusatz;
@dynamic anwendung;
@dynamic belag;
@dynamic gefahrstoffsymbole;
@dynamic merkliste;
@dynamic oberflaeche;
@dynamic phwert_relation;
@dynamic produktvarianten;
@dynamic reinigungsmethode;
@dynamic verbrauch;
@dynamic video;
@dynamic pictogramm;

@end
