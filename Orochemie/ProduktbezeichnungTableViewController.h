//
//  ProduktbezeichnungTableViewController.h
//  Orochemie
//
//  Created by Axel Beckert on 28.01.13.
//  Copyright (c) 2013 Axel Beckert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSMTableViewController.h"

@class Anwendung;
@class Branche;

@interface ProduktbezeichnungTableViewController : JSMTableViewController
@property (nonatomic,strong) Anwendung *anwendungForProduktResult;
@property (nonatomic,strong) Branche *brancheForProduktResult;
@end
