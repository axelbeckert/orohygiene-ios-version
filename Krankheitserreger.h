//
//  Krankheitserreger.h
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Checkliste, Produkte;

NS_ASSUME_NONNULL_BEGIN



@interface Krankheitserreger : NSManagedObject  <JsonProtocol>

+(Krankheitserreger*) insertKrankheitserregerInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext;
-(void)deleteKrankheitserreger;

@end

NS_ASSUME_NONNULL_END

#import "Krankheitserreger+CoreDataProperties.h"
