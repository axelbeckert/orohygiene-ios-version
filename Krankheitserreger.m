//
//  Krankheitserreger.m
//  orochemie 
//
//  Created by Axel Beckert on 02.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//

#import "Krankheitserreger.h"
#import "Checkliste.h"
#import "Produkte.h"

@implementation Krankheitserreger 

+(Krankheitserreger*) insertKrankheitserregerInManagedObjectContext: (NSManagedObjectContext*) managedObjectContext
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Krankheitserreger" inManagedObjectContext:managedObjectContext];
}
-(void)deleteKrankheitserreger
{
    [self.managedObjectContext deleteObject:self];
}

@end
