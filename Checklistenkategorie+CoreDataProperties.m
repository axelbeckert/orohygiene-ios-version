//
//  Checklistenkategorie+CoreDataProperties.m
//  orochemie 
//
//  Created by Axel Beckert on 30.08.16.
//  Copyright © 2016 orochemie GmbH & Co.KG. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Checklistenkategorie+CoreDataProperties.h"

@implementation Checklistenkategorie (CoreDataProperties)

@dynamic bezeichnung;
@dynamic index;
@dynamic musterplan;
@dynamic sortierung;
@dynamic uid;
@dynamic branchmodel;
@dynamic typemodel;
@dynamic checkliste;

@end
